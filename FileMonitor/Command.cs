﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileMonitor {
    class Command {
        public string build { get; set; }
        public string command { get; set; }
        public string user { get; set; }
        public string creationdate { get; set; }
    }
}
