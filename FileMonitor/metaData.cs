﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileMonitor {
    class metaData {

        public string name;
        public string description;
        public string creationDate;
        public string category;
        public string upVector;
        public string textures;
        public string scale;
        public string input;
        public string output;
    }
}

