/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for feature patterns enum
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifndef __A3DPRCFEATUREPATTERNENUMS_H__
#define __A3DPRCFEATUREPATTERNENUMS_H__

/*!
\enum EA3DFRMEnumValue_Pattern
\brief Enumerate the possible types of shape for a pattern <br/>
This allow to specify the type of pattern, and so the Definition you should expect to have under the #kA3DFRMFeatureDefinitionType_Pattern.

\image html frm_pattern_types.jpg

\ingroup a3d_feature_pattern_module
\version 10.2
*/
typedef enum
{
	kA3DFRMEnumValue_Pattern_None = 0,						/*!< Invalid Pattern Type. */
	kA3DFRMEnumValue_Pattern_General,						/*!< Generic Pattern that combine multiple definition. */
	kA3DFRMEnumValue_Pattern_Linear,						/*!< Pattern Linear with all instances placed along a direction. */
	kA3DFRMEnumValue_Pattern_Cyclic,						/*!< Pattern Circular with all instances placed on a circle. */
	kA3DFRMEnumValue_Pattern_Matrix,						/*!< Pattern Rectangular with instances placed on a Grid along 2 directions. */
	kA3DFRMEnumValue_Pattern_CyclicMultiple,				/*!< Pattern Circular with instances placed on several concentric circles. */
	kA3DFRMEnumValue_Pattern_Polygonal,						/*!< Pattern with instances placed on a regular polygon <i>(specific to NX)</i>. */
	kA3DFRMEnumValue_Pattern_PolygonalMultiple,				/*!< Pattern with instances placed on several concentric regular polygons <i>(specific to NX)</i>. */
	kA3DFRMEnumValue_Pattern_Spiral,						/*!< Pattern with instances placed on a 2D Spiral <i>(specific to NX)</i>. */
	kA3DFRMEnumValue_Pattern_ByReference,					/*!< Pattern parameterized with the same properties than another referenced Pattern. */
	kA3DFRMEnumValue_Pattern_Point,							/*!< Pattern with a list of points to specify each instance position. */
	kA3DFRMEnumValue_Pattern_Curve,							/*!< Pattern Curvilinear with all instances placed along a curve. Not implemented Yet */
	kA3DFRMEnumValue_Pattern_Table,							/*!< Pattern with instances specified by a table sheet of parameters. Not implemented Yet. */
	kA3DFRMEnumValue_Pattern_Dimension,						/*!< Pattern parameterized with a dimension and the incremental changes. Not implemented Yet. */
	kA3DFRMEnumValue_Pattern_Fill,							/*!< Pattern with instances that fill up a specified region <i>(loop, sketch, etc.)</i>. Not implemented Yet */
	kA3DFRMEnumValue_Pattern_LinearMultiple					/*!< Pattern Linear with instances placed along multiple directions, in a cross shape for example.
																 Only the master is patterned in each direction. */
}
EA3DFRMEnumValue_Pattern;

/*!
\enum EA3DFRMDefinitionPatternType
\brief Feature definition pattern types <br/>
Enumerate the specific types of Definition Feature that can be found under a #kA3DFRMFeatureDefinitionType_Pattern.
\ingroup a3d_feature_pattern_module
\version 10.2
*/
typedef enum
{
	kA3DFRMDefinitionPatternType_None = 0,					/*!< Invalid Pattern Definition Type. */
	kA3DFRMDefinitionPatternType_PatternMaster,				/*!< Type of Definition containing information specific to Pattern Master:
															- links to master features */
	kA3DFRMDefinitionPatternType_PolygonalShape,		    /*!< Type of Definition containing information specific to Pattern of shape polygon (how the polygon is parameterized):
															- Parameter Definition:
															  - <i>Definition Position</i>: polygon center point
															  - <i>Definition Direction</i>: normal
															- Parameter Data:
															  - <i>IntegerData Count</i>: number of sides
															  - <i>Value Angle</i>: total angle to apply instances */
	kA3DFRMDefinitionPatternType_SpiralShape,			    /*!< Type of Definition containing information specific to Pattern of shape spiral (how the spiral is parameterized):
															- Parameter Definition:
															  - <i>Definition Direction</i>: normal
															- Parameter Data:
															  - <i>IntegerData Count</i>: number of turn
															  - <i>IntegerData Boolean</i>: isClockwise, the spiral rotation direction
															  - <i>Value Angle</i>: total angle to apply instances
															  - <i>Value Length</i>: spiral radial step (between each turn) */
	kA3DFRMDefinitionPatternType_DirectionSpacing,		    /*!< Type of Definition that define a pattern spacing along a linear direction:
															- Parameter Definition:
															  - <i>Definition Direction</i>: linear direction
															- Parameter Data:
															  - <i>IntegerData Count</i>: instance count
															  - <i>DoubleData ExtensionAndStep</i>: extension (first value) and steps (next values)
															  - <i>DoubleData Unit</i>: length unit. <i>(optional)</i> */
	kA3DFRMDefinitionPatternType_AxialSpacing,			    /*!< Type of Definition that define a pattern spacing along a angular direction (around an axis):
															- Parameter Definition:
															  - <i>Definition Position</i>: axis position
															  - <i>Definition Direction</i>: axis direction
															- Parameter Data:
															  - <i>IntegerData Count</i>: instance count
															  - <i>DoubleData ExtensionAndStep</i>: extension (first value) and steps (next values)
															  - <i>DoubleData Unit</i>: angle unit. <i>(optional)</i> */
	kA3DFRMDefinitionPatternType_RadialSpacing,			    /*!< Type of Definition that define a pattern spacing along a radial direction.
															<i>This is used for the second direction in case of pattern with concentric circles/polygons: Pattern of type CyclicMultiple OR PolygonalMultiple</i>:
															- Parameter Data:
															  - <i>IntegerData Count</i>: instance count
															  - <i>DoubleData ExtensionAndStep</i>: extension (first value) and steps (next values)
															  - <i>DoubleData Unit</i>: length unit. <i>(optional)</i> */
	kA3DFRMDefinitionPatternType_PolygonalSpacing,		    /*!< Type of Definition that define a pattern spacing along a polygon:
															- Parameter Definition:
															  - <i>Definition PolygonalShape</i>: polygon shape, of type #kA3DFRMDefinitionPatternType_PolygonalShape
															- Parameter Data:
															  - <i>IntegerData Count</i>: instance count
															  - <i>DoubleData ExtensionAndStep</i>: extension (first value) and steps (next values)
															  - <i>DoubleData Unit</i>: length unit. <i>(optional)</i> */
	kA3DFRMDefinitionPatternType_SpiralSpacing,				/*!< Type of Definition that define a pattern spacing along a spiral:
															- Parameter Definition:
															  - <i>Definition SpiralShape</i>: spiral shape, of type #kA3DFRMDefinitionPatternType_SpiralShape
															- Parameter Data:
															  - <i>IntegerData Count</i>: instance count
															  - <i>DoubleData ExtensionAndStep</i>: extension (first value) and steps (next values)
															  - <i>DoubleData Unit</i>: length unit. <i>(optional)</i> */
	kA3DFRMDefinitionPatternType_InstanceStatus,			/*!< Type of Definition containing status of pattern instances:
															- Parameter Data:
															  - <i>IntegerData Boolean</i>: specify if the instance is enable/disable. In case of boolean array, specify the status of all instances in the pattern.*/
	kA3DFRMDefinitionPatternType_InstanceInformation		/*!< Type of Definition containing information specific to a pattern instance.
															- Parameter Definition:
															  - <i>Definition Position</i>: instance position
															  - <i>Definition InstanceStatus</i>: instance status, of type #kA3DFRMDefinitionPatternType_InstanceStatus
															- Parameter Data:
															  - <i>IntegerData Index</i>: list of instance index for each pattern direction */
}
EA3DFRMDefinitionPatternType;

/*!
\enum EA3DFRMEnumValue_PatternMaster
\brief Enumerate the possible pattern master type 
\ingroup a3d_feature_pattern_module
\version 12
*/
typedef enum
{
	kA3DFRMEnumValue_PatternMaster_None = 0,                    /*!< Invalid Pattern master Type. */
	kA3DFRMEnumValue_PatternMaster_Features ,					/*!<The pattern master is features*/
	kA3DFRMEnumValue_PatternMaster_CurrentBody,					/*!<The pattern master is the current body*/
	kA3DFRMEnumValue_PatternMaster_Geometries					/*!<The pattern master is Geometries*/
}
EA3DFRMEnumValue_PatternMaster;

#endif	/*	__A3DPRCFEATUREPATTERNENUMS_H__ */
