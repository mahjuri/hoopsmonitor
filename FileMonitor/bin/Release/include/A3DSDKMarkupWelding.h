/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for the markup welding module
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifdef A3DAPI_LOAD
#  undef __A3DPRCMarkupWelding_H__
#endif
#ifndef __A3DPRCMarkupWelding_H__
#define __A3DPRCMarkupWelding_H__
#ifndef A3DAPI_LOAD
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKInitializeFunctions.h>
#  include <A3DSDKMarkupDefinition.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD

/*!
\struct A3DMarkupSpotWeldingData
\brief Markup spot welding symbol
\ingroup a3d_markupwelding
\version 4.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16							m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	EA3DMDSpotWeldType					m_eType;							/*!< Undocumented. */
	EA3DMDSpotWeldThickness				m_eThickness;						/*!< Undocumented. */
	A3DBool								m_bIsCritical;						/*!< Undocumented. */
	A3DDouble							m_dDiameter;						/*!< Undocumented. */
	A3DUTF8Char*						m_pcJointId;						/*!< Undocumented. */
	A3DUTF8Char*						m_pcGroupId;						/*!< Undocumented. */
	A3DUTF8Char*						m_pcProcess;						/*!< Undocumented. */
	A3DUns32							m_uiCoordinateEntitiesNotesSize;	/*!< Number of note texts. */
	A3DUTF8Char**						m_ppCoordinateEntitiesNotes;		/*!< Array of note texts. */
	A3DVector3dData						m_sApproachVector;					/*!< Undocumented. */
	A3DVector3dData						m_sClampingVector;					/*!< Undocumented. */
	A3DVector3dData						m_sNormalVector;					/*!< Undocumented. */
	A3DMDTextProperties*				m_pTextProperties;					/*!< Pointer to the text properties. \sa A3DMDTextPropertiesGet */
} A3DMarkupSpotWeldingData;
#endif // A3DAPI_LOAD


/*!
\brief Populates the \ref A3DMarkupSpotWeldingData structure
\ingroup  a3d_markupwelding
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMarkupSpotWeldingGet,(	const A3DMarkupSpotWelding* pSpotWelding,
		 A3DMarkupSpotWeldingData* pData));

/*!
\struct A3DMarkupLineWeldingData
\brief Markup line welding symbol
\ingroup  a3d_markupwelding
\version 4.0
\details
\image html pmi_welding_symbol.png
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16								m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	EA3DMDLineWeldingType					m_eArrowSideType;					/*!< Undocumented. */
	EA3DMDLineWeldingSupplSymbolType		m_eArrowSideSupplType;				/*!< Undocumented. */
	A3DUTF8Char*							m_pcArrowSideValue;					/*!< Undocumented. */
	A3DUTF8Char*							m_pcArrowSideLongitudinalValue;		/*!< Undocumented. */
	A3DUTF8Char*							m_pcArrowSideStaggeredValue;		/*!< Undocumented. */
	EA3DMDLineWeldingFinishSymbol			m_eArrowSideFinishSymbol;			/*!< Undocumented. */
	A3DUTF8Char*							m_pcArrowSideAngle;					/*!< Groove Angle or Countersink Angle, degree symbol to add. */
	A3DUTF8Char*							m_pcArrowSideNumRootDepth;			/*!< Undocumented. */
	A3DUTF8Char*							m_pcArrowSideSize;					/*!< Undocumented. */
	A3DUTF8Char*							m_pcArrowSidePitch;					/*!< Undocumented. */
	EA3DMDLineWeldingType					m_eOtherSideType;					/*!< Undocumented. */
	EA3DMDLineWeldingSupplSymbolType		m_eOtherSideSupplType;				/*!< Undocumented. */
	A3DUTF8Char*							m_pcOtherSideValue;					/*!< Undocumented. */
	A3DUTF8Char*							m_pcOtherSideLongitudinalValue;		/*!< Undocumented. */
	A3DUTF8Char*							m_pcOtherSideStaggeredValue;		/*!< Undocumented. */
	EA3DMDLineWeldingFinishSymbol			m_eOtherSideFinishSymbol;			/*!< Undocumented. */
	A3DUTF8Char*							m_pcOtherSideAngle;					/*!< Undocumented. */
	A3DUTF8Char*							m_pcOtherSideNumRootDepth;			/*!< Undocumented. */
	A3DUTF8Char*							m_pcOtherSideSize;					/*!< Undocumented. */
	A3DUTF8Char*							m_pcOtherSidePitch;					/*!< Undocumented. */
	A3DUTF8Char*							m_pcStandard;						/*!< Undocumented. */
	A3DUns32								m_uiNumberOfProcesses;				/*!< Undocumented. */
	A3DUns32								m_uiNumberOfCoordinate;				/*!< Undocumented. */
	A3DUTF8Char**							m_ppcProcess;						/*!< Undocumented. */
	A3DUns32								m_uiCoordinateEntitiesNotesSize;	/*!< Number of note texts. */
	A3DUTF8Char**							m_ppCoordinateEntitiesNotes;		/*!< Array of note texts. */
	A3DUns32								m_uiProcessNotesSize;				/*!< Number of process texts. */
	A3DUTF8Char**							m_ppProcessNotes;					/*!< Array of process texts. */
	A3DMDTextProperties* m_pTextProperties;										/*!< Pointer to the text properties. \sa A3DMDTextPropertiesGet */
} A3DMarkupLineWeldingData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMarkupLineWeldingData structure
\ingroup  a3d_markupwelding
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMarkupLineWeldingGet,(	const A3DMarkupLineWelding* pLineWelding,
		 A3DMarkupLineWeldingData* pData));

#endif	/*	__A3DPRCMarkupWelding_H__ */
