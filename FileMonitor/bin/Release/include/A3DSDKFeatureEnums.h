/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for representation item entities
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifndef __A3DPRCFEATUREENUMS_H__
#define __A3DPRCFEATUREENUMS_H__

#include "A3DSDKFeatureHoleEnums.h"
#include "A3DSDKFeaturePatternEnums.h"


/*!
\enum EA3DFRMParameterType
\ingroup a3d_feature_parameter_module
\version 10.2
*/
typedef enum
{
	kA3DParameterType_None = 0 ,			/*!< not specified */
	kA3DParameterType_Information,			/*!< contains generic informations (form, name, attributes, ...). */
	kA3DParameterType_Type,					/*!<    */
	kA3DParameterType_Specification,		/*!<  */
	kA3DParameterType_FeatureDefinition,	/*!< contains feature corresponding definition specific to the feature type (extrude/hole/.../pattern specific information) */
	kA3DParameterType_Definition,			/*!< group features that correspond to a definition */
	kA3DParameterType_Container,			/*!< contains all feature nodes that appear in the tree. */
	kA3DParameterType_ContainerInternal,	/*!< contains all feature nodes that not appear in the tree. */
	kA3DParameterType_Data					/*!< contains feature with data (integer, double, ...) */
}
EA3DFRMParameterType;

/*!
\enum EA3DFRMFamily
\brief Feature family type enum
\ingroup a3d_feature_type_module
\version 10.2
*/
typedef enum
{

	kA3DFamily_None = 0,					/*!< . */
	kA3DFamily_Root,						/*!< see \ref EA3DFRMRoot for possible subtypes */
	kA3DFamily_Information,					/*!< not used */
	kA3DFamily_Type,						/*!< see \ref EA3DFRMEnumDataType for possible subtypes */
	kA3DFamily_FeatureDefinition,			/*!< see \ref EA3DFRMFeatureDefinitionType for possible subtypes */
	kA3DFamily_DoubleData,					/*!< see \ref EA3DFRMDoubleDataType for possible subtypes */
	kA3DFamily_IntegerData,					/*!< see \ref EA3DFRMIntegerDataType for possible subtypes */
	kA3DFamily_StringData,					/*!< see \ref EA3DFRMStringDataType for possible subtypes */
	kA3DFamily_Value,						/*!< see \ref EA3DFRMValueType for possible subtypes */
	kA3DFamily_Definition,					/*!< see \ref  for possible subtypes */
	kA3DFamily_Definition_Hole,				/*!< see \ref  for possible subtypes */
	kA3DFamily_Definition_Pattern			/*!< see \ref EA3DFRMDefinitionPatternType for possible subtypes */
}
EA3DFRMFamily;

/*!
\enum EA3DFRMRoot
\brief Feature root features enum
\ingroup a3d_feature_data_root_module
\version 10.2
*/
typedef enum
{
	kA3DFRMRoot_None = 0,		 /*!< . */
	kA3DFRMRoot_Node,			 /*!< feature defining a node in the tree
								 - Parameter Type:
									- <i>Cad type</i>
								 - Parameter Specification:
									- <i>Mode type</i>: cut, srf, thickness...
									- <i>Depth Level</i>: where chamfer is applied. see #EA3DFRMEnumValue_DepthLevel
								 - Parameter Feature_Definition:
									- <i>Definition specific</i> */

	kA3DFRMRoot_Container,		/*!< feature defining a sub tree
								 - Parameter Type:
									- <i>Cad type</i>
								 - Parameter Specification:
									- <i>Mode type</i>: cut, srf, thickness...
								 - Parameter Feature_Definition:
									- <i>Definition specific</i>
								 - Parameter Container:
									- <i>Feature Root</i>: all sub features visible in the tree */

	kA3DFRMRoot_Package			 /*!<  feature package defining sereval feature in one time; and contains multiple feature definition with same type
								 - Parameter Type:
									- <i>Cad type</i>
								 - Parameter Specification:
									- <i>Mode type</i>: cut, srf, thickness...
									- <i>Depth Level</i>: where chamfer is applied. see #EA3DFRMEnumValue_DepthLevel
								 - Parameter Feature_Definition:
									- <i>Definition specific</i>: multiple cad specific definitions
								 - Parameter Container_Internal:
									- <i>Feature Root</i>: all sub features not visible in the tree */
}
EA3DFRMRoot;


/*!
\addtogroup a3d_feature_data_basic_module
@{
The basic features are these defined with one of the following family types kA3DFamily_DoubleData, kA3DFamily_IntegerData, kA3DFamily_StringData.<BR>
These embed basic data: a double, an array of int.. And the sub Type of the feature will indicate what data it is. <BR>
For example, uv parameters will be stored in a feature with type kA3DFamily_DoubleData/kA3DFRMDoubleUVParameter, with two double inside.<BR>

\code
A3DFRMFeatureData sFeatureData;
A3D_INITIALIZE_DATA(A3DFRMFeatureData, sFeatureData);
A3DFRMFeatureGet (pFeatureUV, &sFeatureData);
if( sFeatureData.m_sType.m_eFamily == kA3DFamily_DoubleData
	&& sFeatureData.m_sType.m_uiType ==kA3DFRMDoubleUVParameter)
{
	if( sFeatureData.m_eDataType == kA3DFRMDataDouble)
	{
		A3D_INITIALIZE_DATA(A3DFRMDoubleData, sDoubleData);
		A3DFRMDoubleDataGet(pFeatureUV, &sDoubleData);
		if (sDoubleData.m_uiValuesSize == 2)
		{
			double dU = sDoubleData.m_pdValues[0];
			double dV = sDoubleData.m_pdValues[1];
			//...
		}
		A3DFRMDoubleDataGet(nullptr, &sDoubleData);
	}
}
//...
A3DFRMFeatureGet(nullptr, &sFeatureData);

\endcode


@}
*/


/*!
\enum EA3DFRMDoubleDataType
\brief Double data type
\ingroup a3d_feature_data_basic_module
\version 10.2
*/
typedef enum
{
	kA3DFRMDoubleNone = 0, /*!< . */
	kA3DFRMDoubleValue,				/*!< one or list of double */
	kA3DFRMDoubleUnit,				/*!< one or list of unit */
	kA3DFRMDoubleOffset,			/*!< one or list of offset value */
	kA3DFRMDoubleDepth,				/*!< one or list of depth value */
	kA3DFRMDoubleDiameter,			/*!< one or list of diameter value*/
	kA3DFRMDoubleAngle,				/*!< one or list of angle value */
	kA3DFRMDoublePitch,				/*!< one pitch value */
	kA3DFRMDoubleDistance,			/*!< one or list of  distance value */
	kA3DFRMDoubleExtensionAndStep,	/*!< first value is Extension, and next values are the list of steps. */
	kA3DFRMDoubleLinearParameter,	/*!< one or list of linear parameter, you can have a connection to a curve. */
	kA3DFRMDoubleUVParameter		/*!< two doubles expected or list of pair of doubles, you can have a connection to a surface. */
}
EA3DFRMDoubleDataType;

/*!
\enum EA3DFRMIntegerDataType
\brief Integer data type
\ingroup a3d_feature_data_basic_module
\version 10.2
*/
typedef enum
{
	kA3DFRMIntegerDataNone = 0,	   /*!< . */
	kA3DFRMIntegerDataValue	,			/*!< one or list of values */
	kA3DFRMIntegerDataBoolean,			/*!< one or list of boolean values */
	kA3DFRMIntegerDataIndex,			/*!< one or list of indexes */
	kA3DFRMIntegerDataKeepSpecification,/*!< one or list of booleans, 0/1 corresponding to not keep specification / keep specification. */
	kA3DFRMIntegerDataRadialAlignment,	/*!< one or list of booleans, 1 corresponding to radial alignment active. */
	kA3DFRMIntegerDataClockwise,		/*!< one or list of booleans, 0/1 corresponding to counterclockwise / clockwise. */
	kA3DFRMIntegerDataId,				/*!< one or list of ids */
	kA3DFRMIntegerDataFlip,				/*!< one or list of booleans, 0/1 corresponding to not flip / flip. */
	kA3DFRMIntegerDataType,				/*!< one or list of types  */
	kA3DFRMIntegerDataCount,			/*!< one or list of counts */
	kA3DFRMIntegerDataSize,				/*!< one or list of sizes */
	kA3DFRMIntegerDataNbStart			/*!< Number of start of thread (specific to Solidworks) */
}
EA3DFRMIntegerDataType;



/*!
\enum EA3DFRMStringDataType
\brief String data type
\ingroup a3d_feature_data_basic_module
\version 10.2
*/
typedef enum
{
	kA3DFRMStringDataNone = 0,	   /*!< not specified */
	kA3DFRMStringDataName,		   /*!< name */
	kA3DFRMStringDataAttribute,		/*!< at least two values expected, one for the title and others for the attributes values */
	kA3DFRMStringDataType,		   /*!< type */
	kA3DFRMStringDataValue,		   /*!< value */
	kA3DFRMStringDataOption		   /*!< two values expected: first one the name the option, and the second one string "TRUE" or "FALSE". */
}
EA3DFRMStringDataType;

/*!
\brief Feature type enum
\ingroup a3d_feature_type_module
\version 10.2

#Mechanism for value interpretation
feature with type kA3DFamily_Type/kA3DFRMEnumDataType_CAD
has m_eDataType::m_eDataType set at kA3DFRMDataEnum,
and the function \ref A3DFRMEnumDataGet will return integer values to interpret of values in EA3DFRMEnumValue_CadType enum.
*/
typedef enum
{
	kA3DFRMEnumDataType_None = 0,		/*!< Not defined */
	kA3DFRMEnumDataType_CAD,			/*!< \ref EA3DFRMEnumValue_CadType*/
	kA3DFRMEnumDataType_Mode,			/*!< \ref EA3DFRMEnumValue_ModeType */
	kA3DFRMEnumDataType_Depth,			/*!< \ref EA3DFRMEnumValue_DepthType */
	kA3DFRMEnumDataType_Pattern,		/*!< \ref EA3DFRMEnumValue_Pattern */
	kA3DFRMEnumDataType_HoleShape,		/*!< \ref EA3DFRMEnumValue_Hole */
	kA3DFRMEnumDataType_DepthLevel,		/*!< \ref EA3DFRMEnumValue_DepthLevel */
	kA3DFRMEnumDataType_RevolveAngle,	/*!< \ref EA3DFRMEnumValue_RevolveAngleType */
	kA3DFRMEnumDataType_Chamfer,		/*!< \ref EA3DFRMEnumValue_ChamferType */
	kA3DFRMEnumDataType_Fillet,			/*!< \ref EA3DFRMEnumValue_FilletType */
	kA3DFRMEnumDataType_FilletLength,	/*!< \ref EA3DFRMEnumValue_FilletLengthType */
	kA3DFRMEnumDataType_FilletConic,	/*!< \ref EA3DFRMEnumValue_FilletConicType */
	kA3DFRMEnumDataType_LengthMode,		/*!< \ref EA3DFRMEnumValue_LengthModeType */
	kA3DFRMEnumDataType_PatternMaster,	/*!< \ref EA3DFRMEnumValue_PatternMaster */
	kA3DFRMEnumDataType_ReferenceMaster	/*!< \ref EA3DFRMEnumValue_ReferenceMaster */
}
EA3DFRMEnumDataType;

/*!
\enum EA3DFRMDefinitionType
\brief Feature definition enum
\ingroup a3d_feature_data_definition_module
\version 10.2
*/
typedef enum
{
	kA3DFRMDefinitionType_None = 0,				   /*!< Not specified*/
	kA3DFRMDefinitionType_Depth,				   /*!< Depth */
	kA3DFRMDefinitionType_DepthFrom,			   /*!< Depth from */
	kA3DFRMDefinitionType_Position,				   /*!< Position */
	kA3DFRMDefinitionType_Direction,			   /*!< Direction */
	kA3DFRMDefinitionType_Thread,				   /*!< Thread */
	kA3DFRMDefinitionType_Shape,				   /*!< Shape */
	kA3DFRMDefinitionType_Reference,			   /*!< Reference */
	kA3DFRMDefinitionType_Sketch,				   /*!< Sketch */
	kA3DFRMDefinitionType_RevolveAngle,			   /*!< Revolve angle */
	kA3DFRMDefinitionType_RevolveAngleFrom,		   /*!< Revolve angle from */
	kA3DFRMDefinitionType_Axis,					   /*!< Axis */
	kA3DFRMDefinitionType_Chamfer,				   /*!< Chamfer */
	kA3DFRMDefinitionType_FilletLength,			   /*!< Fillet Length, \see a3d_feature_description_fillet_module */
	kA3DFRMDefinitionType_ReferenceMaster		   /*!< Reference Master \see a3d_feature_description_reference_master_module */
}
EA3DFRMDefinitionType;

/*!
\enum EA3DFRMFeatureDefinitionType
\brief Feature definition enum for tree features
\ingroup a3d_feature_data_specific_module
\version 10.2
*/
typedef enum
{
	kA3DFRMFeatureDefinitionType_None = 0,			   /*!< Invalid FeatureDefinition Type. */
	kA3DFRMFeatureDefinitionType_Hole,				   /*!< Type of FeatureDefinition containing information specific to Hole. */
	kA3DFRMFeatureDefinitionType_Pattern,			   /*!< Type of FeatureDefinition containing information specific to Pattern. */
	kA3DFRMFeatureDefinitionType_Sketch,			   /*!< Type of FeatureDefinition containing information specific to Sketch. */
	kA3DFRMFeatureDefinitionType_Thread,		       /*!< Type of FeatureDefinition containing information specific to Thread. */
	kA3DFRMFeatureDefinitionType_Extrude,		       /*!< Type of FeatureDefinition containing information specific to Extrude. */
	kA3DFRMFeatureDefinitionType_Revolve,		       /*!< Type of FeatureDefinition containing information specific to Revolve. */
	kA3DFRMFeatureDefinitionType_Cosmetic,		       /*!< Type of FeatureDefinition containing information specific to Cosmetic. */
	kA3DFRMFeatureDefinitionType_Chamfer,		       /*!< Type of FeatureDefinition containing information specific to Chamfer. */
	kA3DFRMFeatureDefinitionType_Fillet,			   /*!< Type of FeatureDefinition containing information specific to Fillet. */
	kA3DFRMFeatureDefinitionType_Mirror,		       /*!< Type of FeatureDefinition containing information specific to Mirror. */
	kA3DFRMFeatureDefinitionType_Symmetry,			   /*!< Type of FeatureDefinition containing information specific to Symmetry. */
	kA3DFRMFeatureDefinitionType_Translate,			   /*!< Type of FeatureDefinition containing information specific to Translate. */
	kA3DFRMFeatureDefinitionType_Rotate				   /*!< Type of FeatureDefinition containing information specific to Rotate. */
}
EA3DFRMFeatureDefinitionType;

/*!
\enum EA3DFRMDataType
\brief Feature data enum
\ingroup a3d_feature_data_enum_module
\version 10.2
*/
typedef enum
{
	kA3DFRMDataNone = 0,	   /*!< no data at the feature level.*/
	kA3DFRMDataInteger,		   /*!< string data embedded, see `A3DFRMIntegerDataGet` */
	kA3DFRMDataDouble,		   /*!< double data embedded, see `A3DFRMDoubleDataGet` */
	kA3DFRMDataString,		   /*!< string data embedded, see `A3DFRMStringDataGet` */
	kA3DFRMDataEnum			   /*!< value in enumerate embedded, see `A3DFRMIntegerDataGet` */
}
EA3DFRMDataType;

/*!
\enum EA3DFRMValueType
\brief Feature value type enum
\ingroup a3d_feature_data_value_module
\version 10.2
*/
typedef enum
{
	kA3DFRMValueType_None = 0,		 /*!< . */
	kA3DFRMValueType_Length,		 /*!< . */
	kA3DFRMValueType_Angle,			 /*!< . */
	kA3DFRMValueType_Diameter,		 /*!< . */
	kA3DFRMValueType_Radius,		 /*!< . */
	kA3DFRMValueType_Depth,			 /*!< . */
	kA3DFRMValueType_Thickness,		 /*!< . */
	kA3DFRMValueType_Offset,		 /*!< . */
	kA3DFRMValueType_Distance,		 /*!< . */
	kA3DFRMValueType_Coords,		 /*!< . */
	kA3DFRMValueType_Vector,		 /*!< . */
	kA3DFRMValueType_Matrix,		 /*!< . */
	kA3DFRMValueType_Area,			 /*!< . */
	kA3DFRMValueType_Volume,		 /*!< . */
	kA3DFRMValueType_Mass,			 /*!< . */
	kA3DFRMValueType_Time			 /*!< . */
}
EA3DFRMValueType;


/*!
\enum EA3DFRMLinkType
\brief Feature link type enum
\ingroup a3d_feature_link_module
\version 10.2
details
*/
typedef enum
{
	kA3DFRMLink_None = 0,			/*!<No type specified */
	kA3DFRMLink_Outcome,	 /*!< Link to the item defined by the feature.Generally, this element is highlighted when we select the feature. */
	kA3DFRMLink_Position,	  /*!<The linked element id used to defined a location, an offset. For example, to position a hole on a face, a point can be referenced. */
	kA3DFRMLink_Construction,			/*!< The link refers an entity who is used for defining feature information.
The depth of an extrude can be defined with a reference, "depth to this face", in this case the depth feature has a construction connection to the face. */
	kA3DFRMLink_Support		 /*!< The link refers an entity who is the support of the feature. For example, a chamfer is applied on an edge, this edge is referenced like a support.
A pattern feature will have a link to the master feature(s) with a support link.
This is the same think for the feature master of a pattern. This element can be a TopoItem, or a feature. */
}
EA3DFRMLinkType;


/*!
\enum EA3DFRMStatus
\brief Feature status enum
\ingroup a3d_feature_type_module
\version 10.2
*/
typedef enum
{
	kA3DFRMStatus_Success = 0,			 /*!< No error */
	kA3DFRMStatus_Failed,				 /*!< Error in reading process */
	kA3DFRMStatus_NotYetImplemented		 /*!< Limitation*/
}
EA3DFRMStatus;

/*!
\enum EA3DFRMEnumValue_CadType
\brief Enumerate the possible types of cad type. <br/>
\ingroup a3d_feature_type_module
\version 10.2
*/
typedef enum
{
	kA3DFRMEnumValue_CadType_None = 0,			   /*!< \a No type specified */
	kA3DFRMEnumValue_CadType_Sketch,			   /*!< \ref a3d_feature_description_sketch_module */
	kA3DFRMEnumValue_CadType_Plane,				   /*!< \a Plane */
	kA3DFRMEnumValue_CadType_Point,				   /*!< \a Point */
	kA3DFRMEnumValue_CadType_CoordinateSystem,	   /*!< \a CoordinateSystem */
	kA3DFRMEnumValue_CadType_Direction,			   /*!< \a Direction */
	kA3DFRMEnumValue_CadType_Curve,				   /*!< \a Curve */
	kA3DFRMEnumValue_CadType_Analysis,			   /*!< \a Analysis */
	kA3DFRMEnumValue_CadType_ImportedSolid,		   /*!< \a ImportedSolid */
	kA3DFRMEnumValue_CadType_Extrusion,			   /*!< \ref a3d_feature_description_extrude_module*/
	kA3DFRMEnumValue_CadType_Revolution,		   /*!< \ref a3d_feature_description_revolve_module*/
	kA3DFRMEnumValue_CadType_Surface,			   /*!< \a Surface */
	kA3DFRMEnumValue_CadType_Block,				   /*!< \a Block */
	kA3DFRMEnumValue_CadType_Sphere,			   /*!< \a Sphere */
	kA3DFRMEnumValue_CadType_Cone,				   /*!< \a Cone */
	kA3DFRMEnumValue_CadType_Cylinder,			   /*!< \a Cylinder */
	kA3DFRMEnumValue_CadType_Torus,				   /*!< \a Torus */
	kA3DFRMEnumValue_CadType_Dome,				   /*!< \a Dome */
	kA3DFRMEnumValue_CadType_Helix,				   /*!< \a Helix */
	kA3DFRMEnumValue_CadType_TruncatedPrism,	   /*!< \a TruncatedPrism */
	kA3DFRMEnumValue_CadType_Hole,				   /*!< \ref a3d_feature_description_hole_module*/
	kA3DFRMEnumValue_CadType_Chamfer,			   /*!< \ref a3d_feature_description_chamfer_module*/
	kA3DFRMEnumValue_CadType_Blend,				   /*!< \a Blend*/
	kA3DFRMEnumValue_CadType_Fillet,			   /*!< \ref a3d_feature_description_fillet_module */
	kA3DFRMEnumValue_CadType_Groove,			   /*!< \a Groove */
	kA3DFRMEnumValue_CadType_Pocket,			   /*!< \a Pocket */
	kA3DFRMEnumValue_CadType_Sweep,				   /*!< \a Sweep */
	kA3DFRMEnumValue_CadType_Pipe,				   /*!< \a Pipe */
	kA3DFRMEnumValue_CadType_Group,				   /*!< \a Group */
	kA3DFRMEnumValue_CadType_Mirror,			   /*!< \ref a3d_feature_description_mirror_module*/
	kA3DFRMEnumValue_CadType_Pattern,			   /*!< \ref a3d_feature_description_pattern_module*/
	kA3DFRMEnumValue_CadType_GroupPattern,		   /*!< \ref a3d_feature_description_pattern_module*/
	kA3DFRMEnumValue_CadType_Instance,			   /*!< \a Instance, see also \ref a3d_feature_description_pattern_module */
	kA3DFRMEnumValue_CadType_Package,			   /*!< \a Package */
	kA3DFRMEnumValue_CadType_Shell,				   /*!< \a Shell */
	kA3DFRMEnumValue_CadType_Trim,				   /*!< \a Trim */
	kA3DFRMEnumValue_CadType_Split,				   /*!< \a Split */
	kA3DFRMEnumValue_CadType_Sew,				   /*!< \a Sew */
	kA3DFRMEnumValue_CadType_Rib,				   /*!< \a Rib */
	kA3DFRMEnumValue_CadType_Draft,				   /*!< \a Draft */
	kA3DFRMEnumValue_CadType_Hollow,			   /*!< \a Hollow */
	kA3DFRMEnumValue_CadType_Quilt,				   /*!< \a Quilt */
	kA3DFRMEnumValue_CadType_LocalPush,			   /*!< \a Local Push */
	kA3DFRMEnumValue_CadType_BooleanOperation,	   /*!< \a Boolean Operation */
	kA3DFRMEnumValue_CadType_Intersect,			   /*!< \a Intersect */
	kA3DFRMEnumValue_CadType_CutOut,			   /*!< \a CutOut */
	kA3DFRMEnumValue_CadType_Merge,				   /*!< \a Merge */
	kA3DFRMEnumValue_CadType_Translate,			   /*!< \ref a3d_feature_description_translate_module*/
	kA3DFRMEnumValue_CadType_Rotate,			   /*!< \ref a3d_feature_description_rotate_module*/
	kA3DFRMEnumValue_CadType_Scale,				   /*!< \a Scale */
	kA3DFRMEnumValue_CadType_Offset,			   /*!< \a Offset */
	kA3DFRMEnumValue_CadType_Transformation,	   /*!< \a Transformation */
	kA3DFRMEnumValue_CadType_Thread,			   /*!< \a Thread */
	kA3DFRMEnumValue_CadType_Cosmetic,			   /*!< \a Cosmetic */
	kA3DFRMEnumValue_CadType_Material,			   /*!< \a Material */
	kA3DFRMEnumValue_CadType_Thickness,			   /*!< \a Thickness*/
	kA3DFRMEnumValue_CadType_RemoveFace,		   /*!< \a Remove Face */
	kA3DFRMEnumValue_CadType_UserDefined,		   /*!< \a User Defined*/
	kA3DFRMEnumValue_CadType_Member,			   /*!< \a Member, son of an assembly. See
													    \ref a3d_feature_description_member_module */
	kA3DFRMEnumValue_CadType_Symmetry			   /*!< \ref a3d_feature_description_symmetry_module*/
}
EA3DFRMEnumValue_CadType;

/*!
\enum EA3DFRMEnumValue_ModeType
\brief Enumerate the possible types of mode type.<br/>
\ingroup a3d_feature_tree_module
\version 10.2
*/
typedef enum
{
	kA3DFRMEnumValue_ModeType_None = 0,				/*!< . */
	kA3DFRMEnumValue_ModeType_Cut,					/*!< . */
	kA3DFRMEnumValue_ModeType_CutOut,				/*!< . */
	kA3DFRMEnumValue_ModeType_Surface,				/*!< . */
	kA3DFRMEnumValue_ModeType_SurfaceCappedEnds,	/*!< . */
	kA3DFRMEnumValue_ModeType_PatternGeometry,		/*!< . */
	kA3DFRMEnumValue_ModeType_Symbolic				/*!< . */
}
EA3DFRMEnumValue_ModeType;


/*!
\enum EA3DFRMEnumValue_DepthType
\ingroup a3d_feature_hole_module

\brief all possible type of depth. <br/>
This can be used to specify Depth of Features Extrude, Hole, Thread, etc.

Example of usage in case of Simple Hole Feature:
\image html frm_depth_types.png

\version 10.2
*/
typedef enum
{
	kA3DFRMEnumValue_DepthType_None = 0,   /*!< Invalid Depth Type. */
	kA3DFRMEnumValue_DepthType_Blind,	   /*!< Depth is define by a Depth Value. */
	kA3DFRMEnumValue_DepthType_ThruAll,	   /*!< Depth is define thought all geometry. */
	kA3DFRMEnumValue_DepthType_Reference,  /*!< Depth is define up to the specified reference. */
	kA3DFRMEnumValue_DepthType_Next,	   /*!< Depth is define up to the next surface encountered. */
	kA3DFRMEnumValue_DepthType_Until,	   /*!< Depth is define thought the specified reference, up to the last encounter. */
	kA3DFRMEnumValue_DepthType_AsReference,	   /*!< In case of feature likes Thread, DepthType_AsReference specified that the Depth is the same as the reference feature <i>(example: on a Hole Feature create a full Thread Feature, that is as deep as the Hole Depth)</i>. */
	kA3DFRMEnumValue_DepthType_ThreadPitchCount		/*!< Depth is define by the number of revolution (specific of SLW thread). */
}
EA3DFRMEnumValue_DepthType;




/*!
\enum EA3DFRMEnumValue_RevolveAngleType
\ingroup a3d_feature_type_module
\brief Enumerate the possible types of revolve angle types <br/>
This can be used to specify Angle for a Feature Revolution.

Example of usage for a Feature Revolution:
\image html frm_revolve_angle_types.png

\version 11
*/
typedef enum
{
	kA3DFRMEnumValue_RevolveAngleType_None = 0,				/*!< Invalid Revolve Angle Type. */
	kA3DFRMEnumValue_RevolveAngleType_Variable,				/*!< Revolve angle is define with an Angle Value. */
	kA3DFRMEnumValue_RevolveAngleType_Variable_Symmetric,	/*!< Revolve angle is define with an Angle Value, that should be apply symmetrically on both directions. */
	kA3DFRMEnumValue_RevolveAngleType_Reference,			/*!< Depth is define up to the specified reference. */
	kA3DFRMEnumValue_RevolveAngleType_Next,					/*!< Depth is define up to the next surface encountered. */
}
EA3DFRMEnumValue_RevolveAngleType;



/*!
\enum EA3DFRMEnumValue_ChamferType
\brief Enumerate the possible types of chamfer.<br/>
This allow to specify the type of chamfer, and so the Definition you should expect to have under the #kA3DFRMFeatureDefinitionType_Chamfer.

\image html frm_chamfer_types.png
\image html frm_chamfer_corner_types.png

\ingroup a3d_feature_chamfer_module
\version 11
*/
typedef enum
{
	kA3DFRMEnumValue_ChamferType_None = 0,				/*!< Invalid Chamfer Type. */
	kA3DFRMEnumValue_ChamferType_Distance_Distance,		/*!< Chamfer symmetric defined with one distance:
														- Parameter Data:
															- <i>Value Length</i>: chamfer length */
	kA3DFRMEnumValue_ChamferType_Distance1_Distance2,   /*!< Chamfer asymmetric defined with two distance:
														- Parameter Data:
															- <i>Value Length</i>: chamfer first distance
															- <i>Value Length</i>: chamfer second distance*/
	kA3DFRMEnumValue_ChamferType_Offset_Offset,			/*!< Chamfer symmetric define with one distance:
														- Parameter Data:
															- <i>Value Length</i>: chamfer offset */
	kA3DFRMEnumValue_ChamferType_Offset1_Offset2,		/*!< Chamfer asymmetric defined with two distance:
														- Parameter Data:
															- <i>Value Length</i>: chamfer first offset
															- <i>Value Length</i>: chamfer second offset */
	kA3DFRMEnumValue_ChamferType_Length_Angle,			/*!< Chamfer defined with one distance and an angle:
														- Parameter Data:
															- <i>Value Length</i>: chamfer length
															- <i>Value Angle</i>: chamfer angle */
	kA3DFRMEnumValue_ChamferType_ChordalLength_Angle,	/*!< Chamfer defined with the chordal length and an angle:
														- Parameter Data:
															- <i>Value Length</i>: chamfer chordal length
															- <i>Value Angle</i>: chamfer angle*/
	kA3DFRMEnumValue_ChamferType_Height_Angle,			/*!< Chamfer defined with the distance between the edge and the new face and an angle:
														- Parameter Data:
															- <i>Value Length</i>: chamfer height
															- <i>Value Angle</i>: chamfer angle */
	kA3DFRMEnumValue_ChamferType_Corner					/*!< Chamfer corner. Expect to have three chamfer definition with one length each:
														- Parameter Definition:
															- <i>Definition Chamfer</i>: first chamfer definition
															- <i>Definition Chamfer</i>: second chamfer definition
															- <i>Definition Chamfer</i>: third chamfer definition */
}
EA3DFRMEnumValue_ChamferType;


/*!
\enum EA3DFRMEnumValue_FilletType
\brief Enumerate the possible types of fillet types <br/>
\ingroup a3d_feature_fillet_module
\version 11
*/
typedef enum
{
	kA3DFRMEnumValue_FilletType_None = 0,				/*!< . */
	kA3DFRMEnumValue_FilletType_Edge,		/*!< . */
	kA3DFRMEnumValue_FilletType_FaceToFace,   /*!< . */
	kA3DFRMEnumValue_FilletType_Tritangent				/*!< . */
}
EA3DFRMEnumValue_FilletType;


/*!
\enum EA3DFRMEnumValue_FilletLengthType
\brief Enumerate the possible types of fillet length types <br/>
\ingroup a3d_feature_type_module
\version 11
*/
typedef enum
{
	kA3DFRMEnumValue_FilletLengthType_None = 0,				/*!< . */
	kA3DFRMEnumValue_FilletLengthType_Circular, 			/*!< . */
	kA3DFRMEnumValue_FilletLengthType_Conic,				/*!< . */
	kA3DFRMEnumValue_FilletLengthType_Conic_Asymmetric,		/*!< . */
	kA3DFRMEnumValue_FilletLengthType_Continue,				/*!< . */
	kA3DFRMEnumValue_FilletLengthType_Continue_Asymmetric,	/*!< . */
	kA3DFRMEnumValue_FilletLengthType_Curvature,			/*!< . */
	kA3DFRMEnumValue_FilletLengthType_Curvature_Asymmetric	/*!< . */
}
EA3DFRMEnumValue_FilletLengthType;

/*!
\enum EA3DFRMEnumValue_FilletConicType
\brief Enumerate the possible types of fillet conic types <br/>
Determine how a symmetric ConicLength is define, with a combination of two values between: Boundary Radius, Center Radius, Rho, Chordal Length
\ingroup a3d_feature_fillet_module
\version 11
*/
typedef enum
{
	kA3DFRMEnumValue_FilletConicType_None = 0,				/*!< . */
	kA3DFRMEnumValue_FilletConicType_Boundary_Rho,			/*!< Conic Length is define with values: Boundary Radius, Rho. */
	kA3DFRMEnumValue_FilletConicType_Center_Rho,			/*!< Conic Length is define with values: Center Radius, Rho. */
	kA3DFRMEnumValue_FilletConicType_Boundary_Center,		/*!< Conic Length is define with values: Boundary Radius, Center Radius. */
	kA3DFRMEnumValue_FilletConicType_Chordal_Rho,			/*!< Conic Length is define with values: Chordal Length, Rho. */
}
EA3DFRMEnumValue_FilletConicType;

/*!
\enum EA3DFRMEnumValue_LengthModeType
\brief Enumerate the possible types of length mode types <br/>
\ingroup a3d_feature_type_module
\version 11
*/
typedef enum
{
	kA3DFRMEnumValue_LengthModeType_None = 0,				/*!< . */
	kA3DFRMEnumValue_LengthModeType_ByValue,			/*!< . */
	kA3DFRMEnumValue_LengthModeType_Parameter,			/*!< . */
	kA3DFRMEnumValue_LengthModeType_Reference				/*!< . */
}
EA3DFRMEnumValue_LengthModeType;


/*!
\enum EA3DFRMEnumValue_ReferenceMaster
\brief Enumerate the possible master type
\ingroup a3d_feature_type_module
\version 12
*/
typedef enum
{
	kA3DFRMEnumValue_ReferenceMaster_None = 0,                    /*!< Invalid reference master Type. */
	kA3DFRMEnumValue_ReferenceMaster_Features,					/*!<The reference master is a feature list*/
	kA3DFRMEnumValue_ReferenceMaster_CurrentBody,					/*!<The reference master is the current body*/
	kA3DFRMEnumValue_ReferenceMaster_Geometries					/*!<The reference master is a geometry list*/
}
EA3DFRMEnumValue_ReferenceMaster;

#endif	/*	__A3DPRCFEATUREENUMS_H__ */
