/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for representation item entities
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/
#ifdef A3DAPI_LOAD
#  undef __A3DPRCFEATURES_H__
#endif
#ifndef __A3DPRCFEATURES_H__
#define __A3DPRCFEATURES_H__
#ifndef A3DAPI_LOAD
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKFeatureEnums.h>
#  include <A3DSDKInitializeFunctions.h>
#  include <A3DSDKRepItems.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD


/*!
\defgroup a3d_feature_module Feature module
\ingroup a3d_entitiesdata_module
\brief Accesses to the tree data of a feature based model.
*/

/*!
\defgroup a3d_feature_tree_module Feature Tree
\ingroup a3d_feature_module
*/

/*!
\addtogroup a3d_feature_tree_module
@{
CAD model are represented by a feature tree.
A model tree is composed by nodes. Each of these nodes is a build step, whose definition parameters are stored in a feature.
The following model is defined with construction entities, then a pad, and a hole.
\image html frm_tree.png

The model trees are accessible through the product occurence by using the function \ref A3DAsmProductOccurrenceGet,
and the function \ref A3DFRMFeatureTreeGet will populate the data structure.
An array of feature trees is stored in \ref  A3DAsmProductOccurrenceData, usually there is only one feature tree by model.

\code
A3DUns32 u;
A3DAsmProductOccurrenceData sData;
A3D_INITIALIZE_DATA(A3DAsmProductOccurrenceData, sData);
if( A3DAsmProductOccurrenceGet(pOccurrence, &sData)== A3D_SUCCESS)
{
	for (u = 0; u < sData.m_uiFeatureBasedEntitiesSize; ++u)
	{
		A3D_INITIALIZE_DATA(A3DFRMFeatureTreeData, sTreeData);
		A3DFRMFeatureTreeGet (sData.m_ppFeatureBasedEntities[u], &sTreeData);
		//...
		A3DFRMFeatureTreeGet (NULL, &sTreeData);
	}
	A3DAsmProductOccurrenceGet(NULL, &sData);
}
\endcode



In addition, the function \ref A3DFRMGetSpecificNodes allows to directly get a list of features with a specific cad type.
For instance, to get all features HOLE defined in the tree. In this case,

\code
A3DUns32 iSize;
A3DFRMFeature** ppFeatureNodes;
A3DFRMGetSpecificNodes( pFRMFeatureTree, kA3DFRMEnumValue_CadType_Hole, &iSize, &ppFeatureNodes);
//...
A3DFRMGetSpecificNodes( NULL, kA3DFRMEnumValue_CadType_Hole, &iSize, &ppFeatureNodes);
\endcode

Note that A3DFRMFeatureTree object is an abstraction of \ref A3DRiSet, so it possible to directly call the function \ref A3DFRMFeatureTreeGet on a set

\code
A3DEEntityType eType=kA3DTypeUnknown;
A3DEntityGetType(pRepresentationItem,&eType);
if(eType == kA3DTypeRiSet)
{
	A3DFRMFeatureTreeData oData;
	A3D_INITIALIZE_DATA(A3DFRMFeatureTreeData, sTreeData);
	if (A3DFRMFeatureTreeGet(pRepresentationItem, &sTreeData) == A3D_SUCCESS)
	{
		A3DFRMFeatureTreeGet(NULL, &sTreeData);
	}
	else
	{
		//regular RiSet
	}
}
\endcode


@}
*/

/*!
\defgroup a3d_feature_parameter_module Parameter
\ingroup a3d_feature_module
*/


/*!
\addtogroup a3d_feature_parameter_module
@{
Parameters allow to group features according to the type of information whom they contain.
All features storing data under parameter kA3DParameterType_Data, all feature giving a specification under parameter kA3DParameterType_Specification,
all tree nodes under the parameter kA3DParameterType_Container...

@}
*/

/*!
\defgroup a3d_feature_structure_module Structure
\ingroup a3d_feature_module
*/


/*!
\defgroup a3d_feature_description_module Description
\ingroup a3d_feature_module
*/

/*!
\defgroup a3d_feature_description_hole_module Hole
\ingroup a3d_feature_description_module
\image html frm_hole_2018.svg
*/
/*!
\defgroup a3d_feature_description_pattern_module Pattern
\ingroup a3d_feature_description_module
\image html frm_pattern_cont_inst_2018.svg
\image html frm_pattern_cont_node_2018.svg
\image html frm_pattern_node_2018.svg
For a rectangular pattern, we can have a parameter of type "information", which contains a rotation angle. This rotation is already applied on the direction.
*/
/*!
\defgroup a3d_feature_description_sketch_module Sketch
\ingroup a3d_feature_description_module
\image html frm_sketch_2018.svg
*/
/*!
\defgroup a3d_feature_description_extrude_module Extrude
\ingroup a3d_feature_description_module
\image html frm_extrude_2018.svg
*/
/*!
\defgroup a3d_feature_description_revolve_module Revolve
\ingroup a3d_feature_description_module
\image html frm_revolve_2018.svg
*/
/*!
\defgroup a3d_feature_description_chamfer_module Chamfer
\ingroup a3d_feature_description_module
\image html frm_chamfer_2018.svg
\image html frm_chamfer_corner_2018.svg
\image html frm_chamfer_package_2018.svg
*/
/*!
\defgroup a3d_feature_description_fillet_module Fillet
\ingroup a3d_feature_description_module

<table>
<th>Fillet
<tr><td colspan="3">\image html frm_fillet_2018.svg
<th> Fillet Length circular
<td>\image html frm_fillet_length_circular_2018.svg
<tr>
<th>Fillet Length conic / continue
<td>\image html frm_fillet_length_conic_continue_1_2018.svg
<td>\image html frm_fillet_length_conic_continue_2_2018.svg
<td>\image html frm_fillet_length_conic_continue_3_2018.svg
<th>Fillet Length conic / continue  - asymmetric
<tr><td colspan="3">\image html frm_fillet_length_conic_continue_asymmetric_2018.svg
<th> Fillet Length curvature
<tr><td colspan="3">\image html frm_fillet_length_curvature_2018.svg
<th> Fillet Length curvature - asymmetric
<tr><td colspan="3">\image html frm_fillet_length_curvature_asymmetric_2018.svg
</table>

*/

/*!
\defgroup a3d_feature_description_thread_module Thread
\ingroup a3d_feature_description_module
\image html frm_thread.png
\image html frm_thread.svg
we can have other data in the thread who contain an attribute for define this value or an option who is used in the thread.
we can have a rotation angle, who create an offset for the start of the thread.
we can have an option for the use of mirror or of trim with start or end face.
In the case we have many start, the thread contain a feature of type NB_START.
*/
/*!
\defgroup a3d_feature_description_rotate_module Rotate
\ingroup a3d_feature_description_module
\image html frm_rotate_2019.svg
*/
/*!
\defgroup a3d_feature_description_translate_module Translate
\ingroup a3d_feature_description_module
\image html frm_translate_2019.svg
*/
/*!
\defgroup a3d_feature_description_mirror_module Mirror
\ingroup a3d_feature_description_module
\image html frm_mirror_2019.svg
*/
/*!
\defgroup a3d_feature_description_symmetry_module Symmetry
\ingroup a3d_feature_description_module
\image html frm_symmetry_2019.svg
*/

/*!
\defgroup a3d_feature_description_member_module Member
\ingroup a3d_feature_description_module
\image html frm_member_2019.svg
The feature list of the highlighted product occurrence is located on its deepest prototype.
*/

/*!
\defgroup a3d_feature_description_reference_master_module ReferenceMaster
\ingroup a3d_feature_description_module
\image html frm_reference_master_geometries.svg
\image html frm_reference_master_current_body.svg
Support links are created only if the current body still exists in the latest revision of the file
\image html frm_reference_master_features.svg
*/

/*!
\addtogroup a3d_feature_structure_module
@{

The feature is a structure that allows to store information with various levels of complexity. The hierarchy structures the data from a simple double/integer storage to a cad feature information.
Depending of their Family type what they embed different types of sub-features, group under feature parameters.
Also the pair m_eFamily and m_uiType allows to figure out what represents data underneath.
The family type specifies the way to interpret the type. The type is an integer that need to be interpreted as a value in an enum specify by the family type.

The following table shows the correspondence between the family type  and the associated enum; and the expecetd data defined below a feature with this type of feature.

<table>
<caption >Feature type</caption>
<tr><th>Family                  							<th> Corresponding enum																					<th> Description
<tr><td>Information											<td>																									<td>
<tr><td>Type												<td> \ref EA3DFRMEnumDataType																			<td> Store a value of an enumeration (cad type, depth type, ...) <br> \image html frm_data.png
<tr><td>Double Data ,<br>  Integer Data ,<br>  String Data	<td> \ref EA3DFRMDoubleDataType,<br> \ref EA3DFRMIntegerDataType, <br> \ref EA3DFRMStringDataType 		<td> Type of double data (unit, offset, diameter, ...)<br> \image html frm_type.png
<tr><td>Value												<td> \ref EA3DFRMValueType																				<td> Type of complex feature (Vector, Depth, ...)<br> \image html frm_value.png
<tr><td>Definition											<td> \ref EA3DFRMDefinitionType																			<td> Type of complex feature (Vector, Depth, ...)<br> \image html frm_definition.png
<tr><td>Definition Hole,<br>  Definition  Pattern		    <td> \ref EA3DFRMDefinitionHoleType,<br> \ref EA3DFRMDefinitionPatternType								<td> Type of complex feature defining a hole (Counter sunk, Counter bore, ...)<br> \image html frm_specific_definition.png
<tr><td>Feature Definition									<td> \ref EA3DFRMFeatureDefinitionType																	<td> Store an information specific to a cad feature (hole, patter, extrude, definition)<br> \image html frm_feature_definition.png
<tr><td>Root												<td> \ref EA3DFRMRoot																					<td> Corresponds to cad node <br> \image html frm_root.png
</table>


@}
*/


/*!
\defgroup a3d_feature_type_module Type and Specification
\ingroup a3d_feature_structure_module
*/

/*!
\defgroup a3d_feature_link_module Connection
\ingroup a3d_feature_structure_module
*/

/*!
\addtogroup a3d_feature_link_module
@{
Feature connection indicates a reference and a product occurrence target in case of the reference is defined in another part definition.
In addition, a type specifies what is the link is used for.  Four categories of links are presents, see \ref EA3DFRMLinkType.
\image html frm_connections.png
<b> chamfer connections</b>
\image html frm_chamfer_link.png
@}
*/

/*!
\defgroup a3d_feature_data_module Data
\ingroup a3d_feature_structure_module
*/

/*!
\defgroup a3d_feature_data_root_module Tree information
\ingroup a3d_feature_data_module
*/


/*!
\addtogroup a3d_feature_data_root_module
@{

There are three kinds of features that define the model tree
\par

\li Feature Node (\ref kA3DFamily_Root - \ref kA3DFRMRoot_Node) <br>
This type of feature corresponds to a simple node in the tree, with the following structure
\image html frm_tree_node.png

\par
\li Feature Package (\ref kA3DFamily_Root - \ref kA3DFRMRoot_Package) <br>
This node will embed multiple feature definition, to define in one time several feature with the same cad file.
\image html frm_tree_package.png

<table  >
<tr><td> It's the case of multi-pad, with the possibility to have different depth size.	<td> \image html frm_tree_package_sample_multipad.png
<tr><td> Another example, a feature that define multiple hole at different positions. 	<td> \image html frm_tree_package_sample_hole.png
</table>
\par
\li  Feature Container (\ref kA3DFamily_Root - \ref kA3DFRMRoot_Container) <br>
This type of feature corresponds to a sub tree. Underneath, a parameter with type ref\kA3DParameterType_Container that regroup all sub nodes.
\image html frm_tree_cointainer.png
@}
*/


/*!
\defgroup a3d_feature_data_basic_module Basic
\ingroup a3d_feature_data_module
*/


/*!
\defgroup a3d_feature_data_value_module Value
\ingroup a3d_feature_data_module
*/
/*!
\defgroup a3d_feature_data_definition_module Definition
\ingroup a3d_feature_data_module
*/

/*!
\defgroup a3d_feature_data_specific_module Specific Definition
\ingroup a3d_feature_data_module
*/



/*!
\defgroup a3d_feature_hole_module Definition for hole
\ingroup a3d_feature_data_specific_module
\version 10.2
*/



/*!
\defgroup a3d_feature_pattern_module Definition for pattern
\ingroup a3d_feature_data_specific_module
\version 10.2
*/


/*!
\defgroup a3d_feature_extrude_module Definition for extrude
\ingroup a3d_feature_data_specific_module
\version 10.2
*/


/*!
\defgroup a3d_feature_revolve_module Definition for revolve
\ingroup a3d_feature_data_specific_module
\version 10.2
*/

/*!
\defgroup a3d_feature_fillet_module Definition for fillet
\ingroup a3d_feature_data_specific_module
\version 10.2
*/

/*!
\defgroup a3d_feature_chamfer_module Definition for chamfer
\ingroup a3d_feature_data_specific_module
\version 10.2
*/

/*!
\brief Feature tree data
Populates the \ref A3DFRMFeatureTreeData structure
\ingroup a3d_feature_tree_module
\version 10.2
*/

/*!
\struct A3DFRMFeatureTreeData
\brief Feature tree data
\ingroup a3d_feature_tree_module
\sa \ref A3DFRMFeatureTreeGet, \sa \ref A3DFRMGetSpecificNode
\version 10.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16						m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32						m_uiParametersSize;				/*!< Number of parameters. */
	A3DFRMParameter**				m_ppsParameters;				/*!< list of parameter, containers of sub features. */
	A3DUns32						m_uiIntermediateGeometriesSize;	/*!< Number of intermediate geometries. */
	A3DRiRepresentationItem**		m_ppIntermediateGeometries;		/*!< geometries using to define the feature that not part of the final body, like the support edge of a chamfer */
	A3DUns32						m_uiInternalGeometriesSize;		/*!< Number of internal geometries. */
	A3DRiRepresentationItem**		m_ppInternalGeometries;			/*!< geometries defined as internal reference for the feature as a sketch defining an extrude section. */
} A3DFRMFeatureTreeData;
#endif // A3DAPI_LOAD

/*!
\fnA3DFRMFeatureTreeGet
\brief Populates the \ref A3DFRMFeatureTreeData structure
\ingroup a3d_feature_tree_module
\version 10.2

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_ERROR \n
\return \ref A3D_INVALID_LICENSE\n
\return \ref A3D_SUCCESS.\n
*/
A3D_API(A3DStatus, A3DFRMFeatureTreeGet, (const A3DFRMFeatureTree* pTree, A3DFRMFeatureTreeData* pData));


/*!
\brief Feature parameter data structure
\struct A3DFRMParameterData
\ingroup a3d_feature_parameter_module
\brief Structure describes the feature type
\sa A3DFRMParameterGet
\version 10.2
*/

#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16				m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32				m_uiFeatureSize;	/*!< The size of \ref m_ppFeatures. */
	A3DFRMFeature**			m_ppFeatures;		/*!< Array of Features. */
	EA3DFRMParameterType	m_eType;			/*!< Feature parameter type. */
} A3DFRMParameterData;
#endif // A3DAPI_LOAD


/*!
\brief Populates the \ref A3DFRMParameterData structure
\ingroup a3d_feature_parameter_module
\version 10.2
*/
A3D_API(A3DStatus, A3DFRMParameterGet, (const A3DFRMParameter* pParameter, A3DFRMParameterData* pData));


/*!
\brief Structure describes the feature type
\struct A3DFRMFeatureTypeData
\ingroup a3d_feature_type_module
\sa A3DFRMFeatureTypeGet

\version 10.2
*/

#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16					m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	EA3DFRMFamily				m_eFamily;			/*!< Super type. */
	A3DUns32					m_uiType;			/*!< Sub type. */
	EA3DFRMStatus				m_eStatus;			/*!< Status. */
} A3DFRMFeatureTypeData;
#endif // A3DAPI_LOAD




/*!
\struct A3DFRMStringData
\brief data structure for strings values
\ingroup a3d_feature_data_enum_module
\version 10.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16		m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32		m_uiValuesSize;		/*!< String count. */
	A3DUTF8Char**		m_ppcValues;	/*!< Array of strings. */
} A3DFRMStringData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_feature_data_enum_module
\version 10.2
*/
A3D_API(A3DStatus, A3DFRMStringDataGet, (const A3DFRMFeature* pFeature, A3DFRMStringData* pData));

/*!
\struct A3DFRMDoubleData
\brief data structure for double values
\ingroup a3d_feature_data_enum_module
\version 10.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16		m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32		m_uiValuesSize;		/*!< Value count. */
	A3DDouble*			m_pdValues;		/*!< Array of doubles */
} A3DFRMDoubleData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_feature_data_enum_module
\version 10.2
*/
A3D_API(A3DStatus, A3DFRMDoubleDataGet, (const A3DFRMFeature* pFeature, A3DFRMDoubleData* pData));

/*!
\struct A3DFRMIntegerData
\brief data structure for integers values
\ingroup a3d_feature_data_enum_module
\version 10.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16		m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32		m_uiValuesSize;		/*!< Value count. */
	A3DInt32*			m_piValues;		/*!< Array of integers */
} A3DFRMIntegerData;
#endif // A3DAPI_LOAD

/*!
\fn A3DFRMIntegerDataGet
\ingroup a3d_feature_data_enum_module
\version 10.2
*/
A3D_API(A3DStatus, A3DFRMIntegerDataGet, (const A3DFRMFeature* pFeature, A3DFRMIntegerData* pData));

/*!
\ingroup a3d_feature_data_enum_module
\version 10.2
*/
A3D_API(A3DStatus, A3DFRMEnumDataGet, (const A3DFRMFeature* pFeature, A3DInt32* m_piEnumValue, A3DUTF8Char** ppcValueAsString));


/*!
\brief Feature Linked Item structure
\struct A3DFRMFeatureLinkedItemData
\ingroup a3d_feature_link_module
\version 10.2

*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16					m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	EA3DFRMLinkType				m_eType;						/*!< Type of Link. */
	A3DAsmProductOccurrence*	m_pTargetProductOccurrence;		/*!< If non-null, this member references a remote product occurrence that contains the reference. */
	A3DEntity*					m_pReference;					/*!< Pointer on the referenced entity. Only
																A3DRiRepresentationItem, A3DAsmProductOccurrence,
																A3DMiscReferenceOnTopology, A3DMiscReferenceOnCsysItem
																A3DFRMFeatFeature and A3DMkpMarkup
																are accepted */
} A3DFRMFeatureLinkedItemData;
#endif // A3DAPI_LOAD

/*!
brief Populates the \ref A3DFRMFeatureLinkedItemData structure
\ingroup a3d_feature_link_module
\version 10.2
*/
A3D_API(A3DStatus, A3DFRMFeatureLinkedItemGet, (const A3DFRMFeatureLinkedItem* p, A3DFRMFeatureLinkedItemData* pData));

/*!
\struct A3DFRMFeatureData
\ingroup a3d_feature_structure_module
\brief Structure for all feature information: type, data, connection, access to sub features
\sa A3DFRMFeatureGet
\version 10.2
*/

#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16										m_usStructSize;				// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DFRMFeatureTypeData							m_sType;					/*!< Feature type. */
	A3DUns32										m_uiParametersSize;			/*!< Number of parameters. */
	A3DFRMParameter**								m_ppParameters;				/*!< Array of parameters. */
	EA3DFRMDataType									m_eDataType;				/*!< Type of the data, if it's kA3DFRMDataNone, no data is directly associated to the feature;
																				else use \ref A3DFRMDoubleDataGet,  \ref A3DFRMIntegerDataGet, \ref A3DFRMEnumDataGet, or \ref A3DFRMFeatureGetEnumValueAsString
																				to access to the data according the type*/
	A3DUns32										m_uiConnectionSize;			/*!< Number of connections. */
	A3DFRMFeatureLinkedItem**						m_ppConnections;			/*!< Array of connections, to the geometry, to other feature, to pmi...*/
	A3DBool											m_bIsNode;					/*!< TRUE if the feature corresponds to a node in the cad tree */
	A3DBool											m_bIsSubTree;				/*!< TRUE if the feature corresponds to a sub tree in the cad tree */
} A3DFRMFeatureData;
#endif // A3DAPI_LOAD

/*!
brief Populates the \ref A3DFRMFeatureData structure
\ingroup a3d_feature_structure_module
\version 10.2

\param  pFeature feature pointer
\param  pData  feature data

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_ERROR \n
\return \ref A3D_SUCCESS.\n

*/
A3D_API(A3DStatus, A3DFRMFeatureGet, (const A3DFRMFeature* pFeature, A3DFRMFeatureData* pData));



/*!
\brief Get the  string associated to the feature type

\ingroup a3d_feature_type_module

\version 10.2

\param  pFeature feature pointer
\param  ppcFeatureType associated string

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_ERROR \n
\return \ref A3D_SUCCESS.\n
*/

A3D_API(A3DStatus, A3DFRMFeatureGetTypeAsString, (const A3DFRMFeature* pFeature, A3DUTF8Char** ppcFeatureType));

/*!
\brief Build a List of Features corresponding to a node with specific CAD type.

\ingroup a3d_feature_tree_module

\version 10.2

\param  eCADType cad type of feature (Hole, Pattern, ...)
\param  piSize number with the type specified
\param  ppFeatureNodes features found with the specific cad type (Hole, Pattern, ...)

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_ERROR \n
\return \ref A3D_SUCCESS.\n
*/
A3D_API(A3DStatus, A3DFRMGetSpecificNodes, (const A3DFRMFeatureTree* pTree, EA3DFRMEnumValue_CadType eCADType, A3DUns32* piSize, A3DFRMFeature*** pppFeatureNodes));

/*!
\brief Get all feature trees defined in the part definition.
\deprecated This function is deprecated. Please use the A3DFRMFeatureTree who are in the A3DAsmProductOccurrenceData. version 12.
\ingroup a3d_feature_tree_module

\version 10.2

\param  pPartDefinition part definition to query
\param  piSize number of feature trees contained
\param  pppFeatureTrees feature tree array

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_ERROR \n
\return \ref A3D_INVALID_LICENSE\n
\return \ref A3D_SUCCESS.\n
*/
A3D_API(A3DStatus, A3DAsmPartDefinitionFeatureTreesGet, (const A3DAsmPartDefinition* pPartDefinition, A3DUns32* puiSize, A3DFRMFeatureTree*** pppFeatureTrees));

/*!
\struct A3DFRMFeatureGeomEntitiesData
\brief data structure for representation items extracted from features
\ingroup a3d_feature_structure_module
\version 12.2
\sa A3DFRMFeatureGeomEntitiesExtract
*/

#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16					m_usStructSize;				/*!< Reserved; must be initialized with \ref A3D_INITIALIZE_DATA. */
	A3DUns32					m_uiGeomEntitiesSize;		/*!< Size of \ref m_ppGeomEntities */
	A3DRiRepresentationItem**	m_ppGeomEntities;			/*!< Array of extracted geometries */
} A3DFRMFeatureGeomEntitiesData;
#endif // A3DAPI_LOAD

/*!
\brief Create on the fly geometries related to the feature
\ingroup a3d_feature_structure_module

\version 12.2

\param  pFeature feature pointer
\param  pFeatureGeomEntities struct containing the array of extracted geometries

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_ERROR \n
\return \ref A3D_SUCCESS.\n
*/
A3D_API(A3DStatus, A3DFRMFeatureGeomEntitiesExtract, (const A3DFRMFeature* pFeature, A3DFRMFeatureGeomEntitiesData* pFeatureGeomEntities));

#endif	/*	__A3DPRCFEATURES_H__ */
