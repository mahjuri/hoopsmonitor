/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      A3D SDK Enumerations and static values
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifndef __A3DSDKENUMS_H__
#define __A3DSDKENUMS_H__

#include <A3DSDKTypes.h>

////////////////////////////////////////////////////////////////////////////////
/// A3DSDKTexture
////////////////////////////////////////////////////////////////////////////////

/*!
\defgroup a3d_texturemappingoperator Texture Mapping Operator
\ingroup a3dtexture_definition
@{
*/
/*!
\ingroup a3d_texturemappingoperator
\brief Reserved for future use
\version 2.0

Defines the operator to use for computing mapping coordinates.
The calculated mapping coordinates must be stored on an \ref A3DTess3D object
*/
typedef enum
{
  kA3DTextureMappingOperatorUnknown,		/*!< Default value. */
  kA3DTextureMappingOperatorPlanar,		/*!< Unused. Reserved for future use. */
  kA3DTextureMappingOperatorCylindrical,	/*!< Unused. Reserved for future use. */
  kA3DTextureMappingOperatorSpherical,	/*!< Unused. Reserved for future use. */
  kA3DTextureMappingOperatorCubical		/*!< Unused. Reserved for future use. */
} A3DETextureMappingOperator;
/*!
@} <!-- end of module a3d_texturemappingoperator -->
*/

/*!
\defgroup a3d_texturemapping Texture Mapping Type
\ingroup a3dtexture_definition
@{
*/
/*!
\brief Defines how to get mapping coordinates.
\version 2.0

The \ref kA3DTextureMappingTypeStored enumeration
is used with mapping coordinates stored on \ref A3DTess3D object.
Other values are reserved for future use.
*/
typedef enum
{
  kA3DTextureMappingTypeUnknown,		/*!< Let the application choose. */
  kA3DTextureMappingTypeStored,			/*!< Mapping coordinates are stored. */
  kA3DTextureMappingTypeParametric,	/*!< Get the UV coordinate on the surface as mapping coordinates (reserved for future use). */
  kA3DTextureMappingTypeOperator		/*!< Use the defined \ref a3d_texturemappingoperator to calculate mapping coordinates (reserved for future use). */
} A3DETextureMappingType;
/*!
@} <!-- end of module a3d_texturemapping -->
*/

/*!
\defgroup a3d_texturefunction Texture Function
\ingroup a3dtexture_definition
@{
*/
/*!
\brief Defines how to paint a texture on the surface being rendered.
\version 2.0
*/
typedef enum
{
  kA3DTextureFunctionUnknown,	/*!< Let the application choose. */
  kA3DTextureFunctionModulate,	/*!< Default value. Combines lighting with texturing. */
  kA3DTextureFunctionReplace,	/*!< Replace object color by texture color data. */
  kA3DTextureFunctionBlend,		/*!< Unused. Reserved for future use. */
  kA3DTextureFunctionDecal		/*!< Unused. Reserved for future use. */
} A3DETextureFunction;
/*!
@} <!-- end of module a3d_texturefunction -->
*/

/*!
\defgroup a3d_texturemappingattribute Texture Mapping Attribute
\ingroup a3dtexture_definition
\brief Defines texture-mapping methods
\version 2.0

\ref kA3DTextureMappingDiffuse single value is used as default attribute.
\ref kA3DTextureMappingSphericalReflection and \ref kA3DTextureMappingCubicalReflection indicate that single values
are used as environment mapping.
Other values and combined values are reserved for future use.
@{*/
#define kA3DTextureMappingDiffuse             0x0001 /*!< Diffuse texture mapping attribute. Default value. */
#define kA3DTextureMappingBump                0x0002 /*!< Bump texture mapping attribute. Not yet supported. */
#define kA3DTextureMappingOpacity             0x0004 /*!< Opacity texture mapping attribute. Not yet supported. */
#define kA3DTextureMappingSphericalReflection 0x0008 /*!< Spherical reflection texture mapping attribute (used for environment mapping). */
#define kA3DTextureMappingCubicalReflection   0x0010 /*!< Cubical reflection texture mapping attribute (used for environment mapping). */
#define kA3DTextureMappingRefraction          0x0020 /*!< Refraction texture mapping attribute. Not yet supported. */
#define kA3DTextureMappingSpecular            0x0040 /*!< Specular texture mapping attribute. Not yet supported. */
#define kA3DTextureMappingAmbient             0x0080 /*!< Ambient texture mapping attribute. Not yet supported. */
#define kA3DTextureMappingEmission            0x0100 /*!< Emission texture mapping attribute. Not yet supported. */
#define kA3DTextureMappingNormal              0x0200 /*!< Normal texture mapping attribute. \version 11.2 */
#define kA3DTextureMappingMetallness          0x0400 /*!< Metallness texture mapping attribute (used in Physically-Based Rendering). \version 11.2 */
#define kA3DTextureMappingRoughness           0x0800 /*!< Roughness mapping attribute (used in Physically-Based Rendering). \version 11.2 */
#define kA3DTextureMappingOcclusion           0x1000 /*!< Occlusion mapping attribute. \version 11.2 */

#define kA3DTextureMappingMetallnessRoughness          kA3DTextureMappingMetallness | kA3DTextureMappingRoughness          /*!< Packed metallness-roughness mapping attribute. \version 11.2 */
#define kA3DTextureMappingMetallnessRoughnessOcclusion kA3DTextureMappingMetallnessRoughness | kA3DTextureMappingOcclusion /*!< Packed metallness-roughness-occlusion mapping attribute. \version 11.2 */

/*!
@} <!-- end of module a3d_texturemappingattribute -->
*/

/*!
\defgroup a3d_textureapplyingmode Texture Applying Mode
\ingroup a3dtexture_definition
\brief Defines special modes for applying textures.
\version 2.0

The values here define special modes for applying a texture.
@{*/
#define kA3DTextureApplyingModeNone        0x0000 /*!< All states are disabled. */
#define kA3DTextureApplyingModeLighting    0x0001 /*!< Lighting Enabled. */
#define kA3DTextureApplyingModeAlphaTest   0x0002 /*!< Alpha Test Enabled. */
#define kA3DTextureApplyingModeVertexColor 0x0004 /*!< Use Vertex Color (combine a texture with one-color-per-vertex mode). */
/*!
@} <!-- end of module a3d_textureapplyingmode -->
*/

/*!
\defgroup a3d_textureblendparameter Texture Blend Parameter
\ingroup a3dtexture_definition
@{
*/
/*!
\brief Reserved for future use.
\version 2.0

Defines how to apply blending.
*/
typedef enum
{
  kA3DTextureBlendParameterUnknown,				/*!< Default value. */
  kA3DTextureBlendParameterZero,					/*!< Unused. Reserved for future use. */
  kA3DTextureBlendParameterOne,						/*!< Unused. Reserved for future use. */
  kA3DTextureBlendParameterSrcColor,				/*!< Unused. Reserved for future use. */
  kA3DTextureBlendParameterOneMinusSrcColor,	/*!< Unused. Reserved for future use. */
  kA3DTextureBlendParameterDstColor,				/*!< Unused. Reserved for future use. */
  kA3DTextureBlendParameterOneMinusDstColor,	/*!< Unused. Reserved for future use. */
  kA3DTextureBlendParameterSrcAlpha,				/*!< Unused. Reserved for future use. */
  kA3DTextureBlendParameterOneMinusSrcAlpha,	/*!< Unused. Reserved for future use. */
  kA3DTextureBlendParameterDstAlpha,				/*!< Unused. Reserved for future use. */
  kA3DTextureBlendParameterOneMinusDstAlpha,	/*!< Unused. Reserved for future use. */
  kA3DTextureBlendParameterSrcAlphaSaturate		/*!< Unused. Reserved for future use. */
} A3DETextureBlendParameter;
/*!
@} <!-- end of module a3d_textureblendparameter -->
*/

/*!
\defgroup a3d_texturealphamode Texture Alpha Mode Parameter
\ingroup a3dtexture_definition
@{
*/
/*!
\version 12.0

Defines how to interpret the alpha value of a texture.
The alpha mode and cut-off values are specified as A3DGraphMaterialData additional attributes.
The alpha cut-off applies only to the A3DETextureAlphaModeMask mode.
*/
typedef enum
{
  A3DETextureAlphaModeNone,   /*!< Default value. */
  A3DETextureAlphaModeOpaque, /*!< The alpha value is ignored and the rendered output is fully opaque. */
  A3DETextureAlphaModeMask,   /*!< The rendered output is either fully opaque or fully transparent depending on the alpha value and the specified alpha cutoff value. */
  A3DETextureAlphaModeBlend,  /*!< The alpha value is used to composite the source and destination areas. */
} A3DETextureAlphaMode;
/*!
@} <!-- end of module a3d_texturealphamode -->
*/

/*!
\defgroup a3d_texture_mapping_attributes_components Texture Mapping Attribute Components
\ingroup a3dtexture_definition
@{
*/
/*!
\brief Defines which component(s) must be used to map a texture
These values can be bitwise combined to select several components at once.
\version 2.0
*/
#define kA3DTextureMappingComponentsRed		  0x0001 /*!< Red component. */
#define kA3DTextureMappingComponentsGreen		0x0002 /*!< Green component. */
#define kA3DTextureMappingComponentsBlue		0x0004 /*!< Blue component. */
#define kA3DTextureMappingComponentsRgb		  0x0007 /*!< Full Red-Green-Blue components. */
#define kA3DTextureMappingComponentsAlpha		0x0008 /*!< Alpha component. */
#define kA3DTextureMappingComponentsRgba		0x000f /*!< Full Red-Green-Blue-Alpha components. */
/*!
@} <!-- end of module a3d_texture_mapping_attributes_components -->
*/

/*!
\defgroup a3d_texturealphatest Texture Alpha Test
\ingroup a3dtexture_definition
@{
*/
/*!
\brief Reserved for future use
\version 2.0

Defines how to use Alpha test
*/
typedef enum
{
  kA3DTextureAlphaTestUnknown,		/*!< Default value. */
  kA3DTextureAlphaTestNever,			/*!< Unused. Reserved for future use. */
  kA3DTextureAlphaTestLess,			/*!< Unused. Reserved for future use. */
  kA3DTextureAlphaTestEqual,			/*!< Unused. Reserved for future use. */
  kA3DTextureAlphaTestLequal,		/*!< Unused. Reserved for future use. */
  kA3DTextureAlphaTestGreater,		/*!< Unused. Reserved for future use. */
  kA3DTextureAlphaTestNotequal,		/*!< Unused. Reserved for future use. */
  kA3DTextureAlphaTestGequal,		/*!< Unused. Reserved for future use. */
  kA3DTextureAlphaTestAlways			/*!< Unused. Reserved for future use. */
} A3DETextureAlphaTest;
/*!
@} <!-- end of module a3d_texturealphatest -->
*/

/*!
\defgroup a3d_texturewrappingmode Texture Wrapping Mode
\ingroup a3dtexture_definition
@{
*/
/*!
\brief Defines repeating and clamping texture modes
\version 2.0
*/
typedef enum
{
  kA3DTextureWrappingModeUnknown,			/*!< Let the application choose. */
  kA3DTextureWrappingModeRepeat,			/*!< Display repeated texture on the surface. */
  kA3DTextureWrappingModeClampToBorder,	/*!< Clamp texture to border. Display surface color over texture limits. */
  kA3DTextureWrappingModeClamp,				/*!< Unused. Reserved for future use. */
  kA3DTextureWrappingModeClampToEdge,		/*!< Unused. Reserved for future use. */
  kA3DTextureWrappingModeMirroredRepeat	/*!< Unused. Reserved for future use. */
} A3DETextureWrappingMode;
/*!
@} <!-- end of module a3d_texturewrappingmode -->
*/




////////////////////////////////////////////////////////////////////////////////
/// A3DSDKStructure
////////////////////////////////////////////////////////////////////////////////

/*!
\brief Modeller type
\ingroup a3d_modelfile
\version 2.1
*/
typedef enum
{
  kA3DModellerUnknown = 0,              /*!< User modeller. */
  kA3DModellerCatia = 2,                /*!< CATIA modeller. */
  kA3DModellerCatiaV5 = 3,              /*!< CATIA V5 modeller. */
  kA3DModellerCadds = 4,                /*!< CADDS modeller. */
  kA3DModellerUnigraphics = 5,          /*!< Unigraphics modeller. */
  kA3DModellerParasolid = 6,            /*!< Parasolid modeller. */
  kA3DModellerEuclid = 7,               /*!< Euclid modeller. */
  kA3DModellerIges = 9,                 /*!< IGES modeller. */
  kA3DModellerUnisurf = 10,             /*!< Unisurf modeller. */
  kA3DModellerVda = 11,                 /*!< VDA modeller. */
  kA3DModellerStl = 12,                 /*!< STL modeller. */
  kA3DModellerWrl = 13,                 /*!< WRL modeller. */
  kA3DModellerDxf = 14,                 /*!< DXF modeller. */
  kA3DModellerAcis = 15,                /*!< ACIS modeller. */
  kA3DModellerProE = 16,                /*!< Pro/E modeller. */
  kA3DModellerStep = 18,                /*!< STEP modeller. */
  kA3DModellerIdeas = 19,               /*!< I-DEAS modeller. */
  kA3DModellerJt = 20,                  /*!< JT modeller. */
  kA3DModellerSlw = 22,                 /*!< SolidWorks modeller. */
  kA3DModellerCgr = 23,                 /*!< CGR modeller. */
  kA3DModellerPrc = 24,                 /*!< PRC modeller. */
  kA3DModellerXvl = 25,                 /*!< XVL modeller. */
  kA3DModellerHpgl = 26,                /*!< HPGL modeller. */
  kA3DModellerTopSolid = 27,            /*!< TopSolid modeller. */
  kA3DModellerOneSpaceDesigner = 28,    /*!< OneSpace designer modeller. */
  kA3DModeller3dxml = 29,               /*!< 3DXML modeller. */
  kA3DModellerInventor = 30,            /*!< Inventor modeller. */
  kA3DModellerPostScript = 31,          /*!< Postscript modeller. */
  kA3DModellerPDF = 32,                 /*!< PDF modeller. */
  kA3DModellerU3D = 33,                 /*!< U3D modeller. */
  kA3DModellerIFC = 34,                 /*!< IFC modeller. */
  kA3DModellerDWG = 35,                 /*!< DWG modeller. */
  kA3DModellerDWF = 36,                 /*!< DWF modeller. */
  kA3DModellerSE = 37,                  /*!< SolidEdge modeller. */
  kA3DModellerOBJ = 38,                 /*!< OBJ modeller. */
  kA3DModellerKMZ = 39,                 /*!< KMZ modeller. */
  kA3DModellerDAE = 40,                 /*!< COLLADA modeller. */
  kA3DModeller3DS = 41,                 /*!< 3DS modeller. */
  kA3DModellerRhino = 43,               /*!< Rhino modeller. */
  kA3DModellerXML = 44,                 /*!< XML modeller. */
  kA3DModeller3mf = 45,                 /*!< 3MF modeller. */
  kA3DModellerScs = 46,                 /*!< SCS modeller. */
  kA3DModeller3dHtml = 47,              /*!< 3DHTML modeller. */
  kA3DModellerHsf = 48,                 /*!< Hsf modeller. */
  kA3DModellerGltf = 49,                /*!< GL modeller. */
  kA3DModellerRevit = 50,               /*!< Revit modeller. */
  kA3DModellerFBX = 51,                 /*!< FBX modeller. */
  kA3DModellerStepXML = 52,             /*!< StepXML modeller. */
  kA3DModellerPLMXML = 53,              /*!< PLMXML modeller. */
  kA3DModellerLast
} A3DEModellerType;

/*!
\brief Load status of the PRC model file
\ingroup a3d_productoccurrence
\version 2.1
*/
typedef enum
{
  kA3DProductLoadStatusUnknown = 0,     /*!< Unknown status. */
  kA3DProductLoadStatusError,           /*!< Loading error. For example, there is a missing file. */
  kA3DProductLoadStatusNotLoaded,       /*!< Not loaded. */
  kA3DProductLoadStatusNotLoadable,     /*!< Not loadable. For example, limitations exist that prevent the product from loading. */
  kA3DProductLoadStatusLoaded           /*!< The product was successfully loaded. */
} A3DEProductLoadStatus;


/*!
\defgroup a3d_product_flag Bit field flag definitions for product occurrences
\ingroup a3d_productoccurrence
@{

The product flags are used to define the specific usages of a product occurrence.

They are set in \ref A3DAsmProductOccurrenceData::m_uiProductFlags.
This field can be a combinaison of more than one flag if they describe several
usages at once.
Such usages can be:

### Configuration

A configuration is a specific arrangement of a product with respect to its whole
hierarchy.

With SolidWorks the model data inside the configuration node may be missing.
In that case the flag `A3D_PRODUCT_FLAG_CONFIG_NOT_UPDATED` is also set.

### Container

A container product occurrence acts as a repository of product occurrences that
are not necessarily related to each other.
This is useful for situations where a single CAD file corresponds to a whole
database of parts and assemblies.

### Default

When a product occurrence is marked as default, it means the originating CAD
environment loads it by default.
This flag is applicatble with containers, configurations and view.

### Internal

When a product occurrence is a prototype with no father it is flagged as
internal. It then represents a sub-assembly root or a part root inside the main
assembly.

### View

A view refers to its prototype to denote a particular setting of visibilities
and positions.

### Suppressed

When a product occurrence is marked as suppressed its children are not loaded.

In case of CREO files, this flag is set when the Family Table isn't used or
when the geometry contains boolean operations that are not supported by
HOOPS Exchange.


### External Reference

External Reference is a specific flag currently used for STEP/XML. It this format
when a node initialy refers to an external part it is flagged as External Reference.
For more information, see [the STEP/XML reader page](stepxml_reader.html).

If none of these flags is specified, a product occurrence is regular. If the product occurrence has no
father, it is similar to a configuration.

\warning The flags of a product occurence may contains values that do
not correspond to any documented flags. They correspond to internal values that
shouldn't be used.

\version 2.1

*/
#define A3D_PRODUCT_FLAG_DEFAULT            0x0001 /*!< The product occurrence is the default container, view or configuration. */
#define A3D_PRODUCT_FLAG_INTERNAL           0x0002 /*!< The product occurrence is internal. */
#define A3D_PRODUCT_FLAG_CONTAINER          0x0004 /*!< The product occurrence is a container. */
#define A3D_PRODUCT_FLAG_CONFIG             0x0008 /*!< The product occurrence is a configuration. */
#define A3D_PRODUCT_FLAG_VIEW               0x0010 /*!< The product occurrence is a view. */
#define A3D_PRODUCT_FLAG_SUPPRESSED         0x0040 /*!< The product occurrence is suppressed. */
#define A3D_PRODUCT_FLAG_CONFIG_NOT_UPDATED 0x0100 /*!< The config does not include the model data */
#define A3D_PRODUCT_FLAG_EXTERNAL_REFERENCE 0x2000 /*!< The product occurrence is a external reference. */
/*!
@}
*/

/*!
\brief \ref A3DAsmProductOccurrenceData extension for Catia V4
\ingroup a3d_productoccurrence
\version 8.2
*/
typedef enum
{
  A3DEProductOccurrenceTypeCat_unknown = -1, /*!< Unknwon Catia V4 type */
  A3DEProductOccurrenceTypeCat_model = 0,    /*!< Catia V4 Model */
  A3DEProductOccurrenceTypeCat_sessionModel, /*!< Catia V4 Session Model */
  A3DEProductOccurrenceTypeCat_session,      /*!< Catia V4 Session */
  A3DEProductOccurrenceTypeCat_export        /*!< Catia V4 Export */

} A3DEProductOccurrenceTypeCat;

/*!
\brief \ref A3DAsmProductOccurrenceData extension for Inventor
\ingroup a3d_productoccurrence
\version 8.2
*/
typedef enum
{
  A3DEProductOccurrenceTypeInv_none,    /*!< Unknown Inventor type */
  A3DEProductOccurrenceTypeInv_iam,     /*!< Inventor assembly */
  A3DEProductOccurrenceTypeInv_ipt      /*!< Inventor Part */
} A3DEProductOccurrenceTypeInv;

/*!
\brief \ref A3DAsmProductOccurrenceData extension for SolidWorks
\ingroup a3d_productoccurrence
\version 8.2
*/
typedef enum
{
  A3DEProductOccurrenceTypeSLW_ContainerTess = -2,    /*!< SolidWorks Container with tessellation */
  A3DEProductOccurrenceTypeSLW_Container = -1,        /*!< SolidWorks Container */
  A3DEProductOccurrenceTypeSLW_Part = 0,              /*!< SolidWorks Part  */
  A3DEProductOccurrenceTypeSLW_Assembly,              /*!< SolidWorks Assembly */
  A3DEProductOccurrenceTypeSLW_Drawing,               /*!< SolidWorks 2D Drawing */
  A3DEProductOccurrenceTypeSLW_Unknown,               /*!< Unknown Solidworkd type */
  A3DEProductOccurrenceTypeSLW_PartTesselated,        /*!< SolidWorks Part with tessellation */
  A3DEProductOccurrenceTypeSLW_AssemblyTesselated,    /*!< SolidWorks Assembly with tessellation */
  A3DEProductOccurrenceTypeSLW_StandalonePart         /*!< SolidWorks Stand-alone part */
} A3DEProductOccurrenceTypeSLW;


////////////////////////////////////////////////////////////////////////////////
/// A3DSDKRootEntities
////////////////////////////////////////////////////////////////////////////////


/*!
\brief An enumeration that identifies the type of modeller data represented in an \ref A3DMiscSingleAttributeData structure
\ingroup a3d_attribute
*/
typedef enum
{
  kA3DModellerAttributeTypeNull   = 0,  /*!< Null type; invalid. */
  kA3DModellerAttributeTypeInt    = 1,  /*!< 32-bit unsigned Integer (A3DUns32). */
  kA3DModellerAttributeTypeReal   = 2,  /*!< 32-bit floating point value (A3DFloat). */
  kA3DModellerAttributeTypeTime   = 3,  /*!< 32-bit unsigned Integer, interpreted as \c time_t. */
  kA3DModellerAttributeTypeString = 4   /*!< UTF-8 character c-string (A3DUTF8Char). */
} A3DEModellerAttributeType;


////////////////////////////////////////////////////////////////////////////////
/// A3DSDKReadWrite
////////////////////////////////////////////////////////////////////////////////

/*!
\ingroup a3d_read
\brief ReadingMode of the model file. Set which kind of content should be read.
\version 3.0
*/
typedef enum
{
  kA3DReadGeomOnly = 0, /*!< In this mode, Exchange will avoid reading or generating tessellation on B-rep. Faceted elements from the native file will still be imported.*/
  kA3DReadGeomAndTess,  /*!< Mixed mode: In this mode, the B-rep and faceted elements are read from the native file. Tessellation is then generated from B-rep elements. */
  kA3DReadTessOnly      /*!< In this mode, all faceted elements from the native file are read. If present, the native tessellation of the B-rep is read exclusively. Native tessellation is the display list integrated in the CAD file (only available for CATIA V5, Solidworks, 3DXML, and Inventor). \n\n There are some side effects: The tessellation may be not up-to-date with the geometry, the assembly tree can be different, and hidden objects may be missed. \n\n If an element contains only B-rep, tessellation is generated from the B-rep and subsequently the B-rep is removed to save memory. */
} A3DEReadGeomTessMode;

/*!
\ingroup a3d_write
\brief WritingMode of the model file. Set which kind of content should be written.
\version 6.0
*/
typedef enum
{
  kA3DWriteGeomOnly = 0, /*!< Write only geometry. */
  kA3DWriteGeomAndTess,  /*!< Mixed mode: write geometry and tessellation. */
  kA3DWriteTessOnly      /*!< Write only tessellation. */
} A3DEWriteGeomTessMode;

/*!
\ingroup a3d_read
\brief Unit used in the model file.
\version 3.0
*/
typedef enum
{
  kA3DUnitPoint = 0,      /*!< Point. */
  kA3DUnitInch,           /*!< Inch. */
  kA3DUnitMillimeter,     /*!< Millimeter. */
  kA3DUnitCentimeter,     /*!< Centimeter. */
  kA3DUnitPicas,          /*!< Picas. */
  kA3DUnitFoot,           /*!< Foot. */
  kA3DUnitYard,           /*!< Yard. */
  kA3DUnitMeter,          /*!< Meter. */
  kA3DUnitKilometer,      /*!< Kilometer. */
  kA3DUnitMile,           /*!< Mile. */
  kA3DUnitUnknown         /*!< Unknown. */
} A3DEUnits;

/*!
\defgroup a3d_tessfacetype Bitmasks for Specifying Tessellation Types
\ingroup a3d_tessface
\brief Bitmasks for specifying a type of tessellation face data.

These flags controls the way the vertex coordinates are indexed in a
\ref A3DTessFaceData instance. It's specified in the
\ref A3DTessFaceData::m_usUsedEntitiesFlags member.

\version 2.0
@{
*/
/*!
\brief Reserved for future use
*/
#define kA3DTessFaceDataPolyface        0x0001

/*!
\brief Unique triangle

One triangle described using `6` indexes forming `3` `{Normal, Point} pairs:
    { Normal1, Point1, Normal2, Point2, Normal3, Point3 }
*/
#define kA3DTessFaceDataTriangle							0x0002

/*!
\brief Triangle fan

The layout consists in `2*N` indices where `N` is the number of points.
Each vertex is described as a `{Normal, Point}` pair.

    { Normal1, Point1, Normal2, Point2, ..., ..., NormalN, PointN }
*/
#define kA3DTessFaceDataTriangleFan							0x0004

/*!
\brief Triangle strip

The layout consists in `2*N` indices where `N` is the number of points.
Each vertex is described as a `{Normal, Point}` pair.

    { Normal1, Point1, Normal2, Point2, ..., ..., NormalN, PointN }
*/
#define kA3DTessFaceDataTriangleStripe						0x0008

/*!
\brief Reserved for future use
*/
#define kA3DTessFaceDataPolyfaceOneNormal					0x0010

/*!
\brief Unique triangle with one normal.

Unique triangle with one normal. It is described using 4 indices:

    { Normal, Point1, Point2, Point3 }
*/
#define kA3DTessFaceDataTriangleOneNormal					0x0020

/*!
\brief Triangle fan where the normal is defined either globally or per-point.

The normal is globally defined if \ref kA3DTessFaceDataNormalSingle flag is set.
In that case the layout consists in `N+1` indices, where N is the number of
points:

    { Normal, Point1, Point2, ..., PointN }

If not set, the normal indices are set per-point. The layout is the same as
\ref kA3DTessFaceDataTriangleFan.
*/
#define kA3DTessFaceDataTriangleFanOneNormal				0x0040

/*!
\brief Triangle strip where the normal is defined either globally or per-point.

The normal is globally defined if \ref kA3DTessFaceDataNormalSingle flag is set.
In that case the layout consists in `N+1` indices, where N is the number of
points:

    { Normal, Point1, Point2, ..., PointN }

If not set, the normal indices are set per-point. The layout is the same as
\ref kA3DTessFaceDataTriangleFan.
*/
#define kA3DTessFaceDataTriangleStripeOneNormal				0x0080

/*!
\brief Reserved for future use
*/
#define kA3DTessFaceDataPolyfaceTextured					0x0100

/*!
\brief Unique triangle with texture coordinates.

This layout is similar to \ref kA3DTessFaceDataTriangle with the addition of
texture coordinates indices before the point index.
Each point is then described using the following layout:
`{Normal, Textures..., Point}`, where `Textures...` is the list of texture
indices.

The number of texture indices per point is given by
\ref A3DTessFaceData::m_uiTextureCoordIndexesSize. Thus the number of indices
in the element array is equal to `6 + 3 * m_uiTextureCoordIndexesSize`.

For example, if there are two texture
indices per point, the layout will be a 12 wide array:

    {
      Normal1, Texture1-1, Texture1-2, Point1,
      Normal2, Texture2-1, Texture2-2, Point2,
      Normal3, Texture3-1, Texture3-2, Point3,
    }

*/
#define kA3DTessFaceDataTriangleTextured					0x0200

/*!
\brief Triangle fan  with texture coordinates.

This layout is similar to \ref kA3DTessFaceDataTriangleFan with the addition of
texture coordinates indices before the point index.
Each point is then described using the following layout:
`{Normal, Textures..., Point}`, where `Textures...` is the list of texture
indices.

The number of texture indices per point is given by
\ref A3DTessFaceData::m_uiTextureCoordIndexesSize. Thus the number of indices
in the element array is equal to `N * (m_uiTextureCoordIndexesSize + 2)` where
`N` is the number of points.

For example, if there are two texture
indices per point and two triangles (6 points), the layout will be a 24 wide array:

    {
      Normal1, Texture1-1, Texture1-2, Point1,
      Normal2, Texture2-1, Texture2-2, Point2,
      Normal3, Texture3-1, Texture3-2, Point3,
      Normal4, Texture4-1, Texture4-2, Point4,
      Normal5, Texture5-1, Texture5-2, Point5,
      Normal6, Texture6-1, Texture6-2, Point6
    }

*/
#define kA3DTessFaceDataTriangleFanTextured					0x0400

/*!
\brief Triangle strip  with texture coordinates.

This layout is similar to \ref kA3DTessFaceDataTriangleStripe with the addition of
texture coordinates indices before the point index.
Each point is then described using the following layout:
`{Normal, Textures..., Point}`, where `Textures...` is the list of texture
indices.

The number of texture indices per point is given by
\ref A3DTessFaceData::m_uiTextureCoordIndexesSize. Thus the number of indices
in the element array is equal to `N * (m_uiTextureCoordIndexesSize + 2)` where
`N` is the number of points.

For example, if there are two texture
indices per point and two triangles (6 points), the layout will be a 24 wide array:

    {
      Normal1, Texture1-1, Texture1-2, Point1,
      Normal2, Texture2-1, Texture2-2, Point2,
      Normal3, Texture3-1, Texture3-2, Point3,
      Normal4, Texture4-1, Texture4-2, Point4,
      Normal5, Texture5-1, Texture5-2, Point5,
      Normal6, Texture6-1, Texture6-2, Point6
    }

*/
#define kA3DTessFaceDataTriangleStripeTextured				0x0800

/*!
\brief Reserved for future use
*/
#define kA3DTessFaceDataPolyfaceOneNormalTextured			0x1000

/*!
\brief Unique triangle with texture coordinates and a globally defined normal.

This layout is similar to \ref kA3DTessFaceDataTriangleOneNormal with the addition of
texture coordinates indices before the point index.
The unique normal is defined first.
Each point is then described using the following layout:
`{Textures..., Point}`, where `Textures...` is the list of texture
indices.

The number of texture indices per point is given by
\ref A3DTessFaceData::m_uiTextureCoordIndexesSize. Thus the number of indices
in the element array is equal to `4 + 3 * m_uiTextureCoordIndexesSize`.

For example, if there are two texture
indices per point, the layout will be a 12 wide array:

    {
      Normal
      Texture1-1, Texture1-2, Point1,
      Texture2-1, Texture2-2, Point2,
      Texture3-1, Texture3-2, Point3,
    }
*/
#define kA3DTessFaceDataTriangleOneNormalTextured			0x2000

/*!
\brief Triangle fan with texture coordinates where the normal is defined
either globally or per-point.

This layout is similar to \ref kA3DTessFaceDataTriangleFanOneNormal with the
addition of texture coordinates indices before the point index.

The normal is globally defined if \ref kA3DTessFaceDataNormalSingle flag is set.
In that case the unique normal is defined first.
Each point is then described using the following layout:
`{Textures..., Point}`, where `Textures...` is the list of texture
indices.

The number of texture indices per point is given by
\ref A3DTessFaceData::m_uiTextureCoordIndexesSize. Thus the number of indices
in the element array is equal to `N * (m_uiTextureCoordIndexesSize + 1)` where
`N` is the number of points.

For example, if there are two texture
indices per point and two triangles (6 points), the layout will be a 24 wide array:

    {
      Normal
      Texture1-1, Texture1-2, Point1,
      Texture2-1, Texture2-2, Point2,
      Texture3-1, Texture3-2, Point3,
      Texture4-1, Texture4-2, Point4,
      Texture5-1, Texture5-2, Point5,
      Texture6-1, Texture6-2, Point6
    }

If \ref kA3DTessFaceDataNormalSingle is not set, the layout is the same as
\ref kA3DTessFaceDataTriangleFanTextured.
*/
#define kA3DTessFaceDataTriangleFanOneNormalTextured		0x4000


/*!
\brief Triangle strip  with texture coordinates where the normal is defined
either globally or per-point.

This layout is similar to \ref kA3DTessFaceDataTriangleStripeOneNormal with the
addition of texture coordinates indices before the point index.

The normal is globally defined if \ref kA3DTessFaceDataNormalSingle flag is set.
In that case the unique normal is defined first.
Each point is then described using the following layout:
`{Textures..., Point}`, where `Textures...` is the list of texture
indices.

The number of texture indices per point is given by
\ref A3DTessFaceData::m_uiTextureCoordIndexesSize. Thus the number of indices
in the element array is equal to `N * (m_uiTextureCoordIndexesSize + 1)` where
`N` is the number of points.

For example, if there are two texture
indices per point and two triangles (6 points), the layout will be a 24 wide array:

    {
      Normal
      Texture1-1, Texture1-2, Point1,
      Texture2-1, Texture2-2, Point2,
      Texture3-1, Texture3-2, Point3,
      Texture4-1, Texture4-2, Point4,
      Texture5-1, Texture5-2, Point5,
      Texture6-1, Texture6-2, Point6
    }

If \ref kA3DTessFaceDataNormalSingle is not set, the layout is the same as
\ref kA3DTessFaceDataTriangleStripeTextured.
*/
#define kA3DTessFaceDataTriangleStripeOneNormalTextured		0x8000

/*!
@} <!-- end of a3d_tessfacetype -->
*/

/*!
\defgroup a3d_tessfaceflags Bitmasks for Loop Characteristics in Face Loops
\ingroup a3d_tessface
\brief Bitmasks for specifying loop characteristics of face wires in the \ref A3DTessFaceData::m_puiSizesWires array
\version 2.0
@{
*/
#define kA3DTessFaceDataWireIsNotDrawn						0x4000 	/*!< Current wire is not to be drawn. */

#define kA3DTessFaceDataWireIsClosing						0x8000	/*!< Current wire is closing on first point. */

/*!
@} <!-- end of a3d_tessfaceflags -->
*/

/*!
\defgroup a3d_tessfacenormal Bitmasks for Behavior of Normals
\ingroup a3d_tessface
\brief Bitmasks used to set and access the \ref A3DTessFaceData::m_usUsedEntitiesFlags
\version 2.0

If this flag is set, the corresponding OneNormal entity (see \ref a3d_tessfacetype)
is planar and only one normal is defined for the entity.
Otherwise, one normal per point is defined.
@{
*/
#define kA3DTessFaceDataNormalSingle	0x40000000	/*!< Bit indicating normal behavior for \b OneNormal entities. */
#define kA3DTessFaceDataNormalMask		0x3FFFFFFF	/*!< Access to real value of number of entities. */
/*!
@} <!-- end of a3d_tessfacenormal -->
*/

/*!
\ingroup a3d_read
\brief Structure to specify the level of detail of the tessellation.

See \ref A3DRWParamsTessellationData parameters.
When a level is given, the Chord Height Ratio (\ref A3DRWParamsTessellationData::m_dChordHeightRatio) and
Wireframe Chord Angle (\ref A3DRWParamsTessellationData::m_dAngleToleranceDeg) change to preset values for the selected level.
\version 3.0
*/
typedef enum
{
  kA3DTessLODExtraLow,            /*!< Extra Low level: when selected, these members are automatically defined with the following values:
                                  \li \ref A3DRWParamsTessellationData::m_dChordHeightRatio = 50,
                                  \li \ref A3DRWParamsTessellationData::m_dAngleToleranceDeg = 40. */
  kA3DTessLODLow,                 /*!< Low level: when selected, these members are automatically defined with the following values:
                                  \li \ref A3DRWParamsTessellationData::m_dChordHeightRatio = 600,
                                  \li \ref A3DRWParamsTessellationData::m_dAngleToleranceDeg = 40. */
  kA3DTessLODMedium,              /*!< Medium level: when selected, these members are automatically defined with the following values:
                                  \li \ref A3DRWParamsTessellationData::m_dChordHeightRatio = 2000,
                                  \li \ref A3DRWParamsTessellationData::m_dAngleToleranceDeg = 40. */
  kA3DTessLODHigh,                /*!< High level: when selected, these members are automatically defined with the following values:
                                  \li \ref A3DRWParamsTessellationData::m_dChordHeightRatio = 5000,
                                  \li \ref A3DRWParamsTessellationData::m_dAngleToleranceDeg = 30. */
  kA3DTessLODExtraHigh,           /*!< Extra High level: when selected, these members are automatically defined with the following values:
                                  \li \ref A3DRWParamsTessellationData::m_dChordHeightRatio = 10000,
                                  \li \ref A3DRWParamsTessellationData::m_dAngleToleranceDeg = 20. */
  kA3DTessLODUserDefined,         /*!< User Defined level: when selected, these members should be defined:
                                  \li \ref A3DRWParamsTessellationData::m_dChordHeightRatio if \ref A3DRWParamsTessellationData::m_bUseHeightInsteadOfRatio is set to false,
                                  \li \ref A3DRWParamsTessellationData::m_dMaxChordHeight if \ref A3DRWParamsTessellationData::m_bUseHeightInsteadOfRatio is set to true,
                                  \li \ref A3DRWParamsTessellationData::m_dAngleToleranceDeg.
                                  Be very careful when using A3DRWParamsTessellationData::m_dMaxChordHeight because a too small value would generate a huge tessellation. */
  kA3DTessLODControlledPrecision  /*!< Controlled Precision level: when selected, these members should be defined:
                                  \li \ref A3DRWParamsTessellationData::m_dAngleToleranceDeg,
                                  \li \ref A3DRWParamsTessellationData::m_dMaxChordHeight,
                                  \li \ref A3DRWParamsTessellationData::m_dMinimalTriangleAngleDeg.
                                  \deprecated This is a deprecated feature.
                                  */
} A3DETessellationLevelOfDetail;


/*!
\defgroup a3d_tessmarkupflags Bitmasks for A3DTessMarkupData tessellation
\ingroup a3d_tessmarkup
\version 2.0

\par Sample Use
\include TessMarkupFlags.cpp

To encode color type entities \b and the following masks that apply to your color type:

00000100-00000000-00000000-00000000	kA3DMarkupIsExtraData<br>
00000001-01100000-00000000-00000000	kA3DMarkupColorMask<br>
00000000-00000000-00000000-00000001	Number of additional codes = 1<br>

Here is an example of a code corresponding to a color:

00000101-01100000-00000000-00000001	(In decimal form, this value is 90177537.)

And with no associated doubles, the three codes for a color type are:<br>
uiCode[n] = 90177537;<br>
uiCode[n+1] = 0; no doubles<br>
uiCode[n+2] = id_of_color;<br>

For encoding/decoding extra data types, use macros defined in \ref a3d_tessmarkupextradata.

\sa a3d_tessmarkup
@{
*/
#define kA3DMarkupIsMatrix			0x08000000		/*!< Bit to denote whether the current entity is a matrix. */
#define kA3DMarkupIsExtraData		0x04000000		/*!< Bit to denote whether the current entity is extra data (neither matrix nor polyline). */
#define kA3DMarkupIntegerMask		0xFFFFF			/*!< Integer mask to retrieve sizes. */
#define kA3DMarkupExtraDataType		0x3E00000		/*!< Mask to get the integer type of the entity. */
/*!
@} <!-- end of module a3d_tessmarkupflags -->
*/

/*!
\defgroup a3d_tessmarkupextradata Extra data types for A3DTessMarkupData tessellation
\ingroup a3d_tessmarkup
\version 2.0
@{
*/
#define kA3DMarkupPatternMask		0x000000		/*!< Mask to encode pattern type entity. */
#define kA3DMarkupPictureMask		0x200000		/*!< Mask to encode picture type entity. */
#define kA3DMarkupTrianglesMask		0x400000		/*!< Mask to encode triangles type entity. */
#define kA3DMarkupQuadsMask			0x600000		/*!< Mask to encode quads type entity. */
#define kA3DMarkupFaceViewMask		0xC00000		/*!< Mask to encode face view mode. */
#define kA3DMarkupFrameDrawMask		0xE00000		/*!< Mask to encode frame draw mode. */
#define kA3DMarkupFixedSizeMask		0x1000000		/*!< Mask to encode fixed size mode. */
#define kA3DMarkupSymbolMask		0x1200000		/*!< Mask to encode symbol type entity. */
#define kA3DMarkupCylinderMask		0x1400000		/*!< Mask to encode cylinder type entity. */
#define kA3DMarkupColorMask			0x1600000		/*!< Mask to encode color type entity. */
#define kA3DMarkupLineStippleMask	0x1800000		/*!< Mask to encode line stipple type entity. */
#define kA3DMarkupFontMask			0x1A00000		/*!< Mask to encode font type entity. */
#define kA3DMarkupTextMask			0x1C00000		/*!< Mask to encode text type entity. */
#define kA3DMarkupPointsMask		0x1E00000		/*!< Mask to encode point type entities. */
#define kA3DMarkupPolygonMask		0x2000000		/*!< Mask to encode polygonal type entity. */
#define kA3DMarkupLineWidthMask		0x2200000		/*!< Mask to encode line width type entity. */

/*! Macro to encode an extra data type for storing in a markup's tessellation.

The \c ExtraDataType parameter must be set with one of the above markup encoding definitions.
The \c InnerCodes parameter is the number of inner codes associated with the extra data.
*/
#define A3D_ENCODE_EXTRA_DATA(ExtraDataType, InnerCodes) ((kA3DMarkupIsExtraData | ExtraDataType) + InnerCodes)
/*! Macro to decode the extra data type from a markup's tessellation code. */
#define A3D_DECODE_EXTRA_DATA(TessCode) ((TessCode & kA3DMarkupExtraDataType) >> 21)
/*!
@} <!-- end of module a3d_tessmarkupflags -->
*/

/*!
\defgroup a3d_tessmarkupdef Bitmasks for markup options
\ingroup a3d_tessmarkup
\sa a3d_tessmarkup
\version 2.0
@{
*/
#define kA3DMarkupIsHidden				0x01 /*!< The tessellation is hidden. */
#define kA3DMarkupHasFrame				0x02 /*!< The tessellation has a frame. */
#define kA3DMarkupIsNotModifiable		0x04 /*!< The tessellation is given and must not be modified. */
#define kA3DMarkupIsZoomable			0x08 /*!< The tessellation is zoomable. */
#define kA3DMarkupIsOnTop				0x10 /*!< The tessellation is on top of geometry. */
#define kA3DMarkupIsNotRotatable		0x20 /*!< The tessellation is not rotatable. \version 9.0 */
/*!
@} <!-- end of a3d_tessmarkupdef -->
*/

/*!
\defgroup a3d_tessmarkupfontkeydef Masks for encoding font keys
\ingroup a3d_tessmarkup
\par Sample Use of the masks for Encoding Font Keys
\include TessMarkupFontKey.cpp

\sa a3d_tessmarkup
@{
*/
#define kA3DFontKeyAttrib		0x00000FFF /*!< Mask to encode the font attributes of the \ref A3DFontKeyData structure into the \ref A3DTessMarkupData structure. */
#define kA3DFontKeySize			0x00FFF000 /*!< Mask to encode the font size index of the \ref A3DFontKeyData structure into the \ref A3DTessMarkupData structure. */
#define kA3DFontKeyStyle		0xFF000000 /*!< Mask to encode the font style index of the \ref A3DFontKeyData structure into the \ref A3DTessMarkupData structure. */
/*!
@} <!-- end of a3d_tessmarkupfontkeydef -->
*/


/*!
\ingroup a3d_read
\brief Used to control the default PMI color:
If you load a Wildfire file and set kA3DFileVersionSessionColor to:
- kA3DFileVersionSessionColor, the PMI with no color will be displayed in yellow;
- kA3DLastCreoVersionSessionColor, the PMI with no color will be displayed in blue as in Creo 2 (for HOOPS Exchange 6.0);
- kA3DHExchangeSessionColor, the PMI with no color will be displayed with the color you chose.
\version 6.1
*/
typedef enum
{
  kA3DLastCreoVersionSessionColor = 0,	/*!< Sets the default session color corresponding to the last version of Creo that HOOPS Exchange supports. */
  kA3DHExchangeSessionColor,				/*!< Uses HOOPS Exchange default color. */
  kA3DFileVersionSessionColor				/*!< Lets the user define the color by using graphics parameters options ( \ref A3DRWParamsPmiData::m_sDefaultColor). */
} A3DProESessionColorType;

/*!
\ingroup a3d_read
\brief Used to control the construction entities reading, entity such as sketch, curves...
\version 8.2
*/
typedef enum
{
  A3DProEReadConstructEntities_AsDatum = 0,	/*!<  Read wire according to the datum reading option*/
  A3DProEReadConstructEntities_Yes,				/*!< Read wire*/
  A3DProEReadConstructEntities_No				/*!< Do not read wire*/
} A3DProEReadConstructEntities;

/*!
\ingroup a3d_read
\brief Used to select how to read family tables
\version 9.0
*/
typedef enum
{
  A3DProEFamTabAcceleratorFileOnly = 0,	/*!< Only use accelerator file. If there's tessellation or a generic part, these will not be loaded, even if an accelerator file isn't available. */
  A3DProEFamTabOrUseTessellation = 1,		/*!< If the accelerator file isn't present, then search for the tessellation representation: if found, then use the tessellation. If an accelerator file or tessellation isn't available, then nothing will be loaded. */
  A3DProEFamTabOrUseWireAndGeneric = 2	/*!< If neither the accelerator file nor the tessellation representation is present, then use the generic or wire representation. \n \warning If using the generic or wire representation, be aware that the representation will not be what it should be! A generic part/wire is only a placeholder. */
} A3DProEFamilyTables;

/*!
\ingroup a3d_read
\brief Used to select which name to use from NEXT_ASSEMBLY_USAGE_OCCURRENCE as occurrence name.
\version 4.2
*/
typedef enum
{
  kA3DStepNameFromNAUO_ID = 0,				/*!< First Field of NEXT_ASSEMBLY_USAGE_OCCURRENCE. */
  kA3DStepNameFromNAUO_NAME,					/*!< Second Field of NEXT_ASSEMBLY_USAGE_OCCURRENCE. */
  kA3DStepNameFromNAUO_DESCRIPTION			/*!< Third Field of NEXT_ASSEMBLY_USAGE_OCCURRENCE. */
} A3DEStepNameFromNAUO;

/*!
\ingroup a3d_read
\brief JT LOD to retrieve from file.
\version 8.0
*/
typedef enum
{
  kA3DJTTessLODLow = 0,		/*!< Will load the lowest level of tessellation available in the JT file. */
  kA3DJTTessLODMedium,		/*!< Will load an in-between level of tessellation available in the JT file. */
  kA3DJTTessLODHigh,			/*!< Will load the highest level of tessellation available in the JT file. */
} A3DEJTReadTessellationLevelOfDetail;

/*!
\ingroup a3d_write
\brief Level of compression used to write the model file to PRC format.

This sets the amount, in millimeters, of lossy compression that is applied to geometry.
For best results when exporting geometry, use kA3DCompressionLow.
\version 3.0
*/
typedef enum
{
  kA3DCompressionLow = 0,	/*!< Compression with tolerance set to 0.001 mm (low compression, high accuracy). */
  kA3DCompressionMedium,	/*!< Compression with tolerance set to 0.01 mm (medium compression, medium accuracy). */
  kA3DCompressionHigh		/*!< Compression with tolerance set to 0.1 mm (high compression, low accuracy). */
} A3DECompressBrepType;

/*!
\ingroup a3d_write
\brief STEP formats supported for export.
\version 3.0
*/
typedef enum
{
  kA3DStepAP203 = 0,	/*!< AP 203 Ed 2 since \version 9.1 */
  kA3DStepAP214,		/*!< AP 214. */
  kA3DStepAP242		/*!< AP 242. \version 9.1 */
} A3DEStepFormat;



/*!
\ingroup a3d_write
\brief Control mode of the healing in TranslateToPkParts functions. Used in \ref A3DRWParamsTranslateToPkPartsData.
\version 8.1
*/
typedef enum
{
  kA3DE_HEALING_NO,						/*!< Disable healing. */
  kA3DE_HEALING_YES,						/*!< Enable healing. */
  kA3DE_HEALING_ONLY_IF_NOT_PARASOLID,	/*!< Enable healing only for data coming from neither Parasolid nor from any format that embeds Parasolid data (NX, SolidWorks, Solid Edge and JT) but only if the \ref A3DRWParamsParasolidData::m_bKeepParsedEntities reading option is enabled. */
} A3DETranslateToPkPartsHealing;

/*!
\ingroup a3d_write
\brief Control mode of the accurate edge computation in TranslateToPkParts functions. Used in \ref A3DRWParamsTranslateToPkPartsData.
\version 8.1
*/
typedef enum
{
  kA3DE_ACCURATE_NO,						/*!< Disable accurate edge computation. */
  kA3DE_ACCURATE_YES,						/*!< Enable accurate edge computation. */
  kA3DE_ACCURATE_ONLY_IF_NOT_PARASOLID	/*!< Enable accurate edge computation only for data coming from neither Parasolid nor from any format that embeds Parasolid data (NX, SolidWorks, Solid Edge and JT) but only if the \ref A3DRWParamsParasolidData::m_bKeepParsedEntities reading option is enabled. */
} A3DETranslateToPkPartsAccurate;

/*!
\ingroup a3d_write
\brief Control mode of the simplify in TranslateToPkParts functions. Used in \ref A3DRWParamsTranslateToPkPartsData. It enables the conversion from nurbs to analytical.
\version 8.1
*/
typedef enum
{
  kA3DE_SIMPLIFY_NO,						/*!< Disable simplify. */
  kA3DE_SIMPLIFY_YES,						/*!< Enable simplify. */
  kA3DE_SIMPLIFY_ONLY_IF_NOT_PARASOLID,	/*!< Enable simplify only for data coming from neither Parasolid nor from any format that embeds Parasolid data (NX, SolidWorks, Solid Edge and JT) but only if the \ref A3DRWParamsParasolidData::m_bKeepParsedEntities reading option is enabled. */
} A3DETranslateToPkPartsSimplifyGeometry;

/*!
\ingroup a3d_write
\brief Control mode of the accurate edge computation in TranslateToPkParts functions. Used in \ref A3DRWParamsTranslateToPkPartsData.
\version 8.1
*/
typedef enum
{
  kA3DE_MERGE_NO,							/*!< Disable accurate edge computation. */
  kA3DE_MERGE_YES,						/*!< Enable accurate edge computation. */
  kA3DE_MERGE_ONLY_IF_NOT_PARASOLID,		/*!< Enable accurate edge computation only for data coming from neither Parasolid nor from any format that embeds Parasolid data (NX, SolidWorks, Solid Edge and JT) but only if the \ref A3DRWParamsParasolidData::m_bKeepParsedEntities reading option is enabled. */
} A3DETranslateToPkPartsMergeEntities;

/*!
\ingroup a3d_write
\brief U3D formats supported for export.
\version 4.0
*/

typedef enum
{
  kA3DECMA1,		/*!< ECMA-363, version 1 writing. (Acrobat Reader 7.0 compatible). */
  kA3DECMA3		/*!< ECMA-363, version 3 writing. */
} A3DEU3DVersion;


/*!
\ingroup a3d_write
\brief JT formats supported for export.
\version 10.0
*/
typedef enum
{
  kA3DE_JT81,		/*!< JT version 8.1 writing. */
  kA3DE_JT95,		/*!< JT version 9.5 writing. */
  kA3DE_JT100		/*!< JT version 10.0 writing. */
} A3DEJTVersion;


//////////////////////////////////////////
/// A3DSDKMarkupWielding
//////////////////////////////////////////

/*!
\enum EA3DMDLineWeldingType
\ingroup a3d_markupwelding
\brief Enumerator that describes the direction of lay
*/
typedef enum
{
  KEA3DMDWeldTypeUnknown                  = -1,   /*!< Unknown. */
  KEA3DMDWeldTypeNone                     =  0,   /*!< None. */
  KEA3DMDWeldTypeEdgeFlange               =  1,   /*!< Square edge flange. */
  KEA3DMDWeldTypeSquare                   =  2,   /*!< Square butt weld: \image html KEA3DMDWeldTypeSquare.png */
  KEA3DMDWeldTypeSingleV                  =  3,   /*!< Single V butt weld: \image html KEA3DMDWeldTypeSingleV.png*/
  KEA3DMDWeldTypeSingleBevel              =  4,   /*!< Single bevel butt weld: \image html KEA3DMDWeldTypeSingleBevel.png */
  KEA3DMDWeldTypeBroadRootFaceSingleV     =  5,   /*!< Single-V Butt Weld With Broad Root. */
  KEA3DMDWeldTypeBroadRootFaceSingleBevel =  6,   /*!< Single-Bevel Butt Weld with Broad Root Face . */
  KEA3DMDWeldTypeSingleU                  =  7,   /*!< Single U butt weld: \image html KEA3DMDWeldTypeSingleU.png */
  KEA3DMDWeldTypeSingleJ                  =  8,   /*!< Single J butt weld: \image html KEA3DMDWeldTypeSingleJ.png */
  KEA3DMDWeldTypeBacking                  =  9,   /*!< Back weld: \image html KEA3DMDWeldTypeBacking.png */
  KEA3DMDWeldTypeFillet                   =  10,  /*!< Fillet weld: \image html KEA3DMDWeldTypeFillet.png */
  KEA3DMDWeldTypePlug                     =  11,  /*!< Plug weld: \image html KEA3DMDWeldTypePlug.png */
  KEA3DMDWeldTypeSeam                     =  12,  /*!< Seam. */
  KEA3DMDWeldTypeSteepFlankedSingleV      =  13,  /*!< Steep-flanked single-V butt weld: \image html KEA3DMDWeldTypeSteepFlankedSingleV.png */
  KEA3DMDWeldTypeSteepFlankedSingleBevel  =  14,  /*!< Steep-flanked single-bevel butt weld: \image html KEA3DMDWeldTypeSteepFlankedSingleBevel.png */
  KEA3DMDWeldTypeEdge                     =  15,  /*!< Edge. */
  KEA3DMDWeldTypeSurface                  =  16,  /*!< Surfacing weld: \image html KEA3DMDWeldTypeSurface.png */
  KEA3DMDWeldTypeSurfaceJoint             =  17,  /*!< Surface Joint. */
  KEA3DMDWeldTypeInclinedJoint            =  18,  /*!< Inclined Joint. */
  KEA3DMDWeldTypeFoldJoint                =  19,  /*!< Fold Joint. */
  KEA3DMDWeldTypeSpot                     =  20,  /*!< Spot weld: \image html KEA3DMDWeldTypeSpot.png */
  KEA3DMDWeldTypePermanentBackStrip       =  21,  /*!< Permanent backing strip used: \image html KEA3DMDWeldTypePermanentBackStrip.png */
  KEA3DMDWeldTypeRemovableBackStrip       =  22,  /*!< Removable backing strip used: \image html KEA3DMDWeldTypeRemovableBackStrip.png */
  KEA3DMDWeldTypeUnspecifiedEdgeType      =  23,  /*!< Unspecified Edge. */
  KEA3DMDWeldTypeFlare_V_Butt             =  24,  /*!< Flare V butt weld: \image html KEA3DMDWeldTypeFlare_V_Butt.png */
  KEA3DMDWeldTypeFlare_Bevel_Butt         =  25,  /*!< Flare Bevel butt weld: \image html KEA3DMDWeldTypeFlare_Bevel_Butt.png */
  KEA3DMDWeldTypeSingleEdgeFlange         =  26   /*!< Single Edge Flange. */
} EA3DMDLineWeldingType;

/*!
\enum EA3DMDLineWeldingSupplSymbolType
\ingroup a3d_markupwelding
\brief Line Welding supplementary symbol description
*/
typedef enum
{
  KEA3DMDWeldAddSymbolUnknown = -1, /*!< Unknown. */
  KEA3DMDWeldAddSymbolNone = 0, /*!< None. */
  KEA3DMDWeldAddSymbolFlush = 1, /*!< \image html KE_LineWeldSuppl_Flush.png */
  KEA3DMDWeldAddSymbolConvex = 2, /*!< Weld with convex face: \image html KEA3DMDWeldAddSymbolConvex.png */
  KEA3DMDWeldAddSymbolConcave = 3, /*!< Weld with concave face: \image html KEA3DMDWeldAddSymbolConcave.png */
  KEA3DMDWeldAddSymbolToesBlended = 4, /*!< Fillet weld with smooth blended face: \image html KEA3DMDWeldAddSymbolToesBlended.png */
  KEA3DMDWeldAddSymbolFlushFinished = 5, /*!< Flush finished weld: \image html KEA3DMDWeldAddSymbolFlushFinished.png */
  KEA3DMDWeldAddSymbolBackingPermanent = 6, /*!< Permanent Backing. */
  KEA3DMDWeldAddSymbolBackingpRemovable = 7 /*!< Removable Backing. */
} EA3DMDLineWeldingSupplSymbolType;

/*!
\enum EA3DMDLineWeldingFinishSymbol
\ingroup a3d_markupwelding
\brief Line Welding finish symbol description
*/
typedef enum
{
  KEA3DMDWeldFinishSymbolNone = 0, /*!< No symbol. */
  KEA3DMDWeldFinishSymbolC = 1, /*!< C. */
  KEA3DMDWeldFinishSymbolF = 2, /*!< F. */
  KEA3DMDWeldFinishSymbolG = 3, /*!< G. */
  KEA3DMDWeldFinishSymbolH = 4, /*!< H. */
  KEA3DMDWeldFinishSymbolM = 5, /*!< M. */
  KEA3DMDWeldFinishSymbolR = 6, /*!< R. */
  KEA3DMDWeldFinishSymbolU = 7, /*!< U. */
  KEA3DMDWeldFinishSymbolChipping = 8, /*!< Chipping symbol. */
  KEA3DMDWeldFinishSymbolGrinding = 9, /*!< Grinding symbol. */
  KEA3DMDWeldFinishSymbolHammering = 10, /*!< Hammering symbol. */
  KEA3DMDWeldFinishSymbolMachining = 11, /*!< Machining symbol. */
  KEA3DMDWeldFinishSymbolRolling = 12, /*!< Rolling symbol. */
  KEA3DMDWeldFinishSymbolPeening = 13 /*!< Peening symbol. */
} EA3DMDLineWeldingFinishSymbol;


/*!
\enum EA3DMDLineWeldingOption
\ingroup a3d_markupwelding
\brief Line Welding symbol options description
*/
typedef enum
{
  KEA3DMDLineWeldOptionUnknown = -1, /*!< Unknown. */
  KEA3DMDLineWeldOptionNone = 0, /*!< None. */
  KEA3DMDLineWeldOptionAllAround = 0x00100, /*!< Weld applies all around. */
  KEA3DMDLineWeldOptionFieldWeld = 0x00200, /*!< Unfinished flag. */
  KEA3DMDLineWeldOptionReferenceSign = 0x00400, /*!< Reference frame. */
  KEA3DMDLineWeldOptionTail = 0x00800, /*!< Process symbol. */
  KEA3DMDLineWeldOptionIdentifLine = 0x01000, /*!< Additionnal dotted line. */
  KEA3DMDLineWeldOptionIdentifLineUp = 0x02000, /*!< Additionnal dotted line up. */
  KEA3DMDLineWeldOptionTextSideDown = 0x04000 /*!< Up/down reversing for approx texts and others. */
} EA3DMDLineWeldingOption;

/*!
\enum EA3DMDSpotWeldType
\ingroup a3d_markupwelding
\brief Spot welding symbol type description
*/
typedef enum
{
  KEA3DMDSpotWeldTypeUnknown = -1, /*!< Unknown. */
  KEA3DMDSpotWeldTypeNone = 0, /*!< None. */
  KEA3DMDSpotWeldTypeResistance = 1, /*!< Resistance welding. */
  KEA3DMDSpotWeldTypeProjection = 2 /*!< Projection welding. */
} EA3DMDSpotWeldType;

/*!
\enum EA3DMDSpotWeldThickness
\ingroup a3d_markupwelding
\brief Spot welding thickness description
*/
typedef enum
{
  KEA3DMDSpotWeldThickUnknown = -1, /*!< Unknown. */
  KEA3DMDSpotWeldThick_None = 0, /*!< No thickness level. */
  KEA3DMDSpotWeldTwoThick = 1,   /*!< Level 2 thickness. */
  KEA3DMDSpotWeldThreeThick = 2, /*!< Level 3 thickness. */
  KEA3DMDSpotWeldE_FourThick = 3 /*!< Level 4 thickness. */
} EA3DMDSpotWeldThickness;

////////////////////////////////////////////////////////////////////////////////
//  A3DSDKMarkupTolerance
////////////////////////////////////////////////////////////////////////////////

/*!
\enum EA3DGDTType
\ingroup a3d_markupfcfrow
\brief Enumerations for row type
\version 4.0
\image html pmi_gdt_types.png
*/
typedef enum
{
  KEA3DGDTTypeNone = 0,	/*!< No Type. */
  KEA3DGDTTypeStraightness = 1,	/*!< Straightness. */
  KEA3DGDTTypeFlatness = 2,	/*!< Flatness. */
  KEA3DGDTTypeCircularity = 3,	/*!< Circularity. */
  KEA3DGDTTypeCylindricity = 4,	/*!< Cylindricity. */
  KEA3DGDTTypeLineProfile = 5,	/*!< Profile of a line. */
  KEA3DGDTTypeSurfaceProfile = 6,	/*!< Profile of a surface. */
  KEA3DGDTTypeAngularity = 7,	/*!< Angularity. */
  KEA3DGDTTypePerpendicularity = 8,	/*!< Perpendicularity. */
  KEA3DGDTTypeParallelism = 9,	/*!< Parallel. */
  KEA3DGDTTypePosition = 10,	/*!< Positional. */
  KEA3DGDTTypeConcentricity = 11,	/*!< Concentricity. */
  KEA3DGDTTypeSymmetry = 12,	/*!< Symmetric. */
  KEA3DGDTTypeCircularRunout = 13,	/*!< Circular run out. */
  KEA3DGDTTypeTotalRunout = 14	/*!< Total run out. */
}EA3DGDTType;

/*!
\enum A3DMDGDTValueType
\ingroup a3d_markupfcfrow
\brief Enumerations for modifier type
\version 4.0

*/
typedef enum
{
  KEA3DGDTValueNone = 0, /*!< No modifier defined. */
  KEA3DGDTValueDiameter = 1, /*!< Diameter value type. */
  KEA3DGDTValueRadius = 2, /*!< Radial value type. */
  KEA3DGDTValueSpherical = 3  /*!< Spherical value type. */
} A3DMDGDTValueType;


/*!
\enum EA3DMDGDTModifierType
\ingroup a3d_markupfcfrow
\brief Enumerations for modifier type
\image html pmi_gdt_modifiers.png
\version 4.0

*/
typedef enum
{
  KEA3DGDTModifierNone = 0, /*!< No modifier defined. */
  KEA3DGDTModifierMax = 1, /*!< Maximum material condition. */
  KEA3DGDTModifierMin = 2, /*!< Least material condition. */
  KEA3DGDTModifierProj = 3, /*!< Projected tolerance zone. */
  KEA3DGDTModifierFree = 4, /*!< Free State. */
  KEA3DGDTModifierRfs = 5, /*!< Regardless of feature size. */
  KEA3DGDTModifierTangent = 6, /*!< Tangent plane. */
  KEA3DGDTModifierST = 8  /*!< Statistical. */
} EA3DMDGDTModifierType;


////////////////////////////////////////////////////////////////////////////////
// A3DSDKMarkupText
////////////////////////////////////////////////////////////////////////////////

/*!
\version 4.0
\enum EA3DMarkupFrameType
\ingroup a3d_markuptext
\brief Markup frame identifiers for markup text, datum, ....
*/

typedef enum
{
  KA3DMarkupFrameNone = 0, /*!< No Frame. */
  KA3DMarkupFrameRectangle = 1, /*!< Rectangle Frame. */
  KA3DMarkupFrameSquare = 2, /*!< Square Frame. */
  KA3DMarkupFrameCircle = 3, /*!< Circle Frame. */
  KA3DMarkupFrameScoredCircle = 4, /*!< Scored Circle. */
  KA3DMarkupFrameDiamond = 5, /*!< Diamond Frame. */
  KA3DMarkupFrameTriangle = 6, /*!< Triangle Frame. */
  KA3DMarkupFrameRightFlag = 7, /*!< Right Flag Frame. */
  KA3DMarkupFrameLeftFlag = 8, /*!< Left Flag Frame. */
  KA3DMarkupFrameBothFlag = 9, /*!< Both Flag Frame. */
  KA3DMarkupFrameOblong = 10, /*!< Oblong Frame. */
  KA3DMarkupFrameEllipse = 11, /*!< Ellipse Frame. */
  KA3DMarkupFrameFixRectangle = 51, /*!< Fixed Rectangle Frame. */
  KA3DMarkupFrameFixSquare = 52, /*!< Fixed Square Frame. */
  KA3DMarkupFrameFixCircle = 53, /*!< Fixed Circle Frame. */
  KA3DMarkupFrameFixScoredCircle = 54, /*!< Fixed Scored Circle Frame. */
  KA3DMarkupFrameFixDiamond = 55, /*!< Fixed Diamond Frame. */
  KA3DMarkupFrameFixTriangle = 56, /*!< Fixed Triangle Frame. */
  KA3DMarkupFrameFixRightFlag = 57, /*!< Fixed Right Flag Frame. */
  KA3DMarkupFrameFixLeftFlag = 58, /*!< Fixed Left Flag Frame. */
  KA3DMarkupFrameFixBothFlag = 59, /*!< Fixed Both Flag Frame. */
  KA3DMarkupFrameFixOblong = 60, /*!< Fixed Oblong Frame. */
  KA3DMarkupFrameFixEllipse = 61, /*!< Fixed Ellipse Frame. */
  KA3DMarkupFrameCustom = 255 /*!< Custom Frame. */
}EA3DMarkupFrameType;

/*!
\enum EA3DLeaderAlignementType
\ingroup a3d_markuptext
\brief Enumerations for Leader alignment type
This enumeration defines the way the leader anchors on markup.
\version 4.0

*/
typedef enum
{
  KA3DLeaderAlignement_Simple = 0, /*!< No alignment */
  KA3DLeaderAlignement_Bellow = 1, /*!< For a text note, the leader underlines the first text line. */
  KA3DLeaderAlignement_Underline = 2, /*!< The leader underlines all lines. */
  KA3DLeaderAlignement_ExtToMax = 3, /*!< The leader underlines all lines with the maximum line length. */
  KA3DLeaderAlignement_OnBoxPoint = 4, /*!< The leader anchors to the markup bounding box. */
  KA3DLeaderAlignement_SimpleOnBoxPoint = 5, /*!< The leader anchors to the markup bounding box, and in the middle of the line. */
  KA3DLeaderAlignement_Maximal_Underline = 6, /*The leader underlines all, with the length of the longest line.  \version 11.2.10*/
  KA3DLeaderAlignement_Simple_OppositeAnchor = 7 /*Same as simple, but the leader is attached to the opposite point that one specified by the leader anchor type. \version 11.2.10 */
} EA3DLeaderAlignementType;

////////////////////////////////////////////////////////////////////////////////
// A3DSDKMarkupSymbol
////////////////////////////////////////////////////////////////////////////////

/*!
\enum EA3DMDRoughnessType
\ingroup a3d_markuproughness
\brief Enumerator that describes the roughness type
*/
typedef enum
{
  KA3DRoughnessTypeBasic = 1,	/*!< Basic surface texture. */
  KA3DRoughnessTypeMachining_Required = 2,	/*!< Material removal by machining is required. */
  KA3DRoughnessTypeMachining_Prohibited = 3		/*!< Material removal by machining is prohibited. */
} EA3DMDRoughnessType;

/*!
\enum EA3DMDRoughnessContactArea
\ingroup a3d_markuproughness
\brief Enumerator that describes rough contact area type
*/
typedef enum
{
  KA3DRoughnessContactSurfTexture = 1,	/*!< Surface texture.							*/
  KA3DRoughnessContactSurfTextAndAllSrfAround = 2,	/*!< Surface texture and all surfaces around.	*/
  KA3DRoughnessContactBasic = 3,	 /*!< Basic contact.							*/
  KA3DRoughnessContactSrfAround = 4		/*!< All surfaces around.						*/
} EA3DMDRoughnessContactArea;

/*!
\enum EA3DMDRoughnessMode
\ingroup a3d_markuproughness
\brief Enumerator that describes the direction of lay
*/
typedef enum
{
  KA3DRoughnessMode_None = 0, /*!< No specified lay. */
  KA3DRoughnessModeM = 1, /*!< Lay multi directional. */
  KA3DRoughnessModeC = 2, /*!< Lay approximately circular. */
  KA3DRoughnessModeR = 3, /*!< Lay approximately radial. */
  KA3DRoughnessModeP = 4, /*!< Lay particulate, non-directional, or protuberant. */
  KA3DRoughnessModeX = 5, /*!< Lay angular in both directions. */
  KA3DRoughnessModePARA = 6, /*!< Lay approximately parallel to the line representing the surface. */
  KA3DRoughnessModePERP = 7 /*!< Lay approximately perpendicular to the line representing the surface. */
} EA3DMDRoughnessMode;

/*!
\enum EA3DMDBalloonShape
\ingroup a3d_markupballoon
\brief Enumerator that describes balloon shape
*/
typedef enum
{
  KA3DMDBalloonCircle = 0, /*!< Circle Balloon. */
  KEA3DMDBalloonSquare = 1, /*!< Square Balloon. */
  KEA3DMDBalloonDividedSquare = 2, /*!< Divided Square Balloon. */
  KEA3DMDBalloonQuadrantCircle = 3, /*!< Quadrant Circle Balloon. */
  KEA3DMDBalloonDividedCircle = 4, /*!< Divided Circle Balloon. */
  KEA3DMDBalloonRoundedBox = 5, /*!< Rounded Box Balloon. */
  KEA3DMDBalloonTriangle = 6, /*!< Triangle Balloon. */
  KEA3DMDBalloonTriangleUp = 7, /*!< Triangle Up Balloon. */
  KEA3DMDBalloonTriangleDown = 8, /*!< Triangle Down Balloon. */
  KEA3DMDBalloonHexagon = 9, /*!< Hexagon Balloon. */
  KEA3DMDBalloonDividedHexagon = 10 /*!< Divided Hexagon Balloon. */
} EA3DMDBalloonShape;

/*!
\enum EA3DLocatorSymbol
\ingroup a3d_markuplocator
\brief Enumerations for symbol type
\version 4.0
*/
typedef enum
{
  KEA3DSymbolUnknown = -1,	/*!< Unknown symbol. */
  KEA3DEdge = 0,	/*!< Edge. */
  KEA3DHole = 1,	/*!< Hole. */
  KEA3DSurface = 2,	/*!< Surface. */
  KEA3DRectangle = 3,	/*!< Rectangle. */
  KEA3DSlot = 4,	/*!< Slot. */
  KEA3DFreeform = 5		/*!< Free form. */
}EA3DLocatorSymbol;


/*!
\enum EA3DLocatorType
\ingroup a3d_markuplocator
\brief Enumerations for type
\version 4.0
*/
typedef enum
{
  KEA3DLocatorUnknown = -1,	/*!< Unknown locator. */
  KEA3DMain = 0,	/*!< Main locator. */
  KEA3DSecondary = 1,	/*!< Secondary locator. */
  KEA3DMainPermTransf = 2,	/*!< Main permanent transform. */
  KEA3DSecondaryPermTransf = 3,	/*!< Secondary permanent transform. */
  KEA3DSecondaryCoordination = 4,	/*!< Secondary coordination locator. */
  KEA3DCoordination = 5,	/*!< Coordination locator. */
  KEA3DAccessClearance = 6,	/*!< Access Clearance. */
  KEA3DDetached = 7,	/*!< Detached. */
  KEA3DBlank = 8,	/*!< Blank. */
  KEA3DCorrectable = 9,	/*!< Correctable. */
  KEA3DAuxiliaryRest = 10,	/*!< Auxiliary rest. */
  KEA3DJclamps = 11	/*!< Clamps. */
}EA3DLocatorType;

/*!
\enum EA3DLocatorSubType
\ingroup a3d_markuplocator
\brief Enumerations for type
\version 4.0
*/
typedef enum
{
  KEA3DSubtypeUnknown = -1,	/*!< Unknown locator subtype. */
  KEA3DDiesMolds = 0,	/*!< Dies Molds. */
  KEA3DFixing = 1,	/*!< Fixing. */
  KEA3DTemporaryTransferred = 2,	/*!< Temporary Transferred. */
  KEA3DMechanicallyFastened = 3,	/*!< Mechanically Fastened. */
  KEA3DManufacturingAssembly = 4		/*!< Manufacturing assembly. */
}EA3DLocatorSubType;
/*!
\enum EA3DLocatorCoordinatePlane
\ingroup a3d_markuplocator
\brief Enumerations for type
\version 4.0
*/
typedef enum
{
  KEA3DCoordPlaneUnknown = -1,	/*!< Unknown coordinate plane. */
  KEA3DCoordPlaneX = 0,	/*!< X plane. */
  KEA3DCoordPlaneY = 1,	/*!< Y plane. */
  KEA3DCoordPlaneZ = 2,	/*!< Z plane. */
  KEA3DCoordPlaneXY = 3,	/*!< XY plane. */
  KEA3DCoordPlaneXZ = 4,	/*!< XZ plane. */
  KEA3DCoordPlaneYZ = 5,	/*!< YZ plane. */
  KEA3DCoordPlaneXYZ = 6		/*!< XYZ plane. */
}EA3DLocatorCoordinatePlane;

/*!
\enum EA3DLocatorChangeLevel
\ingroup a3d_markuplocator
\brief Enumerations for type
\version 4.0
*/
typedef enum
{
  KEA3DLevelUnknown = -1,	/*!< Unknown change level. */
  KEA3DHard = 0,	/*!< Hard change level. */
  KEA3DMedium = 1,	/*!< Medium change level. */
  KEA3DSoft = 2		/*!< Soft change level. */
} EA3DLocatorChangeLevel;



/*!
\enum EA3DLocatorSubscript
\ingroup a3d_markuplocator
\brief Enumerations for type
\version 4.0
*/
typedef enum
{
  KEA3DLocatorSubscriptO = 1 << 0,	/*!< Subscript O Locator. */
  KEA3DLocatorSubscriptS = 1 << 1,	/*!< Subscript S Locator. */
  KEA3DLocatorSubscriptK = 1 << 2,	/*!< Subscript K Locator. */
  KEA3DLocatorSubscriptC = 1 << 3,	/*!< Subscript C Locator. */
  KEA3DLocatorSubscriptF = 1 << 4	/*!< Subscript F Locator. */
}EA3DLocatorSubscript;


////////////////////////////////////////////////////////////////////////////////
// A3DSDKMarkupLeaderDefinition.h
////////////////////////////////////////////////////////////////////////////////

/*!
\enum EA3DMDAnchorPointType
\brief Markup anchor point type
\ingroup a3d_markupposition
*/
typedef enum {

  KEA3DMDAnchorIgnored = -1,

  KEA3DMDAnchorTop_left = 0,	/*!< Top left point. */
  KEA3DMDAnchorTop_center = 1,	/*!< Top center point. */
  KEA3DMDAnchorTop_right = 2,	/*!< Top right point. */

  KEA3DMDAnchorMiddle_left = 10,	/*!< Middle left point.	*/
  KEA3DMDAnchorMiddle_center = 11,	/*!< Middle center point. */
  KEA3DMDAnchorMiddle_right = 12,	/*!< Middle right point. */
  KEA3DMDAnchorMiddle_auto = 13,	/*!< Closest point to the middle of the frame. */

  KEA3DMDAnchorBottom_left = 20,	/*!< Bottom left point.	*/
  KEA3DMDAnchorBottom_center = 21,	/*!< Bottom center point. */
  KEA3DMDAnchorBottom_right = 22,	/*!< Bottom right point. */
  KEA3DMDAnchorBottom_auto = 23, /*!< Closest point to the bottom of the frame. */

  KEA3DMDAnchorAutomatic = 24  /*!< Closest point to the frame. */

}EA3DMDAnchorPointType;

/*!
\enum A3DMDLeaderSymbolType
\ingroup a3d_markupleaderdefinition
\brief Enumerate that describes leader end symbols.
*/
typedef enum {
  KA3DMDLeaderSymbolNotUsed = 0, /*!< Unused symbol. */
  KA3DMDLeaderSymbolCross = 1, /*!< Cross. */
  KA3DMDLeaderSymbolPlus = 2, /*!< Plus sign. */
  KA3DMDLeaderSymbolConcentric = 3, /*!< Concentric sign. */
  KA3DMDLeaderSymbolCoincident = 4, /*!< Coincident. */
  KA3DMDLeaderSymbolFullCircle = 5, /*!< Full circle. */
  KA3DMDLeaderSymbolFullSquare = 6, /*!< Full square. */
  KA3DMDLeaderSymbolStar = 7, /*!< Star. */
  KA3DMDLeaderSymbolDot = 8, /*!< Dot. */
  KA3DMDLeaderSymbolSmallDot = 9, /*!< Small dot. */
  KA3DMDLeaderSymbolMisc1 = 10, /*!< Misc1. */
  KA3DMDLeaderSymbolMisc2 = 11, /*!< Misc2. */
  KA3DMDLeaderSymbolFullCircle2 = 12, /*!< Full circle2. */
  KA3DMDLeaderSymbolFullSquare2 = 13, /*!< Full square2. */
  KA3DMDLeaderSymbolOpenArrow = 14, /*!< Open arrow. */
  KA3DMDLeaderSymbolUnfilledArrow = 15, /*!< Transparent arrow. */
  KA3DMDLeaderSymbolBlankedArrow = 16, /*!< Blanked arrow. */
  KA3DMDLeaderSymbolFilledArrow = 17, /*!< Filled arrow. */
  KA3DMDLeaderSymbolUnfilledCircle = 18, /*!< Transparent circle. */
  KA3DMDLeaderSymbolBlankedCircle = 19, /*!< Opaque circle. */
  KA3DMDLeaderSymbolFilledCircle = 20, /*!< Filled circle. */
  KA3DMDLeaderSymbolCrossedCircle = 21, /*!< Crossed circle. */
  KA3DMDLeaderSymbolBlankedSquare = 22, /*!< Opaque square. */
  KA3DMDLeaderSymbolFilledSquare = 23, /*!< Filled square. */
  KA3DMDLeaderSymbolBlankedTriangle = 24, /*!< Opaque triangle. */
  KA3DMDLeaderSymbolFilledTriangle = 25, /*!< Filled triangle. */
  KA3DMDLeaderSymbolManipulatorSquare = 26, /*!< Manipulator square. */
  KA3DMDLeaderSymbolMamipulatorDiamond = 27, /*!< Mamipulator diamond. */
  KA3DMDLeaderSymbolManipulatorCircle = 28, /*!< Manipulator circle. */
  KA3DMDLeaderSymbolManipulatorTriangle = 29, /*!< Manipulator triangle. */
  KA3DMDLeaderSymbolDoubleOpenArrow = 30, /*!< Double open arrow. */
  KA3DMDLeaderSymbolWave = 31, /*!< Wave sign. */
  KA3DMDLeaderSymbolSegment = 32, /*!< Segment. */
  KA3DMDLeaderSymbolDoubleFilledArrow = 33, /*!< Double filled arrow. */
  KA3DMDLeaderSymbolDoubleClosedArrow = 34, /*!< Double closed arrow. */
  KA3DMDLeaderSymbolHalfOpenArrowUp = 35, /*!< Half open arrow up. */
  KA3DMDLeaderSymbolHalfOpenArrowDown = 36, /*!< Half open arrow down. */
  KA3DMDLeaderSymbolHalfFilledArrowUp = 37, /*!< Half filled arrow up. */
  KA3DMDLeaderSymbolHalfFilledArrowDown = 38, /*!< Half filled arrow down. */
  KA3DMDLeaderSymbolSlash = 39, /*!< Slash. */
  KA3DMDLeaderSymbolDoubleBlankedArrow = 40, /*!< Double blanked arrow. */
  KA3DMDLeaderSymbolIntegral = 41, /*!< Integral. */
  KA3DMDLeaderSymbolZoneGlobalAllAround = 50,		/*!< Global All Around. */
  KA3DMDLeaderSymbolZonePartialAllAround = 51,	/*!< Partial All Around. */
  KA3DMDLeaderSymbolZoneGlobalAllAboutWithHorizontalAxisIndicator = 52, /*!< Zone global all about with horizontal axis indicator. */
  KA3DMDLeaderSymbolZoneGlobalAllAboutWithVerticalAxisIndicator = 53, /*!< Zone global all about with vertical axis indicator. */
  KA3DMDLeaderSymbolZonePartialAllAboutWithHorizontalAxisIndicator = 54, /*!< Zone partial all about with horizontal axis indicator. */
  KA3DMDLeaderSymbolZonePartialAllAboutWithVerticalAxisIndicator = 55, /*!< Zone partial all about with vertical axis indicator. */
  KA3DMDLeaderSymbolZoneGlobalAllOver = 56, /*!< Zone global all over. */
  KA3DMDLeaderSymbolZonePartialAllOver = 57 /*!< Zone partial all over. */
} A3DMDLeaderSymbolType;


////////////////////////////////////////////////////////////////////////////////
// A3DSDKMarkupDimension
////////////////////////////////////////////////////////////////////////////////

/*!
\enum EA3DMDDimensionValueOption
\ingroup a3d_markupdimensionvalue
\brief Enumerator that describes dimension's option
\version 9.0
*/
typedef enum
{
  KEA3DMDDimensionValueOptionNone = 0, /*!< Dimension has no option. */
  KEA3DMDDimensionValueOptionMin = 1,	/*!< Dimension has the min modifier. */
  KEA3DMDDimensionValueOptionMax = 2,	/*!< Dimension has the max modifier. */
  KEA3DMDDimensionValueOptionEnvelopeModifier = 4,	/*!< Dimension has the enveloppe modifier. */
  KEA3DMDDimensionValueOptionIndependencyModifier = 8,	/*!< Dimension has the independency modifier. */
  KEA3DMDDimensionValueOptionStaticalTolerancingModifier = 16,	/*!< Dimension has the statical tolerancing modifier. */
  KEA3DMDDimensionValueOptionPerfectFormAtMMCNotRequired = 32,	/*!< Dimension has the option perfect form at MMC not required. */
  KEA3DMDDimensionValueOptionReferenceOrAuxiliary = 64,	/*!< ASME:Reference ISO:Auxiliary. A dimension given for information purposes only. The value is given in parentheses and no tolerance applies to it. \version 12. */
  KEA3DMDDimensionValueOptionBasicOrTheorical = 128	    /*!< ASME:Basic     ISO:Theorical. Dimensions enclosed in a frame are the theoretically exact location, orientation, or profile of a feature or group of features. \version 12 */
} EA3DMDDimensionValueOption;

/*!
\enum EA3DMDDimensionLineGraphicalRepresentation
\ingroup a3d_markupdimensionline
\brief Enumerator that describes dimension's line graphical representation
*/
typedef enum
{
  KEA3DMDDimensionGraphicalRepresentationRegular = 0,	/*!< Regular dimension line representation: \image html pmi_markup_dimension_GraphRep_Regular.png */
  KEA3DMDDimensionGraphicalRepresentationTwoParts = 1,	/*!< Two parts dimension line representation: \image html pmi_markup_dimension_GraphRep_Two_Parts.png */
  KEA3DMDDimensionGraphicalRepresentationLeaderOnePart = 2,	/*!< Leader one part dimension line representation: \image html pmi_markup_dimension_GraphRep_Leader_one_Part.png */
  KEA3DMDDimensionGraphicalRepresentationLeaderTwoParts = 3		/*!< Leader two parts dimension line representation: \image html pmi_markup_dimension_GraphRep_Leader_two_Parts.png */
} EA3DMDDimensionLineGraphicalRepresentation;

/*!
\enum EA3DMDDimensionOrientation
\ingroup a3d_markupdimensionline
\brief Enumerator that describes dimension's orientation
\sa A3DMDDimensionSecondPartData, A3DMarkupDimensionData
*/
typedef enum
{
  KEA3DMDDimensionOrientationScreenHorizontal = 0,	/*!< Dimension along horizontal line of the screen. */
  KEA3DMDDimensionOrientationScreenVertical = 1,	/*!< Dimension along vertical line of the screen. */
  KEA3DMDDimensionOrientationScreenAngle = 2,	/*!< Uses m_AnnAngle of \ref A3DMarkupDefinitionData. */
  KEA3DMDDimensionOrientationViewHorizontal = 3,	/*!< Dimension along horizontal line of the view. */
  KEA3DMDDimensionOrientationViewVertical = 4,	/*!< Dimension along vertical line of the view. */
  KEA3DMDDimensionOrientationViewAngle = 5,	/*!< Uses m_AnnAngle of \ref A3DMarkupDefinitionData. */
  KEA3DMDDimensionOrientationParallel = 6,	/*!< Dimension is parallel to the dimension line. */
  KEA3DMDDimensionOrientationPerpendicular = 7,	/*!< Dimension is perpendicular to the dimension line. */
  KEA3DMDDimensionOrientationAngle = 8		/*!< Uses m_AnnAngle of \ref A3DMarkupDefinitionData. */
} EA3DMDDimensionOrientation;


/*!
\enum EA3DMDDimensionSymbolType
\ingroup a3d_markupdimensionline
\brief Enumerator that describes line symbols
\sa A3DMarkupDimensionData
*/
typedef enum
{
	KEA3DDimensionSymbolTypeUNKNOW			= -1, /*!< Unknow */
	KEA3DDimensionSymbolTypeNONE			= 0, /*!< None */
	KEA3DDimensionSymbolTypePROJTOLZONE		= 1, /*!< Projection tolerance zone */
	KEA3DDimensionSymbolTypeMAXMATERIAL		= 2, /*!< Max material */
	KEA3DDimensionSymbolTypeLEASTMATERIAL	= 3, /*!< Least material */
	KEA3DDimensionSymbolTypeFREESTATE		= 4, /*!< Freestate */
	KEA3DDimensionSymbolTypeOHM				= 5, /*!< Ohm */
	KEA3DDimensionSymbolTypeCENTERLINE		= 6, /*!< Center line */
	KEA3DDimensionSymbolTypeDEPTH			= 7, /*!< Depth */
	KEA3DDimensionSymbolTypeCOUNTERBORE		= 8, /*!< Counter bore */
	KEA3DDimensionSymbolTypeCOUNTERSUNK		= 9, /*!< Counter sunk */
	KEA3DDimensionSymbolTypeCIRCULARRUNOUT	= 10, /*!< Circular runout */
	KEA3DDimensionSymbolTypeSURFPROFILE		= 11, /*!< Surface profile */
	KEA3DDimensionSymbolTypeLINEPROFILE		= 12, /*!< Line profile */
	KEA3DDimensionSymbolTypeFLATNESS		= 13, /*!< Flatness */
	KEA3DDimensionSymbolTypeSTRAIGHT		= 14, /*!< Straight */
	KEA3DDimensionSymbolTypeTOTALRUNOUT		= 15, /*!< Total runout */
	KEA3DDimensionSymbolTypeSYMMETRY		= 16, /*!< Symmetry */
	KEA3DDimensionSymbolTypePERPENDICULAR	= 17, /*!< Perpendicular */
	KEA3DDimensionSymbolTypePARALLEL		= 18, /*!< Parallel */
	KEA3DDimensionSymbolTypeCYLINDRIC		= 19, /*!< Cylindric */
	KEA3DDimensionSymbolTypeCONCENTRIC		= 20, /*!< Concentric */
	KEA3DDimensionSymbolTypeCIRCULAR		= 21, /*!< Circular */
	KEA3DDimensionSymbolTypeANGULAR			= 22, /*!< Angular */
	KEA3DDimensionSymbolTypeMICRO			= 23, /*!< Micro */
	KEA3DDimensionSymbolTypeDEGREE			= 24, /*!< Degree */
	KEA3DDimensionSymbolTypePLUSMINUS		= 25, /*!< Plus-minus */
	KEA3DDimensionSymbolTypePOSITION		= 26, /*!< Position */
	KEA3DDimensionSymbolTypeDIAMETER		= 27, /*!< Diameter */
	KEA3DDimensionSymbolTypeENVELOPE		= 28, /*!< Envelope */
	KEA3DDimensionSymbolTypeARROW			= 29, /*!< Arrow */
	KEA3DDimensionSymbolTypeNOACTUALSIZE	= 30, /*!< No actual size */
	KEA3DDimensionSymbolTypeTANGENTPLANE	= 31, /*!< Tangent plane */
	KEA3DDimensionSymbolTypeLOWEROREQUAL	= 32, /*!< Lower or equal */
	KEA3DDimensionSymbolTypeGREATEROREQUAL	= 33, /*!< Greater or equal */
	KEA3DDimensionSymbolTypeTHREADPREFIX	= 34, /*!< Thread prefix */
	KEA3DDimensionSymbolTypeSLOPE			= 35, /*!< Slope */
	KEA3DDimensionSymbolTypeCONICALTAPER	= 36, /*!< Conical taper */
	KEA3DDimensionSymbolTypeUPTRIANGLE		= 37, /*!< Up triangle */
	KEA3DDimensionSymbolTypeSQUARE			= 38, /*!< Square */
	KEA3DDimensionSymbolTypeST				= 39,  /*!< Statistical */
	KEA3DDimensionSymbolTypeSDIAMETER		= 40,  /*!< Spherical Diameter*/
	KEA3DDimensionSymbolTypeRADIUS			= 41,  /*!< Radius */
	KEA3DDimensionSymbolTypeSRADIUS			= 42,  /*!< Spherical Radius */
	KEA3DDimensionSymbolTypeCRADIUS			= 43,  /*!< Controlled Radius */
	KEA3DDimensionSymbolTypeUNEQUALLY		= 44, /*!< Unequally */
	KEA3DDimensionSymbolTypeCF				= 45,  /*!< Continuous Feature */
	//a symbol can use to precise the dimension value,
	//It can be placed before or after the gloval dimension value,
	//the following value, precise that the symbol is to be placed after the dim value
	KEA3DDimensionSymbolTypeSET_AFTER		= 1000  /*!< Set after */
} EA3DMDDimensionSymbolType;




/*!
\enum EA3DMDDimensionSymbolShape
\ingroup a3d_markupdimensionline
\brief Enumerator that describes line symbols
\sa A3DMDDimensionLineSymbolData
*/
typedef enum
{
  KEA3DDimensionSymbolNone = 0,	/*!< No symbol. */
  KEA3DDimensionSymbolOpenArrow = 1,	/*!< Open arrow. */
  KEA3DDimensionSymbolClosedArrow = 2,	/*!< Closed arrow. */
  KEA3DDimensionSymbolFilledArrow = 3,	/*!< Filled arrow. */
  KEA3DDimensionSymbolSymArrow = 4,	/*!< Symetric arrow. */
  KEA3DDimensionSymbolSlash = 5,	/*!< Slash. */
  KEA3DDimensionSymbolCircle = 6,	/*!< Circle. */
  KEA3DDimensionSymbolFilledCircle = 7,	/*!< Filled circle. */
  KEA3DDimensionSymbolScoredCircle = 8,	/*!< Scored circle. */
  KEA3DDimensionSymbolCircledCross = 9,	/*!< Circled cross. */
  KEA3DDimensionSymbolTriangle = 10,	/*!< Triangle. */
  KEA3DDimensionSymbolFilledTriangle = 11,	/*!< Filled Triangle. */
  KEA3DDimensionSymbolCross = 12,	/*!< Cross. */
  KEA3DDimensionSymbolXCross = 13,	/*!< X cross. */
  KEA3DDimensionSymbolDoubleArrow = 14,	/*!< Double arrow. */
  KEA3DDimensionSymbolSquare = 15,	/*!< Box. \version 7.1 */
  KEA3DDimensionSymbolFilledSquare = 16,	/*!< Filled box. \version 7.1 */
  KEA3DDimensionSymbolWave = 17	/*!< Wave. \version 7.1 */
} EA3DMDDimensionSymbolShape;


/*!
\enum EA3DMDDimensionLineExtensionType
\ingroup a3d_markupdimensionline
\brief Enumerator that describes the extension type
\sa A3DMDDimensionLineGet
*/
typedef enum
{
  KEA3DMDDimensionExtensionFromStandard = 0,	/*!< Extension from standard. */
  KEA3DMDDimensionExtensionTillCenter = 1,	/*!< Extension till center. */
  KEA3DMDDimensionExtensionNotTillCenter = 2,	/*!< Extension not till center. */
  KEA3DMDDimensionExtensionHideFirstPartOfDimLine = 4,	/*!< Extension hide first part of dim line. */
  KEA3DMDDimensionExtensionHideFSecondPartOfDimLine = 8,	/*!< Extension hide f second part of dim line. */
  KEA3DMDDimensionExtensionHideEntireDimLine = 16,	/*!< Extension hide entire dim line. */
  KEA3DMDDimensionExtensionAddIsoLineUnderTextValue = 32	/*!< Extension add iso line under text value. */
} EA3DMDDimensionLineExtensionType;


/*!
\enum EA3DMDDimensionType Dimension type
\ingroup a3d_markupdimension
\brief Enumerator that describes the dimension's type
\sa A3DMarkupDimensionData
*/
typedef enum
{
  KEA3DMDDimensionTypeDistance = 0,	/*!< distance. */
  KEA3DMDDimensionTypeDistanceOffset = 1,	/*!< distance offset. */
  KEA3DMDDimensionTypeLength = 2,	/*!< length. */
  KEA3DMDDimensionTypeLengthCurvilinear = 3,	/*!< length curvilinear. */
  KEA3DMDDimensionTypeAngle = 4,	/*!< angle. */
  KEA3DMDDimensionTypeRadius = 5,	/*!< radius. */
  KEA3DMDDimensionTypeRadiusTangent = 6,	/*!< radius tangent. */
  KEA3DMDDimensionTypeRadiusCylinder = 7,	/*!< radius cylinder. */
  KEA3DMDDimensionTypeRadiusEdge = 8,	/*!< radius edge. */
  KEA3DMDDimensionTypeDiameter = 9,	/*!< diameter. */
  KEA3DMDDimensionTypeDiameterTangent = 10,	/*!< diameter tangent. */
  KEA3DMDDimensionTypeDiameterCylinder = 11,	/*!< diameter cylinder. */
  KEA3DMDDimensionTypeDiameterEdge = 12,	/*!< diameter edge. */
  KEA3DMDDimensionTypeDiameterCone = 13,	/*!< diameter cone. */
  KEA3DMDDimensionTypeChamfer = 14,	/*!< chamfer. */
  KEA3DMDDimensionTypeSlope = 15	/*!< Slope. */
} EA3DMDDimensionType;

/*!
\enum EA3DMDDimensionDualDisplay Dimension dual value display
\ingroup a3d_markupdimension
\brief Enumerator that describes dual value display
\sa A3DMarkupDimensionData
*/
typedef enum
{
  KEA3DMDDimensionDualDisplayNone = 0, /*!< No. */
  KEA3DMDDimensionDualDisplayBelow = 1, /*!< Display below. */
  KEA3DMDDimensionDualDisplayFractional = 2, /*!< fractional display. */
  KEA3DMDDimensionDualDisplaySideBySide = 3,  /*!< Side by side display. */
  KEA3DMDDimensionDualDisplayOnLeft = 4 , /*!< Dual value display before main value. */
  KEA3DMDDimensionDualDisplayOnRight = 5,/*!< Dual value display after main value. */
  KEA3DMDDimensionDualDisplayAbove = 6,	/*!< Dual value display above main value. */
  KEA3DMDDimensionDualDisplayOnly = 7, /*!< Display only dual value, main value is hidden. */
} EA3DMDDimensionDualDisplay;


/*!
\enum EA3DMDDimensionFrame Dimension frame
\ingroup a3d_markupdimension
\brief Enumerator that describes dimension's frame
\sa A3DMarkupDimensionData
*/
typedef enum
{
  KEA3DMDDimensionFrameNone = 0, /*!< No frame. */
  KEA3DMDDimensionFrameCircle = 1, /*!< Circle frame. */
  KEA3DMDDimensionFrameScoredCircle = 2, /*!< Scored circle frame. */
  KEA3DMDDimensionFrameDiamondShaped = 3, /*!< Diamond frame. */
  KEA3DMDDimensionFrameSquare = 4, /*!< Square frame. */
  KEA3DMDDimensionFrameRectangle = 5, /*!< Rectangle frame. */
  KEA3DMDDimensionFrameOblong = 6, /*!< Oblong frame. */
  KEA3DMDDimensionFrameRightFlag = 7, /*!< Right flag frame. */
  KEA3DMDDimensionFrameRightTriangle = 8  /*!< Triangle frame. */
} EA3DMDDimensionFrame;

/*!
\enum EA3DMDDimensionScore Dimension score
\ingroup a3d_markupdimension
\brief Enumerator that describes angle sector used
\sa A3DMarkupDimensionData
*/
typedef enum
{
  KEA3DMDDimensionScoreNone = 0, /*!< No Score. */
  KEA3DMDDimensionUnderScored = 1, /*!< Underscored. */
  KEA3DMDDimensionScored = 2, /*!< Scored. */
  KEA3DMDDimensionOverScored = 3 /*!< overscored. */
}EA3DMDDimensionScore;

/*!
\enum EA3DMDDimensionAngleSector Dimension angle sector
\ingroup a3d_markupdimension
\brief Enumerator that describes angle sector used
\sa A3DMarkupDimensionData
*/
typedef enum
{
	KEA3DMDDimensionAngleSector_None = 0, /*!< No angle sector. */
	KEA3DMDDimensionAngleSector_1 = 1, /*!< First angle sector. */
	KEA3DMDDimensionAngleSector_2 = 2, /*!< Second angle sector. */
	KEA3DMDDimensionAngleSector_3 = 3, /*!< Third angle sector. */
	KEA3DMDDimensionAngleSector_4 = 4, /*!< Fourth angle sector. */
	KEA3DMDDimensionAngleSector_Complementary = 5, /*!< Complementary angle sector. */
} EA3DMDDimensionAngleSector;

/*!
\enum EA3DMDDimensionReferenceType Dimension Reference Type
\ingroup a3d_markupdimension
\brief Enumerator that describes Reference type used
\sa A3DMarkupDimensionData
*/
typedef enum
{
	KEA3DMDDimensionRefType_None = 0,  /*!< No reference type. */
	KEA3DMDDimensionRefType_Reference = 1, /*!< . */
	KEA3DMDDimensionRefType_Parenthesis = 2, /*!< Add parenthesis: ( value ). */
	KEA3DMDDimensionRefType_Match = 3,	/*!< . */
	KEA3DMDDimensionRefType_Diameter_Reference = 4,	/*!< . */
	KEA3DMDDimensionRefType_Not_To_Scale = 5,	/*!< . */
	KEA3DMDDimensionRefType_Basic = 6	/*!< . */
}EA3DMDDimensionReferenceType;

////////////////////////////////////////////////////////////////////////////////
// A3DSDKMarkupDefinition
////////////////////////////////////////////////////////////////////////////////



/*!
\enum EA3DMDMarkupDisplayType
\ingroup a3d_markupdefinition_module
\details
* The markup position is defined by an attach point and an attach type.
*
* The attach type specifies the point of markup frame on which the attach point will be aligned.
* See below.\image html pmi_attach_type.png
*/
typedef enum {
  KEA3DMDDisplayType_Unknown = -1, /*!< Unknown. */
  KEA3DMDDisplayType_Unspecified = 0, /*!< Unspecified. */
  KEA3DMDDisplayType_FlatToScreen = 1,/*!< Flat to screen. */
  KEA3DMDDisplayType_FlatToSurface = 2/*!< Flat to surface. */
}EA3DMDMarkupDisplayType;

/*!
\enum EA3DMDTextPropertiesScore
\ingroup a3d_markupdefinition_module
\brief Enumerator that describes the score type applied on text
*/
typedef enum {
  KEA3DMDTextPropertiesScoreNone = 0,	/*!< Text is not scored.																	*/
  KEA3DMDTextPropertiesUnderScored = 1,	/*!< Text is underscored: draw a horizontal line below the text.						*/
  KEA3DMDTextPropertiesScored = 2,	/*!< Also called strike through, Text is scored: draw a horizontal line through the text. */
  KEA3DMDTextPropertiesOverScored = 3		/*!< Text is overscored: draw a horizontal line above the text.						*/
}EA3DMDTextPropertiesScore;

/*!
\enum EA3DMDTextPropertiesFormat
\ingroup a3d_markupdefinition_module
\brief Enumerator that describes the format text
*/
typedef enum {
  KEA3DMDTextPropertiesFormatNormal = 0, 	/*!< Classical text.							*/
  KEA3DMDTextPropertiesFormatUnderLine = 1, 	/*!< Text positioning under the classical text.	*/
  KEA3DMDTextPropertiesFormatOverLine = 2, 	/*!< Text positioning over the classical text.	*/
  KEA3DMDTextPropertiesFormatExposant = 3, 	/*!< Text positioning as an exposant.			*/
  KEA3DMDTextPropertiesFormatindice = 4 	/*!< Text positioning as an indice.				*/
}EA3DMDTextPropertiesFormat;

/*!
\enum EA3DMDTextPropertiesJustification
\ingroup a3d_markupdefinition_module
\brief Enumerator that justifies the text
*/
typedef enum {
  KEA3DMDTextPropertiesJustificationLeft = 0, /*!< Justifies the text on the left side. */
  KEA3DMDTextPropertiesJustificationCenter = 1, /*!< Centers the text. */
  KEA3DMDTextPropertiesJustificationRight = 2  /*!< Justifies the text on the right side. */
}EA3DMDTextPropertiesJustification;

////////////////////////////////////////////////////////////////////////////////
// A3DSDKMarkup
////////////////////////////////////////////////////////////////////////////////


/*!
\defgroup a3d_markup_enums Markup Types and Subtypes
\ingroup a3d_markup_module
\brief Enumerations for markup types and subtypes for use in the \ref A3DMkpMarkupData structure
@{
*/

/*!
\version 2.0

Markup type identifiers
*/
typedef enum
{
  kA3DMarkupTypeUnknown = 0,		/*!< Unknown value. */
  kA3DMarkupTypeText,				/*!< Plain text. */
  kA3DMarkupTypeDimension,		/*!< Dimension. */
  kA3DMarkupTypeArrow,			/*!< Arrow. */
  kA3DMarkupTypeBalloon,			/*!< Balloon. */
  kA3DMarkupTypeCircleCenter,		/*!< Center of circle. */
  kA3DMarkupTypeCoordinate,		/*!< Coordinate. */
  kA3DMarkupTypeDatum,			/*!< Datum. */
  kA3DMarkupTypeFastener,			/*!< Fastener. */
  kA3DMarkupTypeGdt,				/*!< GD&T. */
  kA3DMarkupTypeLocator,			/*!< Locator. */
  kA3DMarkupTypeMeasurementPoint,	/*!< Point. */
  kA3DMarkupTypeRoughness,		/*!< Roughness. */
  kA3DMarkupTypeWelding,			/*!< Welding. */
  kA3DMarkupTypeTable,			/*!< Table. */
  kA3DMarkupTypeOther				/*!< Other. */
} A3DEMarkupType;

/*!
\brief Markup subtype identifiers
\version 2.0
*/
typedef enum
{
  kA3DMarkupSubTypeUnknown = 0,	/*!< Unknown value. */
  kA3DMarkupSubTypeEnumMax,								/*!< Unknown max value. \version 2.2 */

  kA3DMarkupSubTypeDatumIdent = 1,	/*!< Datum subtype. */
  kA3DMarkupSubTypeDatumTarget,							/*!< Datum subtype. */
  kA3DMarkupSubTypeDatumEnumMax,							/*!< Datum max value. \version 2.2 */

  kA3DMarkupSubTypeDimensionDistance = 1,	/*!< Dimension distance subtype. */
  kA3DMarkupSubTypeDimensionDistanceOffset,				/*!< Dimension distance subtype. */
  kA3DMarkupSubTypeDimensionDistanceCumulate,				/*!< Dimension distance subtype. */
  kA3DMarkupSubTypeDimensionChamfer,						/*!< Dimension chamfer subtype. */
  kA3DMarkupSubTypeDimensionSlope,						/*!< Dimension slope subtype. */
  kA3DMarkupSubTypeDimensionOrdinate,						/*!< Dimension ordinate subtype. */
  kA3DMarkupSubTypeDimensionRadius,						/*!< Dimension radius subtype. */
  kA3DMarkupSubTypeDimensionRadiusTangent,				/*!< Dimension radius subtype. */
  kA3DMarkupSubTypeDimensionRadiusCylinder,				/*!< Dimension radius subtype. */
  kA3DMarkupSubTypeDimensionRadiusEdge,					/*!< Dimension radius subtype. */
  kA3DMarkupSubTypeDimensionDiameter,						/*!< Dimension diameter subtype. */
  kA3DMarkupSubTypeDimensionDiameterTangent,				/*!< Dimension diameter subtype. */
  kA3DMarkupSubTypeDimensionDiameterCylinder,				/*!< Dimension diameter subtype. */
  kA3DMarkupSubTypeDimensionDiameterEdge,					/*!< Dimension diameter subtype. */
  kA3DMarkupSubTypeDimensionDiameterCone,					/*!< Dimension diameter subtype. */
  kA3DMarkupSubTypeDimensionLength,						/*!< Dimension length subtype. */
  kA3DMarkupSubTypeDimensionLengthCurvilinear,			/*!< Dimension length subtype. */
  kA3DMarkupSubTypeDimensionLengthCircular,				/*!< Dimension length subtype. */
  kA3DMarkupSubTypeDimensionAngle,						/*!< Dimension angle subtype. */
  kA3DMarkupSubTypeDimensionEnumMax,						/*!< Dimension max value. \version 2.2 */

  kA3DMarkupSubTypeGdtFcf = 1,	/*!< GD&T Feature control frame subtype. */
  kA3DMarkupSubTypeGdtEnumMax,							/*!< GD&T max value. \version 2.2 */

  kA3DMarkupSubTypeWeldingLine = 1,	/*!< Line welding subtype. */
  kA3DMarkupSubTypeWeldingSpot,							/*!< Spot welding subtype. */
  kA3DMarkupSubTypeWeldingEnumMax,						/*!< Welding max value. \version 2.2 */

  kA3DMarkupSubTypeOtherSymbolUser = 1,	/*!< User symbol, other subtype. */
  kA3DMarkupSubTypeOtherSymbolUtility,					/*!< Utility symbol, other subtype. */
  kA3DMarkupSubTypeOtherSymbolCustom,						/*!< Custom symbol, other subtype. */
  kA3DMarkupSubTypeOtherGeometricReference,				/*!< Geometric reference, other subtype. */
  kA3DMarkupSubTypeOtherRegion,							/*!< Region symbol, other subtype. \version 7.2 */
  kA3DMarkupSubTypeOtherEnumMax							/*!< Other max value. \version 2.2 */
} A3DEMarkupSubType;

/*!
\ingroup a3d_markup_rtf
\brief Symbol identifiers used in RTF strings
\version 4.2
*/

typedef enum
{
  KEA3DSymbol_UNKNOWN = 0,                 /*!< Not a symbol. */
  KEA3DSymbol_Angularity,                /*!< Angularity. */
  KEA3DSymbol_MoreOrLess,				   /*!< MoreOrLess. */
  KEA3DSymbol_Micron,					   /*!< Micron. */
  KEA3DSymbol_Circularity,			   /*!< Circularity. */
  KEA3DSymbol_Concentricity,			   /*!< Concentricity. */
  KEA3DSymbol_Cylindricity,			   /*!< Cylindricity. */
  KEA3DSymbol_Parallelism,			   /*!< Parallelism. */
  KEA3DSymbol_Perpendicularity,		   /*!< Perpendicularity. */
  KEA3DSymbol_Symmetry,				   /*!< Symmetry. */
  KEA3DSymbol_TotalRunout,			   /*!< TotalRunout. */
  KEA3DSymbol_Straightness,			   /*!< Straightness. */
  KEA3DSymbol_Flatness,				   /*!< Flatness. */
  KEA3DSymbol_LineProfile,			   /*!< LineProfile. */
  KEA3DSymbol_SurfaceProfile,			   /*!< SurfaceProfile. */
  KEA3DSymbol_Runout,					   /*!< Runout. */
  KEA3DSymbol_Countersink,			   /*!< Countersink. */
  KEA3DSymbol_Counterbore,			   /*!< Counterbore. */
  KEA3DSymbol_Depth,					   /*!< Depth. */
  KEA3DSymbol_Centerline,				   /*!< Centerline. */
  KEA3DSymbol_Ohm,					   /*!< Ohm. */
  KEA3DSymbol_Omega,					   /*!< Omega. */
  KEA3DSymbol_SmallerOrEqual,			   /*!< SmallerOrEqual. */
  KEA3DSymbol_GreaterOrEqual,			   /*!< GreaterOrEqual. */
  KEA3DSymbol_Slope,					   /*!< Slope. */
  KEA3DSymbol_ConicalTaper,			   /*!< ConicalTaper. */
  KEA3DSymbol_Diameter,				   /*!< Diameter. */
  KEA3DSymbol_FreeState,				   /*!< FreeState. */
  KEA3DSymbol_LeastMaterial,			   /*!< LeastMaterial. */
  KEA3DSymbol_MaximumMaterial,		   /*!< MaximumMaterial. */
  KEA3DSymbol_ProjectedTolerance,		   /*!< ProjectedTolerance. */
  KEA3DSymbol_TangentPlane,			   /*!< TangentPlane. */
  KEA3DSymbol_Statistical,			   /*!< Statistical. */
  KEA3DSymbol_Position,				   /*!< Position. */
  KEA3DSymbol_Square,					   /*!< Square. */
  KEA3DSymbol_Triangle,				   /*!< Triangle. */
  KEA3DSymbol_between,				   /*!< between. */
  KEA3DSymbol_RegardlessOfFeatureSize,   /*!< RegardlessOfFeatureSize. */
  KEA3DSymbol_Degre,					   /*!< Degre. */
  KEA3DSymbol_Encompassing,			   /*!< Encompassing. */
  KEA3DSymbol_CircledU,				   /*!< CircledU. */
  KEA3DSymbol_CircledR,				   /*!< CircledR. */
  KEA3DSymbol_RightwardArrow,			   /*!< RightwardArrow. */
  KEA3DSymbol_SubsetOf,				   /*!< SubsetOf. */
  KEA3DSymbol_1STANG,					   /*!< 1STANG. */
  KEA3DSymbol_3RDANG,					   /*!< 3RDANG. */
  KEA3DSymbol_BREAK15,				   /*!< BREAK15. */
  KEA3DSymbol_BREAK30,				   /*!< BREAK30. */
  KEA3DSymbol_DATUM1,					   /*!< DATUM1. */
  KEA3DSymbol_DATUM2,					   /*!< DATUM2. */
  KEA3DSymbol_MINUS,					   /*!< MINUS. */
  KEA3DSymbol_PLINE,					   /*!< PLINE. */
  KEA3DSymbol_PLUS,					   /*!< PLUS. */
  KEA3DSymbol_STRETCH,				   /*!< STRETCH. */
  KEA3DSymbol_STS,					   /*!< STS. */
  KEA3DSymbol_STS_,					   /*!< STS_. */
  KEA3DSymbol_STS_ANG,				   /*!< STS_ANG. */
  KEA3DSymbol_STS_ANG_,				   /*!< STS_ANG_. */
  KEA3DSymbol_STS_BAS,				   /*!< STS_BAS. */
  KEA3DSymbol_STS_BAS_,				   /*!< STS_BAS_. */
  KEA3DSymbol_STS_CIR,				   /*!< STS_CIR. */
  KEA3DSymbol_STS_CIR_,				   /*!< STS_CIR_. */
  KEA3DSymbol_STS_MAT,				   /*!< STS_MAT. */
  KEA3DSymbol_STS_MAT_,				   /*!< STS_MAT_. */
  KEA3DSymbol_STS_MLT,				   /*!< STS_MLT. */
  KEA3DSymbol_STS_MLT_,				   /*!< STS_MLT_. */
  KEA3DSymbol_STS_NOM,				   /*!< STS_NOM. */
  KEA3DSymbol_STS_NOM_,				   /*!< STS_NOM_. */
  KEA3DSymbol_STS_PAR,				   /*!< STS_PAR. */
  KEA3DSymbol_STS_PAR_,				   /*!< STS_PAR_. */
  KEA3DSymbol_STS_PER,				   /*!< STS_PER. */
  KEA3DSymbol_STS_PER_,				   /*!< STS_PER_. */
  KEA3DSymbol_STS_PRT,				   /*!< STS_PRT. */
  KEA3DSymbol_STS_PRT_,				   /*!< STS_PRT_. */
  KEA3DSymbol_STS_RAD,				   /*!< STS_RAD. */
  KEA3DSymbol_STS_RAD_				   /*!< STS_RAD_. */
} A3DEMarkupSymbol;

/*!
\defgroup a3d_fontattribdef Flags for Font and Font-Key Attributes
\ingroup a3d_fonts
\version 2.0

These masks are used to define the attributes for \ref A3DFontData and \ref A3DFontKeyData
\note The bit 0x01 is unused.
@{
*/
#define kA3DFontBold			0x02	/*!< Bold. */
#define kA3DFontItalic			0x04	/*!< Italic. */
#define kA3DFontUnderlined		0x08	/*!< Underlined. */
#define kA3DFontStrikedOut		0x10	/*!< Striked-out. */
#define kA3DFontOverlined		0x20	/*!< Overlined. */
#define kA3DFontStreched		0x40	/*!<
Streched. In case the font used is not the original font,
it indicates that the text needs to be stretched to fit its bounding box. */
#define kA3DFontWired			0x80	/*!< Wired. Indicates that the original font is a wirefame font. */
#define kA3DFontFixedWidth		0x100	/*!< Fixed width. Indicates that the original font is not proportional (each glyph has the same width). */
/*!
@} <!-- end of module a3d_fontattribdef -->
*/

/*!
\brief Font key CharSet
\ingroup a3d_fonts
\version 2.0
*/
typedef enum
{
  kA3DCharsetUnknown = -1,		/*!< Unknown charset ; */
  kA3DCharsetRoman = 0,			/*!< Roman charset */
  kA3DCharsetJapanese,			/*!< Japanese charset */
  kA3DCharsetTraditionalChinese,	/*!< Traditional chinese charset */
  kA3DCharsetKorean,				/*!< Korean charset */
  kA3DCharsetArabic,				/*!< Arabic charset */
  kA3DCharsetHebrew,				/*!< Hebrew charset */
  kA3DCharsetGreek,				/*!< Greek charset */
  kA3DCharsetCyrillic,			/*!< Cyrillic charset */
  kA3DCharsetRightLeft,			/*!< Right left charset */
  kA3DCharsetDevanagari,			/*!< Devanagari charset */
  kA3DCharsetGurmukhi,			/*!< Gurmukhi charset */
  kA3DCharsetGujarati,			/*!< Gujarati charset */
  kA3DCharsetOriya,				/*!< Oriya charset */
  kA3DCharsetBengali,				/*!< Bengali charset */
  kA3DCharsetTamil,				/*!< Tamil charset */
  kA3DCharsetTelugu,				/*!< Telugu charset */
  kA3DCharsetKannada,				/*!< Kannada charset */
  kA3DCharsetMalayalam,			/*!< Malayalam charset */
  kA3DCharsetSinhalese,			/*!< Sinhalese charset */
  kA3DCharsetBurmese,				/*!< Burmese charset */
  kA3DCharsetKhmer,				/*!< Khmer charset */
  kA3DCharsetThai,				/*!< Thai charset */
  kA3DCharsetLaotian,				/*!< Laotian charset */
  kA3DCharsetGeorgian,			/*!< Georgian charset */
  kA3DCharsetArmenian,			/*!< Armenian charset */
  kA3DCharsetSimplifiedChinese,	/*!< Simplified chinese charset */
  kA3DCharsetTibetan,				/*!< Tibetan charset */
  kA3DCharsetMongolian,			/*!< Mongolian charset */
  kA3DCharsetGeez,				/*!< Geez charset */
  kA3DCharsetEastEuropeanRoman,	/*!< East european roman charset */
  kA3DCharsetVietnamese,			/*!< Vietnamese charset */
  kA3DCharsetExtendedArabic		/*!< Extended arabic charset */
} A3DECharSet;


/*!
@} <!-- end of a3d_markup_enums -->
*/

////////////////////////////////////////////////////////////////////////////////
// A3DSDKGraphics
////////////////////////////////////////////////////////////////////////////////

/*!
\defgroup a3d_graphicsbits Bit Field for Behavior on Graphics
\ingroup a3d_graphics
\version 2.0

This bit field defines the behavior of a given entity, regarding its visibility, color, transparency, layer, line pattern and line width, given
its position in the tree of entities. The inheritance works as follows:<BR>
<BR>
Following the path of a leaf in the tree of entities:<BR>
\li If there are <b>SonHerit</b> flags, it is the lowest node in the tree which has this flag which defines the value, except if it has a <b>FatherHerit</b> flag.
\li If there are <b>FatherHerit</b> flags, it is the highest node in the tree which has this flag which defines the value.
\li If there is no flag, the current value is set, if any.<BR>

To determine the value of a son regarding his father, the following rules can be applied:<BR>
\li If the father has a <b>SonHerit</b> flag and the son has a <b>SonHerit</b> flag too, the value is the one of the son.
\li If the father has a <b>SonHerit </b>flag but not the son, the value is the one of the father.
\li If the father does not have a <b>SonHerit</b> flag, but the son has one, the value is the one of the son.
\li If the father does not have a <b>SonHerit</b> flag neither a <b>FatherHerit</b> flag, and the son does not have a <b>SonHerit</b> flag too, the value is the one of the son.
\li If none of those rules can match, the value is the current one.<BR>

Following are different configurations that can be found:<BR>
<TABLE border=1 border=1 rules="all" cellspacing="0" cellpadding="10" align="left">
<TR>
<TD>\image html graphicsbits_image001.png</TD>
<TD>This tree has no flag; each node defines its own color. This is the reference.
Each following diagram is the same than this one, but with different flags on some nodes.  The 'S' means that the node has a
<b>SonHerit</b> flag, the 'F'; that the node has a <b>FatherHerit</b> flag.</TD>
</TR>
<TR>
<TD>\image html graphicsbits_image002.png</TD>
<TD>The second leaf does not have a <b>SonHerit</b> flag. It takes the pattern of its father.</TD>
</TR>
<TR>
<TD>\image html graphicsbits_image003.png</TD>
<TD>The second leaf does not have a <b>SonHerit</b> flag, and also its father. So it takes its own value of pattern.</TD>
</TR>
<TR>
<TD>\image html graphicsbits_image004.png</TD>
<TD>The second node has a <b>FatherHerit</b> flag. Its sons don't have any flag. They take the pattern of their father.</TD>
</TR>
<TR>
<TD>\image html graphicsbits_image005.png</TD>
<TD>The second node has a <b>FatherHerit</b> flag. The first leaf has a <b>SonHerit</b> flag. It takes its own pattern.
But the second leaf doesn't have a <b>SonHerit</b> flag, so it takes the pattern of its father.</TD>
<TR>
<TD>\image html graphicsbits_image006.png</TD>
<TD>The root node has a <b>FatherHerit</b> flag. The entire tree takes its pattern.</TD>
</TR>
<TR>
<TD>\image html graphicsbits_image007.png</TD>
<TD>The root node has a <b>FatherHerit</b> flag. The second node too, so it takes the pattern of its father. The first
leaf has a <b>SonHerit</b> flag, so it takes its own pattern. But the second
leaf has no flag, so it takes the pattern of its father.</TD>
</TR>
</TABLE>
<b>NB1:</b> The <b>SonHerit</b> flag overrides the <b>FatherHerit</b> flag.<BR>
@{
*/
#define kA3DGraphicsShow					0x0001	/*!< Entity is shown. */
#define kA3DGraphicsSonHeritShow			0x0002	/*!< Show inheritance. */
#define kA3DGraphicsFatherHeritShow			0x0004	/*!< Show inheritance. */
#define kA3DGraphicsSonHeritColor			0x0008	/*!< Color / material inheritance. */
#define kA3DGraphicsFatherHeritColor		0x0010 	/*!< Color / material inheritance. */
#define kA3DGraphicsSonHeritLayer			0x0020 	/*!< Layer inheritance. */
#define kA3DGraphicsFatherHeritLayer		0x0040 	/*!< Layer inheritance. */
#define kA3DGraphicsSonHeritTransparency	0x0080 	/*!< Transparency inheritance. */
#define kA3DGraphicsFatherHeritTransparency	0x0100 	/*!< Transparency inheritance. */
#define kA3DGraphicsSonHeritLinePattern		0x0200 	/*!< Line pattern inheritance. */
#define kA3DGraphicsFatherHeritLinePattern	0x0400	/*!< Line pattern inheritance. */
#define kA3DGraphicsSonHeritLineWidth		0x0800 	/*!< Line width inheritance. */
#define kA3DGraphicsFatherHeritLineWidth	0x1000 	/*!< Line width inheritance. */
#define kA3DGraphicsRemoved					0x2000 	/*!< Entity is removed. As a result, the entity no longer appears in the tree. */
/*!
@} <!-- End of module a3d_graphicsbits -->
*/

/*!
\defgroup a3d_style Display Style
\ingroup a3d_graphics
Entity type is \ref kA3DTypeGraphStyle.
*/

/*!
\ingroup a3d_style
\brief Rendering mode
\version 8.2
*/
typedef enum
{
  kA3DSolid,					/*!< Solid rendering mode. */
  kA3DSolidOutline,				/*!< Solid outline rendering mode. */
  kA3DWireframe,				/*!< Wireframe rendering mode. */
  kA3DHLR,						/*!< HLR rendering mode. */
  kA3DRMDefault = 100,			/*!< Rendering mode not defined, a default value should be applied. */
} A3DERenderingMode;


/*!
\ingroup a3d_picture
\brief Image format
\version 2.0

\note The conventions described here are similar to the common scene graphics conventions described for
other 3D standards such as OpenGL and DirectX.

*/
typedef enum
{
  kA3DPicturePng,					/*!< PNG format. */
  kA3DPictureJpg,					/*!< JPEG format. */
  kA3DPictureBmp,					/*!< BMP format. */
  kA3DPictureBitmapRgbByte,		/*!< Array of Red Green and Blue bytes. */
  kA3DPictureBitmapRgbaByte,		/*!< Array of Red Green Blue and Alpha bytes */
  kA3DPictureBitmapGreyByte,		/*!< Array of Greyscale bytes */
  kA3DPictureBitmapGreyaByte		/*!< Array of Greyscale and alpha bytes */
} A3DEPictureDataFormat;

////////////////////////////////////////////////////////////////////////////////
//  A3DSDKGeometrySrf
////////////////////////////////////////////////////////////////////////////////

/*!
\brief Enumeration for characterizing a surface's form
\ingroup a3d_srfnurbs
\version 2.0
*/
typedef enum
{
  kA3DBSplineSurfaceFormPlane,					/*!< Planar surface. */
  kA3DBSplineSurfaceFormCylindrical,			/*!< Cylindrical surface. */
  kA3DBSplineSurfaceFormConical,				/*!< Conical surface. */
  kA3DBSplineSurfaceFormSpherical,				/*!< Spherical surface. */
  kA3DBSplineSurfaceFormRevolution,			/*!< Surface of revolution. */
  kA3DBSplineSurfaceFormRuled,					/*!< Ruled surface. */
  kA3DBSplineSurfaceFormGeneralizedCone,		/*!< Cone. */
  kA3DBSplineSurfaceFormQuadric,				/*!< Quadric surface. */
  kA3DBSplineSurfaceFormLinearExtrusion,		/*!< Surface of extrusion. */
  kA3DBSplineSurfaceFormUnspecified,			/*!< Unspecified. */
  kA3DBSplineSurfaceFormPolynomial				/*!< Polynomial surface. */
} A3DEBSplineSurfaceForm;

////////////////////////////////////////////////////////////////////////////////
//  A3DSDKGeometryCrv
////////////////////////////////////////////////////////////////////////////////

/*!
\brief enumerate to characterize knot vector
\ingroup a3d_crvnurbs
\version 2.0
*/
typedef enum
{
  kA3DKnotTypeUniformKnots,			/*!< Uniform. */
  kA3DKnotTypeUnspecified,			/*!< No particularity. */
  kA3DKnotTypeQuasiUniformKnots,		/*!< Quasi-uniform. */
  kA3DKnotTypePieceWiseBezierKnots	/*!< Extrema with multiplicities of degree + 1, internal is degree. */
} A3DEKnotType;

/*!
\brief Enumerate to characterize curve form
\ingroup a3d_crvnurbs
\version 2.0
*/
typedef enum
{
  kA3DBSplineCurveFormUnspecified,	/*!< No particularity. */
  kA3DBSplineCurveFormPolyline,		/*!< Polyline. */
  kA3DBSplineCurveFormCircularArc,	/*!< Circle arc. */
  kA3DBSplineCurveFormEllipticArc,	/*!< Elliptic arc. */
  kA3DBSplineCurveFormParabolicArc,	/*!< Parabolic arc. */
  kA3DBSplineCurveFormHyperbolicArc	/*!< Hyperbolic arc. */
} A3DEBSplineCurveForm;

/*!
\brief Enumerated values that characterize helix curves
\ingroup a3d_crvhelix
\version 2.0
*/
typedef enum
{
  kA3DConstantPitch = 0,		/*!< Constant pitch. */
  kA3DVariablePitch			/*!< Variable pitch. */
} A3DEHelixType;

/*!
\enum A3DEIntersectionLimitType
\brief Intersection limit type
\ingroup a3d_crvintersection
\version 2.0
*/
typedef enum
{
  kA3DIntersectionLimitTypeHelp,			/*!< Arbitary limit on a closed intersection curve. */
  kA3DIntersectionLimitTypeTerminator,	/*!< Limit where one of the two intersection surface normals is degenerate or where they become colinear. */
  kA3DIntersectionLimitTypeLimit,			/*!< Artificial limit to avoid an infinite curve. */
  kA3DIntersectionLimitTypeBoundary		/*!< Limit of the curve if a A3DSurfBlend02 surface (that uses the intersection curve as its center curve) becomes degenerate. */
} A3DEIntersectionLimitType;


////////////////////////////////////////////////////////////////////////////////
// A3DSDKDrawing
////////////////////////////////////////////////////////////////////////////////

/*!
\version 5.0

Drawing view type identifiers
*/

typedef enum
{
  kA3DDrawingViewTypeUnknown = 0,	/*!< Unknown value. */
  kA3DDrawingViewTypeIso = 1,	/*!< ISO view. */
  kA3DDrawingViewTypeTop = 2,	/*!< Top view. */
  kA3DDrawingViewTypeBottom = 3,	/*!< Bottom view. */
  kA3DDrawingViewTypeLeft = 4,	/*!< Left view. */
  kA3DDrawingViewTypeRight = 5,	/*!< Right view. */
  kA3DDrawingViewTypeFront = 6,	/*!< Front view. */
  kA3DDrawingViewTypeBack = 7,	/*!< Back view. */
  kA3DDrawingViewTypeBackground = 8,	/*!< Background view. */
  kA3DDrawingViewTypeWorking = 9,	/*!< Working view. */
  kA3DDrawingViewTypeProjected = 10,	/*!< Projected view. */
  kA3DDrawingViewTypeAuxiliary = 11,	/*!< Auxiliary view. */
  kA3DDrawingViewTypeSection = 12,	/*!< Section view. */
  kA3DDrawingViewTypeDetail = 13,	/*!< Detail view. */
} A3DEDrawingViewType;

/*!
\brief A3DEDrawingClipFrameType enum
\ingroup a3d_drawing_clipframe
\version 5.0

\image html drawing_view_clipping_type.png

*/
typedef enum
{
  kA3DDrawingClipFrameTypeNone = 0,				/*!< Unknown value. */
  kA3DDrawingClipFrameTypeRectangular = 1,				/*!< The frame is a rectangle. */
  kA3DDrawingClipFrameTypeUserDefine = 2				/*!< The frame is defined by user. */
} A3DEDrawingClipFrameType;

/*!
\version 5.0

Drawing operator type identifiers
*/

typedef enum
{
  kA3DDrawingOperatorTypeUnknwon = 0,	/*!< Unknown value. */
  kA3DDrawingOperatorTypeDetail = 1,	/*!< View of detail. */
  kA3DDrawingOperatorTypeSection = 2	/*!< View of section. */

} A3DEDrawingOperatorType;

//##############################################################################
/*!
\addtogroup a3d_drawing_filledarea
@{
*/

/*!
\version 5.0

\brief Enumerations for drawing filled area mode for use in the \ref A3DDrawingFilledAreaData structure

In case of intersecting, or self-intersecting boundaries, \ref A3DEDrawingFilledAreaMode is used to determine the fill area.

For example, in case of two intersecting square boundaries, three different fill areas can be obtained like in the next picture.
\image html drawing_fillmode.png
*/

typedef enum
{
  kA3DDrawingFiledAreaModeOR = 0,	/*!< Use OR operator. */
  kA3DDrawingFiledAreaModeAND = 1,	/*!< Use AND operator. */
  kA3DDrawingFiledAreaModeXOR = 2	/*!< Use XOR operator. */
} A3DEDrawingFilledAreaMode;

/*!
@} <!-- end of a3d_drawing_filledareamode_enums -->
*/

////////////////////////////////////////////////////////////////////////////////
// A3DSDKDraw
////////////////////////////////////////////////////////////////////////////////

/*!
\brief Enumeration for characterizing material
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef enum
{
  kA3DDrawMaterialAmbient, /*!< Ambient color. RGBA. */
  kA3DDrawMaterialDiffuse, /*!< Diffuse color. RGBA. */
  kA3DDrawMaterialSpecular, /*!< Specular color. RGBA. */
  kA3DDrawMaterialEmission, /*!< Emission color. RGBA. */
  kA3DDrawMaterialShininess /*!< Shininess color. Single value. */
} A3DEDrawMaterialType;

/*!
\brief Enumeration for characterizing begin and end callbacks
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef enum
{
  kA3DDrawBeginEndProductOccurrence, /*!< Begins or ends drawing a \ref A3DAsmProductOccurrence. */
  kA3DDrawBeginEndRepresentationItem, /*!< Begins or ends drawing a \ref A3DRiRepresentationItem. */
  kA3DDrawBeginEndMarkup /*!< Begins or ends drawing a \ref A3DMkpMarkup. */
} A3DEDrawBeginEndType;



/*!
\defgroup a3d_drawflags Flags for Drawing Model File Entities
\version 2.0
\brief These flags apply to the \ref A3DDraw and \ref A3DDrawGetBoundingBox functions.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_module
@{
*/
#define kA3DDraw3D		0x1 /*!< Draws only 3D tessellation. */
#define kA3DDrawMarkups	0x2 /*!< Draws only markups. */
/*!
@} <!-- End of a3d_drawflags -->
*/

////////////////////////////////////////////////////////////////////////////////
// A3DSDKAdvancedTools
////////////////////////////////////////////////////////////////////////////////

/*!
\version 5.0
\enum A3DEAnalyticType
\ingroup a3d_evaluate
\brief Tells what kind of curve has been created by A3DGetCurveAsAnalytic
*/
typedef enum {
  kA3DAnalyticNone,				/*!< No Curve */
  kA3DAnalyticCircle,			/*!< Changed to circle */
  kA3DAnalyticAlreadyCircle,	/*!< Kept as a circle */
  kA3DAnalyticLine,				/*!< Changed to Line */
  kA3DAnalyticAlreadyLine,		/*!< Kept as a line */
  kA3DAnalyticCone,				/*!< Changed to Cone */
  kA3DAnalyticAlreadyCone,		/*!< Kept as a cone */
  kA3DAnalyticCylinder,			/*!< Changed to Cylinder */
  kA3DAnalyticAlreadyCylinder,	/*!< Kept as a cylinder */
  kA3DAnalyticPlane,			/*!< Changed to Plane */
  kA3DAnalyticAlreadyPlane,		/*!< Kept as a plane */
  kA3DAnalyticSphere,			/*!< Changed to Sphere */
  kA3DAnalyticAlreadySphere,	/*!< Kept as a sphere */
  kA3DAnalyticTorus,			/*!< Changed to Torus */
  kA3DAnalyticAlreadyTorus		/*!< Kept as a torus */
} A3DEAnalyticType;

/*!
\defgroup a3d_hlr_curve_type Type of HLR curves result.
\ingroup a3d_HLR_Compute
\version 8.1
@{
*/
#define A3D_HLR_TYPE_UNKNOWN				0 /*!< Unknown type */
#define A3D_HLR_TYPE_EDGE					1 /*!< curve is a edge in the \ref A3DRiRepresentationItem input */
#define A3D_HLR_TYPE_SILHOUETTE				2 /*!< curve is a silhouette */
#define A3D_HLR_TYPE_SECTION				3 /*!< curve is a section of previous boolean operation*/
#define A3D_HLR_TYPE_SILHOUETTE_SECTION		4 /*!< curve is a silhouette and a section of previous boolean operation */
/*!
@} <!-- end of a3d_hlr_curve_type -->
*/



////////////////////////////////////////////////////////////////////////////////
// A3DSDKMisc
////////////////////////////////////////////////////////////////////////////////

/*!
\defgroup a3d_transformationbit Cartesian Transformation Types
\ingroup a3d_cartesiantransfo3d
\brief Bitmasks that can be OR'd together to specify the transformation behavior
\version 2.0

The bitmasks specify the transformations used in an \ref A3DMiscCartesianTransformation.
To specify multiple transformation behaviors, use a bitwise OR operation to combine the bitmasks for each transformation type.
\note Specifying the \ref kA3DTransformationIdentity flag precludes any other types of transformations.
That is, if you create a transformation value that ORs \ref kA3DTransformationIdentity and \ref kA3DTransformationRotate,
only the identity transformation is applied.

@{
*/
#define kA3DTransformationIdentity			0x00 /*!< Identity transformation. */
#define kA3DTransformationTranslate			0x01 /*!< Translate transformation */
#define kA3DTransformationRotate			0x02 /*!< Rotate transformation. */
#define kA3DTransformationMirror			0x04 /*!< Mirror transformation. */
#define kA3DTransformationScale				0x08 /*!< Scale transformation. */
#define kA3DTransformationNonUniformScale	0x10 /*!< Non-uniform scale transformation. */
/*!
@} <!-- end of module a3d_transformationbit -->
*/

/*!
\enum A3DMaterialPhysicType
\brief Material physical type properties
\ingroup a3d_misc_module
\version 9.0
*/
typedef enum
{
  A3DPhysicType_None,				/*!< If no material type set */
  A3DPhysicType_Fiber,          	/*!< Fiber Type */
  A3DPhysicType_HoneyComb,      	/*!< Honey Comb Type */
  A3DPhysicType_Isotropic,      	/*!< Isotropic Type */
  A3DPhysicType_Orthotropic2D,  	/*!< Orthotropic Type */
  A3DPhysicType_Orthotropic3D,  	/*!< Orthotropic 3D Type */
  A3DPhysicType_Anisotropic,    	/*!< Anisotropic Type */
} A3DMaterialPhysicType;



/*!
\enum A3DCollisionType
\brief Collision type between to \ref A3DRiRepresentationItem
\ingroup a3d_collision
\version 11.1
*/
typedef enum
{
	A3DCollision_Unknown 			= 0,	/*!< Cannot compute the result */
	A3DCollision_NoCollision 		= 1,	/*!< There is no colision or contact between the two \ref A3DRiRepresentationItem */
	A3DCollision_Clearance			= 2,	/*!< The safety distance is not verified between the two \ref A3DRiRepresentationItem */
	A3DCollision_Contact			= 3,	/*!< There is a contact between the two \ref A3DRiRepresentationItem */
	A3DCollision_Collision			= 4,	/*!< There is a collision between the two \ref A3DRiRepresentationItem */
	A3DCollision_FirstInside		= 5,	/*!< The first \ref A3DRiRepresentationItem is inside of the second \ref A3DRiRepresentationItem */
	A3DCollision_SecondInside		= 6		/*!< The second \ref A3DRiRepresentationItem is inside of the first \ref A3DRiRepresentationItem */
} A3DCollisionType;



/*!
\defgroup a3d_drawing_modelbit Value for undefined active sheet
\ingroup a3d_drawing_model
\brief This value determines the case where no active sheet has been set
\version 11.1

This case can happen with DWG files where the 3d model is active.
@{
*/
#define kA3DNoActiveSheet ~0 /*!< No active sheet defined. */
/*!
@} <!-- end of module a3d_drawing_modelbit -->
*/

/*!
\enum A3DEReadingMode2D3D
\brief This value determine if we load only 3D, only Drawings or both. Both only apply to DWG/DXF for the moment
\version 12.0
*/
typedef enum
{
	kA3DRead_3D = 0,		/*!< Read 3D Only */
	kA3DRead_Drawings = 1,	/*!< Read Drawings only */
	kA3DRead_Both = 2		/*!< Read 3D and Drawings */
} A3DEReadingMode2D3D;
/*!
\enum A3DEBasicUnit
\brief Unit enums

*/
typedef enum
{
	kA3DUnit_None = 0,              /*!< No Unit */
	kA3DUnit_Ampere = 1,            /*!< Ampere */
	kA3DUnit_Becquerel = 2,         /*!< Becquerel */
	kA3DUnit_Candela = 3,           /*!< Candela */
	kA3DUnit_Coulomb = 4,           /*!< Coulomb */
	kA3DUnit_Degree_celsius = 5,    /*!< Degree in Celsius */
	kA3DUnit_Fahrenheit = 6,        /*!< Fahrenheit */
	kA3DUnit_Farad = 7,             /*!< Farad */
	kA3DUnit_Foot = 8,              /*!< Foot */
	kA3DUnit_GallonUS = 9,          /*!< GallonUS */
	kA3DUnit_Gram = 10,             /*!< Gram */
	kA3DUnit_Gray = 11,             /*!< Gray */
	kA3DUnit_Henry = 12,            /*!< Henry */
	kA3DUnit_Hertz = 13,            /*!< Hertz */
	kA3DUnit_Hour = 14,             /*!< Hour */
	kA3DUnit_Inch = 15,             /*!< Inch */
	kA3DUnit_Joule = 16,            /*!< Joule */
	kA3DUnit_Kelvin = 17,           /*!< Kelvin */
	kA3DUnit_Liter = 18,            /*!< Liter */
	kA3DUnit_Lumen = 19,            /*!< Lumen */
	kA3DUnit_Lux = 20,              /*!< Lux */
	kA3DUnit_Metre = 21,            /*!< Metre */
	kA3DUnit_Minute = 22,           /*!< Minute */
	kA3DUnit_Mole = 23,             /*!< Mole */
	kA3DUnit_Newton = 24,           /*!< Newton */
	kA3DUnit_Ohm = 25,              /*!< Ohm */
	kA3DUnit_Pascal = 26,           /*!< Pascal */
	kA3DUnit_Radian = 27,           /*!< Radian */
	kA3DUnit_Rankine = 28,          /*!< Rankine */
	kA3DUnit_Second = 29,           /*!< Second */
	kA3DUnit_Siemens = 30,          /*!< Siemens */
	kA3DUnit_Sievert = 31,          /*!< Sievert */
	kA3DUnit_Steradian = 32,        /*!< Steradian */
	kA3DUnit_Tesla = 33,            /*!< Tesla */
	kA3DUnit_Volt = 34,             /*!< Volt */
	kA3DUnit_Watt = 35,             /*!< Watt */
	kA3DUnit_Weber = 36             /*!< Weber */
} A3DEBasicUnit;


/*!
\enum EA3DFCFIndicatorType
\brief feature control frame indicator type
\version 12.2
*/
typedef enum
{
	kA3DFCFIndicatorType_Undef = 0,              /*!< Undef */
	kA3DFCFIndicatorType_DirectionFeature,       /*!< DirectionFeature */
	kA3DFCFIndicatorType_CollectionPlane,        /*!< CollectionPlane */
	kA3DFCFIndicatorType_IntersectionPlane,      /*!< IntersectionPlane */
	kA3DFCFIndicatorType_OrientationPlane        /*!< OrientationPlane */
} EA3DFCFIndicatorType;



/*!
\enum EA3DFCFIndicatorSymbol
\brief Symbol define in the feature control frame indicator
\version 12.2
*/
typedef enum
{
  kA3DFCFIndicatorSymbol_Undef = 0,        /*!< Undef */
  kA3DFCFIndicatorSymbol_Angularity,       /*!< Angularity */
  kA3DFCFIndicatorSymbol_Perpendicularity, /*!< Perpendicularity */
  kA3DFCFIndicatorSymbol_Parallelism,      /*!< Parallelism */
  kA3DFCFIndicatorSymbol_Symmetry          /*!< Symmetry */
} EA3DFCFIndicatorSymbol;
#endif


