/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for tessellation
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifdef A3DAPI_LOAD
#  undef __A3DPRCTESSELLATION_H__
#endif
#ifndef __A3DPRCTESSELLATION_H__
#define __A3DPRCTESSELLATION_H__
#ifndef A3DAPI_LOAD
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKInitializeFunctions.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD

/*!
\defgroup a3d_tessellation_module Tessellation Module
\ingroup a3d_entitiesdata_module
\brief Creates and accesses tessellation entities

This module describes how to access and encode tessellation for a variety of entities.
It also provides information about tessellations, associated textures, and markup representations.

\par Usage
Tessellation entities can appear in the \c pTessBase member of all types of representation items,
except \ref A3DRiCoordinateSystem and \ref A3DRiPointSet.

If your application parses PRC content, you must develop dedicated functions to parse the base tessellation data
and to parse each type of tessellation entity. The following sample code demonstrates the following basic steps:
<ol>
	<li>Use the \ref A3DTessBaseGet function to get the tessellation base data.</li>
	<li>Use the \ref A3DEntityGetType function to determine the specific tessellation type.</li>
	<li>Invoke a custom function to parse the entity contents.</li>
</ol>
\par Sample code
\include TessellationModule.cpp
\sa \ref A3DTess3D
\sa \ref A3DTess3DWire
\sa \ref A3DTessMarkup
*/

/*!
\brief Functions for setting and getting data common to all tessellation entities
\defgroup a3d_tessellation_base Tessellation Base
\ingroup a3d_tessellation_module
\par

Entity type is \ref kA3DTypeTessBase.
@{
*/

/*!
\brief Structure for defining tessellation base data
\version	2.0

*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16		m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool			m_bIsCalculated;	/*!< A value of true indicates that during import tessellation was calculated from exact geometry. */
	A3DUns32		m_uiCoordSize;		/*!< The size of \ref m_pdCoords. */
	A3DDouble*		m_pdCoords;			/*!< Array of \ref A3DDouble. See \ref A3DTess3DData, \ref A3DTess3DWireData, or \ref A3DTessMarkupData for explanations. */
} A3DTessBaseData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DTessBaseData structure
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DTessBaseGet,(const A3DTessBase* pTessBase,
										 A3DTessBaseData* pData));

/*!
\brief Function to set the tessellation base from the \ref A3DTessBaseData structure. Results use a hash table to simplify the final tesselation.
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_TESSBASE_INCONSISTENT \n
\return \ref A3D_TESSBASE_POINTS_INCONSISTENT_DATA \n
\return \ref A3D_TESSBASE_POINTS_BAD_SIZE \n
\return \ref A3D_SUCCESS\n
*/
A3D_API (A3DStatus, A3DTessBaseSet,(A3DTessBase* pTessBase,
										 const A3DTessBaseData* pData));

/*!
\brief Function to set the tessellation base from the \ref A3DTessBaseData structure. Results do not use a hash table to simplify the final tesselation.
\version 10.2

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_TESSBASE_INCONSISTENT \n
\return \ref A3D_TESSBASE_POINTS_INCONSISTENT_DATA \n
\return \ref A3D_TESSBASE_POINTS_BAD_SIZE \n
\return \ref A3D_SUCCESS\n
*/
A3D_API(A3DStatus, A3DTessBaseSetNoHash, (A3DTessBase* pTessBase,
											const A3DTessBaseData* pData));
/*!
@}
*/


/*!
\defgroup a3d_tessface TessFace
\ingroup a3d_tess3d
\brief Functions and structures for defining tessellation face data

Entity type is \ref kA3DTypeTessFace.

This entity is used only within the \ref kA3DTypeTess3D entity and is never shared.
Therefore, no pointer is available for this entity type.
\sa A3DTessFaceData
*/

/*!
\struct A3DTessFaceData
\ingroup a3d_tessface
\brief Structure that describes basic entities of a face in a solid or surface mesh representations
\version 2.0

In the context of solid or surface mesh representations, an entity can be any
number of triangles. If more than one, they maybe organized as a fan or strip.

The \ref m_uiStyleIndexesSize member has these values:
\li 0 indicates that there are no graphics (in this case, all graphics are inherited
	from the owner (parent) of the \ref A3DTess3DData).
\li 1 indicates that one graphic is associated with the face.
\li Greater than 1 indicates that the number of graphics is linked to the number of entities
	in the \ref m_puiSizesTriangulated member.

\par Wireframe representation

The following \ref A3DTessFaceData members are used to describe a wireframe representation:
\li \ref m_uiStartWire : The starting index for the wire in the array of point indexes of the \ref A3DTess3DData.
\li \ref m_puiSizesWires : Array of the number of indexes for each wire edge of the face,
	where the number of indexes is OR'd with the relevant flags
	\ref kA3DTessFaceDataWireIsNotDrawn and/or \ref kA3DTessFaceDataWireIsClosing.

Consider an example of an \ref A3DTessFaceData structure in which both loops describe closed wire edges:
\li First loop consists of two wire edges:
- First edge has 16 points (`0x10`).
- Second edge has 32 points (`0x20`).
\li Second loop is one edge having 18 points (`0x12`).

For this example, the \ref m_puiSizesWires array has these entries:
`[0x0010, 0x8020, 0x8012]`.
The second and third array entries are the number of edges OR'd with the \ref kA3DTessFaceDataWireIsClosing (0x8000).
Notice that the indexes for each edge's extremity are always stored.
Therefore, the last point of the first edge is followed by the first point of the second edge.

\note In an \ref A3DTessFaceData structure, the size of a wire edge is limited to `16383` (`0x3FFF`) points.

\par Solid representation

The following \ref A3DTessFaceData members are for solid representations:
\li \ref m_usUsedEntitiesFlags (see \ref a3d_tessfacetype): This flag specifies how to interpret the array \ref m_puiSizesTriangulated.\n
\li \ref m_uiStartTriangulated : Starting index for the triangulated data in the array of point indexes of the \ref A3DTess3DData.\n
\li \ref m_puiSizesTriangulated : Array of the number of indexes for each triangulated entity of the face. \n

The triangulated data sizes are stored following the order of values in \ref a3d_tessfacetype.
Thus, the triangles are stored before triangle fans and before textured triangles.
\li For triangle types, the \ref m_puiSizesTriangulated member specifies the number of triangles.
\li For triangle fan and strips types, the \ref m_puiSizesTriangulated member specifies the number of fans or strips
	followed by the number of points for each of them.

Consider an example of an \ref A3DTessFaceData structure that describes five triangles,
two fans of 5 and 7 points respectively, and one strip of 11 indexes.
For this example, set \ref m_usUsedEntitiesFlags to \ref kA3DTessFaceDataTriangle & \ref kA3DTessFaceDataTriangleFan & \ref kA3DTessFaceDataTriangleStripe .
In this case, \ref m_puiSizesTriangulated will be [5,2,5,7,1,11].

In case of the tessellation types \b kA3DTessFaceData*OneNormal* (i.e., one of the bitmasks specifying a tessellation type with one normal),
the number of points can bear the flag \ref kA3DTessFaceDataNormalSingle,
modifying the way of reading normals information. In such a case, you must use \ref kA3DTessFaceDataNormalSingle to characterize
status and \ref kA3DTessFaceDataNormalMask to access the real value.

The following \ref A3DTessFaceData members also apply to solid representations:
\li \ref m_pucRGBAVertices : Colors stored directly on vertices. Either there is no color on vertices or every
vertex must bear a color. Therefore, \ref m_uiRGBAVerticesSize is either 0 or equal to the number of point indexes as calculated
from \ref m_puiSizesTriangulated, multiplied by 3 if RGB, by 4 if RGBA. (In the previous example, \ref m_uiRGBAVerticesSize is 38*(m_bIsRGBA?4:3) = (5*3 + 5 + 7 + 11)*(m_bIsRGBA?4:3)).
\li \ref m_bIsRGBA : If true, the color scheme is RGBa; otherwise, it is RGB.
\li \ref m_usBehaviour : This member denotes the graphics behavior for this \ref A3DTessFaceData towards owning entity in the tree,
as described in \ref a3d_graphicsbits.
For face behavior, \ref kA3DGraphicsShow and \ref kA3DGraphicsRemoved are not currently supported.
\li \ref m_uiTextureCoordIndexesSize : Number of texture coordinate indexes (see \ref a3d_tessfacetype).
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16	m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32	m_uiStyleIndexesSize;			/*!< The size of \ref m_puiStyleIndexes. */
	A3DUns32*	m_puiStyleIndexes;				/*!< Array of display styles. See explanations above. */
	A3DUns32	m_uiStartWire;					/*!< See explanations above. */
	A3DUns32	m_uiSizesWiresSize;				/*!< The size of \ref m_puiSizesWires. */
	A3DUns32*	m_puiSizesWires;				/*!< See explanations above. */
	A3DUns16	m_usUsedEntitiesFlags;			/*!< Bit field containing list of used entities in current face tessellation. See explanations above. \sa a3d_tessfacetype*/
	A3DUns32	m_uiStartTriangulated;			/*!< See explanations above. */
	A3DUns32	m_uiSizesTriangulatedSize;		/*!< The size of \ref m_puiSizesTriangulated. */
	A3DUns32*	m_puiSizesTriangulated;			/*!< See explanations above. */
	A3DBool		m_bIsRGBA;						/*!<
													A value of true specifies that the array is made of RGBA.
													A value of false indicates that it is made of RGB. */
	A3DUns32	m_uiRGBAVerticesSize;			/*!< The size of \ref m_pucRGBAVertices. */
	A3DUns8*	m_pucRGBAVertices;				/*!< See explanations above. */
	A3DUns16	m_usBehaviour;					/*!< See explanations above. */
	A3DUns32	m_uiTextureCoordIndexesSize;	/*!< See explanations above. */
} A3DTessFaceData;
#endif // A3DAPI_LOAD


/*!
\defgroup a3d_tess3d Tess3D
\ingroup a3d_tessellation_module
\brief Tessellation for solids and surfaces

Entity type is \ref kA3DTypeTess3D.

An \ref A3DTess3D is a tessellation dedicated to solids and surfaces. This structure is a derivation of \ref A3DTessBase
and can be obtained by using the \ref A3DTessBaseGet function on any \ref A3DRiRepresentationItem.
This structure may contain collections of points that define mesh vertices, normals (per vertex, per entity), and UV coordinates if
textures are stored.
*/

/*!
\brief Structure used to create or parse an \ref A3DTess3D entity
\ingroup a3d_tess3d
\version 2.0

\note The \ref A3DTess3D entity includes data stored in the \ref A3DTessBase entity.

Structure containing all global information for solid representation.

The \ref m_bHasFaces member indicates whether the \ref A3DTess3D entity is built with a concept of geometrical faces.

Point coordinates \ref A3DTessBaseData::m_pdCoords are taken 3 by 3 to build up 3D vectors.

Normal coordinates \ref m_pdNormals are taken 3 by 3 to build up 3D vectors.

Wire indexes in the \ref m_puiWireIndexes array are the point indexes describing the face's wire
contours in the array of points of \ref A3DTessBaseData::m_pdCoords.

Triangulated indexes \ref m_puiTriangulatedIndexes are the point, normal, and texture indexes describing the
face triangulated representation (triangles, triangle fans, triangle strips) in the array
of points, normals, and textures coordinates. The indexes are multiples of 3 for points and normals, and should be multiples of
1 to 4 for textures coordinates.

The contents of the \ref A3DTessFaceData structure specifies how to interpret the data in this array,
as described in \ref a3d_tessface.

The contents of the \ref A3DTessFaceData structure also  specifies
how the texture coordinates \ref m_pdTextureCoords are interpreted according to the
final graphics of each \ref A3DTessFaceData. This graphics is given either on the \ref A3DTessFaceData structure
or by the representation item owning the \ref A3DTess3DData. Then, this graphics  corresponds
to a texture with an appropriate number of coordinates.

An \ref A3DTessFaceData structure corresponds to a geometrical face if there is an indication that the tessellation has face data
(as denoted by \ref m_bHasFaces). Otherwise, it is simply a large container of any tessellated data.


\warning \ref A3DTessBaseData::m_uiCoordSize represents the size of the array referenced by \ref A3DTessBaseData::m_pdCoords.
It is not the number of 3D points.
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16			m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32			m_bHasFaces;					/*!< A value of true indicates the geometrical face notion. */
	A3DUns32			m_uiNormalSize;					/*!< The size of \ref m_pdNormals */
	A3DDouble*			m_pdNormals;					/*!< Array of \ref A3DDouble, \b x \b y \b z for each normal. */
	A3DUns32			m_uiWireIndexSize;				/*!< The size of \ref m_puiWireIndexes */
	A3DUns32*			m_puiWireIndexes;				/*!< Array of indexes of points in A3DTessBaseData::m_pdCoords. */
	A3DUns32			m_uiTriangulatedIndexSize;		/*!< The size of \ref m_puiTriangulatedIndexes */
	A3DUns32*			m_puiTriangulatedIndexes;		/*!< Array of indexes of points in A3DTessBaseData::m_pdCoords. */
	A3DUns32			m_uiFaceTessSize;				/*!< The size of \ref m_psFaceTessData */
	A3DTessFaceData*	m_psFaceTessData;				/*!< Array of face tessellation definition. */
	A3DUns32			m_uiTextureCoordSize;			/*!< The size of \ref m_pdTextureCoords */
	A3DDouble*			m_pdTextureCoords;				/*!< Array of \ref A3DDouble, as texture coordinates. */
	A3DBool				m_bMustRecalculateNormals;		/*!< Normals must be recalculated. */
	A3DUns8				m_ucNormalsRecalculationFlags;	/*!< Unused parameter. */
	A3DDouble			m_dCreaseAngle;					/*!< Unused parameter; definition similar to VRML. */
} A3DTess3DData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DTess3DData structure
\ingroup a3d_tess3d
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DTess3DGet,(const A3DTess3D* pTess,
									  A3DTess3DData* pData));

/*!
\brief Creates an \ref A3DTess3D from an \ref A3DTess3DData structure
\ingroup a3d_tess3d
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_TESS3D_NORMALS_INCONSISTENT_DATA \n
\return \ref A3D_TESS3D_NORMALS_BAD_SIZE \n
\return \ref A3D_TESS3D_FACE_INCONSISTENT_DATA \n
\return \ref A3D_TESSFACE_TRIANGULATED_INCONSISTENT_DATA \n
\return \ref A3D_TESSFACE_TRIANGULATED_INCONSISTENT_EMPTY \n
\return \ref A3D_TESSFACE_USEDENTITIES_BAD_TYPE \n
\return \ref A3D_TESSFACE_STARTTRIANGULATED_INCONSISTENT_DATA \n
\return \ref A3D_TESSFACE_STARTWIRE_INCONSISTENT_DATA \n
\return \ref A3D_TESSFACE_STYLEINDEXESSIZE_INCONSISTENT_DATA \n
\return \ref A3D_TESSFACE_RGBAVERTICESSIZE_INCONSISTENT_DATA \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DTess3DCreate,(const A3DTess3DData* pData,
										  A3DTess3D** ppTess));


/*!
\defgroup a3d_tess3dwire Tess3DWire
\ingroup a3d_tessellation_module
\brief Tessellation for 3D wireframes

Entity type is \ref kA3DTypeTess3DWire.

An \ref A3DTess3DWire is a tessellation dedicated to curves. This structure is a derivation of \ref A3DTessBase
and can be obtained by using the \ref A3DTessBaseGet function on any \ref A3DRiRepresentationItem entity.
*/

/*!
\defgroup a3d_tess3dwireflags Flags for Specifying Wire Characteristics
\ingroup a3d_tess3dwire
\brief Bitmasks used in the \ref A3DTess3DWireData structure to describe wire characteristics
\version 2.0

These bitmasks must be applied on the first unsigned int containing the number of related indexes in the array
used to described current wire.
@{
*/
#ifndef A3DAPI_LOAD
#define  kA3DTess3DWireDataIsClosing		0x10000000	/*!< The current wire is closed. */
#define  kA3DTess3DWireDataIsContinuous		0x20000000	/*!< Indicates that the last point of preceding wire should be linked with the first point of the current one. */
#endif // A3DAPI_LOAD
/*!
@}
*/

/*!
\struct A3DTess3DWireData
\brief 3D wire data structure
\ingroup a3d_tess3dwire
\version 2.0

\note The \ref A3DTess3DWire entity includes data stored in the \ref A3DTessBaseData structure.

The \ref A3DTessBaseData::m_pdCoords member represents point coordinates, which are interpreted in groups of three (X, Y, Z) to represent 3D vectors.\n\n
\ref A3DTess3DWireData enables you to organize these coordinates as wires. The A3DTess3DWireData structure uses the \ref A3DTessBaseData::m_pdCoords array to retrieve the point coordinates.\n\n
The \ref m_puiSizesWires member contains indexes referencing the array of points in \ref A3DTessBaseData::m_pdCoords. The index for each point is a multiple of 3 and refers to the first coordinate in the 3D point.
As an example, for a single 4-point wire the \ref m_puiSizesWires array would contain the following elements, with the first element representing the number of points in the wire and subsequent elements representing point indexes in \ref A3DTessBaseData::m_pdCoords : {number of indexes, indexes of the wire edge} =  {4, 0, 3, 6, 9}.\n\n
The \ref m_uiSizesWiresSize member represents the exact number of elements in the \ref m_puiSizesWires array. Our single 4-point wire contains 5 elements {4, 0, 3, 6, 9}, so \ref m_uiSizesWiresSize would be set to 5.\n\n
If \ref m_uiSizesWiresSize is set to zero, the tessellation is given as a single wire edge with the array of point indexes contained in \ref A3DTessBaseData::m_pdCoords.\n\n
If \ref m_uiSizesWiresSize contains multiple wires, then the array referenced by \ref m_puiSizesWires describes the edges as follows:\n\n


<center>![Wire Indexes](images/WireIndexes.png "Wire Indexes")</center>

If \ref m_uiSizesWiresSize is non-zero, \ref m_puiSizesWires can include the flags described in \link a3d_tess3dwireflags Flags for Specifying Wire Characteristics\endlink as follows:
[number of indexes | flag, indexes 1, indexes 2, ...] \n\n
\ref m_bIsRGBA indicates the color format used is RGBA. A value of true indicates the use of RGBA, while a value of false indicates the use of RGB.\n\n
\ref m_bIsSegmentColor : A value of true indicates the colors are for the entire segment.\n\n
\ref m_pucRGBAVertices specifies the color channels for each vertex / segment.\n\n
\ref m_uiRGBAVerticesSize specifies the size of the \ref m_pucRGBAVertices.\n\n
When the colors are for points (\ref m_bIsSegmentColor = false), the number of colors needed must be equal to the number of points.\n\n
When the colors are for segments (\ref m_bIsSegmentColor = true), the number of colors needed must be equal to the number of segments. So if a wire has 3 points, it will need 2 colors. If this 3-point wire is closed or continuous with a previous wire, it will need 3 colors.\n\n
The number of colors needed can be calculated:

\code
A3Duns32 iNumberColorNeeded = 0;

if (sTess3DWireData.m_bIsSegmentColor && !bIsWireClosed && !bIsWireContinuous)
{
	iNumberOfColorsNeeded = sData->m_uiSize - 1;
}
else
{
	iNumberOfColorsNeeded  = sData->m_uiSize;
}
\endcode
The member \ref m_uiRGBAVerticesSize is 3 times the number of colors needed for RGB, 4 times the number of colors needed for RGBA.
\code
sTess3DWireData.m_uiRGBAVerticesSize = (sTess3DWireData.m_bIsRGBA ? 4 : 3) * iNumberOfColorsNeeded;
\endcode


Example:
Consider the following example of two wires: one defined by 3 points, and another wire defined by 4 points.

<center>![Wire Segments](images/WireSegments.png "Wire Segments")</center>
\htmlonly <style>.WireList {padding-bottom:7px;}</style> \endhtmlonly
<ul>
	<li class="WireList">\ref m_uiSizesWiresSize = 9 = (1+3+1+4). There are two wires, one with 3 points and another wire of 4 points, each with a count element directly preceding the indexes, so 9 elements will be needed in the \ref m_puiSizesWires array.
	</li>
	<li class="WireList">\ref m_puiSizesWires = [3, index of a, index of b, index of c, 4, index of w, index of x, index of y, index of z]
	</li>
	<li class="WireList">If the last point of the first wire is linked to the first point of the second wire, the stored values are as follows:
	</li>
	<li class="WireList">\ref m_puiSizesWires = [3, index of a, index of b, index of c, 4 | \ref kA3DTess3DWireDataIsContinuous, index of w, index of x, index of y, index of z]
	</li>
	<li class="WireList">If the second wire is closed, the stored values are as follows:<br>
		\ref m_puiSizesWires = [3, index of a, index of b, index of c, 4 | \ref kA3DTess3DWireDataIsClosing, index of w, index of x, index of y, index of z]
	</li>
	<li class="WireList">If \ref m_bIsSegmentColor = false, colors are given for points.
		<ul>
			<li>\ref m_bIsRGBA = false, 3 channels are needed for 7 points. \ref m_uiRGBAVerticesSize = 7 * 3 = 21
			</li>
			<li>\ref m_bIsRGBA = true, 4 channels are needed for 7 points. \ref m_uiRGBAVerticesSize = 7 * 4 = 28
			</li>
		</ul>
	</li>
	<li class="WireList">If \ref m_bIsSegmentColor = true, colors are given for the entire segment.
		<ul>
			<li>If no wires are closed or continuous, and there are only 5 segments, then for RGB \ref m_uiRGBAVerticesSize = 5 * 3 = 15. For RGBA, \ref m_uiRGBAVerticesSize = 5 * 4 = 20
			</li>
			<li>If a wire is closed or continuous, there are 6 segments, so for RGB \ref m_uiRGBAVerticesSize = 6 * 3 = 18. For RGBA \ref m_uiRGBAVerticesSize = 6 * 4 = 24
			</li>
		</ul>
	</li>
</ul>
\warning Implicit points coming from \ref a3d_tess3dwireflags must also bear a color.
\warning \ref A3DTessBaseData::m_uiCoordSize represents size of array \ref A3DTessBaseData::m_pdCoords and not a number of 3D points.
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_uiSizesWiresSize;	/*!< The size of \ref m_puiSizesWires. */
	A3DUns32* m_puiSizesWires;		/*!< See explanations in \b Detailed \b Description. */
	A3DBool m_bIsRGBA;				/*!< See explanations in \b Detailed \b Description. */
	A3DBool m_bIsSegmentColor;		/*!< See explanations in \b Detailed \b Description. */
	A3DUns32 m_uiRGBAVerticesSize;	/*!< The size of \ref m_pucRGBAVertices. */
	A3DUns8* m_pucRGBAVertices;		/*!< See explanations above. */
} A3DTess3DWireData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DTess3DWireData structure
\ingroup a3d_tess3dwire
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DTess3DWireGet,(const A3DTess3DWire* pTess,
											A3DTess3DWireData* pData));
/*!
\brief Creates the \ref A3DTess3DWire from \ref A3DTess3DWireData structure
\ingroup a3d_tess3dwire
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_TESSWIRE_RGBAVERTICESSIZE_INCONSISTENT_DATA \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DTess3DWireCreate,(const A3DTess3DWireData* pData,
												A3DTess3DWire** ppTess));




/*!
\defgroup a3d_tessmarkup Tessellation for Markups
\ingroup a3d_tessellation_module
\brief Tessellation for markups

Entity type is \ref kA3DTypeTessMarkup.

The tessellation uses two arrays: one for the codes and another for the coordinates.
The codes array describes the type of contents in tessellation (the entities).
The coordinates array contains point coordinates and other floating point values used by the entities.
The coordinates array is specified in the \ref A3DTessBaseData structure.

Each entity occupies at least two codes. The first code contains the entity type and the number of specific codes.
The second code contains the number of doubles (coordinates) for the entity.
The doubles (coordinates) are in the coordinates array of the \ref A3DTessBaseData structure.

\par Explanation of the first code:

Use one of the following masks to identify the entity type with the first code:
\li \ref kA3DMarkupIsMatrix denotes a matrix.
\li \ref kA3DMarkupIsExtraData indicates other entities (extra data).
\li \ref kA3DMarkupExtraDataType is the type of extra data.

If none of these masks are set, the entity is a polyline.

\ref kA3DMarkupIsMatrix must not be set if \ref kA3DMarkupIsExtraData is set and \ref kA3DMarkupExtraDataType represents
the type of entity only if \ref kA3DMarkupIsExtraData is set.<br>
The \ref kA3DMarkupIntegerMask mask represents the number of codes that refers to the entity (inner codes).<br>
\ref kA3DMarkupIsMatrix and \ref kA3DMarkupIsExtraData are booleans; whereas, \ref kA3DMarkupExtraDataType is
an integer.<br>
The other bits should be set to zero.

See \ref a3d_tessmarkupflags and \ref a3d_tessmarkupextradata for more details.

\par Explanation of the second code:

The second code is the number of doubles needed by the entity.
Each time the first double of block refers to updated position within
BaseData Coord array: starting from 0 if reading a matrix first and next double value to read will be at Coord[16].

The following table describes the inner codes and doubles that can be defined for each entity.
It also describes the extra data type number.


\verbatim
Entity              Extra data type   Number of inner codes     Number of doubles
---------------------------------------------------------------------------------------------
Polyline            none              0                         number of points * 3
Matrix mode[1]      none              0 or number in block      0 or number in block
Pattern                0              3 + number of loops       number of points in loops * 3
Picture                1              1                         0
Triangles              2              0                         number of triangles * 9
Quads                  3              0                         number of quads * 12
Face view mode[1]      6              0 or number in block      0 or number in block
Frame draw mode[1]     7              0 or number in block      0 or number in block
Fixed size mode[1]     8              0 or number in block      0 or number in block
Symbol                 9              1                         3
Cylinder              10              0                         3
Color                 11              1                         0
Line stipple[2]       12              0 or 1                    0
Font                  13              2                         0
Text                  14              1                         2
Points                15              0                         number of points * 3
Polygon               16              0                         number of points * 3
Line width[2]         17              0                         0 or 1
\endverbatim
See \ref A3D_ENCODE_EXTRA_DATA macro to encode extra data.

\par About Blocks

Blocks are defined by face view, frame draw, fixed size, and matrix modes (indicated by [1]).
Each block is surrounded by the corresponding entity. At the start of a block, the entity modifies
the state (style, current transformation matrix, ...). Then this state is restored at the end of the block.
For example, a matrix mode starts by defining a matrix (which will multiply the current transformation matrix),
draws some entities, and ends with another matrix mode entity for indicating the end of the mode.

[1]
The face view, frame draw, fixed size, and matrix modes start with the corresponding entity
and end with the same entity; therefore, they define blocks.
The starting entity has a non-zero number of inner codes. It represents the number of codes until the end of the block,
excluding the 2 mandatory codes for each entity. The same rule applies to the doubles.
The ending entity has 0 inner codes and 0 doubles.
The use of this number is to easily skip a block when reading tessellation.
To treat the content of a block, the numbers to use are as follows:

\verbatim
Mode                      Number of inner codes          Number of doubles
--------------------------------------------------------------------------------
Face view (starting)      0                              3
Face view (ending)        0                              0
Frame draw (starting)     0                              3
Frame draw (ending)       0                              0
Fixed size (starting)     0                              3
Fixed size (ending)       0                              0
Matrix (starting)         0                              16
Matrix (ending)           0                              0
\endverbatim

For example: matrix mode
\verbatim
        --+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--
Codes     |  |M1|16|C |C |C |C |C |C |C |C |C |C |C |M2|0 |
        --+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--
\endverbatim

M1 code is \ref kA3DMarkupIsMatrix (0 additional code), which is the same as M2.
This example shows the codes for defining a matrix mode. The M1 code defines the beginning
of the matrix mode, and the M2 code defines the closing of that mode. The number of doubles for the M1 code is 16.
After that, some entities are drawn (the Cs) and the M2 code defines the end of the matrix mode.
The M2 code has 0 codes and 0 doubles because it is only reverting back to the initial matrix.

[2]
The line stipple and line width modes operate identically to [1], but the numbers
correspond to the entity only and not to the block. This is because the treatment
of those can be more easily omitted than for the previous ones.
For line stipple mode, the number of inner codes denotes the start (1) or the end (0) of the block.
For line width mode, the number of doubles denotes the start (1) or the end (0) of the block.

\par Explanation of each entity:

The array for each entity is organized as described here:
\li First column shows the mandatory codes and the inner codes
\li Second column shows the doubles

\b Polyline

\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
0                                 X
number of points * 3              Y
                                  Z
\endverbatim
There is a (x,y,z) triplet for each point of the polyline.

\b Triangles

A list of triangles.
\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
0                                 X
number of triangles * 9           Y
                                  Z
\endverbatim
There is a (x,y,z) triplet for each point of the triangle list.

\b Quads

A list of quads.
\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
0                                 X
number of quads * 12              Y
                                  Z
\endverbatim
There is a (x,y,z) triplet for each point of the quad list.

\b Polygon

\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
0                                 X
number of points * 3              Y
                                  Z
\endverbatim
There is a (x,y,z) triplet for each point of the polygon.

\b Points

A list of points.
\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
0                                 X
number of points * 3              Y
                                  Z
\endverbatim
There is a (x,y,z) triplet for each point.

<b>Face view mode</b>

In this mode, all the drawing entities are parallel to the screen (billboard).
The point given in the doubles corresponds to the origin of the new coordinate system in
which we draw parallel to the screen.
\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
0 or number in block              X
0 or number in block              Y
                                  Z
\endverbatim

<b>Frame draw mode</b>

In this mode, all the drawing entities are given in 2D space.
The point given in the doubles corresponds to a 3D point projected onto the screen giving
origin of 2D coordinate system in which we draw in 2D (viewport).
\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
0 or number in block              X
0 or number in block              Y
                                  Z
\endverbatim

<b>Fixed size mode</b>

In this mode, all the drawing entities are drawn at a fixed size, independent of the zoom setting.
The point given in the doubles corresponds to the origin of the new coordinate system in which
we draw at fixed size.
\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
0 or number in block              X
0 or number in block              Y
                                  Z
\endverbatim

<b>Matrix mode</b>

In this mode, all the drawing entities are transformed by the current transformation matrix
multiplied by the matrix given in the doubles.
At the end of the mode, the previously active transformation matrix is restored.
\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
0 or number in block              a(1,1)
0 or number in block              a(2,1)
                                  a(3,1)
                                  a(4,1)
                                  a(1,2)
                                  a(2,2)
                                  ...
                                  a(3,4)
                                  a(4,4)
\endverbatim

\b Symbol

The point given in the doubles corresponds to the position of the symbol in 3D.
\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
1                                 X
3                                 Y
id of pattern                     Z
\endverbatim
The pattern is an \ref A3DGraphVPicturePatternData type.
The pattern identifier is obtained by invoking the \ref A3DGlobalInsertGraphVPicturePattern function.

\b Color

This entity defines a color that will be effective until a new one is defined.
\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
1
0
id of color
\endverbatim
Color identifier is obtained by invoking the \ref A3DGlobalInsertGraphRgbColor function.

<b>Line stipple mode</b>

This entity defines the style that will be effective inside the block.
\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
0 or 1
0
id of style
\endverbatim
The first code is 1 for beginning the block and 0 for ending the block.
The style identifier is obtained by invoking the \ref A3DGlobalInsertGraphStyle function.

\b Font

This entity defines the font used for the Text entity (described next).
\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
2
0
family index
style index/size index/attributes
\endverbatim
The indexes and the attributes are taken from the \ref A3DFontKeyData structure.
To create the \ref A3DFontKeyData structure, invoke the \ref A3DGlobalFontKeyCreate function.

The font style, size indexes, and attributes are encoded into a single code
by using the bitmasks in \ref a3d_tessmarkupfontkeydef. See \ref a3d_fonts.

\b Text

This entity defines a text string to be rendered with the current font (defined by \ref A3DFontData entity).
\sa \ref a3d_fonts

\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
1                                 W
2                                 H
index of text
\endverbatim
The index of text refers to the zero-based text index in \ref A3DTessMarkupData::m_ppcTexts.<br>
The \b W and \b H correspond to the width and height (respectively) of text in real display coordinates.

<b>Line width mode</b>

This entity defines the line width that will be effective inside the block.
\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
0                                 W
0 or 1
\endverbatim
The number of doubles is 1 for beginning the block and 0 for ending the block.
The \b W double is the line width to use in the block.
There is no \b W double when ending the block.

\b Cylinder

\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
0                                 Base radius
3                                 Top radius
                                  Height
\endverbatim
The cylinder is positioned by a matrix mode, oriented with Z axis, base at Z = 0 and top at Z = Height.

\b Picture

This entity defines a picture positioned at the current position.
\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
1
0
id of picture
\endverbatim
Picture identifier is obtained by invoking the \ref A3DGlobalInsertGraphPicture function.

\b Pattern

\verbatim
Codes                             Doubles
--------------------------------------------------------------------------------
3 + number of loops               X
number of points in loops * 3     Y
id of pattern                     Z
filled mode
behavior
number of points for loop 1
...
number of points for loop n
\endverbatim
The filled mode is one of these values: \c 0=OR, \c 1=AND, or \c 2=XOR.
The behavior is a bit field, with the 0x1 bit specifying whether to ignore the view transformation.
If true, the pattern is not transformed by the current view transformation.
The other bits should be set to zero.
There is a (x,y,z) triplet for each point of loops, sequentially.

Pattern identifier is created by invoking one of the following functions:
\li \ref A3DGlobalInsertGraphDottingPattern
\li \ref A3DGlobalInsertGraphHatchingPattern
\li \ref A3DGlobalInsertGraphSolidPattern
\li \ref A3DGlobalInsertGraphVPicturePattern
*/


/*!
\brief TessMarkupData structure
\ingroup a3d_tessmarkup
\version 2.0

The \ref A3DTessMarkup entity contains data stored in the \ref A3DTessBaseData structure.
For this structure, the \ref A3DTessBaseData::m_bIsCalculated is not applicable and must be set to false.

For information about the values for the \ref m_cBehaviour member, see \ref a3d_tessmarkupdef.

Text referenced by the \ref m_ppcTexts member must be UTF-8 encoded.
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_uiCodesSize;		/*!< The size of \ref m_puiCodes. */
	A3DUns32* m_puiCodes;		/*!< Array of codes. See explanations in \ref a3d_tessmarkup. */
	A3DUns32 m_uiTextsSize;		/*!< The size of \ref m_ppcTexts. */
	A3DUTF8Char** m_ppcTexts;	/*!< Texts used in tessellation. See explanations in \ref a3d_tessmarkup. */
	A3DUTF8Char* m_pcLabel;		/*!< Markup label. */
	A3DInt8 m_cBehaviour;		/*!< Flags for tessellation. See \ref a3d_tessmarkupdef. */
} A3DTessMarkupData;
#endif // A3DAPI_LOAD


/*!
\brief Populates the \ref A3DTessMarkupData structure
\ingroup a3d_tessmarkup
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DTessMarkupGet,(const A3DTessMarkup* pTess,
											A3DTessMarkupData* pData));

/*!
\brief Creates an \ref A3DTessMarkup entity from an \ref A3DTessMarkupData structure
\ingroup a3d_tessmarkup
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_TESSMARKUP_HAS_INVALID_FONTKEY \n
\return \ref A3D_TESSMARKUP_HAS_INVALID_TEXT_INDEX \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DTessMarkupCreate,(const A3DTessMarkupData* pData,
												A3DTessMarkup** ppTess));

/*!< \ingroup a3d_tessmarkup */


#endif	/*	__A3DPRCTESSELLATION_H__ */
