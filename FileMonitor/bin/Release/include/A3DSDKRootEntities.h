/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Root entities section
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/
#ifdef A3DAPI_LOAD
#  undef __A3DPRCROOTENTITIES_H__
#endif
#ifndef __A3DPRCROOTENTITIES_H__
#define __A3DPRCROOTENTITIES_H__
#ifndef A3DAPI_LOAD
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKInitializeFunctions.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD


/*!
\defgroup a3d_rootentities_module Root Entities Module
\ingroup a3d_entitiesdata_module
\brief Creates and accesses root entities that can be associated with any PRC entity or with any graphics PRC entity

This module defines the root entity attributes that apply to all PRC entities
or to all PRC entities in the \ref a3d_graphics_module.

All PRC entities declared by \COMPONENT_A3D_API are of type \ref A3DEntity.
All \ref A3DEntity entities inherit the \ref A3DRootBase entity,
which can include text names that provide modeller data and other data about the
more specific form of the PRC entities.

PRC entities in the \ref a3d_graphics_types also inherit the \ref A3DRootBaseWithGraphics entity.
The \ref A3DRootBaseWithGraphics entity has attributes that reference
graphics data stored in \ref A3DGlobal PRC entities and that specify
inheritance behaviour.
*/

/*!
@} <!-- a3d_rootentities_module -->
*/


/*!
\defgroup a3d_entitytype_functions Entity Type Determination
\ingroup a3d_entitytypes_module
\brief Determines the type of a PRC entity
*/

/*!
\ingroup a3d_entitytype_functions
\brief Gets the actual type of the entity

This function returns an integer that specifies an Entity type.
The integer corresponds to one of the values described by the \ref A3DEEntityType enumeration.

\version 2.0

\par Sample code
\include TypeCastingFunctions.cpp

\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n

\sa A3DEntity
*/
A3D_API(A3DStatus, A3DEntityGetType, (const A3DEntity* pEntity,
	A3DEEntityType* peEntityType));

/*!
\ingroup a3d_entitytype_functions
\brief Indicates whether an entity base type corresponds to the \ref kA3DTypeRootBaseWithGraphics type enumeration.

This function returns a value of <code>TRUE</code>
if an entity type is based on the abstract root type
for a PRC entity that can represent graphic data.
Such an abstract root type is identified by the \ref kA3DTypeRootBaseWithGraphics type enumeration.

\remark Because this function does not return A3DStatus the internal thread safety policy is to wait for the availability of the API.

\version 2.0
You may need to use this function to sort entities for particular treatments.

*/
A3D_API(A3DBool, A3DEntityIsBaseWithGraphicsType, (const A3DEntity* pEntity));

/*!
\ingroup a3d_entitytype_functions
\brief Indicates whether an entity base type corresponds to the \ref kA3DTypeRootBase type enumeration

This function returns a value of <code>TRUE</code>
if an entity type is based on the abstract root type
for any PRC entity (with or without graphic data).
Such an abstract root type is identified by the \ref kA3DTypeRootBase type enumeration.

\remark Because this function does not return A3DStatus the internal thread safety policy is to wait for the availability of the API.

\version 2.0
You may need to use this function to sort entities for particular treatments.

*/
A3D_API(A3DBool, A3DEntityIsBaseType, (const A3DEntity* pEntity));


/*!
\defgroup a3d_rootbase Entity Base
\ingroup a3d_rootentities_module
\brief Creates and accesses a hierarchy of descriptive names and modeller data that can be applied to any PRC entity

Entity type is \ref kA3DTypeRootBase

This module lets you create a hierarchy of descriptive names and modeller data (called <i>root-level attributes</i>)
that can be applied to any PRC entity. These attributes are packaged as an \ref A3DRootBase entity that is referenced
from the PRC entity they describe.

The following sample code shows how to create root-level attributes for any PRC entity.
In this case, the attributes include modeller data.
For restrictions on specifying modeller data, see \ref A3DMiscAttributeData.

\include MiscAttributeCreation.cpp

The following illustration shows the result of the sample code.

The sample code creates an \ref A3DRootBase entity that houses three \ref A3DMiscAttribute entities,
each of which reference an \ref A3DMiscSingleAttributeData structure.
Each \ref A3DMiscSingleAttributeData structure provides modeller data of type \ref kA3DModellerAttributeTypeString.

\image html PRC_RootAttributes.png

\sa a3d_root_types


*/

/*!
\brief A structure representing a hierarchy of descriptive names and modeler data that applies to any PRC entity
\ingroup a3d_rootbase
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_uiSize;					/*!< The size of \ref m_ppAttributes. */
	A3DMiscAttribute** m_ppAttributes;	/*!< Array of pointers to \ref A3DMiscAttribute structures. \sa a3d_attribute */
	A3DUTF8Char* m_pcName;				/*!< String containing entity name. */
	A3DUns32 m_uiPersistentId;			/*!< Persistent ID. \version 2.2 */
	A3DUTF8Char* m_pcPersistentId;		/*!< Persistent ID other version, as CLSID, Name... */
	A3DUns32 m_uiNonPersistentId;		/*!< Non persistent ID. \version 2.2 */
} A3DRootBaseData;
#endif // A3DAPI_LOAD

/*!
\brief Populates an \ref A3DRootBaseData structure with the data from a PRC entity
\ingroup a3d_rootbase
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRootBaseGet,(const A3DRootBase* pRootBase,
												A3DRootBaseData* pData));

/*!
\brief Adds an \ref A3DRootBaseData structure to an existing PRC entity.

According to what is defined in the parameter pData, the A3DEntity will be updated.
<ul>
<li>If A3DRootBaseData::m_pcName is initialized, the entity will be renamed. </li>
<li>Persistent and non-persistent id's are systematically replaced. </li>
<li>The array A3DRootBaseData::m_ppAttributes will be appended to the current attribute array of the Entity. To manipulate just the Entity attributes, please use the following functions:
\ref A3DRootBaseAttributeRemoveAll, \ref A3DRootBaseAttributeRemove, \ref A3DRootBaseAttributeRemoveAt, \ref A3DRootBaseAttributeAppend, \ref A3DRootBaseAttributeAdd.</li>
</ul>

\ingroup a3d_rootbase
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
\note \ref A3DRootBase is an abstract class and cannot be directly created;
however, any entity created with a function of the form <code>A3D<i>Entity_name</i>Create</code> results
in the creation of a PRC entity of type \c A3DRootBase and of type <code>A3D<i>Entity_name</i></code>.
*/
A3D_API (A3DStatus, A3DRootBaseSet,(A3DRootBase* pRootBase,
												const A3DRootBaseData* pData));


/*!
\brief Remove all attributes stored in a PRC entity
\ingroup a3d_rootbase
\version 11.1

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_BASE_BAD_ENTITY_TYPE
\return \ref A3D_SUCCESS \n
\note \ref A3DRootBase is an abstract class and cannot be directly created;
however, any entity created with a function of the form <code>A3D<i>Entity_name</i>Create</code> results
in the creation of a PRC entity of type \c A3DRootBase and of type <code>A3D<i>Entity_name</i></code>.
*/
A3D_API(A3DStatus, A3DRootBaseAttributeRemoveAll, (A3DRootBase* pEntity));



/*!
\brief Remove attribute in the attribute array of PRC entity with a specific title
\ingroup a3d_rootbase
\version 11.1

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_BASE_BAD_ENTITY_TYPE
\return \ref A3D_SUCCESS \n
\note \ref A3DRootBase is an abstract class and cannot be directly created;
however, any entity created with a function of the form <code>A3D<i>Entity_name</i>Create</code> results
in the creation of a PRC entity of type \c A3DRootBase and of type <code>A3D<i>Entity_name</i></code>.
*/
A3D_API(A3DStatus, A3DRootBaseAttributeRemove,(A3DRootBase* pEntity, A3DUTF8Char* pcTitle));


/*!
\brief Remove attribute of the current attribute array stored in a PRC entity, at a specific position
\ingroup a3d_rootbase
\version 11.1

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_BASE_BAD_ENTITY_TYPE
\return \ref A3D_SUCCESS \n
\note \ref A3DRootBase is an abstract class and cannot be directly created;
however, any entity created with a function of the form <code>A3D<i>Entity_name</i>Create</code> results
in the creation of a PRC entity of type \c A3DRootBase and of type <code>A3D<i>Entity_name</i></code>.
*/
A3D_API(A3DStatus, A3DRootBaseAttributeRemoveAt,(A3DRootBase* pEntity, A3DUns32 uIndex));


/*!
\brief Append attributes to the current attribute array stored in a PRC entity
\ingroup a3d_rootbase
\version 11.1

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_BASE_BAD_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
\note \ref A3DRootBase is an abstract class and cannot be directly created;
however, any entity created with a function of the form <code>A3D<i>Entity_name</i>Create</code> results
in the creation of a PRC entity of type \c A3DRootBase and of type <code>A3D<i>Entity_name</i></code>.
*/
A3D_API(A3DStatus, A3DRootBaseAttributeAppend, (A3DRootBase* pEntity, A3DUns32 uiSize, A3DMiscAttribute** m_ppAttributes));


/*!
\brief Add an attribute to the current attribute array stored in a PRC entity
\ingroup a3d_rootbase
\version 11.1

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_BASE_BAD_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
\note \ref A3DRootBase is an abstract class and cannot be directly created;
however, any entity created with a function of the form <code>A3D<i>Entity_name</i>Create</code> results
in the creation of a PRC entity of type \c A3DRootBase and of type <code>A3D<i>Entity_name</i></code>.
*/
A3D_API(A3DStatus, A3DRootBaseAttributeAdd, (A3DRootBase* pEntity, A3DUTF8Char* pcTitle, A3DUTF8Char* pcValue));


/*!
\defgroup a3d_rootbasewithgraphics Entity Base with Graphics
\ingroup a3d_rootentities_module
\brief Creates and accesses global graphic characteristics that apply to any PRC entity in the \ref a3d_graphics_module
\version 11.1

Entity type is \ref kA3DTypeRootBaseWithGraphics.

Any PRC entity that bears graphics can have an \ref A3DRootBaseWithGraphics entity.

The \ref A3DRootBaseWithGraphics entity references graphic attributes
such as line patterns, RGB colors, and textures.
Those graphic attributes are used in the more specific PRC entity.

\sa a3d_graphics

*/

/*!
\brief A structure specifying root graphics data
\ingroup a3d_rootbasewithgraphics
\version 11.1

*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DGraphics* m_pGraphics;	/*!< Associated graphics. \sa a3d_graphics */
} A3DRootBaseWithGraphicsData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DRootBaseWithGraphicsData structure
\ingroup a3d_rootbasewithgraphics
\version 11.1

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRootBaseWithGraphicsGet,(const A3DRootBaseWithGraphics* pRoot,
																A3DRootBaseWithGraphicsData* pData));

/*!
\brief Adds an \ref A3DRootBaseWithGraphicsData structure to an existing \ref A3DRootBaseWithGraphics entity
\ingroup a3d_rootbasewithgraphics
\version 11.1

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
\note The \ref A3DRootBaseWithGraphics entity type is an abstract class and cannot be directly created;
however, any graphic entity created with a function of the form <code>A3D<i>Graphics_entity_name</i>Create</code> results
in the creation of a PRC entity of type \c A3DRootBaseWithGraphics and of type <code>A3D<i>Entity_name</i></code>.
That is, a function of the form <code>A3D<i>Graphics_entity_name</i>Create</code>
adds specific data to the base class, for example \ref A3DRootBaseWithGraphicsData::m_pGraphics.
*/
A3D_API (A3DStatus, A3DRootBaseWithGraphicsSet,(A3DRootBaseWithGraphics* pRoot,
																const A3DRootBaseWithGraphicsData* pData));




/*!
\defgroup a3d_attribute Miscellaneous Attribute Entity
\ingroup a3d_rootbase
\brief Creates and accesses descriptive names and modeller data associated with PRC entities
\version 11.1

Entity type is \ref kA3DTypeMiscAttribute.

This structure defines the miscellaneous attributes owned by an \ref A3DRootBase entity.

*/

/*!
\brief A structure that specifies modeler data attribute
\ingroup a3d_attribute
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bTitleIsInt;				/*!< A value of TRUE indicates the \ref m_pcTitle member represents an integer. */
	A3DUTF8Char* m_pcTitle;				/*!< Title as string. */
	A3DEModellerAttributeType m_eType;	/*!< Attribute type. */
	A3DUTF8Char* m_pcData;				/*!< Data chunk to be interpreted according to previous type. */
	A3DUns16 m_usUnit;                  /*!< Index in Unit array if m_eType==kA3DModellerAttributeTypeReal. A3D_DEFAULT_NO_UNIT means no unit associated \version 12.1 */
} A3DMiscSingleAttributeData;
#endif // A3DAPI_LOAD

/*!
\brief A structure that specifies descriptive names and modeller data
\ingroup a3d_attribute
\version 2.0

The `A3DMiscAttributeData` structure specifies a descriptive name as a character string or integer.
It can also reference an array of `A3DMiscSingleAttributeData` structure, which specifies modeler data.

If `m_bTitleIsInt` is `A3DTrue`, `m_pcTitle` will be parsed as an `A3DUns32`.

\code
A3DVoid setAttributesSample(A3DEntity* pEntity)
{
	A3DMiscAttribute* pAttributes[2];

	// ----------------------------------------------------------------
	// creation of the first A3DMiscAttributeData

	A3DMiscSingleAttributeData pSingleAttributes[2];
	// creation of the first A3DMiscSingleAttribute
	A3DMiscSingleAttributeData sSingleAttributeData1;
	A3D_INITIALIZE_DATA(A3DMiscSingleAttributeData, sSingleAttributeData1);
	sSingleAttributeData1.m_eType = kA3DModellerAttributeTypeString;
	sSingleAttributeData1.m_pcTitle = (char*) "First single attribute";
	sSingleAttributeData1.m_pcData = (char*) "Simple attribute 1 demonstration";
	pSingleAttributes[0] = sSingleAttributeData1;
	// creation of the second A3DMiscSingleAttribute
	A3DMiscSingleAttributeData sSingleAttributeData2;
	A3D_INITIALIZE_DATA(A3DMiscSingleAttributeData, sSingleAttributeData2);
	sSingleAttributeData2.m_eType = kA3DModellerAttributeTypeString;
	sSingleAttributeData2.m_pcTitle = (char*) "Second single attribute";
	sSingleAttributeData2.m_pcData = (char*) "Simple attribute 2 demonstration";
	pSingleAttributes[1] = sSingleAttributeData2;

	// creation of the first A3DMiscAttribute
	A3DMiscAttributeData sAttributeData1;
	A3D_INITIALIZE_DATA(A3DMiscAttributeData, sAttributeData1);
	sAttributeData1.m_pcTitle = "First attribute";
	sAttributeData1.m_bTitleIsInt = false;
	sAttributeData1.m_pSingleAttributesData = pSingleAttributes;
	sAttributeData1.m_uiSize = 2;
	A3DMiscAttributeCreate(&sAttributeData1, &pAttributes[0]);

	// ----------------------------------------------------------------
	// creation of the second A3DMiscAttribute
	A3DMiscAttributeData sAttributeData2;
	A3D_INITIALIZE_DATA(A3DMiscAttributeData, sAttributeData2);
	A3DUns32 uiTitleAsInt = 12345;
	sAttributeData2.m_pcTitle = (A3DUTF8Char*)(&uiTitleAsInt);
	sAttributeData2.m_bTitleIsInt = true;
	sAttributeData2.m_uiSize = 0;
	A3DMiscAttributeCreate(&sAttributeData2, &pAttributes[1]);


	// ----------------------------------------------------------------
	// creation of the A3DRootBase
	A3DRootBaseData sRootData;
	A3D_INITIALIZE_DATA(A3DRootBaseData, sRootData);
	sRootData.m_pcName = (char*) "Root base information";
	sRootData.m_ppAttributes = pAttributes;
	sRootData.m_uiSize = 2;
	A3DRootBaseSet(pEntity, &sRootData);

	for(A3DUns32 i = 0; i < sRootData.m_uiSize; ++i)
	{
		A3DEntityDelete(sRootData.m_ppAttributes[i]);
	}
}
\endcode
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;								// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bTitleIsInt;									/*!< A value of `A3DTrue` indicates that `m_pcTitle` member represents an integer. */
	A3DUTF8Char* m_pcTitle;									/*!< Title as string. */
	A3DUns32 m_uiSize;										/*!< Size of m_asSingleAttributesData. */
	A3DMiscSingleAttributeData* m_asSingleAttributesData;	/*!< Pointer to an array of single attribute structures. */
} A3DMiscAttributeData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMiscAttributeData structure
\ingroup a3d_attribute
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMiscAttributeGet,(	const A3DMiscAttribute* pAttribute,
														A3DMiscAttributeData* pData));


/*!
\brief Creates an \ref A3DMiscAttribute from \ref A3DMiscAttributeData structure
\ingroup a3d_attribute
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMiscAttributeCreate,(	const A3DMiscAttributeData* pData,
															A3DMiscAttribute** ppAttribute));

#ifndef A3DAPI_LOAD
/*!
\brief A structure that specifies base unit information
\ingroup a3d_attribute

&nbsp;
*/

typedef struct
{
	A3DUns16 m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DEBasicUnit m_eUnit;          /*!<  &nbsp; */
	A3DInt32 m_iExponent;			/*!<  ie : 2 for square , 3 for cubic. May be negative for a "per" unit; ie : km/h */
	A3DDouble m_dFactor;            /*!<  ie : 1000 for millimeter */
} A3DMiscAttributeBasicUnitData;
#endif // A3DAPI_LOAD

/*!
\brief A structure that specifies unit information
\ingroup a3d_attribute
\version 12.1

The \ref A3DMiscUnitData structure specifies information for units used by Metadata.
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcName;							/*!<  String containing unit name. */
	A3DUns32 m_uiBasicUnitSize;						/*!<  size of m_asBasicUnitData */
	A3DMiscAttributeBasicUnitData** m_ppBasicUnits;		/*!< Array of pointers to \ref A3DMiscAttributeBasicUnitData structures. \sa a3d_attribute */
} A3DMiscAttributeUnitData;
#endif // A3DAPI_LOAD

/*!
\brief Get unit information related to a \ref A3DMiscSingleAttributeData structure
\ingroup a3d_attribute
\version 12.1

\return \ref A3D_INVALID_UNIT_INDEX \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DGlobalGetUnit,(const A3DUns32 uiIndexUnit, A3DMiscAttributeUnit** ppUnit));

/*!
\brief Get unit information related to a \ref A3DMiscSingleAttributeData structure
\ingroup a3d_attribute
\version 12.1

\return \ref A3D_INVALID_UNIT_INDEX \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DGlobalGetUnitData, (A3DMiscAttributeUnit* const pUnit, A3DMiscAttributeUnitData *pData));

#endif	/*	__A3DPRCROOTENTITIES_H__ */
