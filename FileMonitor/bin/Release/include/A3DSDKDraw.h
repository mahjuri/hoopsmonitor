/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for the draw module
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/
#ifdef A3DAPI_LOAD
#  undef __A3DPRCDRAWMODELFILE_H__
#endif
#ifndef __A3DPRCDRAWMODELFILE_H__
#define __A3DPRCDRAWMODELFILE_H__
#ifndef A3DAPI_LOAD
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKGraphics.h>
#  include <A3DSDKInitializeFunctions.h>
#  include <A3DSDKMarkup.h>
#  include <A3DSDKTexture.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD



#ifndef A3DAPI_LOAD
/*!
\defgroup a3d_draw_module Draw Module
\deprecated The Draw module is deprecated.
\ingroup a3d_tools_module

\brief Draws model file entities using callback functions you provide.

The functions and callback functions in this module allow you to
draw PRC model files using drawing functions that you provide.
*/

/*!
\defgroup a3d_draw_functions_pointers Callback-Function Type Definitions
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_module
*/

/*!
\brief Pushes the current matrix onto the stack.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawPushMatrix)(void);
/*!
\brief Pops the matrix off the stack.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawPopMatrix)(void);
/*!
\brief Multiplies the matrix on the top of the stack by another matrix.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawMultMatrix)(const A3DDouble adMatrix[16]);
/*!
\brief Begins drawing.
\deprecated The Draw module is deprecated.
\version 2.0
The \c pcName argument can be NULL if there is no name.
The \c uiTriangleCount argument is meaningful only
when the \c eType argument has a value of \c kA3DDrawBeginEndRepresentationItem;
otherwise, its value is 0.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DCallbackDrawBegin)(A3DEDrawBeginEndType eType, const A3DUTF8Char* pcName, A3DUns32 uiTrianglesCount);
/*!
\brief Ends drawing.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawEnd)(A3DEDrawBeginEndType eType);
/*!
\brief Returns all the points of a representation item tessellation.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawSetTessellationPoints)(const A3DVector3dData* pasPoint, A3DUns32 uiPointsSize);
/*!
\brief Projects the point.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawProject)(const A3DVector3dData* psPoint, A3DVector3dData* psResult);
/*!
\brief Un-projects the point.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawUnProject)(const A3DVector3dData* psPoint, A3DVector3dData* psResult);
/*!
\brief Draws a list of triangles.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0

Each point of each triangle has its own normal.

*/
typedef void (*A3DCallbackDrawTriangle)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, A3DUns32 uiPointsSize);
/*!
\brief Draws a fan of triangles.
\deprecated The Draw module is deprecated.
\version 2.0

Each point of the fan has its own normal.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DCallbackDrawTriangleFan)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, A3DUns32 uiPointsSize);
/*!
\brief Draws a triangle strip.
\deprecated The Draw module is deprecated.
\version 2.0

Each point of the strip has its own normal.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DCallbackDrawTriangleStripe)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, A3DUns32 uiPointsSize);
/*!
\brief Draws a list of triangles where each triangle has only one normal.
\deprecated The Draw module is deprecated.
\version 2.0

Each triangle has only one normal. Therefore, the number of normals is \c uiPointsSize/3.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DCallbackDrawTriangleOneNormal)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, A3DUns32 uiPointsSize);
/*!
\brief Draws a fan of triangles with one normal.
\deprecated The Draw module is deprecated.
\version 2.0

The fan has only one normal, psNormal.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawTriangleFanOneNormal)(const A3DVector3dData* psNormal, const A3DVector3dData* pasPoints, A3DUns32 uiPointsSize);
/*!
\brief Draws a triangle strip with one normal.
\deprecated The Draw module is deprecated.
\version 2.0

The strip has only one normal, which is identified by the \c psNormal argument.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DCallbackDrawTriangleStripeOneNormal)(const A3DVector3dData* psNormal, const A3DVector3dData* pasPoints, A3DUns32 uiPointsSize);
/*!
\brief Draws a list of textured triangles.
\deprecated The Draw module is deprecated.
\version 2.0

Each point of each triangle has its own normal.
\note Textures are not yet implemented.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DCallbackDrawTriangleTextured)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, A3DUns32 uiPointsSize);
/*!
\brief Draws a fan of textured triangles
\deprecated The Draw module is deprecated.
\version 2.0

Each point of the fan has its own normal.
\note Textures are not yet implemented.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DCallbackDrawTriangleFanTextured)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, A3DUns32 uiPointsSize);
/*!
\brief Draws a triangle strip.
\deprecated The Draw module is deprecated.
\version 2.0

Each point of the strip has its own normal.
\note Textures are not yet implemented.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DCallbackDrawTriangleStripeTextured)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, A3DUns32 uiPointsSize);
/*!
\brief Draws a list of textured triangles where each triangle has only one normal.
\deprecated The Draw module is deprecated.
\version 2.0

Each triangle has only one normal. Therefore, the number of normals is \c uiPointsSize/3.
\note Textures are not yet implemented.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DCallbackDrawTriangleOneNormalTextured)(const A3DVector3dData* pasNormals, const A3DVector3dData* pasPoints, A3DUns32 uiPointsSize);
/*!
\brief Draws a fan of textured triangles, where each triangle has only one normal.
\deprecated The Draw module is deprecated.
\version 2.0

The fan has only one normal, which is \c psNormal.
\note Textures are not yet implemented.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DCallbackDrawTriangleFanOneNormalTextured)(const A3DVector3dData* psNormal, const A3DVector3dData* pasPoints, A3DUns32 uiPointsSize);
/*!
\brief Draws a triangle strip with one normal.
\deprecated The Draw module is deprecated.
\version 2.0

The strip has only one normal, which is returned in the \c psNormal argument.
\note Textures are not yet implemented.
\ingroup a3d_draw_functions_pointers
*/
typedef void (*A3DCallbackDrawTriangleStripeOneNormalTextured)(const A3DVector3dData* psNormal, const A3DVector3dData* pasPoints, A3DUns32 uiPointsSize);
/*!
\brief Defines the color to be used for all subsequent entities.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 6.1
*/
typedef void (*A3DCallbackDrawColor)(const A3DDouble adPoints[3]);

/*!
\deprecated The Draw module is deprecated.
\brief Defines the material to be used for all subsequent entities.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawMaterial)(A3DEDrawMaterialType eType, const A3DDouble* pdValues, A3DUns32 uiValuesSize);
/*!
\brief Store current materials of the environment.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawBeginMaterial)(void);
/*!
\brief Restore materials of the environment previously stored.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawEndMaterial)(void);
/*!
\brief Requests the projection, modelview matrix and the viewport. (See classical Open GL definition for more information.)
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawGetDrawContext)(A3DDouble adProjection[16], A3DDouble adModelView[16], A3DInt32 aiViewport[4]);
/*!
\brief Draws a list of triangles without normals, for markups.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawMarkupTriangle)(const A3DDouble* pdPoints, A3DUns32 uiPointSize);
/*!
\brief Sets the environment to draw with screen coordinates.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawBeginFrameDraw)(const A3DVector3dData* psPoint3d, A3DBool bIsZoomable, A3DDouble dFixedSize);
/*!
\brief Ends the draw with screen coordinates.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawEndFrameDraw)(void);
/*!
\brief Sets the environment to draw with a fixed size.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawBeginFixedSize)(const A3DVector3dData* psPoint3d);
/*!
\brief Ends the draw with fixed size.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawEndFixedSize)(void);
/*!
\brief Draws a cylinder.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawCylinder)(A3DDouble dBaseRadius, A3DDouble dTopRadius, A3DDouble dHeight);
/*!
\brief Draws a polygon.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawPolygon)(const A3DDouble* pdPoints, A3DUns32 uiPointSize);
/*!
\brief Sets the environment to draw with a line width.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawBeginLineWidth)(A3DDouble dWidth);
/*!
\brief Ends the draw with a line width.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawEndLineWidth)(void);
/*!
\brief Draws a list of points.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawPoint)(const A3DDouble* pdPoints, A3DUns32 uiPointSize);
/*!
\brief Defines a font.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawFont)(const A3DFontKeyData* psFontKeyData);
/*!
\brief Sets the environment to draw with a line stipple.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawBeginLineStipple)(const A3DGraphStyleData* psGraphStyleData);
/*!
\brief Ends the draw with a line stipple.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawEndLineStipple)(void);
/*!
\brief Draws a symbol at the 3D position.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawSymbol)(const A3DGraphVPicturePatternData* psPatternData, const A3DVector3dData* psPosition);
/*!
\brief Draws a polyline.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawPolyLine)(const A3DDouble* pdPoints, A3DUns32 uiPointSize);
/*!
\brief Draws a text at current position.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawText)(const A3DUTF8Char* pcBuffer, A3DDouble dWidth, A3DDouble dHeight);
/*!
\brief Draws a pattern.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawPattern)(A3DUns32 uiLoopsSize, A3DUns32 uiPatternId, A3DUns32 uiFilledMode, A3DUns32 uiBehaviour, const A3DDouble* pdPoints, const A3DUns32* puiLoopsPointSize);
/*!
\brief Draws a picture at current position.
\deprecated The Draw module is deprecated.
\ingroup a3d_draw_functions_pointers
\version 2.0
*/
typedef void (*A3DCallbackDrawPicture)(const A3DGraphPictureData* psPictureData);
#endif // A3DAPI_LOAD
/*!
\deprecated The Draw module is deprecated.
\brief Structure for specifying callback functions for drawing.
\ingroup a3d_draw_module
\version 2.0

Use this structure to define the callback functions \COMPONENT_A3D_LIBRARY will use to draw a model file entity.
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DCallbackDrawPushMatrix m_pfuncPushMatrix; /*!< \copydoc A3DCallbackDrawPushMatrix */
	A3DCallbackDrawPopMatrix m_pfuncPopMatrix; /*!< \copydoc A3DCallbackDrawPopMatrix */
	A3DCallbackDrawMultMatrix m_pfuncMultMatrix; /*!< \copydoc A3DCallbackDrawMultMatrix  */
	A3DCallbackDrawBegin m_pfuncBegin; /*!< \copydoc A3DCallbackDrawBegin */
	A3DCallbackDrawEnd m_pfuncEnd; /*!< \copydoc A3DCallbackDrawEnd */
	A3DCallbackDrawSetTessellationPoints m_pfuncSetTessellationPoints; /*!< \copydoc A3DCallbackDrawSetTessellationPoints */
	A3DCallbackDrawTriangle m_pfuncTriangle; /*!< \copydoc A3DCallbackDrawTriangle */
	A3DCallbackDrawTriangleFan m_pfuncTriangleFan; /*!< \copydoc A3DCallbackDrawTriangleFan */
	A3DCallbackDrawTriangleStripe m_pfuncTriangleStripe; /*!< \copydoc A3DCallbackDrawTriangleStripe */
	A3DCallbackDrawTriangleOneNormal m_pfuncTriangleOneNormal; /*!< \copydoc A3DCallbackDrawTriangleOneNormal */
	A3DCallbackDrawTriangleFanOneNormal m_pfuncTriangleFanOneNormal; /*!< \copydoc A3DCallbackDrawTriangleFanOneNormal */
	A3DCallbackDrawTriangleStripeOneNormal m_pfuncTriangleStripeOneNormal; /*!< \copydoc A3DCallbackDrawTriangleStripeOneNormal */
	A3DCallbackDrawTriangleTextured m_pfuncTriangleTextured; /*!< \copydoc A3DCallbackDrawTriangleTextured */
	A3DCallbackDrawTriangleFanTextured m_pfuncTriangleFanTextured; /*!< \copydoc A3DCallbackDrawTriangleFanTextured */
	A3DCallbackDrawTriangleStripeTextured m_pfuncTriangleStripeTextured; /*!< \copydoc A3DCallbackDrawTriangleStripeTextured */
	A3DCallbackDrawTriangleOneNormalTextured m_pfuncTriangleOneNormalTextured; /*!< \copydoc A3DCallbackDrawTriangleOneNormalTextured */
	A3DCallbackDrawTriangleFanOneNormalTextured m_pfuncTriangleFanOneNormalTextured; /*!< \copydoc A3DCallbackDrawTriangleFanOneNormalTextured */
	A3DCallbackDrawTriangleStripeOneNormalTextured m_pfuncTriangleStripeOneNormalTextured; /*!< \copydoc A3DCallbackDrawTriangleStripeOneNormalTextured */
	A3DCallbackDrawMaterial m_pfuncMaterial; /*!< \copydoc A3DCallbackDrawMaterial */
	A3DCallbackDrawGetDrawContext m_pfuncGetDrawContext; /*!< \copydoc A3DCallbackDrawGetDrawContext */
	A3DCallbackDrawMarkupTriangle m_pfuncMarkupTriangle; /*!< \copydoc A3DCallbackDrawMarkupTriangle */
	A3DCallbackDrawUnProject m_pfuncUnProject; /*!< \copydoc A3DCallbackDrawUnProject */
	A3DCallbackDrawBeginFrameDraw m_pfuncBeginFrameDraw; /*!< \copydoc A3DCallbackDrawBeginFrameDraw */
	A3DCallbackDrawEndFrameDraw m_pfuncEndFrameDraw; /*!< \copydoc A3DCallbackDrawEndFrameDraw */
	A3DCallbackDrawBeginFixedSize m_pfuncBeginFixedSize; /*!< \copydoc A3DCallbackDrawBeginFixedSize */
	A3DCallbackDrawEndFixedSize m_pfuncEndFixedSize; /*!< \copydoc A3DCallbackDrawEndFixedSize */
	A3DCallbackDrawCylinder m_pfuncCylinder; /*!< \copydoc A3DCallbackDrawCylinder */
	A3DCallbackDrawPolygon m_pfuncPolygon; /*!< \copydoc A3DCallbackDrawPolygon */
	A3DCallbackDrawBeginLineWidth m_pfuncBeginLineWidth; /*!< \copydoc A3DCallbackDrawBeginLineWidth */
	A3DCallbackDrawEndLineWidth m_pfuncEndLineWidth; /*!< \copydoc A3DCallbackDrawEndLineWidth */
	A3DCallbackDrawPoint m_pfuncPoint; /*!< \copydoc A3DCallbackDrawPoint */
	A3DCallbackDrawFont m_pfuncFont; /*!< \copydoc A3DCallbackDrawFont */
	A3DCallbackDrawBeginLineStipple m_pfuncBeginLineStipple; /*!< \copydoc A3DCallbackDrawBeginLineStipple */
	A3DCallbackDrawEndLineStipple m_pfuncEndLineStipple; /*!< \copydoc A3DCallbackDrawEndLineStipple */
	A3DCallbackDrawSymbol m_pfuncSymbol; /*!< \copydoc A3DCallbackDrawSymbol */
	A3DCallbackDrawPolyLine m_pfuncPolyLine; /*!< \copydoc A3DCallbackDrawPolyLine */
	A3DCallbackDrawText m_pfuncText; /*!< \copydoc A3DCallbackDrawText */
	A3DCallbackDrawPattern m_pfuncPattern; /*!< \copydoc A3DCallbackDrawPattern */
	A3DCallbackDrawPicture m_pfuncPicture; /*!< \copydoc A3DCallbackDrawPicture */
	A3DCallbackDrawBeginMaterial m_pfuncBeginMaterial; /*!< \copydoc A3DCallbackDrawBeginMaterial */
	A3DCallbackDrawEndMaterial m_pfuncEndMaterial; /*!< \copydoc A3DCallbackDrawEndMaterial */
	A3DCallbackDrawColor m_pfuncColor; /*!< \copydoc A3DCallbackDrawColor */
} A3DDrawCallbacksData;
#endif // A3DAPI_LOAD

/*!
\deprecated The Draw module is deprecated.
\brief Initializes the callbacks used for drawing
\ingroup a3d_draw_module
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DDrawInitCallbacks,( A3DDrawCallbacksData* psCallbacks));

/*!
\deprecated The Draw module is deprecated.
\brief Draws the model file entities, using the callbacks defined by \ref A3DDrawInitCallbacks
\ingroup a3d_draw_module
\version 2.0

To set the \c uiDrawFlags argument, use the flags defined in \ref a3d_drawflags.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DDraw,(const A3DAsmModelFile* pModelFile, A3DUns32 uiDrawFlags));


/*!
\deprecated The Draw module is deprecated.
\brief Draws the representation item entities, using the callbacks defined by \ref A3DDrawInitCallbacks
\ingroup a3d_draw_module
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DDrawRepresentationItem,(	const A3DRiRepresentationItem* pRepItem, const A3DMiscCascadedAttributes* pFatherAttr));


/*!
\deprecated The Draw module is deprecated.
\brief Draws the markup item entities, using the callbacks defined by \ref A3DDrawInitCallbacks
\ingroup a3d_draw_module
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DDrawMarkup, (const A3DMkpMarkup* pMarkup, const A3DMiscCascadedAttributes* pFatherAttr));

/*!
\deprecated The Draw module is deprecated.
\brief Calculates the bounding box of the model file entity, without using any callback functions
\version 2.0

To set the \c uiDrawFlags argument, use the flags defined in \ref a3d_drawflags.
\ingroup a3d_draw_module
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DDrawGetBoundingBox,(const A3DAsmModelFile* pModelFile, A3DBoundingBoxData* psBoundingBox, A3DUns32 uiDrawFlags));

#endif // #ifndef __A3DPRCDRAWMODELFILE_H__
