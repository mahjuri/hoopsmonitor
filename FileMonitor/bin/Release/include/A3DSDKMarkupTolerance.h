/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for the tolerance markup module
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/


#ifdef A3DAPI_LOAD
#  undef __A3DPRCMARKUPTOLERANCE_H__
#endif

#ifndef __A3DPRCMARKUPTOLERANCE_H__
#define __A3DPRCMARKUPTOLERANCE_H__

#ifndef A3DAPI_LOAD

#include <A3DSDKEnums.h>
#include <A3DSDKErrorCodes.h>
#include <A3DSDKInitializeFunctions.h>
#include <A3DSDKMarkupDefinition.h>
#include <A3DSDKTypes.h>
#include <A3DSDKMarkupDimension.h>

/*!
\struct A3DMarkupDatumData
\ingroup a3d_markupdatum
\brief Markup datum
\details
This structure stored the definition of markups datum identifier and markups datum target according the several options
\image html pmi_datum.png
*/
typedef struct
{
	A3DUns16						m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char*					m_pcUpText;			/*!< Up text. */
	A3DUTF8Char*					m_pcDownText;		/*!< Down text. */
	A3DBool							m_bTarget;			/*!< Option: Is target? */
	A3DBool							m_bDiameter;		/*!< Option: Is diameter? */
	A3DBool							m_bIndividual;		/*!< Option: Is individual? */
	A3DMDTextProperties*			m_pTextProperties;	/*!< Pointer to the text properties. \sa A3DMDTextPropertiesGet */
	A3DUTF8Char*					m_pcAboveText;					/*!< Above text in RTF format. See \ref a3d_markup_rtf.\version 12.2 */
	A3DUTF8Char*					m_pcBelowText;					/*!< Below text in RTF format. See \ref a3d_markup_rtf.\version 12.2 */
	A3DUTF8Char*					m_pcBeforeText;					/*!< Before text in RTF format. See \ref a3d_markup_rtf.\version 12.2 */
	A3DUTF8Char*					m_pcAfterText;					/*!< After text in RTF format. See \ref a3d_markup_rtf.\version 12.2*/
}A3DMarkupDatumData;


/*!
\struct A3DMDFCFDraftingRowData
\ingroup a3d_markupfcfdftrow
\brief FCF drafting row
\sa A3DMDFCFDraftingRowGet
*/
typedef struct
{
	A3DUns16			m_usStructSize;				// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	EA3DGDTType			m_eType;					/*!< GDT type. */
	A3DUns32			m_uiNumberOfTextsInBoxes;	/*!< Number of texts in boxes. */
	A3DUTF8Char**		m_ppcTextsInBoxes;			/*!< Texts in row boxes, code with RTF format. See \ref a3d_markup_rtf. */
}A3DMDFCFDraftingRowData;




/*!
\struct A3DMDFCValueData
\ingroup a3d_markupfcf_semanticrow
\brief Tolerance value. Value can be double or string according to m_bIsValue.
\sa A3DMDFCFToleranceValueData
*/
typedef struct
{
	A3DUns16				m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble					m_dValue;	/*!< Value. */
	A3DUTF8Char*			m_pcValue;		/*!< User value. */
	A3DBool					m_bIsValue;		/*!< If it's true, value is user value. */
}A3DMDFCValueData;


/*!
\struct A3DMDFCTolerancePerUnitData
\ingroup a3d_markupfcf_semanticrow
\brief Tolerance per unit
\sa AA3DMDFCTolerancePerUnitData
*/
typedef struct
{
	A3DUns16				m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble				m_dUnitLengthOrAreaFirstValue;	/*!< Length 1. */
	A3DDouble*				m_pdUnitAreaSecondLength;		/*!< Length 2: if non null, it defines the second value of the area. */
	A3DMDFCValue*			m_psRefinementZone;				/*!< Refinement zone. */
}A3DMDFCTolerancePerUnitData;



/*!
\struct A3DMDFCProjectedZoneData
\ingroup a3d_markupfcf_semanticrow
\brief Projected tolerance zone
\sa AA3DMDFCProjectedZoneData
*/
typedef struct
{
	A3DUns16				m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DMDFCValueData		m_sLength;			/*!< Length. */
	A3DDouble*				m_pdPosition;		/*!< Position minimum dimension. */
}A3DMDFCProjectedZoneData;



/*!
\struct A3DMDFCFToleranceValueData
\ingroup a3d_markupfcf_semanticrow
\brief FCF tolerance value
\sa A3DMDFCFToleranceValueData
*/
typedef struct
{
	A3DUns16						m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DMDFCValueData				m_sValue;				/*!< Global value. */
	A3DMDFCTolerancePerUnit*		m_psTolerancePerUnit;	/*!< Tolerance per unit definition. */
	A3DMDFCProjectedZone*			m_psProjectedZone;		/*!< Projected tolerance zone definition. */
	A3DMDFCValue*					m_psProfileTolerance;	/*!< Precision of the profile tolerance. */
	A3DMDFCValue*					m_psMaximumBonus;		/*!< Maximum bonus. */

	A3DMDGDTValueType				m_eValueType;		/*!< Value type. */
	EA3DMDGDTModifierType			m_eModifier;		/*!< Modifier type. */
	A3DBool							m_bFreeState;		/*!< Free state. */
	A3DBool							m_bStatistical;		/*!< Statistical. */

	A3DMDFCFToleranceValue*			m_psNext;			/*! Second tolerance value */
}A3DMDFCFToleranceValueData;


/*!
\struct A3DMDFCFRowDatumData
\ingroup a3d_markupfcfrow
\brief FCF datum row
\sa A3DMDFCFRowDatumData
*/
typedef struct
{
	A3DUns16						m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char*					m_pcDatum;			/*!< Datum label. */
	EA3DMDGDTModifierType			m_eModifier;		/*!< Modifier type. */
	A3DMDFCFRowDatum*				m_pNext;			/*!< Next datum, non null in case of composite datum. */
	A3DMiscMarkupLinkedItem* m_pLinkedItem;				/*!< Represents the link with an entity. It must be one of owner (markup GDT or reference frame) of linked item array (see A3DMkpMarkupData). */
}A3DMDFCFRowDatumData;


/*!
\struct A3DMDFCFDrawingRowData
\ingroup a3d_markupfcfrow
\brief FCF drawing row
\sa A3DMDFCFDrawingRowData
\version 4.0
*/
typedef struct
{
	A3DUns16					m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	EA3DGDTType					m_eType;			/*!< GDT type. */
	A3DUTF8Char*				m_pcValue;			/*!< Value. */
	A3DMDFCFToleranceValue*		m_psSemanticValue;	/*!< Semantic value. */

	A3DMDFCFRowDatum*			m_psPrimaryRef;		/*!< Primary reference. */
	A3DMDFCFRowDatum*			m_psSecondaryRef;	/*!< Secondary reference. */
	A3DMDFCFRowDatum*			m_psTertiaryRef;	/*!< Tertiary reference. */
}A3DMDFCFDrawingRowData;



/*!
\struct A3DMDFCFIndicatorData
\ingroup a3d_markupfcf
\brief Feature control frame indicator
\version 12.2
*/
typedef struct
{
	A3DUns16			m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	EA3DFCFIndicatorType m_eType;			/*!< Type. */
	EA3DFCFIndicatorSymbol m_eSymbol;		/*!< Symbol. */
	A3DMDFCFRowDatum*			m_pDatum;	/*!< Datum reference. */
}A3DMDFCFIndicatorData;


/*!
\struct A3DMDFeatureControlFrameData
\ingroup a3d_markupfcf
\brief Feature control frame
*/
typedef struct
{
	A3DUns16			m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32			m_uiNumberOfRows;	/*!< Number of rows. */
	A3DMDFCFRow**		m_ppRows;			/*!< Array of rows. */
	A3DUns32			m_uiNumberOfIndicators;	/*!< Number of indicators. \version 12.2 */
	A3DMDFCFIndicator**		m_ppIndicators;		/*!< Array of indicators. \version 12.2 */
}A3DMDFeatureControlFrameData;


/*!
\struct A3DMDToleranceSizeValueData
\ingroup a3d_markuptolerancesizevalue
\brief tolerance size value.
*/
typedef struct
{
	A3DUns16							m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	EA3DMDDimensionType					m_eType;				/*!< Dimension type. */
	A3DMDDimensionValueData				m_sMainValue;			/*!< Main Value. */
	A3DMDDimensionValue*				m_pDualValue;			/*!< Dual Value. */
	A3DDouble							m_dValue;				/*!< Value of the tolerance size.*/
	A3DUTF8Char*						m_pcSeparator;			/*!< Separator with previous A3DMDToleranceSizeValueData.*/
	EA3DMDDimensionSymbolType			m_eSymbol;				/*!< Optional symbol before the value. */
}A3DMDToleranceSizeValueData;

/*!
\struct A3DMDToleranceSizeData
\ingroup a3d_markuptolerancesize
\brief tolerance size.
*/
typedef struct
{
	A3DUns16			m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32			m_uiNumberOfSizeValues;	/*!< Number of A3DMDToleranceSizeValue on the row. */
	A3DMDToleranceSizeValue** m_ppTolSizeValue;	/*!< Array of A3DMDToleranceSizeValue. */
}A3DMDToleranceSizeData;


/*!
\struct A3DMarkupGDTData
\brief Markup geometrical and dimensioning tolerance
\ingroup a3d_markupgdt
\brief Markup geometrical dimensioning tolerance data structure
*/
typedef struct
{
	A3DUns16						m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char*					m_pcAboveText;					/*!< Above text in RTF format. See \ref a3d_markup_rtf. */
	A3DUTF8Char*					m_pcBelowText;					/*!< Below text in RTF format. See \ref a3d_markup_rtf. */
	A3DUTF8Char*					m_pcBeforeText;					/*!< Before text in RTF format. See \ref a3d_markup_rtf. */
	A3DUTF8Char*					m_pcAfterText;					/*!< After text in RTF format. See \ref a3d_markup_rtf. */
	A3DUns32						m_uiNumberOfMarkupTolerances;	/*!< Number of tolerances. */
	A3DMDTolerance**				m_ppsMarkupTolerances;			/*!< Array of tolerances. The tolerance can be of two types :
																	 - A3DMDFeatureControlFrame;
																	 - A3DMDToleranceSize, which allow to define all the dimension of an element.
																		For example for a hole we can have a tolerance size that will contain the value of the diameter, the value of the depth and the angle defining the V shape at the bottom of the hole.*/
	A3DMDTextProperties*			m_pTextProperties;				/*!< Pointer to the text properties. \sa A3DMDTextPropertiesGet */
}A3DMarkupGDTData;

#endif



/*!
\defgroup a3d_markupdatum Datum
\ingroup a3d_markuptolerance
\version 4.0
*/


/*!
\fn A3DStatus A3DMarkupDatumGet( const A3DMarkupDatum* pMarkupDatum, A3DMarkupDatumData* pData)
\brief Populates the \ref A3DMarkupDatumData structure
\ingroup a3d_markupdatum
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMarkupDatumGet,( const A3DMarkupDatum* pMarkupDatum,
												A3DMarkupDatumData* pData));







/*!
\defgroup a3d_markupgdt Geometrical dimensioning tolerance
\ingroup a3d_markuptolerance
\version 4.0
\details
A markup GDT is composed of appended texts and an array of tolerances. Tolerances that can be of two types :
 - features control frame tolerance (ref A3DMDFeatureControlFrameData).
\image html pmi_markup_gdt.png
 - Tolerances Sizes (ref A3DMDToleranceSizeData). which allow to define all the dimension of an element.
	For example for a hole we can have a tolerance size that will contain the value of the diameter, the value of the depth and the angle defining the V shape at the bottom of the hole.
\image html pmi_markup_gdt_tolerance_size.png
*/

/*!
\defgroup a3d_markuptolerancesize Tolerance size
\ingroup a3d_markupgdt
\version 12.0
\details
Tolerances sizes contain all the dimensions of a line in the GDT. So for a GDT containing tolerances sizes values on two lines, we will have two tolerances sizes.
\image html pmi_markup_tolerance_size.png
*/

/*!
\defgroup a3d_markuptolerancesizevalue Tolerance size value
\ingroup a3d_markuptolerancesize
\version 12.0
\details
The tolerances sizes values contain the information to define a tolerance value. for example the depth of a hole.
For this we have:
- the value
- the type of dimension
- the main value (defines the display and the tolerance of the value)
- the dual value (defines the display and the tolerance of the dual value)
- the string of characters separating the value from the previous value.
- The symbol in front of the value.
\image html pmi_markup_tolerance_size_value.png
*/

/*!
\defgroup a3d_markupfcf Feature control frame
\ingroup a3d_markupgdt
\version 4.0
\details
see in the following description of a feature control frame with two rows
\image html pmi_markup_fcf_row.png
*/

/*!
\defgroup a3d_markupfcfrow Drawing row
\ingroup a3d_markupfcf
\version 4.0
\details
\image html pmi_markup_fcf_drawing_row.png
*/

/*!
\defgroup a3d_markupfcf_semanticrow Semantic drawing row value
\ingroup a3d_markupfcfrow
\version 5.2
*/
/*!
\defgroup a3d_markupfcfdftrow Drafting row
\ingroup a3d_markupfcf
\version 4.0
\details
\image html pmi_markup_fcf_drafting_row.png
*/



/*!
\fn A3DStatus A3DMDFCFDraftingRowGet( const A3DMDFCFDraftingRow* pFDraftingRow, A3DMDFCFDraftingRowData* pData)
\brief Populates the \ref A3DMDFCFDraftingRowData structure
\ingroup a3d_markupfcfdftrow
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDFCFDraftingRowGet,( const A3DMDFCFDraftingRow* pDraftingRow,
		 A3DMDFCFDraftingRowData* pData));







/*!
\fn A3DStatus A3DMDFCValueGet( const A3DMDFCValue* pToleranceValue, A3DMDFCValueData* pData)
\brief Populates the \ref A3DMDFCValueData structure
\ingroup a3d_markupfcf_semanticrow
\version 5.2


\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDFCValueGet,( const A3DMDFCValue* pValue,
		 A3DMDFCValueData* pData));


/*!
\fn A3DStatus A3DMDFCTolerancePerUnitGet( const A3DMDFCTolerancePerUnit* pTolerancePerUnit, A3DMDFCTolerancePerUnitData* pData)
\brief Populates the \ref A3DMDFCTolerancePerUnitData structure
\ingroup a3d_markupfcf_semanticrow
\version 5.2


\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDFCTolerancePerUnitGet,( const A3DMDFCTolerancePerUnit* pTolerancePerUnit,
		 A3DMDFCTolerancePerUnitData* pData));



/*!
\fn A3DStatus A3DMDFCProjectedZoneGet( const A3DMDFCProjectedZone* pProjectedZone, A3DMDFCProjectedZoneData* pData)
\brief Populates the \ref A3DMDFCProjectedZoneData structure
\ingroup a3d_markupfcf_semanticrow
\version 5.2


\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDFCProjectedZoneGet,( const A3DMDFCProjectedZone* pProjectedZone,
		 A3DMDFCProjectedZoneData* pData));



/*!
\fn A3DStatus A3DMDFCFToleranceValueGet( const A3DMDFCFToleranceValue* pToleranceValue, A3DMDFCFToleranceValueData* pData)
\brief Populates the \ref A3DMDFCFToleranceValueData structure
\ingroup a3d_markupfcfrow
\version 5.2


\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDFCFToleranceValueGet,( const A3DMDFCFToleranceValue* pToleranceValue,
		 A3DMDFCFToleranceValueData* pData));




/*!
\fn A3DStatus A3DMDFCFRowDatumGet( const A3DMDFCFRowDatum* pRowDatum, A3DMDFCFRowDatumData* pData)
\brief Populates the \ref A3DMDFCFRowDatumData structure
\ingroup a3d_markupfcfrow
\version 4.0


\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDFCFRowDatumGet,( const A3DMDFCFRowDatum* pRowDatum,
		 A3DMDFCFRowDatumData* pData));



/*!
\fn A3DStatus A3DMDFCFDrawingRowGet( const A3DMDFCFDrawingRow* pDrawingRow, A3DMDFCFDrawingRowData* pData)
\brief Populates the \ref A3DMDFCFDrawingRowData structure
\ingroup a3d_markupfcfrow
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDFCFDrawingRowGet,( const A3DMDFCFDrawingRow* pDrawingRow,
		 A3DMDFCFDrawingRowData* pData));





/*!
\brief Populates the \ref A3DMDFCFDrawingRowData structure whatever the row type
\brief Note that if the drawing row is not defined with a tolerance value, the function returns \ref A3D_MARKUP_SEMANTIC_TOL_VALUE_NOT_SET
\sa A3DMDFCFDrawingRowData

\ingroup a3d_markupfcfrow
\version 5.2

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_MARKUP_SEMANTIC_DEFINITION_NOT_SET \n
\return \ref A3D_MARKUP_SEMANTIC_TOL_VALUE_NOT_SET \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDFCFSemanticRowGet,( const A3DMDFCFRow* pRow,
		 A3DMDFCFDrawingRowData* pData));



/*!
\fn A3DStatus A3DMDFCFIndicatorGet( const A3DMDFCFIndicator* pIndicator, A3DMDFCFIndicatorData* pData)
\brief Populates the \ref A3DMDFCFIndicatorData structure
\ingroup a3d_markupfcf
\version 12.2

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DMDFCFIndicatorGet, (const A3DMDFCFIndicator* pIndicator, A3DMDFCFIndicatorData* pData));




/*!
\fn A3DStatus A3DMDFeatureControlFrameGet( const A3DMDFeatureControlFrame* pFCF, A3DMDFeatureControlFrameData* pData)
\brief Populates the \ref A3DMDFeatureControlFrameData structure
\ingroup a3d_markupfcf
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDFeatureControlFrameGet,( const A3DMDFeatureControlFrame* pFCF,
		 A3DMDFeatureControlFrameData* pData));





/*!
\fn A3DStatus A3DMDSemanticFeatureControlFrameGet( const A3DMDFeatureControlFrame* pFCF, A3DMDFeatureControlFrameData* pData)
\brief Retrieves the semantic definition if set and populates the \ref A3DMDFeatureControlFrameData structure
\ingroup a3d_markupfcf
\version 5.2

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_MARKUP_SEMANTIC_DEFINITION_NOT_SET \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDSemanticFeatureControlFrameGet,( const A3DMDFeatureControlFrame* pFCF,
		 A3DMDFeatureControlFrameData* pData));




/*!
\fn A3DStatus A3DMDToleranceSizeGet( const A3DMDToleranceSize* pToleranceSize, A3DMDToleranceSizeData* pData)
\brief Populates the \ref A3DMDToleranceSizeData structure
\ingroup a3d_markuptolerancesize
\version 12.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DMDToleranceSizeGet, (const A3DMDToleranceSize* pToleranceSize,
	A3DMDToleranceSizeData* pData));

/*!
\fn A3DStatus A3DMDToleranceSizeValueGet( const A3DMDToleranceSizeValue* pToleranceSizeValue, A3DMDToleranceSizeValueData* pData)
\brief Populates the \ref A3DMDToleranceSizeValueData structure
\ingroup a3d_markuptolerancesizevalue
\version 12.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DMDToleranceSizeValueGet, (const A3DMDToleranceSizeValue* pToleranceSizeValue,
	A3DMDToleranceSizeValueData* pData));

/*!
\fn A3DStatus A3DMarkupGDTGet( const A3DMarkupGDT* pMarkupGDT, A3DMarkupGDTData* pData)
\brief Populates the \ref A3DMarkupGDTData structure
\ingroup a3d_markupgdt
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMarkupGDTGet,( const A3DMarkupGDT* pMarkupGDT,
		 A3DMarkupGDTData* pData));





#endif	/*	__A3DPRCMARKUPTOLERANCE_H__ */
