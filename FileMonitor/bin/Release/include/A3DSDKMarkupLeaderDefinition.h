/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for the leader definition module
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifdef A3DAPI_LOAD
#  undef __A3DPRCMARKUPLEADERDEFINITION__
#endif
#ifndef __A3DPRCMARKUPLEADERDEFINITION__
#define __A3DPRCMARKUPLEADERDEFINITION__
#ifndef A3DAPI_LOAD
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKGeometry.h>
#  include <A3DSDKInitializeFunctions.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD


/*!
\defgroup a3d_markupleaderdefinition Leader definition
\ingroup a3d_markupleader
\version 4.0
*/


/*!\addtogroup a3d_markupposition
\details
Positionning information are stored in one of this three structure:
\li \ref A3DMDPosition3DData, defined by a 3D vector
\li \ref A3DMDPosition2DData, defined by a 2D vector and a plane
\li \ref A3DMDPositionReferenceData, according to another position
*/
/*!
\struct A3DMDPosition3DData
\brief Definition of a position with a 3D vector
\ingroup a3d_markupposition
\version 4.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16		m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DVector3dData	m_sPosition;	/*!< 3D point defining the position. */
}A3DMDPosition3DData;
#endif // A3DAPI_LOAD


/*!
\brief Populates the \ref A3DMDPosition3DData structure
\ingroup a3d_markupposition
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDPosition3DGet,( const A3DMDPosition3D* pPosition3D,
		 A3DMDPosition3DData* pData));
/*!
\struct A3DMDPosition2DData
\brief Definition of a position with a 2D vector, a plane and an offset
\ingroup a3d_markupposition
\version 4.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16			m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DVector2dData		m_sPosition;	/*!< 2D point defining the position. */
	A3DDouble			m_dOffset;		/*!< Offset from plane along normal vector. */
	A3DSurfPlane*		m_pPlane;		/*!< Plane definition. */
}A3DMDPosition2DData;
#endif // A3DAPI_LOAD


/*!
\brief Populates the \ref A3DMDPosition2DData structure
\ingroup a3d_markupposition
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDPosition2DGet,( const A3DMDPosition2D* pPosition2D,
		 A3DMDPosition2DData* pData));

/*!
\struct A3DMDPositionReferenceData
\brief Definition of a position according to another position
\ingroup a3d_markupposition
\version 4.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16			m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DMiscMarkupLinkedItem*	m_psLinkedItem;	/*!< Position reference. */
	EA3DMDAnchorPointType		m_eAttachType;	/*!< Indicates which point of reference markup frame corresponds to the calculate attach. */
	A3DVector2dData m_sOffsetToReference;		/*!< 2D point defining the offset. */

}A3DMDPositionReferenceData;
#endif // A3DAPI_LOAD


/*!
\brief Populates the \ref A3DMDPositionReferenceData structure
\ingroup a3d_markupposition
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDPositionReferenceGet,(	const A3DMDPositionReference* pPositionReference,
		 A3DMDPositionReferenceData* pData));






/*!
\struct A3DMDLeaderSymbolData
\brief Markup leader symbol data structure
\ingroup a3d_markupleaderdefinition
\version 4.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16					m_usStructSize;				// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DMDLeaderSymbolType		m_eHeadSymbol;				/*!< Terminating symbol. */
	A3DDouble					m_dLength;					/*!< Arrow length or circle diameter,... defined in model unit. */
	A3DDouble					m_dAdditionalParameter;		/*!< Angle for symbol arrow, thickness for symbol segment (defined in degree). */
}A3DMDLeaderSymbolData;
#endif // A3DAPI_LOAD


/*!
\brief Populates the \ref A3DMDLeaderSymbolData structure
\ingroup a3d_markupleaderdefinition
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDLeaderSymbolGet,(	const A3DMDLeaderSymbol* pLeaderSymbol,
		 A3DMDLeaderSymbolData* pData));



/*!
\struct A3DMDMarkupLeaderStubData
\brief Markup Leader stub data structure
\ingroup a3d_markupleaderdefinition
\version 4.0
\details
The stub is a line which links the leader(s) to the markup box. It is vertical or horizontal line according to the attach point type, and its length depends on the attach point type.
Therefore, in the structure of markup leader stub, there are two arrays to be considered in parallel:
the first defines the length of the stub, and the second the anchor point type corresponding to the length.
\image html pmi_leader_stub.png


*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16				m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.

	A3DUns32				m_uiValuesAndAnchorTypesSize;		/*!< Size of array of values and anchor types. For example, if there are two leader lines pointing from the PMI box, this value will be two.*/
	A3DDouble*				m_pdValues;		/*!<  The length of the first segment of the arrow if it is segmented (i.e., the stub) -- see the horizontal segment in the righthand image above. */
	A3DUns32*				m_piAnchorTypes;		/*!< To get the anchor type, use the following bitwise operation: <b>m_piAnchorTypes[i] & 0x1F</b>. See \ref EA3DMDAnchorPointType for the available types. \n\n If the PMI box contains several sub-boxes (as in the image below), use <b>m_piAnchorTypes[i] >> 5</b> to find the sub-box index. \n\n \image html sub_box_index.jpg */
} A3DMDMarkupLeaderStubData;
#endif // A3DAPI_LOAD


/*!
\brief Populates the \ref A3DMDMarkupLeaderStubData structure
\ingroup a3d_markupleaderdefinition
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDMarkupLeaderStubGet,(	const A3DMDMarkupLeaderStub* pLeaderStub,
		 A3DMDMarkupLeaderStubData* pData));

/*!
\struct A3DMDLeaderDefinitionData
\brief Leader data structure
\ingroup a3d_markupleaderdefinition
\version 4.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16					m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.

	A3DUns32					m_uiNumberOfPathLeaderPositions;	/*!< Number of path leader positions. */
	A3DMDPosition**				m_ppsPathLeaderPositions;
	/*!< Array of path leader positions
	* This set of markup positions indicates the points: starting with the point on geometry
	* and finishing with the nearest attach point. See below \image html pmi_leader_path_points.png
	\par Dimension cases
	* Dimension markups use same leaders as others, but the path positions have to be interpreted array according to the dimension type.
	* Therefore, for each type, the markup contains specific leader: \image html pmi_leader_path_points_dimension.png
	*/

	A3DUns32					m_uiNbGapsElements;					/*!< Number of gap in following list. */
	A3DDouble*					m_pdGapList;
	/*!< Array defining leader line interruptions between the entity pointed and the started point.
	* For each interruption two values are stored to indicate the start and end points,
	* and values are ration leader length/ distance to the end.
	*/

	A3DMDLeaderSymbol*			m_pHeadSymbol;						/*!< Leader symbol, ref to \ref A3DMDLeaderSymbolData. May be NULL. */
	A3DMDLeaderSymbolType		m_eTailSymbol;						/*!< Terminating symbol. */

	A3DMDLeaderDefinition*		m_pNextLeader;						/*!<
																	Next leader, may be NULL.
																	*The attach point type defined on the next leader denotes if there is an additional line between both leaders.
																	*If the attach type is zero, we have to create a segment between the first point of the next leader and the last of the preceding leader. In the first case,
																	*the markup (usally a datum markup) has only one point to define the leader; this type of next leader is used to have additional points.
																	*Otherwise, the next leader appears as an extension line.
																	\image html pmi_markup_leader_next_classical_case.png

																	* The next leader can also be used when a markup is attached to radial dimension markup leader.
																	*The next points define a circle arc: the first point is the arc center, the second is the start point of the arc;
																	*the end point is the last point of the previous (owning) leader.
																	*The start point can be offset by the second value of overrun and the end point by the second value of blanking.
																	\image html pmi_markup_leader_next_dimension_case.png
																	*/
	A3DUns32					m_uAnchorFrame;						/*!<
																	Defines to which box the leader will be attached.
																	* In most cases, the index is equal to zero when there is only one box.
																	* It can be different for the feature frame control: see next figure to understand the numbering process. \image html pmi_leader_anchor_type.png
																	*/
	A3DUns32					m_uAnchorPoint;						/*!< Defines the anchor point type, the.point of the box where the leader starts; this information is the same as for the markup attach type*/
	A3DMDMarkupLeaderStub*		m_pStub;							/*!< Leader stub, ref to \ref A3DMDMarkupLeaderStubData. May be NULL. */

} A3DMDLeaderDefinitionData;
#endif // A3DAPI_LOAD



/*!
\brief Populates the \ref A3DMDLeaderDefinitionData structure
\ingroup a3d_markupleaderdefinition
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDLeaderDefinitionGet,(	const A3DMDLeaderDefinition* pLeader,
		 A3DMDLeaderDefinitionData* pData));

#endif	/*	__A3DPRCMARKUPLEADERDEFINITION__ */

