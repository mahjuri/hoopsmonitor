/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header of <b>A3DSDK</b>. Tool section.
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifdef A3DAPI_LOAD
#  undef __A3DPRCTOOLS_H__
#endif
#ifndef __A3DPRCTOOLS_H__
#define __A3DPRCTOOLS_H__
#ifndef A3DAPI_LOAD
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKGeometry.h>
#  include <A3DSDKInitializeFunctions.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD

/*!
\addtogroup a3d_tools_module Tools Module
*/


/*!
\defgroup a3d_copy_and_adapt_brep_model Copy and adapt B-rep model
\ingroup a3d_tools_module
*/

/*!
\ingroup a3d_copy_and_adapt_brep_model
\brief Copy and convert to NURBS parameter
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16				m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool					m_bUseSameParam;				/*!< If `A3D_TRUE`, surfaces will keep their parametrization when converted to NURBS. */
	A3DDouble				m_dTol;							/*!< Tolerance value of resulting B-rep. The value is relative to the scale of the model. */
	A3DBool					m_bDeleteCrossingUV;			/*!< If `A3D_TRUE`, UV curves that cross seams of periodic surfaces are replaced by 3D curves */
	A3DBool					m_bSplitFaces;					/*!< If `A3D_TRUE`, the faces with a periodic basis surface are split on parametric seams */
	A3DBool					m_bSplitClosedFaces;			/*!< If `A3D_TRUE`, the faces with a closed basis surface are split into faces at the parametric seam and mid-parameter */
	A3DBool					m_bForceComputeUV;				/*!< If `A3D_TRUE`, UV curves are computed from the B-rep data */
	A3DBool					m_bAllowUVCrossingSeams;		/*!< If `A3D_TRUE` and m_bForceComputeUV is set to `A3D_TRUE`, computed UV curves can cross seams. It will be automatically disabled if m_bSplitFaces or m_bSplitClosedFaces are set to `A3D_TRUE`. \version 9.0 */
	A3DBool					m_bForceCompute3D;				/*!< If `A3D_TRUE`, 3D curves are computed from the B-rep data */
	A3DUns32				m_uiAcceptableSurfacesSize;		/*!< Length of m_puiAcceptableSurfaces array */
	A3DUns32*				m_puiAcceptableSurfaces;		/*!< A list of acceptable surface types that your modeler can handle. Acceptable surface types are listed in A3DSDKTypes.h and begin with 'kA3DTypeSurf' */
	A3DUns32				m_uiAcceptableCurvesSize;		/*!< Length of m_puiAcceptableCurves array */
	A3DUns32*				m_puiAcceptableCurves;			/*!< A list of acceptable curve types that your modeler can handle. Acceptable curve types are listed in A3DSDKTypes.h and begin with 'kA3DTypeCrv' */
	A3DBool					m_bContinueOnError;				/*!< Continue processing even if an error occurs. Use \ref A3DCopyAndAdaptBrepModelAdvanced to get the error status. */
	A3DBool					m_bClampTolerantUVCurvesInsideUVDomain; /*!< If `A3D_FALSE`, UV curves may stray outside the UV domain as long as the 3D edge tolerance is respected. If set to `A3D_TRUE`, the UV curves will be clamped to the UV domain (if the clamp still leaves them within the edge tolerance). */
	A3DBool					m_bForceDuplicateGeometries;	/*!< If `A3D_TRUE`, break the sharing of surfaces and curves into topologies.*/
} A3DCopyAndAdaptBrepModelData;
#endif // A3D_API_LOAD


/*!
\ingroup a3d_copy_and_adapt_brep_model
\brief Copy and adapt B-rep parameters

<p>The B-rep used by HOOPS Exchange may not be completely compatible with modeling systems that do not support the full range of
B-rep as employed by PRC. This function attempts to convert PRC B-rep into a format that is compatible with your system.</p>

<p>In order to use this function, you create a A3DCopyAndAdaptBrepModelData structure and configure it to create a new B-rep
model using entities you are able to support. The function can transform predetermined surface types and curves to NURBS. It
also can process parametric curves and 3D curves, as well as split periodic parametric surfaces.</p>

<p>Finally, it creates a new B-rep model. After using it, the new model should be deleted.</p>

	<p>A3DCopyAndAdaptBrepModel will perform the following steps:</p>

	\li For each face, the bearing surface and the 3D curves are converted
	\li Surfaces are reduced in size
	\li Cross-period UV curves are deleted if necessary
	\li Faces are split at periodic seams and/or mid-parameter for closed surfaces
	\li Final UV curves are computed
	\li A second surface shrink is performed with new UV parameters
	\li Final 3D curves are computed

\note When the function A3DCopyAndAdaptBrepModel returns an error, you may try to
	set the parameters m_bSplitFaces and/or m_bForceComputeUV to 'false' and retry the
	conversion.

\param [in] p A pointer to the source B-rep entity
\param [in] psSetting The structure that defines which types of B-rep you would like to convert to
\param [out] pp A pointer to the result B-rep entity or entities

\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SRF_INVALID_PARAMETERS \n
\return \ref A3D_CRV_INVALID_PARAMETER \n
\return \ref A3D_TOOLS_NURBSCONVERT_GENERAL_FAILURE \n
\return \ref A3D_ERROR \n
\return \ref A3D_SUCCESS \n
\return \ref A3D_INITIALIZE_BAD_VALUES \n
\return \ref A3D_NOT_IMPLEMENTED \n
\return \ref A3D_TOPO_VERTICES_MISSING \n
\return \ref A3D_COEDGE_BAD_ORIENTATION_DATA \n
\return \ref A3D_TOPO_GEOMETRY_MISSING \n
\return \ref A3D_LOOP_BAD_ORIENTATION_DATA \n
\return \ref A3D_SHELL_BAD_ORIENTATION_DATA \n
\return \ref A3D_TOPO_WRONG_TOLERANCES \n
\return \ref A3D_TOPO_CONSISTENCY \n
\return \ref A3D_TOPO_NON_CONNEX_SHELL \n
\return \ref A3D_TOPO_OPEN_SOLID \n
\return \ref A3D_TOOLS_NURBSCONVERT_SURFACE_FAILURE \n
\return \ref A3D_TOOLS_NURBSCONVERT_UV_FAILURE \n
\return \ref A3D_TOOLS_NURBSCONVERT_3D_FAILURE \n
\return \ref A3D_TOOLS_NURBSCONVERT_GENERAL_FAILURE \n
\return \ref A3D_TOOLS_SPLIT_FAILURE \n
\return \ref A3D_TOOLS_COMPUTE_UV_FAILURE \n
\return \ref A3D_TOOLS_COMPUTE_3D_FAILURE \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_EXCEPTION \n
\return \ref A3D_BREPDATA_CANNOT_GETBOUNDINGBOX \n
\return \ref A3D_INCOMPATIBLE_FUNCTION_WITH_KEEP_PARSED_ENTITY_MODE \n

*/

A3D_API (A3DStatus, A3DCopyAndAdaptBrepModel, (	const A3DEntity* p,
																const A3DCopyAndAdaptBrepModelData *psSetting,
																A3DEntity** const pp) );


/*!
\ingroup a3d_copy_and_adapt_brep_model
\brief Copy and adapt B-rep parameters

<p>Same as \ref A3DCopyAndAdaptBrepModelData , but return status of each face in error if \ref A3DCopyAndAdaptBrepModelData.m_bContinueOnError is set to true
<p> puiNbErrors return the number of faces in error
<p> paiErrors corresponds to a set of indexes and error codes
<code>
paiErrors[0] = type the topological element
paiErrors[1] = error status
paiErrors[2] = number of indexes to find element (connex = 1, shell = 2, face = 3, loop = 4, coedge /edge = 5)
paiErrors[3] = first index
...
paiErrors[2+paiErrors[2]] = last index
</code>

\param [in] p A pointer to the source B-rep entity
\param [in] psSetting The structure that defines which types of B-rep you would like to convert to
\param [out] pp A pointer to the result B-rep entity or entities
\param [out] puiNbErrors Number of elements in error
\param [out] paiErrors Index and error code of each element

\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SRF_INVALID_PARAMETERS \n
\return \ref A3D_CRV_INVALID_PARAMETER \n
\return \ref A3D_TOOLS_NURBSCONVERT_GENERAL_FAILURE \n
\return \ref A3D_ERROR \n
\return \ref A3D_SUCCESS \n
\return \ref A3D_INITIALIZE_BAD_VALUES \n
\return \ref A3D_NOT_IMPLEMENTED \n
\return \ref A3D_TOPO_VERTICES_MISSING \n
\return \ref A3D_COEDGE_BAD_ORIENTATION_DATA \n
\return \ref A3D_TOPO_GEOMETRY_MISSING \n
\return \ref A3D_LOOP_BAD_ORIENTATION_DATA \n
\return \ref A3D_SHELL_BAD_ORIENTATION_DATA \n
\return \ref A3D_TOPO_WRONG_TOLERANCES \n
\return \ref A3D_TOPO_CONSISTENCY \n
\return \ref A3D_TOPO_NON_CONNEX_SHELL \n
\return \ref A3D_TOPO_OPEN_SOLID \n
\return \ref A3D_TOOLS_NURBSCONVERT_SURFACE_FAILURE \n
\return \ref A3D_TOOLS_NURBSCONVERT_UV_FAILURE \n
\return \ref A3D_TOOLS_NURBSCONVERT_3D_FAILURE \n
\return \ref A3D_TOOLS_NURBSCONVERT_GENERAL_FAILURE \n
\return \ref A3D_TOOLS_SPLIT_FAILURE \n
\return \ref A3D_TOOLS_COMPUTE_UV_FAILURE \n
\return \ref A3D_TOOLS_COMPUTE_3D_FAILURE \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_EXCEPTION \n
\return \ref A3D_BREPDATA_CANNOT_GETBOUNDINGBOX \n
\return \ref A3D_INCOMPATIBLE_FUNCTION_WITH_KEEP_PARSED_ENTITY_MODE \n

*/
A3D_API (A3DStatus, A3DCopyAndAdaptBrepModelAdvanced, (const A3DEntity* p,
																		const A3DCopyAndAdaptBrepModelData *psSetting,
																		A3DEntity** const pp,
																		A3DUns32 *puiNbErrors,
																		A3DInt32 **paiErrors) );


/*!
\defgroup a3d_adapt_and_replace_all_brep_in_modelfile Adapt and replace all B-rep in model file
\ingroup a3d_tools_module
*/


/*!
\ingroup a3d_adapt_and_replace_all_brep_in_modelfile
\brief Entity Convert to NURBS facility

	\param [in] pModelFile The source model file to adapt.
	\param [in] psSettings The structure that defines which types of B-rep you would like to convert to.

	\return \ref A3D_CRV_INVALID_PARAMETER if `psSetting ->m_puiAcceptableCurves` contains invalid types.
	\return \ref A3D_SRF_INVALID_PARAMETERS if `psSetting->m_puiAcceptableSurfaces` contains invalid types.
	\return \ref A3D_TOOLS_NURBSCONVERT_GENERAL_FAILURE

	This function adapts all B-rep in a Model File to customers' needs.
	It enables the transformation of predetermined types of surfaces and curves as NURBS.
	It also permits computation of parametric curves and/or 3D curves, splitting periodic surfaces, etc.

	These are the steps performed, according to the settings in A3DCopyAndAdaptBrepModelData:
	\li For each face, the bearing surface is converted and then the 3D curves that are present
	\li Surfaces are shrinked.
	\li Cross-period UV curves are deleted if needed.
	\li Faces are split at periodic seams and/or mid-parameter for closed surfaces.
	\li Final UV curves are computed.
	\li A second surface shrink is performed with new UVs.
	\li Final 3D curves are computed.

	In case where `psSetting->m_bContinueOnError` is `A3D_TRUE`, the function does not return any error code, unless it is considered as a fatal error.
	In that case, if you wish to know when an error occurs, use \ref A3DAdaptAndReplaceAllBrepInModelFileAdvanced instead. Then check its `puiNbErrors` parameter.

	\see A3DAdaptAndReplaceAllBrepInModelFileAdvanced

\version 5.2
\note When the function A3AdaptAndReplaceAllBrepInModelFile ends in error, you may try to
	set the parameters m_bSplitFaces and/or m_bForceComputeUV to `A3D_FALSE` and retry the
	conversion.
*/

A3D_API (A3DStatus, A3DAdaptAndReplaceAllBrepInModelFile, (		A3DAsmModelFile* pModelFile,
																const A3DCopyAndAdaptBrepModelData *psSetting
																) );



/*!
\ingroup a3d_adapt_and_replace_all_brep_in_modelfile
\brief Copy and convert to NURBS error result

<p> paiErrors corresponds to a set of indexes and error codes
<ul>
<li><code>paiErrors[0]</code> = type the topological element</li>
<li><code>paiErrors[1]</code> = error status</li>
<li><code>paiErrors[2]</code> = number of indexes to find element (0 = brepdata, connex = 1, shell = 2, face = 3, loop = 4, coedge /edge = 5)</li>
<li><code>paiErrors[3]</code> = first index</li>
<li>... </li>
<li><code>paiErrors[3+paiErrors[2]]</code> = last index</li>
</ul>
\version 9.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DEntity*  	m_pEntity;		/*!< Entity which have error during function call */
	A3DUns32			m_uiNbError;	/*!< Number of elements in error */
	A3DInt32*			m_paiErrors;	/*!< Index and error code of each element */

} A3DCopyAndAdaptBrepModelErrorData;
#endif // A3D_API_LOAD


/*!
\ingroup a3d_adapt_and_replace_all_brep_in_modelfile
\brief Entity Convert to NURBS facility

<p>Same as \ref A3DAdaptAndReplaceAllBrepInModelFile , but return status of each element in error if \ref A3DCopyAndAdaptBrepModelData.m_bContinueOnError is set to true
<p> puiNbErrors return the number of brep in error
<p> pErrors corresponds to the list of error for each brep

\param [in] p A pointer to the model file
\param [in] psSetting The structure that defines which types of B-rep you would like to convert to
\param [out] puiNbErrors Number of brep in error
\param [out] pErrors list of errors for each brep

\return \ref A3D_TOOLS_CONTINUE_ON_ERROR if there is errors, but A3DCopyAndAdaptBrepModelData.m_bContinueOnError is set to true \n
\return \ref A3D_INCOMPATIBLE_FUNCTION_WITH_KEEP_PARSED_ENTITY_MODE \n

\version 9.2
*/
A3D_API(A3DStatus, A3DAdaptAndReplaceAllBrepInModelFileAdvanced, (	A3DAsmModelFile* p,
																	const A3DCopyAndAdaptBrepModelData *psSetting,
																	A3DUns32 *puiNbErrors,
																	A3DCopyAndAdaptBrepModelErrorData **pErrors) );


/*!
\defgroup a3d_entity_delete Delete Entities
\ingroup a3d_tools_module
*/

/*!
\ingroup a3d_entity_delete
\brief Recursively deletes the entity and its child entities.

This function deletes the given entity along with all it's child entities.
Usually deleting any entity of a PRC tree is implicitly done when the tree root
is deleted using \ref A3DAsmModelFileDelete.
However, it is possible that some entities may not be attached to any PRC. This is usually
the case when a CAD model is manipulated for modification purpose.

As a rule of thumb, you should always explicitly delete an entity you did'nt
attach to any other entity.

\remark Some entity types require a specific function for deletion. Please see:
- \ref A3DFaceUVPointInsideManagerDelete
- \ref A3DFileContextDelete
- \ref A3DMiscCascadedAttributesDelete
- \ref A3DMkpRTFDelete
- \ref A3DMkpRTFFieldDelete
- \ref A3DProjectPointCloudManagerDelete

\warning This function must be called only on detached entities. Using it on an
attached entity _will_ resulting in a double free error when the PRC tree is deleted.

\warning This function must not be applied within an [HOOPS Visualize Context](getting_started.html).

\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DEntityDelete,(A3DEntity* pEntity));



/*!
\defgroup a3d_entity_pdfid Entity PDF IDs
\ingroup a3d_tools_module
*/
/*!
\ingroup a3d_entity_pdfid
\brief Gets the PDF node unique IDs for the entities.

Given a pointer to the PRC entity, this function gets the unique name that will be used in the PDF scene graph.
This function must be used AFTER the PRC file has been created from the model file.

\param [in] pEntity The pointer to the PRC entity
\param [in] pEntityFather The pointer to the product occurrence owner to the PRC entity. NULL if pEntity is a product occurrence.
\param [in] pA3DRWParamsPrcWriteHelper The pointer to the helper object generally built by the function A3DAsmModelFileExportToPrcFile.
\param [out] pcUTF8Name The identifier as a string.

\version 4.1

\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DEntityGetPDFNodeIdFromWrite,(const A3DEntity* pEntity,
	const A3DEntity* pEntityFather,
	const A3DRWParamsPrcWriteHelper* pA3DRWParamsPrcWriteHelper,
	A3DUTF8Char** pcUTF8Name));

/*!
\ingroup a3d_entity_pdfid
\brief Gets the PDF node unique IDs for the entities.

Given a pointer to the PRC entity, this function gets the unique name that will be used in the PDF scene graph.
This function must be used AFTER the PRC file has been created from the model file.

\param [in] pEntity The pointer to the PRC entity
\param [in] pEntityFather The pointer to the product occurrence owner to the PRC entity. NULL if pEntity is a product occurrence.
\param [in] pA3DRWParamsPrcReadHelper The pointer to the helper object generally built by the function A3DAsmModelFileLoadFromPrcStream.
\param [out] pcUTF8Name The identifier as a string.

\version 9.2

\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DEntityGetPDFNodeIdFromRead, (const A3DEntity* pEntity,
	const A3DEntity* pEntityFather,
	const A3DRWParamsPrcReadHelper* pA3DRWParamsPrcReadHelper,
	A3DUTF8Char** pcUTF8Name));

/*!
\defgroup a3d_tools_topoitemowners_module Topological item owners
\ingroup a3d_tools_module
\version 5.0
*/

/*!
\brief Structure is a linked list for storing topology items owners
\ingroup a3d_tools_topoitemowners_module
\version 5.0

This structure gives you an access to topology item owners.
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	const A3DTopoItem*		m_pTopoItem;					/*!< Topology item owner. */
	void*					m_pNext;						/*!< If the topology item is shared by several owners, this pointer gives access to the next owner item. */
} A3DTopoItemOwner;
#endif // A3D_API_LOAD


/*!
\ingroup a3d_tools_topoitemowners_module
\brief Structure for storing topology items owners
\version 5.0
Methods and structures dedicated to managing the link between topology items and their owner(s).
*/

/*!
\brief Creates manager of topology items fron an \ref A3DRiRepresentationItem
\ingroup a3d_tools_topoitemowners_module
\version 5.0
\par
This function creates a map, on request, between topological items and linked lists of owners.
When the map is built, the function \ref A3DTopoItemOwnersGet lets you retrieve owners of specific topological item.
For example, if you need the faces owner of edges, first you build your map, then you retrieve the owner list for each edge.
The following code shows how to use it.

\code
	// map creation
	A3DTopoItemOwnersManager* pTopologyOwnersManager = NULL;
	if(A3DTopoItemOwnersManagerGet(pBrepOwner, pTopologyOwnersManager) != A3D_SUCCESS)
		return A3D_ERROR;
	if(pTopologyOwnersManager == NULL)
		return A3D_ERROR;

	// edge owners = CoEdge
	A3DTopoItemOwner* pOwningCoEdge = NULL;
	A3DTopoItemOwnersGet(pTopologyOwnersManager, pEdge, pOwningCoEdge);
	while( pOwningCoEdge!=NULL)
	{
		// coedge owners = Loop
		A3DTopoItemOwner* pOwningLoop = NULL;
		A3DTopoItemOwnersGet(pTopologyOwnersManager, pOwningCoEdge->m_pTopoItem, pOwningLoop);
		while( pOwningLoop!=NULL)
		{
			// Loop owners = face
			A3DTopoItemOwner* pOwingFace = NULL;
			A3DTopoItemOwnersGet(pTopologyOwnersManager, pOwningLoop->m_pTopoItem, pOwingFace);
			while( pOwingFace!=NULL)
			{
				if(A3DEntityGetType(pOwingFace->m_pTopoItem, &entityType) != A3D_SUCCESS)
					return A3D_ERROR;
				if(entityType != kA3DTypeTopoFace)
					return A3D_ERROR;

				pOwingFace = pOwingFace->m_pNext;
			}
			pOwningLoop = pOwningLoop->m_pNext;
		}
		pOwningCoEdge = pOwningCoEdge->m_pNext;
	}

\endcode

Note that, if A3DTopoItemOwnersManagerGet is called several times with the same representation item, the map is not recomputed; and if the
representation item is null, the map is released.

\return \ref A3D_SUCCESS \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_ERROR \n
*/
A3D_API (A3DStatus, A3DTopoItemOwnersManagerGet,( const A3DRiRepresentationItem* pBrepOwner, A3DTopoItemOwnersManager** ppTopoItemOwnersManager));


/*!
\ingroup a3d_tools_topoitemowners_module
\brief Gets owners of topology item
\version 5.0

\return \ref A3D_SUCCESS \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_ERROR \n
*/
A3D_API (A3DStatus, A3DTopoItemOwnersGet,( A3DTopoItemOwnersManager* pTopoItemOwnersManager, const A3DTopoItem* pTopoItem, A3DTopoItemOwner** ppTopoItemOwner));




/*!
\defgroup a3d_physical_properties Physical properties
\ingroup a3d_tools_module
\version 5.2
*/

/*!
\ingroup a3d_physical_properties
\brief Physical property settings
\version 5.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16				m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DVector3dData			m_sGravityCenter;					/*!< Gravity center. The value returned is in the unit of the entity (Modelfile unit if using A3DComputeModelFilePhysicalProperties on model file, Brep Unit (adjustable through an input scale) if using A3DComputePolyBrepPhysicalProperties or A3DComputePhysicalProperties). */
	A3DDouble				m_dSurface;							/*!< B-rep model surface area of resulting B-rep. The value returned is in the unit of the entity (Modelfile unit if using A3DComputeModelFilePhysicalProperties on model file, Brep Unit (adjustable through an input scale) if using A3DComputePolyBrepPhysicalProperties or A3DComputePhysicalProperties). */
	A3DBool					m_bVolumeComputed;					/*!< When false, typically in case the B-rep is not a solid, indicates that the volumic properties were not computed. */
	A3DDouble				m_dVolume;							/*!< B-rep model volume of resulting B-rep. The value returned is in the unit of the entity (Modelfile unit if using A3DComputeModelFilePhysicalProperties on model file, Brep Unit (adjustable through an input scale) if using A3DComputePolyBrepPhysicalProperties or A3DComputePhysicalProperties). */
	A3DVector3dData			m_sSurfacicGravityCenter;			/*!< Gravity center using the surfaces' area as weight, rather than volume. The value returned is in the unit of the entity (Modelfile unit if using A3DComputeModelFilePhysicalProperties on model file, Brep Unit (adjustable through an input scale) if using A3DComputePolyBrepPhysicalProperties or A3DComputePhysicalProperties). \version 11.2 */
	A3DDouble				m_adAreaMatrixOfInertia[9];			/*!< The inertia matrix calculated using the surface rather than the volume. The value returned is in the unit of the entity (Modelfile unit if using A3DComputeModelFilePhysicalProperties on model file, Brep Unit (adjustable through an input scale) if using A3DComputePolyBrepPhysicalProperties or A3DComputePhysicalProperties). \version 11.2 */
	A3DDouble				m_adVolumeMatrixOfInertia[9];		/*!< The volumic inertia matrix. The value returned is in the unit of the entity (Modelfile unit if using A3DComputeModelFilePhysicalProperties on model file, Brep Unit (adjustable through an input scale) if using A3DComputePolyBrepPhysicalProperties or A3DComputePhysicalProperties). \version 11.2 */
	A3DBool					m_bUseGeometryOnRiBRep;				/*!< An input value which, if set to false, starts the calculations using RI tessellation (and internally computes a temporary tessellation if none is present). If set to true, uses an A3DRiBrepModel's geometry, and returns an error if it does not have any geometry. False by default. \version 11.2 */
	A3DDouble				m_dAccuracyLevel;					/*!< An input value between 0.0 and 1.0 (default is 0.99). Approaching 1.0 will make the calculation more accurate, but slower and less stable. The dependency is not linear. This can affect calculations on both tessellation and geometry, and when m_bUseGeometryOnRiBRep=false, will internally recompute tessellation on A3DRiBrepModels that contain geometry, without affecting input data. \version 11.2 */
	A3DBool					m_bIncludeHiddenRIs;					/*!< An input boolean (false by default), that determines whether hidden solids and surfaces are to be taken into account when requesting the computation on a modelfile.  */
} A3DPhysicalPropertiesData;
#endif // A3D_API_LOAD


/*!
\ingroup a3d_physical_properties
\brief Function to compute the physical properties (surface area, volume and gravity center, and since 11.2 the surfacic gravity center and the surfacic and volumic inertia matrix) of a BrepModel.
<p>To obtain the values in the unit of the model file, pass the scaling of the B-rep in the psScale parameter.
</p>

<p>Note that since 11.2, the input psPhysicalPropertiesData structure now has a couple of input parameters with which precision can be adjusted, in addition to the new return values.
</p>

<p>The value of psScale should be the cumulative scaling that results from traversing the assembly tree from the root to the designated
B-rep, multiplying the scaling of the current node by the cumulative scaling of all of its parent nodes.
</p>

<p>The inertia matrices are provided relative to the computed gravity centers, with respect to the global X, Y and Z directions.
</p>

\param [in] p The input B-rep model.
\param [in] psScale The optional scale.
\param [out] psPhysicalPropertiesData The computed physical properties.

\return \ref A3D_SUCCESS \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_RI_BREPMODEL_CANNOT_ACCESS_GEOMETRY \n
\return \ref A3D_TOOLS_COMPUTE_UV_FAILURE \n
\return \ref A3D_ERROR \n
\return \ref A3D_INCOMPATIBLE_FUNCTION_WITH_KEEP_PARSED_ENTITY_MODE \n
\return \ref A3D_TOOLS_TESSELLATION_ISSUE \n
\return \ref A3D_TOOLS_PHYSICALPROPERTIES_FAILURE \n

\version 5.2
*/
A3D_API (A3DStatus, A3DComputePhysicalProperties,(	const A3DRiBrepModel* p,
																	const A3DVector3dData* psScale,
																	A3DPhysicalPropertiesData* psPhysicalPropertiesData));

/*!
\ingroup a3d_physical_properties
\brief Function to compute the surface of the current A3DTopoFace. The current TopoContext is needed in order to get the correct current scale.


\param [in] pFace The input A3DTopoFace.
\param [in] pBrepDataTopoContext The corresponding A3DTopoContext of the current A3DTopoBrepData who is owing the A3DTopoFace.
\param [out] pArea The surface of the A3DTopoFace in the current unit.

\return \ref A3D_SUCCESS \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_ERROR \n
\return \ref A3D_TOOLS_TESSELLATION_ISSUE \n

\version 7.0
*/
A3D_API (A3DStatus, A3DComputeFaceArea, ( const A3DTopoFace* pFace, const A3DTopoContext* pBrepDataTopoContext, A3DDouble* pArea));
/*!
\ingroup a3d_physical_properties
\brief Function to compute the physical properties (surface area, volume and gravity center, and since 11.2 the surfacic gravity center and the surfacic and volumic inertia matrix) of a PolyBrepModel.
An optional scale, psScale, can be used. It can be either uniform or not. Physical properties will be computed after having scaled the
geometrical data in the A3DRiPolyBrepModel.

<p>Note that since 11.2, the input psPhysicalPropertiesData structure now has a couple of input parameters with which precision can be adjusted, in addition to the new return values.
</p>

<p>The inertia matrices are provided relative to the computed gravity centers, with respect to the global X, Y and Z directions.
</p>

\param [in] p The input poly B-rep model.
\param [in] psScale The optional scale.
\param [out] psPhysicalPropertiesData The wanted physical properties.

\return \ref A3D_SUCCESS \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_ERROR \n
\return \ref A3D_INCOMPATIBLE_FUNCTION_WITH_KEEP_PARSED_ENTITY_MODE \n
\return \ref A3D_TOOLS_TESSELLATION_ISSUE \n
\return \ref A3D_TOOLS_PHYSICALPROPERTIES_FAILURE \n

\version 6.1
*/
A3D_API (A3DStatus, A3DComputePolyBrepPhysicalProperties,(	const A3DRiPolyBrepModel* p,
															const A3DVector3dData* psScale,
															A3DPhysicalPropertiesData* psPhysicalPropertiesData));


/*!
\ingroup a3d_physical_properties
\brief Function to compute the physical properties (surface area, volume and gravity center, and since 11.2 the surfacic gravity center and the surfacic and volumic inertia matrix) of a modelfile, typically an assembly.
Data is returned in the modelfile unit, without density.

<p>Note that since 11.2, the input psPhysicalPropertiesData structure now has a couple of input parameters with which precision can be adjusted, in addition to the new return values.
</p>

<p>The inertia matrices are provided relative to the computed gravity centers, with respect to the global X, Y and Z directions. The return values are provided in the modelfile's unit.
The surfacic quantities take into account all representation items, whereas the ones for which the volume was not computed will not be used for the volumic quantities.
Note that density is not taken into account. Return values are purely volumic or surfacic. For models with a constant density different than 1 unit of weight per unit of volume, results must be adjusted: matrices must be multiplied by the density of the input CAD file.
</p>

\param [in] p The input modelfile.
\param [out] psPhysicalProperties The physical properties.

\return \ref A3D_SUCCESS \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_RI_BREPMODEL_CANNOT_ACCESS_GEOMETRY \n
\return \ref A3D_TOOLS_COMPUTE_UV_FAILURE \n
\return \ref A3D_ERROR \n
\return \ref A3D_INCOMPATIBLE_FUNCTION_WITH_KEEP_PARSED_ENTITY_MODE \n
\return \ref A3D_TOOLS_TESSELLATION_ISSUE \n
\return \ref A3D_TOOLS_PHYSICALPROPERTIES_FAILURE \n

\version 5.2
*/
A3D_API (A3DStatus, A3DComputeModelFilePhysicalProperties,(	const A3DAsmModelFile* p,
															A3DPhysicalPropertiesData* psPhysicalProperties));


/*!
\defgroup a3d_simplify_modelfile_curve_and_surface_as_analytic Simplify modelfile curves and surfaces as analytic
\ingroup a3d_tools_module
*/

/*!
\ingroup a3d_simplify_modelfile_curve_and_surface_as_analytic
\brief Function to traverse the entire modelfile and simplify all curves and surfaces to analytics if possible, according to a certain tolerance.
\deprecated Use \ref A3DSimplifyModelFileWithAnalytics
\param [in, out] p The input modelfile.
\param [in] dTol The tolerance.

\return \ref A3D_SUCCESS \n
\return \ref A3D_ERROR \n
\return \ref A3D_INCOMPATIBLE_FUNCTION_WITH_KEEP_PARSED_ENTITY_MODE \n

\version 5.2
*/
A3D_API (A3DStatus, A3DSimplifyModelFileCurveAndSurfaceAsAnalytic,(	A3DAsmModelFile* p,
																	double dTol));


/*!
\defgroup a3d_simplify_modelfile_curve_and_surface_as_analytic Simplify modelfile curves and surfaces as analytic
\ingroup a3d_tools_module
*/

/*!
\ingroup a3d_simplify_modelfile_curve_and_surface_as_analytic
\brief Function to traverse the entire modelfile and simplify all curves and surfaces to analytics if possible, according to a certain tolerance.
\n\n Recognized surface types are:\n
\ref a3d_srfcone,
\ref a3d_srfcylinder,
\ref a3d_srfplane,
\ref a3d_srfsphere,
\ref a3d_srftorus
\n\n Recognized curve types are:\n
\ref a3d_crvline,
\ref a3d_crvcircle
\n\n If uNbRecognizedType and pOptRecognizedType are defined, \ref A3DSimplifyModelFileWithAnalytics recognizes only the given types.

\param [in, out] p The input modelfile.
\param [in] dTol The tolerance (in millimeters).
\param [in] uNbRecognizedType Size of array pOptRecognizedType.
\param [in] pOptRecognizedType Optional type of surface we want to recognize.

\return \ref A3D_SUCCESS \n
\return \ref A3D_ERROR \n
\return \ref A3D_INCOMPATIBLE_FUNCTION_WITH_KEEP_PARSED_ENTITY_MODE \n

\version 7.0
*/
A3D_API (A3DStatus, A3DSimplifyModelFileWithAnalytics,(	A3DAsmModelFile* p,
																			A3DDouble dTol,
																			A3DUns32 uNbRecognizedType,
																			A3DEEntityType const *pOptRecognizedType));

#endif	/*	__A3DPRCTOOLS_H__ */
