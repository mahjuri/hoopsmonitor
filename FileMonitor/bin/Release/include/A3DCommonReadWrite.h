
/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file       A3DCommonReadWrite.h
\brief      Header file for the read-write module
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/


#ifdef A3DAPI_LOAD
#  undef __A3DCOMMONREADWRITE_H__
#endif
#ifndef __A3DCOMMONREADWRITE_H__
#define __A3DCOMMONREADWRITE_H__
#ifndef A3DAPI_LOAD
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKInitializeFunctions.h>
#  include <A3DSDKStructure.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD



/*!
	\ingroup a3d_read
	\brief Reading options to filter the types of information that are read when CAD files are loaded.

	All the `m_bRead*` fields are boolean options defaulted to `A3D_FALSE`, which means the related entity is skipped upon reading.

	\remark Even if these options are very common among CAD formats, some of them may be irrelevant to a specific format.

	\see [Loading a model](load_model.html)
	\see [PRC basics](prc.html)
	\see [Reading views and PMIs](markup2.html)
	\see [Traversing feature trees](feature_tree.html)

	\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;											// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bReadSolids;												/*!<	Allows reading of solid parts. */
	A3DBool m_bReadSurfaces;											/*!<	Allows reading of surfaces entities. */
	A3DBool m_bReadWireframes;										/*!<	Allows reading of wireframe entities. */
	A3DBool m_bReadPmis;													/*!<	Allows reading of PMIs. */
	A3DBool m_bReadAttributes;										/*!<	Allows reading of non-geometric information linked to entities and files.
																											\remark Attributes linked to solids and entities, such as material and density,
																											are always read, even if `m_bReadAttributes` is `A3D_FALSE`.*/
	A3DBool m_bReadHiddenObjects;									/*!<	Allows reading of representation items which are marked as hidden in the CAD file. Default is `A3D_TRUE`.
																											When set to `A3D_FALSE`, representation items considered hidden in the CAD file won't be kept in the resulting PRC.
																											This can be useful when the objective is to observe the visible appearance of the resulting PRC.
																											\remark This option only impacts representation items. Reading of assembly-level entities (parts and product occurrences) is not affected, may the CAD file identify them as hidden or not.
																											\warning Setting `m_bReadHiddenObjects` to `false` when reading PMIs, Views or complex assemblies may lead to unexpected behavior due to the potential for removal of bodies
																											referenced by residual PMI or Views.*/
	A3DBool m_bReadConstructionAndReferences;			/*!<	Allows reading of construction entities, such as planes and axes. */
	A3DBool m_bReadActiveFilter;									/*!<	Allows reading of only data stored in the active filter or on layers corresponding to the active filter. */
	A3DEReadingMode2D3D m_eReadingMode2D3D;				/*!<	If applicable, choose whether reading 2D drawing, 3D model or both. For IGES format, \c kA3DRead_Both is not available and will default to \c kA3DRead_3D. */
	A3DEReadGeomTessMode m_eReadGeomTessMode;			/*!<	Select between reading geometry, tessellation or both. */
	A3DEUnits m_eDefaultUnit;											/*!<	DefaultUnit option restricted to those formats where unit never exists or is unreliable (U3D, CGR, STL, VRML)
																											\li If it is different than \ref kA3DUnitUnknown, the default unit used is the one defined with this enum
																											\li If it is set to \ref kA3DUnitUnknown, the default unit is considered unknown and the value is 1.0. */
	A3DBool m_bReadFeature;												/*!<	Allows reading of model feature tree.
																										\version 10.2 */
	A3DInt32 m_iNbMultiProcess;										/*!< Allows multiprocess reading by specifying the number of processes to run.
																										This option must be set to `0` or `1` to fully disable multiprocess reading.
																										A value higher than `1` indicated the number of processes to run.
																										Default is `0` (No multiprocess).
																										\deprecated Due to its limitations, this functionality will be removed in the next major release (HE 2020).
																										\version 10.0. */
	A3DUns32 m_uiSearchTextureDirectoriesSize;		/*!< The size of \ref m_ppcSearchTextureDirectories array.\version 8.0  */
	A3DUTF8Char ** m_ppcSearchTextureDirectories;	/*!< Additional search directories for texture files. \version 8.0 */
} A3DRWParamsGeneralData;
#endif


/*!
\ingroup a3d_read
\brief Structure to specify the PMI reading parameters.

Only used if A3DRWParamsGeneralData::m_bReadPmis is set to \c TRUE.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bAlwaysSubstituteFont;	/*!< A value of \c TRUE is to substitute the fonts in the CAD file with the font that is specified in \ref m_pcSubstitutionFont. */
	A3DUTF8Char* m_pcSubstitutionFont;	/*!< The font used to replace any fonts that are not installed on the computer. If \ref m_bAlwaysSubstituteFont is \c TRUE, this font replaces all the fonts in the file. */
	int m_iNumberOfDigitsAfterDot;		/*!< Number of decimal places to use for numeric values if no decimal information is specified in the CAD file. */
	A3DEUnits m_eDefaultUnit;			/*!<
											Units of measure to use if no unit information is specified in the CAD file.
											Note that only a short set of CAD formats do not specify the unit information and will use this member,
											other formats should specify \ref kA3DUnitUnknown. */
	A3DUns32 m_uiProprietaryFontDirectoriesSize;	/*!< The size of \ref m_ppcProprietaryFontDirectories. */
	A3DUTF8Char** m_ppcProprietaryFontDirectories;	/*!<
														Usually, fonts are retrieved from the system directory.
														These paths enable to specify the locations of additional fonts.
														For DWG it allows to define where the .shx fonts are located.*/
	A3DGraphRgbColorData m_sDefaultColor;			/*!< Color to use when PMI has no color. \version 6.1 */
	A3DBool m_bAlwaysUseDefaultColor;				/*!< A value of \c TRUE is to substitute the color of PMI in the CAD file with the default color that is specified in \ref m_sDefaultColor. \version 9.0 */
} A3DRWParamsPmiData;
#endif


/*!
\ingroup a3d_read
\brief Structure to set the tessellation parameters.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;				// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DETessellationLevelOfDetail m_eTessellationLevelOfDetail;		/*!< Enum to specify predefined values for some following members. */
	A3DDouble m_dChordHeightRatio;			/*!< Specifies the ratio of the diagonal length of the bounding box to the chord height. A value of 50 means that the diagonal of the bounding box is 50 times greater than the chord height. Values can range from 50 to 10,000. Higher values will generate more accurate tessellation. */
	A3DDouble m_dAngleToleranceDeg;			/*!< Specifies the maximum angle between two contiguous tessellation segments describing the curve of a topological edge for every face.
												\n\n During tessellation, the curve describing the edge of a topological face will be approximated using straight line segments. <span class="code">m_dAngleToleranceDeg</span> is the maximum angle between two consecutive segments.
												\n\n Allowable values range from 10 to 40. Lower values will result in greater accuracy. */
	A3DDouble m_dMinimalTriangleAngleDeg;	/*!< Specifies the angle between two contiguous segments of wire edges for every face.
												Allowable values range from 10 through 30.
												\deprecated This is a deprecated feature. */
	A3DDouble m_dMaxChordHeight;			/*!< Specifies the maximum distance between the original geometric surface specified in the CAD file and the resulting tessellation. Be careful, very small values can generate very large tessellation files.
												\n\n Conversely, very large values can generate inaccurate tessellation. In such cases, it may be better to use the \c m_dChordHeightRatio member to scale this value relative to the object that's being tessellated.
												\note \c m_dMaxChordHeight is ignored if \c m_bUseHeightInsteadOfRatio isn't set to true.*/

	A3DBool m_bAccurateTessellation;		/*!< Accurate tessellation. Uses standard parameters. 'A3D_FALSE' indicates the tessellation is set for visualization. Setting this value to 'A3D_TRUE' will generate tessellation more suited for analysis. Can be used with all TessellationLevelOfDetails. */
	A3DBool m_bAccurateTessellationWithGrid;/*!< Enable accurate tessellation with faces inner points on a grid.*/
	A3DDouble m_dAccurateTessellationWithGridMaximumStitchLength; 	/*!< Maximal grid stitch length. Disabled if value is 0. Be careful, a too small value can generate a huge tessellation. */
	A3DBool m_bAccurateSurfaceCurvatures;	/*!< Take into account surfaces curvature in accurate tessellation to controle triangles elongation directions. Ignored if \c m_bAccurateTessellation isn't set to true.*/

	A3DBool m_bDoNotComputeNormalsInAccurateTessellation;  /*!< Do not compute surface normals in accurate tessellation. Ignored if \c m_bAccurateTessellation isn't set to true.*/

	A3DBool m_bKeepUVPoints;				/*!< Keep parametric points as texture points. */
	A3DBool m_bUseHeightInsteadOfRatio;		/*!< Use \ref m_dMaxChordHeight instead of \ref m_dChordHeightRatio if \ref m_eTessellationLevelOfDetail = \ref kA3DTessLODUserDefined. */
	A3DDouble m_dMaximalTriangleEdgeLength;	/*!< Maximal length of the edges of triangles. Disabled if value is 0. Be careful, a too small value can generate a huge tessellation. */
} A3DRWParamsTessellationData;
#endif

/*!
\ingroup a3d_read
\brief Structure to define additional directories that the load function will search in addition to the root directory.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcPhysicalPath;		/*!< Path of the additional directory. */
	A3DUTF8Char* m_pcLogicalName;		/*!< Only used with CATIA V4 files (see \ref A3DRWParamsCatiaV4Data). */
	A3DBool m_bRecursive;				/*!< A value of \c TRUE specifies that the load function uses recursive searching for the additional directory. */
} A3DRWParamsSearchDirData;
#endif

/*!
\ingroup a3d_read
\brief Structure to define parameters to manage CAD assemblies reading.

This is valid only for CAD formats handling assemblies.
These options identify the locations that the load function searches to load subassemblies and parts for the assembly document.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bUseRootDirectory;		/*!<
											With a value of \c TRUE, the load function searches subparts in the same directory as the head of the assembly.
											If not found, they are searched for in the additional search directories (\ref m_ppcSearchDirectories). */
	A3DBool m_bRootDirRecursive;		/*!<
											Only used if \ref m_bUseRootDirectory is \c TRUE.
											With a value of \c TRUE, the load function searches for all subparts of an assembly in the root directory of the assembly file.
											Then, it searches the subdirectories for any files that it did not find in the root directory. */
	A3DUns32 m_uiSearchDirectoriesSize;	/*!< The Size of \ref m_ppcSearchDirectories. */
	A3DRWParamsSearchDirData** m_ppcSearchDirectories;		/*!< Array of additional directories that the load function will search in addition to the root. */
	A3DUns32 m_uiPathDefinitionsSize;	/*!< The Size of \ref m_ppcPathDefinitions. */
	A3DUTF8Char** m_ppcPathDefinitions; /*!<
											Array to specify the location of text files that define additional paths.
											A text file contains all paths and recursive options. It follows this format: <tt> "path name", [recursive = ] 1/0</tt> \n
											Examples:
											\code
											"c:\data\project\",recursive=1
											"f:\parts",recursive=0
											"d:\special ",1
											"x:\standard-data\production",0
											\endcode */
} A3DRWParamsAssemblyData;
#endif

/*!
\ingroup a3d_read
\brief Structure to define which configurations or sub-models should be loaded when the CAD file has multiple entries.

Some CAD formats may create CAD files with multiple entries. For these files, the A3DRWParamsMultiEntriesData structure
must be used to specify to the load function which entry should be loaded.
The entry can be specified by its name, or it can be the default entry if the CAD file contains one.
<br>
The A3DRWParamsMultiEntriesData structure is implemented for following CAD formats:
<ul>
<li> CATIA V4: Multiple entries are for sub-models. CATIA V4 files never contain default entries. </li>
	<ul>
	<li> \ref m_bLoadDefault is not implemented.</li>
	<li> \ref m_ppcEntries should contain a single name, the name of the root product occurrence to load. </li>
	<li> The load function loads the selected entry, other entries are not loaded, in any way.	</li>
	<li> Entries names can be retrieved from a first call to the load function, and a parsing of the returned model file and the
	product occurrences structure. Here, note that only the root product occurrences should be parsed.</li>
	</ul>
<li> SolidWorks: Multiple entries are for configurations. SolidWorks files always contain a default entry.</li>
	<ul>
	<li> \ref m_bLoadDefault is implemented. </li>
	<li> \ref m_ppcEntries should contain a single name, the name of the configuration to load. </li>
	<li> The load function loads the selected entry, other entries are loaded in a 'structure only' mode
	(only the Product Occurrences structure is loaded, not the geometric content; all these Product Occurrences
	have a status which is not Loaded).</li>
	<li> Entries names can be retrieved from a first call to the load function, and a parsing of the returned model file and the
	product occurrences structure. Here, note that only the product occurrences of type 'Configuration', sons of the root
	'Container' product	should be parsed (see \ref A3DAsmProductOccurrenceData::m_uiProductFlags and \ref a3d_product_flag).	</li>
	</ul>
</ul>


The load function behaves as follows:
\li If no MultiEntries parameter is specified and the file contains multiple entries, the load function loads the product
structure only, and returns an error \ref A3D_LOAD_MULTI_MODELS_CADFILE. The user can use the returned model file to get
the Entry names which will be useful to a second call of the load function with the selected entry.
\li If no MultiEntries parameter is specified and the file contains only one entry, the load function automatically loads the model.
\li If the file contains a default entry and \ref m_bLoadDefault is \c TRUE, the load function automatically loads the default entry.
\li if \ref m_ppcEntries defines an entry as described above and the file contains multiple entries, the selected entry is loaded.


\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bLoadDefault;			/*!< A value of \c TRUE is to load the default entry on a multi-entry file. Nothing changes for non multi-entry files. */
	A3DUns32 m_uiEntriesSize;		/*!< The size of \ref m_ppcEntries. */
	A3DUTF8Char** m_ppcEntries;		/*!< Strings defining the entry to load in case of a multi-entry file. See description paragraph above. */
} A3DRWParamsMultiEntriesData;
#endif



/*!
\ingroup a3d_read
\brief Reading parameters specific to CATIA V4 CAD files.

These parameters are related to CATIA V4 logical names handling (DDName). They should be used in conjunction with the Assembly options
specified in A3DRWParamsAssemblyData and A3DRWParamsSearchDirData.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcRootDirLogicalName;		/*!<
													Only used if \ref A3DRWParamsAssemblyData::m_bUseRootDirectory is \c TRUE.
													If so, the load function first searches subparts in the same directory as the head of the assembly.
													This member enables to define the LogicalName used to perform this root search. */
	A3DBool m_bAllowSearchInOtherLogicalNames;	/*!<
													A value of \c TRUE specifies that the search can be also performed in other directories.
													A value of \c FALSE specifies that the search should be done only in the real logical name of the file. */
} A3DRWParamsCatiaV4Data;
#endif

/*!
\ingroup a3d_read
\brief Reading parameters specific to CATIA V5 CAD files.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bCacheActivation;		/*!< A value of \c TRUE activates the representation mode which may be used with large CATIA V5 assemblies. */
	A3DUTF8Char* m_pcCachePath;		/*!< Specifies the folder that the load function uses as the cache folder. */
} A3DRWParamsCatiaV5Data;
#endif

/*!
\ingroup a3d_read
\brief Reading parameters specific to Unigraphics CAD files.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bApplyToAllLevels;				/*!< With a value of \c TRUE, the reference sets will apply to all the components including subassemblies. */
	A3DUns32 m_uiPreferredReferenceSetsSize;	/*!< The size of \ref m_ppcPreferredReferenceSets. */
	A3DUTF8Char** m_ppcPreferredReferenceSets;	/*!< Determines how reference sets are determined and in what priority. */
	A3DBool m_bFitAllToUpdateViewCameras;		/*!< With a value of \c TRUE, the camera views will be computed after a fit all. Zoom, position, and rotation of the view will be updated. */
} A3DRWParamsUnigraphicsData;
#endif

/*!
\ingroup a3d_read
\brief Reading parameters specific to Pro/ENGINEER CAD files.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcCodePageName;				/*!<
													Code page to use for text conversion to Unicode.
													The values for CodePageName are those usable in the iconv GNU Package
													Example: EUC-JP for Japanese files. */
	A3DBool m_bDisplayTolerance;				/*!< With a value of \c TRUE, read and display the tolerances for dimensions. */
	A3DBool m_bDisplaySubpartAnnotations;		/*!< With a value of \c TRUE, access to the PMIs in subparts in an assembly. */
	A3DProESessionColorType m_eSessionColorType;/*!< Defines the default PMI color type. */
	A3DProEFamilyTables m_eFamilyTables;		/*!< How to read family tables. The following enums are available: \n\n <b> \ref A3DProEFamTabAcceleratorFileOnly "A3DProEFamTabAcceleratorFileOnly:" </b> Nothing will be loaded if there's no XPR/XAS. \n\n <b> \ref A3DProEFamTabOrUseTessellation "A3DProEFamTabOrUseTessellation:"</b> XPR/XAS will be used first; if not available, the tessellation will be used. If tessellation isn't available, nothing will be loaded. \n\n <b> \ref A3DProEFamTabOrUseWireAndGeneric "A3DProEFamTabOrUseWireAndGeneric:" </b> XPR/XAS will be used. If not available, the tessellation will be used. If tessellation isn't available, the wire that represents the family table will be used. If the wire isn't available, the generic part will be loaded. Be sure to see \ref A3DProEFamTabOrUseWireAndGeneric "this warning" regarding this setting. \version 9.0 */
	A3DBool m_bBoolOpUseGenericIfNoTess;		/*!< With a value of \c TRUE, display the generic form of the element if boolean operation and no tessellation to represent it. \version 9.0 */
	A3DBool m_bFlexCompUseGenericIfNoTess;		/*!< With a value of \c TRUE, display the generic form of the element if flexible component and no tessellation to represent it. \version 9.0 */
	A3DBool m_bHideSkeletons;					/*!< With a value of \c TRUE, hides all skeleton type elements from the file. */
	A3DBool m_bReadExplodeStateAsView;			/*!< With a value of \c TRUE, create a view for each explode state. */
	A3DBool m_bDisplayVisibleDatum;				/*!< Manage the visibility of datum entities \version 9.1 */
	A3DProEReadConstructEntities m_eReadConstructEntities;		/*!< Control the sketch reading */
	A3DBool m_bComputeHomeView;					/*!< With a value of \c TRUE, there will be an additional view that reflects the current states (orientation, explode, ...). */
	A3DBool m_bHandlePMIScreenLocation;			/*!<  With a value of \c TRUE, treat screen location in markup reading. \version 9.1 */
	A3DBool m_bIsometricDefaultView;			/*!<  If true, default is isometric else it's trimetric. \version 9.1 */
} A3DRWParamsProEData;
#endif

/*!
\ingroup a3d_read
\brief Threshold values used for checking validation properties in Step.

Step files may contain validation properties that are used to check the validity of a CAD model.
Those values, once read from the file, are compared with the ones computed by Exchange.
Threshold values define the acceptable limit between the read and the computed value.

Two kinds of threshold are used :
- Deviation: maximum distance between points (mm)
- Percent: maximum difference for real values (%)

\version 9.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.

	// Geometry
	A3DDouble m_dGEOMPercentVolume;                      /*!< (%) Volume of solids. */
	A3DDouble m_dGEOMPercentSurfaceArea;                 /*!< (%) Surface of solids. */
	A3DDouble m_dGEOMPercentIndependentSurfaceArea;      /*!< (%) Surface of surfaces. */
	A3DDouble m_dGEOMPercentIndependentCurveLength;      /*!< (%) Length of curves. */

	A3DDouble m_dGEOMDeviationCentroid;                  /*!< (mm) Centroid of solids. */
	A3DDouble m_dGEOMDeviationIndependentSurfaceCentroid;/*!< (mm) Centroid of Surfaces. */
	A3DDouble m_dGEOMDeviationIndependentCurveCentroid;  /*!< (mm) Centroid of curves. */
	A3DDouble m_dGEOMDeviationPointSetCentroid;          /*!< (mm) Centroid of points set . */

	A3DDouble m_dGEOMDeviationBoundingBox;               /*!< (mm) Bounding box of any geometry. */

	//PMI
	A3DDouble m_dPMIPercentAffectedArea;                 /*!< (%) Total area of the faces or surfaces the PMI is attached to. */
	A3DDouble m_dPMIPercentAffectedCurveLength;          /*!< (%) Total curve length of the edges or curves the PMI is attached to. */
	A3DDouble m_dPMIPercentCurveLength;                  /*!< (%) Total length of the PMI wireframe part. */
	A3DDouble m_dPMIPercentSurfaceArea;                  /*!< (%) Total surface of the PMI tessellated part. */
	A3DDouble m_dPMIDeviationCurveCentroid;              /*!< (mm) Centroid of the PMI wireframe part. */
	A3DDouble m_dPMIDeviationSurfaceCentroid;            /*!< (mm) Centroid of the PMI tessellated part. */

	//ASSEMBLY
	A3DDouble m_dASMDeviationNotionalSolidsCentroid;     /*!< (mm) Centroid of the assembly. */

} A3DRWValidationPropertiesThresholdData;
#endif

/*!
\ingroup a3d_read
\brief Reading parameters specific to STEP files.
\version 4.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bPreferProductName;				/*!<
													With a value of \c TRUE, the name of the occurrence is the one defined by PRODUCT.
													Otherwise it is the one defined by NEXT_ASSEMBLY_USAGE_OCCURRENCE. */
	A3DBool m_bPreferFirstColor;				/*!< In case a geometry has several colors (with STYLED_ITEM), prefer the first one. */
	A3DEStepNameFromNAUO m_eNameFromNAUO;		/*!< In case \ref m_bPreferProductName = \c FALSE, indicate which name to use from NEXT_ASSEMBLY_USAGE_OCCURRENCE. */
	A3DUTF8Char* m_pcCodePageName;				/*!<
													Code page to use for text conversion to Unicode.
								   					The values for CodePageName are those usable in the iconv GNU Package
								   					Example: EUC-JP for Japanese files. \version 7.1 */
	A3DBool m_bSplitSHELL_BASED_SURFACE_MODEL;	/*!< If \c TRUE, split SHELL_BASED_SURFACE_MODEL with several OPEN_SHELLs into several bodies. \version 7.2 */
	A3DBool m_bHealOrientations;				/*!< If \c TRUE, activate orientation healing on the modelfile. Reading time will be longer. \version 7.2 */
	A3DBool m_bReadValidationProperties;		/*!< Read validation properties from the file \version 9.2 */
	A3DBool m_bComputeValidationProperties;		/*!< If \c TRUE and m_bReadValidationProperties is \c TRUE Exchange will compute validation properties and compare them
													with the ones read from the file. Only available with AP242. \version 9.2 */
	A3DBool m_bAddResultToName;					/*!< If \c TRUE and m_bComputeValidationProperties \c TRUE Exchange will add (OK) or (FAIL) to the name of the item that has validation properties. \version 9.2 */
	A3DRWValidationPropertiesThresholdData m_sValidationPropertiesThreshold; /*!< if m_bComputeValidationProperties is \c TRUE, m_sValidationPropertiesThreshold will give the precision to use for compare. \version 9.2 */
} A3DRWParamsStepData;
#endif

/*!
\ingroup a3d_read
\brief Reading parameters specific to IGES files.
\version 5.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bSewBrepModels;					/*!< \deprecated Please use \ref A3DAsmModelFileSew instead. */
} A3DRWParamsIGESData;
#endif

/*!
\ingroup a3d_read
\brief Reading parameters specific to IFC files.
\version 5.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcCodePageName;				/*!<
													Code page to use for text conversion to Unicode.
													The values for CodePageName are those usable in the iconv Gnu Package
													Example: EUC-JP for Japanese files. */
	A3DUTF8Char* m_pcXMLFilePathForAttributes;	/*!< If set and m_bReadAttributes==true , attributes will be saved to this external XML file. \version 7.1 */
	A3DBool m_bIFCOWNERHISTORYOptimized;		/*!<
													If true, IFCOWNERHISTORY will be set only when necessary in order to avoid repetition.
													Otherwise, IFCOWNERHISTORY will be set on all items (default behavior until version 7.1). \version 7.1 */
	A3DBool m_bFACETED_BREPAsOneFace;			/*!< If true, FACETEDBREP faces are gathered in one face. \version 8.0. */
	A3DBool m_bAttributesOnlyGlobalId;          /*!< If true, writes only GlobalId as attribute. \version 8.0. */
	A3DBool m_bHideWireframes;                   /*!< If true and m_bReadWireframes==true hide wireframe . \version 12.1. */
} A3DRWParamsIFCData;
#endif

/*!
\ingroup a3d_read
\brief A structure that specifies parameters used to read the model file to JT format.
\version 8.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;									// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DEJTReadTessellationLevelOfDetail m_eReadTessellationLevelOfDetail;	/*!<  LOD to retrieve: low, medium or high. */
} A3DRWParamsJTData;
#endif


/*!
\ingroup a3d_read
\brief A structure that specifies parameters used to read the model file to Parasolid format.
\version 8.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;									// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bKeepParsedEntities;								/*!< Keep parsed data to avoid passing by PRC data when rewriting to parasolid */
} A3DRWParamsParasolidData;
#endif

/*!
\ingroup a3d_read
\brief A structure that specifies parameters used to read the model file to Solidworks format.
\version 8.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;									// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bLoadAllConfigsData;								/*!< Ask to load data, tree, attributes, representation, PMI, for all configs */
	A3DUns16 m_usDisplayVisibleDatum;							/*!< Manage the visibility of datum entities:
																											- if set to 0, visibility is hidden.
																											- if set to 1, visibility is as in SolidWorks feature tree manager.
																											\remark If \ref A3DRWParamsGeneralData::m_bReadConstructionAndReferences is set to A3D_FALSE,
																											then datum are not loaded, so m_usDisplayVisibleDatum is not taken into account
																*/
} A3DRWParamsSolidworksData;
#endif

/*!
\ingroup a3d_read
\brief A structure that specifies parameters used to read the model file to Inventor format.
\version 9.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;									// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bUseTessForFile;									/*!< Use the embedded tessellation instead of generating one. \version 9.0 */
} A3DRWParamsInventorData;
#endif

/*!
\ingroup a3d_read
\brief A structure that specifies parameters used to read the model file to Rhino format.
\version 11.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;									// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bForceRenderedModeColors;								/*!< Force rendered mode colors instead of shading mode colors. \version 11.2 */
} A3DRWParamsRhinoData;
#endif


/*!
\ingroup a3d_read
\brief Structure to specify reading parameters specific to some CAD formats.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DRWParamsCatiaV4Data m_sCatiaV4;			/*!< Reading parameters specific to CATIA V4. */
	A3DRWParamsCatiaV5Data m_sCatiaV5;			/*!< Reading parameters specific to CATIA V5. */
	A3DRWParamsUnigraphicsData m_sUnigraphics;	/*!< Reading parameters specific to UG. */
	A3DRWParamsProEData m_sProE;				/*!< Reading parameters specific to ProE. */
	A3DRWParamsStepData m_sStep;				/*!< Reading parameters specific to STEP. */
	A3DRWParamsIGESData m_sIGES;				/*!< Reading parameters specific to IGES. */
	A3DRWParamsIFCData m_sIFC;					/*!< Reading parameters specific to IFC. \version 5.2. */
	A3DRWParamsJTData m_sJT;					/*!< Reading parameters specific to JT.  \version 8.0. */
	A3DRWParamsParasolidData m_sParasolid;		/*!< Reading parameters specific to Parasolid.  \version 8.1. */
	A3DRWParamsSolidworksData m_sSolidworks;	/*!< Reading parameters specific to Solidworks.  \version 8.2. */
	A3DRWParamsInventorData m_sInventor;		/*!< Reading parameters specific to Inventor.  \version 9.0. */
	A3DRWParamsRhinoData m_sRhino;				/*!< Reading parameters specific to Inventor.  \version 11.2. */
} A3DRWParamsSpecificLoadData;
#endif

/*!
\ingroup a3d_read
\brief Structure to specify reading parameters to load some specific parts of an assembly.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;							// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bLoadStructureOnly;						/*!< With a value of \c TRUE, read and return only the tree of the assembly in a model file. */
	A3DBool m_bLoadNoDependencies;						/*!< When m_bLoadNoDependencies is true, the shattered reading mode is activated; please note, this is an independent type of incremental loading, separate from typical incremental loading. (As an illustration, please see the "Shattered" code sample in the HOOPS Exchange samples.) \n\n Each CAD file from a top assembly (parts and sub-assembly) is loaded and exported to PRC separately, resulting in one intermediate PRC file per CAD file. Only the specified import file content is read. \n\n Exchange will import as much information as possible (and will override some import settings in the process). If the file is a part, only the part content will be read (geometry / tessellation). If the file is an assembly, only the content of the file will be read (assembly tree, transformation, assembly attributes, etc.). External subassembly files and external part files are not treated. \n\n Once this process is complete, all of the intermediate PRC files should be reassembled into a single PRC file with the function \ref A3DAsmModelFileLoadFromPRCFiles. */
	A3DAsmProductOccurrence *m_pRootProductOccurrence;	/*!< Root of the product occurrence to read. */
	A3DUns32 m_uiProductOccurrencesSize;					/*!< The size of \ref m_ppProductOccurrences. */
	A3DAsmProductOccurrence **m_ppProductOccurrences;	/*!< Array of product occurrences to read. They must be sons of m_pRootProductOccurrence. */
} A3DRWParamsIncrementalLoadData;
#endif

/*!
\ingroup a3d_read
\brief Structure to define the reading parameters used by the load function \ref A3DAsmModelFileLoadFromFile.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;							// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DRWParamsGeneralData m_sGeneral;					/*!< Specifies the general reading parameters. */
	A3DRWParamsPmiData m_sPmi;							/*!< Specifies the PMI reading parameters. */
	A3DRWParamsTessellationData m_sTessellation;		/*!< Specifies the tessellation reading parameters. */
	A3DRWParamsAssemblyData m_sAssembly;				/*!< Specifies the reading parameters used to load Assembly files. */
	A3DRWParamsMultiEntriesData m_sMultiEntries;		/*!< Specifies the parameters used to read multiple-models. */
	A3DRWParamsSpecificLoadData m_sSpecifics;			/*!< Specifies the reading parameters for some specific CAD formats. */
	A3DRWParamsIncrementalLoadData m_sIncremental;		/*!< Specifies the reading parameters used to load specific parts of an assembly. */
} A3DRWParamsLoadData;
#endif

/*!
\ingroup a3d_read
\brief Loads an \ref A3DAsmModelFile from a physical file.

This function loads an \ref A3DAsmModelFile from a file. The file could be in any supported CAD format.

\param [in] pcFileName References the path to the file containing the 3D CAD model.
\param [in] pLoadParametersData References the parameters for reading.
\param [out] ppModelFile References a pointer into which should be stored the location
of the model file. Set this pointer to null before calling the function.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_LOAD_CANNOT_ACCESS_CADFILE \n
\return \ref A3D_LOAD_CANNOT_LOAD_MODEL \n
\return \ref A3D_LOAD_CANNOT_LOAD_MULTIENTRY \n
\return \ref A3D_LOAD_EMPTY_MULTI_MODEL \n
\return \ref A3D_LOAD_FILE_TOO_OLD \n
\return \ref A3D_LOAD_FILE_TOO_RECENT \n
\return \ref A3D_LOAD_INVALID_FILE_FORMAT \n
\return \ref A3D_LOAD_MISSING_COMPONENTS \n
\return \ref A3D_LOAD_MULTI_MODELS_CADFILE if the file contains multiple entries (see A3DRWParamsMultiEntriesData). \n
\return \ref A3D_LOAD_READER_NOT_IMPLEMENTED \n
\return \ref A3D_SUCCESS \n

\version 3.0
*/
A3D_API (A3DStatus, A3DAsmModelFileLoadFromFile,(
	const A3DUTF8Char* pcFileName,
	const A3DRWParamsLoadData* pLoadParametersData,
	A3DAsmModelFile** ppModelFile));


/*!
\ingroup a3d_read
\brief Loads an \ref A3DAsmModelFile from a PRC Buffer.

This function loads an \ref A3DAsmModelFile from a PRC Buffer.

\param [in] pcBufferStream Buffer containing the PRC stream.
\param [in] uBufferLength Length of pcBufferStream.
\param [in,out] ppPrcReadHelper Reserved for future use.
\param [out] ppModelFile References a pointer into which the location should be stored.
of the model file. Set this pointer to null before calling the function.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_LOAD_CANNOT_ACCESS_CADFILE \n
\return \ref A3D_LOAD_CANNOT_LOAD_MODEL \n
\return \ref A3D_LOAD_CANNOT_LOAD_MULTIENTRY \n
\return \ref A3D_LOAD_EMPTY_MULTI_MODEL \n
\return \ref A3D_LOAD_FILE_TOO_OLD
\return \ref A3D_LOAD_FILE_TOO_RECENT
\return \ref A3D_LOAD_INVALID_FILE_FORMAT \n
\return \ref A3D_LOAD_MISSING_COMPONENTS \n
\return \ref A3D_LOAD_MULTI_MODELS_CADFILE if the file contains multiple entries (see A3DRWParamsMultiEntriesData). \n
\return \ref A3D_LOAD_READER_NOT_IMPLEMENTED \n
\return \ref A3D_SUCCESS \n

\version 5.2
*/
A3D_API (A3DStatus, A3DAsmModelFileLoadFromPrcStream,(
	const A3DUTF8Char* pcBufferStream,
	unsigned int uBufferLength,
	A3DRWParamsPrcReadHelper** ppPrcReadHelper,
	A3DAsmModelFile** ppModelFile));


/*!
\ingroup a3d_read
\brief Free PRC read helper.
\param [in,out] pA3DRWParamsPrcReadHelper The helper to free.
\remark Because this function does not return A3DStatus the internal thread safety policy is to wait for the availability of the API.
\version 8.1
*/
A3D_API(A3DVoid, A3DRWParamsPrcReadHelperFree, (A3DRWParamsPrcReadHelper* pA3DRWParamsPrcReadHelper));

/*!
\ingroup a3d_read
\brief Loads an \ref A3DAsmModelFile from a PRC physical file.

This function loads an \ref A3DAsmModelFile from a PRC file. The file must be in PRC format.

\param [in] pcFileName References the path to the file containing the 3D CAD model.
\param [in,out] ppPrcReadHelper Reserved for future use.
\param [out] ppModelFile References a pointer into which should be stored the location
of the model file. Set this pointer to null before calling the function.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_LOAD_CANNOT_ACCESS_CADFILE \n
\return \ref A3D_LOAD_CANNOT_LOAD_MODEL \n
\return \ref A3D_LOAD_CANNOT_LOAD_MULTIENTRY \n
\return \ref A3D_LOAD_EMPTY_MULTI_MODEL \n
\return \ref A3D_LOAD_FILE_TOO_OLD \n
\return \ref A3D_LOAD_FILE_TOO_RECENT \n
\return \ref A3D_LOAD_INVALID_FILE_FORMAT \n
\return \ref A3D_LOAD_MISSING_COMPONENTS \n
\return \ref A3D_LOAD_MULTI_MODELS_CADFILE if the file contains multiple entries (see A3DRWParamsMultiEntriesData). \n
\return \ref A3D_LOAD_READER_NOT_IMPLEMENTED \n
\return \ref A3D_SUCCESS \n

\version 4.0
*/
A3D_API (A3DStatus, A3DAsmModelFileLoadFromPrcFile,(
	const A3DUTF8Char* pcFileName,
	A3DRWParamsPrcReadHelper** ppPrcReadHelper,
	A3DAsmModelFile** ppModelFile));


/*!
\ingroup a3d_read
\brief Binary stream for a 3D model stored in a PDF file. The data format is PRC or U3D.

\version 6.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char *m_acStream;					/*!< 3D stream. */
	A3DBool m_bIsPrc;							/*!< True if stream is PRC data, false if it is U3D data. */
	A3DUns32 m_uiStreamSize;					/*!< Size of the stream. */
}A3DStream3DPDFData;
#endif // A3DAPI_LOAD



/*!
\ingroup a3d_read
\brief Function to retrieve all 3D streams embedded in a PDF document.

The stream is the raw binary data stored as a char* stream. A PRC stream can be interpreted with the function A3DAsmModelFileLoadFromPrcStream.
A U3D stream needs to be written as a physical file before being read with classical A3DAsmModelFileLoadFromFile function.
\param [in] pcFileName References the path to the PDF file
\param [out] ppStreamData Array of stream data
\param [out] piNumStreams Number of streams

If pcFileName is NULL, *ppStreamData will be freed if *piNumStreams is non-null. A3DGet3DPDFStreams(NULL, ppStreamData, piNumStreams) to
release *ppStreamData.

\return \ref A3D_LOAD_CANNOT_ACCESS_CADFILE \n
\return \ref A3D_SUCCESS \n

\version 6.0
*/
A3D_API (A3DStatus, A3DGet3DPDFStreams, (
		 const A3DUTF8Char* pcFileName,
		 A3DStream3DPDFData** ppStreamData,
		 A3DInt32* piNumStreams));

/*!
\ingroup a3d_write
\brief A structure that specifies parameters used to write the model file to PRC format.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bCompressBrep;					/*!< Is the B-rep data to be compressed? */
	A3DBool m_bCompressTessellation;			/*!< A value of \c TRUE applies lossy compression to polygons. */
	A3DECompressBrepType m_eCompressBrepType;	/*!< Level of B-rep compression. */
	A3DBool m_bRemoveBRep;						/*!< Removes B-rep data from the model. \version 6.1 */
	A3DBool m_bRemoveAttributes;                /*!< Removes attributes from the model. \version 6.1 */
} A3DRWParamsExportPrcData;
#endif // A3DAPI_LOAD



/*!
\ingroup a3d_write
\brief Writes a model file to a physical file in a PRC format.

This function writes the \ref A3DAsmModelFile entity to a physical file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [in] pcCADFileName References the path of the file into which the function stores the model file.
\param [in,out] ppPrcWriteHelper Used to get PRC data such as unique identifiers of PRC nodes.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 3.0
*/
A3D_API ( A3DStatus, A3DAsmModelFileExportToPrcFile, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportPrcData* pParamsExportData,
	const A3DUTF8Char* pcCADFileName,
	A3DRWParamsPrcWriteHelper** ppPrcWriteHelper));

/*!
\ingroup a3d_write
\brief Free PRC write helper.
\param [in,out] pA3DRWParamsPrcWriteHelper The helper to free.
\remark Because this function does not return A3DStatus the internal thread safety policy is to wait for the availability of the API.
\version 3.0
*/
A3D_API (A3DVoid, A3DRWParamsPrcWriteHelperFree,(A3DRWParamsPrcWriteHelper* pA3DRWParamsPrcWriteHelper));


/*!
\ingroup a3d_write
\brief A structure that specifies parameters used to write the model file to U3D format.
\version 4.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;				// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DEU3DVersion m_eU3DVersion;			/*!< Output type. */
	A3DBool m_bMeshQuality;					/*!< Are tessellations to be compressed? */
	A3DUns8 m_ucMeshQualityValue;			/*!< Level of compression [0, 100]. */
} A3DRWParamsExportU3DData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_write
\brief Writes a model file to a physical file in Universal 3D format.

This function writes the \ref A3DAsmModelFile entity to a physical file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [in] pcCADFileName References the path of the file into which the function stores the model file.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 4.0
*/
A3D_API ( A3DStatus, A3DAsmModelFileExportToU3DFile, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportU3DData* pParamsExportData,
	const A3DUTF8Char* pcCADFileName));




/*!
\ingroup a3d_write
\brief A structure that specifies parameters used to write the model file to a STEP format.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DEStepFormat m_eStepFormat;		/*!< Determines the STEP format. */
						/*!< if the file extension is .stpZ the file will be compressed. \version 10.2 */
	A3DBool m_bSaveFacetedToWireframe;	/*!<
										Determines if entities are faceted or exported as wireframes.
										\li A value of \c TRUE save polyedrics as wireframe.
										\li A value of \c FALSE save polyedrics as faces. */
	A3DBool m_bSaveAnalyticsToNurbs;	/*!<
										Determines the format for saving analytic surfaces, such as cylinders, cones, planes, and so forth.
										\li A value of \c TRUE converts all analytics to NURBS surfaces.
										\li A value of \c FALSE keeps analytics as analytics. */
	A3DBool m_bUseShortNames;			/*!< A value of \c TRUE shortens entity names to reduce the file size. It is not recommended to use that option
										as not all CAD software are able to import such files.
										Not available in AP242.*/
	A3DUTF8Char* m_pcApplication;		/*!< Application name (will be reported in the output file). */
	A3DUTF8Char* m_pcVersion;			/*!< Application version (will be reported in the output file). */
	A3DBool m_bWritePMI;				/*!< Export or not PMI. Only available for AP242 \version 9.1 */
	A3DBool m_bWriteAttributes;			/*!< Export or not Attributes. Only available for AP242 \version 9.1 */
	A3DBool m_bWriteUVCurves;			/*!< Export PCURVE + 3D Curve on Brep \version 9.1 */
	A3DUTF8Char* m_pcConfig;			/*!< Export only the configuration with this name \version 9.1 */
	A3DBool m_bWriteValidationProperties;/*!< Compute and Export ValidationPropertiess. Only available for AP242 \version 9.2 */
	A3DBool m_bWritePMIWithSemantic;	/*!< if m_bWritePMI is true, export PMIs with semantic information. Only available for AP242 \version 10.1 */
	A3DBool m_bWritePMIAsTessellated;	/*!< if m_bWritePMI is true, export PMIs as Tessellated rather than polyline. Files are bigger with this option. Only available for AP242 \version 10.1 */
	A3DUTF8Char* m_pcUser;				/*!< User name (will be reported in the output file). If NULL, the system user name will be output. */
	A3DUTF8Char* m_pcOrganisation;		/*!< Organisation name (will be reported in the output file). If NULL, "Unknown organisation" will be output. */
	A3DUTF8Char* m_pcAuthorisation;		/*!< Authorisation name (will be reported in the output file). If NULL, "Unknown authorisation" will be output. */
} A3DRWParamsExportStepData;
#endif // A3DAPI_LOAD


/*!
\ingroup a3d_write
\brief Writes a model file to a physical file in STEP format.

This function writes the \ref A3DAsmModelFile entity to a physical file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [in] pcCADFileName References the path of the file into which the function stores the model file.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 3.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToStepFile, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportStepData* pParamsExportData,
	const A3DUTF8Char* pcCADFileName));


/*!
\ingroup a3d_write
\brief A structure that specifies parameters used to write the model file to JT format.
\version 6.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DEWriteGeomTessMode m_eWriteGeomTessMode;	/*!< Enum to set the content to write (Geometry and/or Tessellation). */
	A3DBool m_bWriteHiddenObjects;				/*!< Export or not hidden objects. */
	A3DBool m_bWritePMI;						/*!< Export or not PMI. */
	A3DEJTVersion m_eJTVersion;					/*!< JT Version to export. \version 9.2 */
} A3DRWParamsExportJTData;
#endif // A3DAPI_LOAD


/*!
\ingroup a3d_write
\brief Writes a model file to a physical file in JT format.

This function writes the \ref A3DAsmModelFile entity to a physical file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [in] pcCADFileName References the path of the file into which the function stores the model file.

\return \ref A3D_SUCCESS \n

\version 6.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToJTFile, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportJTData* pParamsExportData,
	const A3DUTF8Char* pcCADFileName));

/*!
\ingroup a3d_write
\brief Writes a model file to a physical file in VRML format.

This function writes the \ref A3DAsmModelFile entity to a physical file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pcCADFileName References the path of the file into which the function stores the model file.

\return \ref A3D_INVALID_LICENSE \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 6.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToVrmlFile, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DUTF8Char* pcCADFileName));


/*!
\ingroup a3d_write
\brief A structure that specifies parameters used to write the model file to IGES format.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bSaveAnalyticsToNurbs;	/*!<
										Determines the format for saving analytic surfaces, such as cylinders, cones, planes, and so forth.
										\li A value of \c TRUE converts all analytics to NURBS surfaces.
										\li A value of \c FALSE keeps analytics as analytics. */
	A3DBool m_bSaveFacetedToWireframe;	/*!<
										Determines if entities are faceted or exported as wireframes.
										\li A value of \c TRUE save polyedrics as wireframe.
										\li A value of \c FALSE save polyedrics as faces. */
	A3DBool m_bSaveSolidsAsFaces;		/*!<
										Specifies how solids are written.
										\li With a value of \c TRUE, solids are written as independent faces.
										\li With a value of \c FALSE, solid topology is kept. */
	A3DBool m_bWriteHiddenObjects;		/*!< A value of \c TRUE includes any hidden entities in the exported file. */
	A3DBool m_bWriteTessellation;		/*!< If \c TRUE tessellations will be converted to bodies. if \c FALSE tessellations will be ignored. */
	A3DUTF8Char* m_pcApplication;       /*!< Application name (will be reported in the output file). */
	A3DUTF8Char* m_pcVersion;		    /*!< Application version (will be reported in the output file). */
} A3DRWParamsExportIgesData;
#endif // A3DAPI_LOAD


/*!
\ingroup a3d_write
\brief Writes a model file to a physical file in IGES format.

This function writes the \ref A3DAsmModelFile entity to a physical file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [in] pcCADFileName References the path of the file into which the function stores the model file.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 3.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToIgesFile, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportIgesData* pParamsExportData,
	const A3DUTF8Char* pcCADFileName));



/*!
\ingroup a3d_write
\brief A structure that specifies parameters used to write the model file to STL format.
\version 10.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;				// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bBinaryFile;					/*!< With a value of \c TRUE, compresses the STL file to reduce file size. */
	A3DETessellationLevelOfDetail m_eTessellationLevelOfDetail;	/*!< Enum to specify predefined values for some following members. */
	A3DDouble m_dChordHeightRatio;			/*!<Specifies the ratio of the diagonal length of the bounding box to the chord height. A value of 50 means that the diagonal of the bounding box is 50 times greater than the chord height. Values can range from 50 to 10,000. Higher values will generate more accurate tessellation. */
	A3DDouble m_dAngleToleranceDeg;			/*!<
											Specifies the maximum angle between two contiguous segments of wire edges for every face.
											The value must be from 10 through 40. */
	A3DDouble m_dMinimalTriangleAngleDeg;	/*!<
											Specifies the angle between two contiguous segments of wire edges for every face.
											Allowable values range from 10 through 30. */
	A3DDouble m_dMaxChordHeight;			/*!< \copydoc A3DRWParamsTessellationData::m_dMaxChordHeight */

	A3DBool m_bAccurateTessellation;		/*!< Accurate tessellation. Uses standard parameters. 'false' indicates the tessellation is set for visualization. Setting this value to 'true' will generate tessellation more suited for analysis. Can be used with all TessellationLevelOfDetails. */
	A3DBool m_bAccurateTessellationWithGrid;/*!< Accurate tessellation with faces inner points on a grid.*/
	A3DDouble m_dAccurateTessellationWithGridMaximumStitchLength; 	/*!< Maximal grid stitch length. Disabled if value is 0. Be careful, a too small value can generate a huge tessellation. */
	A3DBool m_bAccurateSurfaceCurvatures;	/*!< Take into account surfaces curvature in accurate tessellation to controle triangles elongation directions. Ignored if \c m_bAccurateTessellation isn't set to true.*/

	A3DBool m_bKeepCurrentTessellation;		/*!< With a value of \c TRUE, keeps the current tessellation. */
	A3DBool m_bUseHeightInsteadOfRatio;		/*!< Use \ref m_dMaxChordHeight instead of \ref m_dChordHeightRatio if \ref m_eTessellationLevelOfDetail = \ref kA3DTessLODUserDefined. */
	A3DDouble m_dMaximalTriangleEdgeLength;	/*!< Maximal length of the edges of triangles. Disabled if value is 0. Be careful, a too small value can generate a huge tessellation. */
	A3DEUnits m_eExportUnit;				/*!< Unit of the exported model. STL files have no unit however this value allow you to define it. This scales the model to match the target unit. Default value is set to kA3DUnitMillimeter. Setting to kA3DUnitUnknown keeps the original unit.*/
} A3DRWParamsExportStlData;
#endif // A3DAPI_LOAD


/*!
\ingroup a3d_write
\brief Writes a model file to a physical file in STL format.

This function writes the \ref A3DAsmModelFile entity to a physical file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [in] pcCADFileName References the path of the file into which the function stores the model file.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 3.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToStlFile, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportStlData* pParamsExportData,
	const A3DUTF8Char* pcCADFileName));


/*!
\ingroup a3d_write
\brief A structure that specifies parameters used to write the model file to 3MF format.
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;				       // Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bKeepCurrentTessellation;            /*!< if true, keep thesellation of the Brep , otherwise retessallate BRep using m_sTessellation. */
	A3DRWParamsTessellationData m_sTessellation;   /*!< Specifies the tessellation parameters. Used when no tessellation is present or if m_bKeepCurrentTessellation is false. */
	A3DUTF8Char* m_pcDescription;	           /*!< Description (will be reported in the output file as metadata). */
	A3DUTF8Char* m_pcCopyright;	           /*!< Copyright; (will be reported in the output file as metadata). */
	A3DUTF8Char* m_pcLicenseTerms;	       /*!< License Terms (will be reported in the output file as metadata). */
} A3DRWParamsExport3mfData;
#endif // A3DAPI_LOAD



/*!
\ingroup a3d_write
\brief Writes a model file to a physical file in 3MF format.

This function writes the \ref A3DAsmModelFile entity to a physical file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [in] pcCADFileName References the path of the file into which the function stores the model file.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 10.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportTo3mfFile, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExport3mfData* pParamsExportData,
	const A3DUTF8Char* pcCADFileName));










/*!
\ingroup a3d_write
\brief A structure that specifies parameters used to write the model file to XML format
\version 7.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;     					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool  m_bExportMetadata;						/*!< Set to true to export metadata to XML. */
	A3DBool  m_bExportTransformations;				/*!< Set to true to export transformations to XML. */
	A3DBool  m_bExportColorMaterial;				/*!< Set to true to export material and color tables to XML. */
	A3DBool  m_bExportProductInformationByFormat;	/*!< Set to true to export Product information specific to format to XML. */
} A3DRWParamsExportXMLData;
#endif // A3DAPI_LOAD


/*!
\ingroup a3d_write
\brief Writes a model file to a physical file in XML format

This function writes the \ref A3DAsmModelFile entity to a physical file,
and fills two arrays of size uMapSize (puMapXmlId and ppMapProductOccurrences)
that will act as a map of the product occurrences and their respective IDs in the XML file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [in] pcXMLFileName References the path of the file into which the function stores the model file.
\param [out] uMapSize Size of the two following arrays. If set to NULL, the map construction is bypassed.
\param [out] puMapXmlIds Array of the product occurrence IDs in the XML file. If set to NULL, the map construction is bypassed. It has to be freed with the custom free memory callback.
\param [out] ppMapProductOccurrences Array of the product occurrences in the XML file. If set to NULL, the map construction is bypassed. It has to be freed with the custom free memory callback.

\return \ref A3D_INVALID_LICENSE \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n
\version 7.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToXMLFile, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportXMLData* pParamsExportData,
	const A3DUTF8Char* pcXMLFileName,
	unsigned int* uMapSize,
	unsigned int** puMapXmlIds,
	A3DEntity*** ppMapProductOccurrences));



/*!
\ingroup a3d_write
\brief A structure that specifies parameters used to write the model file to OBJ format.
\version 12.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;				// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcTextureFolder;			/*!< Optional folder where texture will be exported (if any). If NULL, then texture are written in the same directory as the .obj and mtl. Symbol @ is expanded as Filename of current file (without extension) */
} A3DRWParamsExportObjData;
#endif // A3DAPI_LOAD



/*!
\ingroup a3d_write
\brief Writes a model file to a physical file in OBJ format (with MTL for the materials).

This function writes the \ref A3DAsmModelFile entity to a physical file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [in] pcCADFileName References the path of the file into which the function stores the model file.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 12.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToObjFile, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportObjData* pParamsExportData,
	const A3DUTF8Char* pcCADFileName));


/*!
\ingroup a3d_write
\brief A structure that specifies parameters used to write the model file to FBX format.
\version 12.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;				// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bAscii;						/*!< Control if the FBX is in Ascii or Binary format */
	A3DUTF8Char* m_pcTextureFolder;			/*!< Optional folder where texture will be exported (if any). If NULL, then texture are written in the same directory as the .fbx. Symbol @ is expanded as Filename of current file (without extension) */
} A3DRWParamsExportFbxData;
#endif // A3DAPI_LOAD



/*!
\ingroup a3d_write
\brief Writes a model file to a physical file in FBX format.

This function writes the \ref A3DAsmModelFile entity to a physical file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [in] pcCADFileName References the path of the file into which the function stores the model file.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 12.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToFbxFile, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportFbxData* pParamsExportData,
	const A3DUTF8Char* pcCADFileName));



#endif // __A3DCOMMONREADWRITE_H__
