/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for the Publish module
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifdef A3DAPI_LOAD
#  undef __A3DPDFADVANCEDPUBLISHSDK_H__
#endif
#ifndef __A3DPDFADVANCEDPUBLISHSDK_H__
#define __A3DPDFADVANCEDPUBLISHSDK_H__
#ifndef A3DAPI_LOAD
#  include <A3DPDFInitializeFunctions.h>
#  include <A3DPDFPublishSDK.h>
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_widget_module
\brief Function to add an item to a form field of type list

\param [in,out] pPage The Page object to work with.
\param [in] pcFieldName The name of the form field. Only Acroform fields are supported.
\param [in] pcItemValue The item string which will be visible to the user in the list field.
\param [in] pcItemExportValue The export value for the item. This value acts as a 'symbolic value' for the item.
\return \ref A3D_SUCCESS \n
\version 4.3
*/
A3D_API (A3DStatus, A3DPDFPageFieldListAddItem, (A3DPDFPage* pPage, const A3DUTF8Char* pcFieldName, const A3DUTF8Char* pcItemValue, const A3DUTF8Char* pcItemExportValue));


/*!
\defgroup a3d_pdf_javascript_module JavaScript Module
\ingroup a3d_pdf_interactivity_module
\brief <b>(HOOPS Publish Advanced) </b>Functions for setting JavaScript code on PDF entities.

This module describes the functions and structures that allow you to define JavaScript code on PDF entities,
such as Document, Page, 3D annot, or Fields.
*/

/*!
\ingroup a3d_pdf_javascript_module
\brief Adds JavaScript to a document. This JavaScript is launched when the file is opened.

\param [in,out] pDoc The Document object to work with.
\param [in] pcScriptName The name of the script. It is not used by Acrobat but needs to be unique.
\param [in] pcJavascriptString The string which contains the JavaScript.
\return \ref A3D_SUCCESS \n
\version 4.3
*/
A3D_API (A3DStatus, A3DPDFDocumentAddJavascriptFromString, (A3DPDFDocument* pDoc, const A3DUTF8Char* pcScriptName, const A3DUTF8Char* pcJavascriptString));


/*!
\ingroup a3d_pdf_javascript_module
\brief Function to attach JavaScript to a form field. The JavaScript is defined as a string.

The goal of this function is to define a JavaScript action to be launched on an event. The action event is different depending on the type of the field.
For fields of type:
<ul>
<li> dropdown list: the action is the selection of a list item.</li>
<li> listbox list: the action is the selection of a list item.</li>
<li> button: the action is the button pushed.</li>
</ul>
\param [in,out] pPage The Page object to work with.
\param [in] pcFieldName The name of the form field. Only Acroform fields are supported.
\param [in] pcJavascriptString The string which contains the JavaScript.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFPageFieldSetActionJavascriptFromString, (A3DPDFPage* pPage, const A3DUTF8Char* pcFieldName, const A3DUTF8Char* pcJavascriptString));

/*!
\ingroup a3d_pdf_javascript_module
\brief Function to attach JavaScript to a form field. The JavaScript is defined as a string.

The goal of this function is to define a JavaScript action to be launched on an event. The action event is different depending on the type of the field.
For fields of type:
<ul>
<li> dropdown list: the action is the selection of a list item.</li>
<li> listbox list: the action is the selection of a list item.</li>
<li> button: the action is the button pushed.</li>
</ul>
\param [in,out] pPage The Page object to work with.
\param [in] pcFieldName The name of the form field. Only Acroform fields are supported.
\param [in] eEventActionType The action type. Only \ref kA3DPDFEventPageOpened and \ref kA3DPDFEventPageClosed are relevant at page level.
\param [in] pcJavascriptString The string which contains the JavaScript.
\return \ref A3D_SUCCESS \n
\version 9.0
*/
A3D_API (A3DStatus, A3DPDFPageFieldSetActionJavascript, (A3DPDFPage* pPage, const A3DUTF8Char* pcFieldName, const A3DPDFEEventActionType eEventActionType, const A3DUTF8Char* pcJavascriptString));


/*!
\ingroup a3d_pdf_javascript_module
\brief Function to define a javaScript action to an event triggered on the document. The JavaScript is defined as a string.

The goal of this function is to define a JavaScript action to be launched on an event triggered on the document level.
\param [in] pDoc The Document object to work with.
\param [in] eEventActionType The action type.
Only \ref kA3DPDFEventDocWillClose,  \ref kA3DPDFEventDocWillSave \ref kA3DPDFEventDocDidSave \ref kA3DPDFEventDocWillPrint and \ref kA3DPDFEventDocDidPrint are relevant at document level.
\param [in] pcJavascriptString The string which contains the JavaScript.
\return \ref A3D_SUCCESS \n
\version 9.0
*/
A3D_API (A3DStatus, A3DPDFDocumentSetActionJavascript, (A3DPDFDocument* pDoc, const A3DPDFEEventActionType eEventActionType, const A3DUTF8Char* pcJavascriptString));

/*!
\ingroup a3d_pdf_javascript_module
\brief Function to define a javaScript action to an event triggered on the page. The JavaScript is defined as a string.

The goal of this function is to define a JavaScript action to be launched on an event triggered on the page level.
\param [in] pPage The Page object to work with.
\param [in] eEventActionType The action type. Only \ref kA3DPDFEventPageOpened and \ref kA3DPDFEventPageClosed are relevant at page level.
\param [in] pcJavascriptString The string which contains the JavaScript.
\return \ref A3D_SUCCESS \n
\version 9.0
*/
A3D_API (A3DStatus, A3DPDFPageSetActionJavascript, (A3DPDFPage* pPage, const A3DPDFEEventActionType eEventActionType, const A3DUTF8Char* pcJavascriptString));

/*!
\ingroup a3d_pdf_javascript_module
\brief Function to define a javaScript action to an event triggered on an annotation. The JavaScript is defined as a string.

The goal of this function is to define a JavaScript action to be launched on an event triggered on an annotation.
\param [in] pPage The Page object to work with.
\param [in] p3DAnnot The 3D annot object to work with.
\param [in] eEventActionType The action type.
\param [in] pcJavascriptString The string which contains the JavaScript.
\return \ref A3D_SUCCESS \n
\version 9.0
*/
A3D_API (A3DStatus, A3DPDF3DAnnotSetActionJavascript, (A3DPDFPage* pPage, A3DPDF3DAnnot* p3DAnnot, const A3DPDFEEventActionType eEventActionType, const A3DUTF8Char* pcJavascriptString));

/*!
\defgroup a3d_pdf_iconimage_module Icon Image Module
\ingroup a3d_pdf_interactivity_module
\brief <b>(HOOPS Publish Advanced) </b>Module to define icon image used for interactivity.

This module describes the functions that allows to define an icon image that can be used for interactivity.

Icon images are stored into the document on a way that is accessible for interactivity.
An icon image is referenced by its identifier during interactivity. Identifier must be unique in the document.
An Icon image can be set as button icon, which can be set by javascript.
Also, icons can be set interactively on widgets populated by data model (Button or Scrolltable).
*/

/*!
\ingroup a3d_pdf_iconimage_module
\brief Function to store an image in the document in a way it can be used as an icon for interactive widgets.
\deprecated This function is deprecated. Please use \ref A3DPDFDocumentAddImageAsIcon2 instead.

Stored in that way, the image can be dynamically retrieved by the Acrobat JavaScript document function 'getIcon'.
For example, if the function is called with A3DPDFDocumentAddImageAsIcon(pimage, "myicon"), then the following
JavaScript code might be used on the document to dynamically set the icon on a button : <br>
<code>
var but = this.getField("buttonfieldtopopulate"); <br>
var icon = this.getIcon("myicon"); <br>
but.buttonSetIcon(icon);
</code>

\param[in, out] pDoc The Document object to work with.
\param[in] pcIconName The icon name that will be used to identify the image.
\param[in] pImage The image.
\return \ref A3D_SUCCESS \n
\version 6.1
*/

A3D_API (A3DStatus, A3DPDFDocumentAddImageAsIcon, (A3DPDFDocument* pDoc, const A3DPDFImage* pImage, const A3DUTF8Char* pcIconName));

/*!
\ingroup a3d_pdf_iconimage_module
\brief Function to store an image in the document in a way it can be used as an icon for interactive widgets.

Stored in that way, the image can be dynamically retrieved by the Acrobat JavaScript document function 'getIcon'.
For example, if the function is called with A3DPDFDocumentAddImageAsIcon2(pimage, "myicon",NULL), then the following
JavaScript code might be used on the document to dynamically set the icon on a button : <br>
<code>
var but = this.getField("buttonfieldtopopulate"); <br>
var icon = this.getIcon("myicon"); <br>
but.buttonSetIcon(icon);
</code>

<p>Also, the icon can be used in widgets automatically handled with data model. For this, the icon must be stored in the document
with \ref A3DPDFDocumentAddImageAsIcon2, and the icon id must be used into data table.
</p>

\param[in, out] pDoc The Document object to work with.
\param[in] pImage The image to store as icon in the Document.
\param[in] pcInIconId The icon identifier. If provided, this is the internal id used into PDF document to name the icon.
	Warning, the name should be unique into the document!
	If NULL is specified, a unique id is automatically generated. The unique id is returned in \ref ppcOutIconId argument.
\param [out] ppcOutIconId The identifier as a string. NULL is accepted if this info is not useful.
\return \ref A3D_SUCCESS \n
\version 10.2
*/
A3D_API(A3DStatus, A3DPDFDocumentAddImageAsIcon2, (A3DPDFDocument* pDoc, const A3DPDFImage* pImage, const A3DUTF8Char* pcInIconId, A3DUTF8Char** ppcOutIconId));

/*!
\defgroup a3d_publish_bom_module BillOfMaterial Module
\ingroup a3d_publish_modelfile_module
\brief <b>(HOOPS Publish Advanced) </b>Module to calculate a BOM on a modelfile.

This module describes the functions and structures that allow you to calculate a Bill Of Material on a modelfile.
*/
/*!
\ingroup a3d_publish_bom_module
\brief Structure that defines a set of 3D nodes. Usually used to be referred in a slide table row.
Each element can contain several instances. We can get the uuids for each instance.
\version 9.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
  A3DUns16 m_usStructSize;    // Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
  A3DUns32 m_iNbNodes;      /*!< Size of the following array. */
  A3DUTF8Char** m_ppUUIDsForNodes;/*!< The unique IDs of instances for the nodes. */
} A3DPDF3DNodesReferencesData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_publish_bom_module
\brief Structure that defines an element in a BOM.

Each element can contain several instances. We can get the uuids for each instance.
\version 8.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcName;		/*!< Name of the current element. */
	A3DInt32 m_iLevel;			/*!< The level in the assembly. */
} A3DPDFBomElementInfoData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_publish_bom_module
\brief Function to automatically compute a bill of material (BOM) from a modelfile.

The function returns an array of elements in the BOM.
Each element is the set of components in the assembly associated with the part.

\param [in] pModelFile The model file to work with.
\param [in] pPrcWriteHelper Used to get PRC data such as unique identifiers for PRC nodes.
\param [in] bHierarchical True if the BOM is hierarchical, false if it is flattened.
\param [out] piNbElements The number of elements in the BOM. In a bom table, it would be the number of lines.
\param [out] ppBomElementInfo Array of details of the elements in the BOM.
\param [out] pp3DNodesReferences Array of details of the node references for elements in the BOM.
\return \ref A3D_SUCCESS \n
\version 8.1
*/
A3D_API (A3DStatus, A3DPDFGetBOMInfo, (A3DAsmModelFile* pModelFile,
	A3DRWParamsPrcWriteHelper* pPrcWriteHelper,
	A3DBool bHierarchical,
	A3DInt32 *piNbElements,
	A3DPDFBomElementInfoData **ppBomElementInfo,
	A3DPDF3DNodesReferencesData **pp3DNodesReferences));

/*!
\ingroup a3d_publish_bom_module
\brief Function to free the memory allocated for the information of the BOM.

\param [in] iNbElements The number of elements to free.
\param [in,out] ppBomElementInfo Array of details of the elements in the BOM.
\param [in,out] pp3DNodesReferences Array of details of the node references for elements in the BOM.
\return \ref A3D_SUCCESS \n
\version 8.1
*/
A3D_API (A3DStatus, A3DPDFFreeBOMInfo, (A3DInt32 iNbElements,
	A3DPDFBomElementInfoData **ppBomElementInfo,
	A3DPDF3DNodesReferencesData **pp3DNodesReferences));

/*!
\defgroup a3d_publish_modelfilenodes_module ModelFile Nodes Module
\ingroup a3d_publish_modelfile_module
\brief <b>(HOOPS Publish Advanced) </b>Module to get list of nodes from a modelfile.

This module describes the functions and structures that allow you to calculate a list of nodes from a modelfile.
*/
/*!
\ingroup a3d_publish_modelfilenodes_module
\brief Structure that defines an attribute of a PDF node in a modelfile. \sa A3DPDFNodeData.
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcType;		/*!< Type of the attribute. */
	A3DUTF8Char* m_pcValue;		/*!< Value of the attribute. */
	A3DBool m_bIsCADAttribute;	/*!< if false, it's a computed attribute (like #, QTY and NAME) */
} A3DPDFNodeAttributeData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_publish_modelfilenodes_module
\brief Structure that defines information for a PDF node in a modelfile.
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DEntity* m_pEntity;						/*!< The pointer on PRC entity. */
	A3DInt32 m_iPocFatherIndex;					/*!< The index of the product occurence father in the list of nodes into modelfile (\ref A3DPDFModelFileNodesData.m_ppNodes). */
	A3DPDFEModelFileNodeType m_eNodeType;		/*!< Type of the entity (product occurence, geometry node, or 3D PMI node). */
	A3DUTF8Char* m_pcNodeUid;					/*!< The PDF unique id of the node. This can be used in javascript code to interact witrh 3D nodes. */
	A3DUTF8Char* m_pcName;						/*!< Name of the PRC entity associated with the node. */
	A3DUTF8Char* m_pcOriginalFilePath;			/*!< If the node is for a product occurence, this is the original file name stored in the CAD file. */
	A3DUTF8Char* m_pcFilePath;					/*!< If the node is for a product occurence, this is the full path name of the file associated with the product occurrence. */
	A3DInt32 m_iNbAttributes;					/*!< Size of the following array. */
	A3DPDFNodeAttributeData** m_ppAttributes;	/*!< Array of attributes. */
} A3DPDFNodeData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_publish_modelfilenodes_module
\brief Structure that defines information on PDF nodes computed from a modelfile.

\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DInt32 m_iNbNodes;		/*!< Size of the following array. */
	A3DPDFNodeData** m_ppNodes;	/*!< Array of PDF nodes in the modelfile. */
} A3DPDFModelFileNodesData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_publish_modelfilenodes_module
\brief Function to get information on PDF nodes computed from a modelfile.

\param [in] pModelFile The model file to work with.
\param [in] pPrcWriteHelper Used to get PRC data such as unique identifiers for PRC nodes.
\param [out] ppModelFileNodesInfo a struct that contains all infos about model file nodes.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDFGetModelFileNodes, (A3DAsmModelFile* pModelFile,
	A3DRWParamsPrcWriteHelper* pPrcWriteHelper,
	A3DPDFModelFileNodesData** ppModelFileNodesInfo));



/*!
\defgroup a3d_pdffield_checkbox Check Box Module
\ingroup a3d_pdf_widget_module
\brief <b>(HOOPS Publish Advanced) </b>Module to access and define Check Box field

This module describes the functions and structures that allow you to define a Check Box.
A Check Box is an Acrobat Field.
*/
/*!
\ingroup a3d_pdffield_checkbox
\brief Structure to define a check box field
\version 8.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;							// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcName;								/*!< Name of the field (it serves as the field identifier, it should be unique in the document). */
	A3DUTF8Char* m_pcTooltip;							/*!<
															Displayed text that the hesitant user may find helpful in filling in the form field.
															Tooltips appear when the pointer hovers briefly over the form field. */
	A3DPDFEFormField m_eFormField;						/*!< Specifies whether the form field can be seen, either on screen or in print. */
	A3DPDFETextOrientation m_eTextOrientation;			/*!< Orientation of the text inside the field. */
	A3DBool m_bIsReadOnly;								/*!< Defines if the field is editable. */
	A3DBool m_bIsRequired;								/*!< Defines if the field is required. */
	A3DBool m_bIsLocked;								/*!< If locked, prevents any further changes to any form field properties (in Acrobat 'Properties' dialog box). */
	A3DBool m_bHasBorder;								/*!< If true, there is a border around the field. */
	A3DPDFRgbColorData m_sBorderColor;					/*!< Color of the border of the field (if any). */
	A3DPDFEThicknessBorder m_eThicknessBorder;			/*!< Thickness of the border. */
	A3DPDFELineStyleBorder m_eLineStyleBorder;			/*!< Line style of the border. */
	A3DBool m_bHasFillColor;							/*!< If true, the following field (m_sFillColor) is used. */
	A3DPDFRgbColorData m_sFillColor;					/*!< Color which the inside of the field will be filled with (if m_bHasFillColor is true). */
	A3DPDFRgbColorData m_sTextColor;					/*!< Color of the text. */
	A3DUTF8Char* m_pcFontName;							/*!<
															Font name of the text.
		It should be the name of the font as appearing in Acrobat font list, with style separated with a comma.
		For example, 'Calibri Bold Italic' should be specified as "Calibri,BoldItalic".
		Also, specific values exist for Adobe default fonts:
		eTimesRoman is "TiRo" ; eTimesItalic is  "TiIt" ; eTimesBold is "TiBo" ; eTimesBoldItalic is "TiBI" ;
		eHelvetica is "Helv" ; eHelveticaOblique is "HeOb" ; eHelveticaBold is "HeBo" ; eHelveticaBoldOblique is "HeBO" ;
		eCourier is "Cour" ; eCourierOblique is "CoOb" ; eCourierBold is "CoBo" ; eCourierBoldOblique is "CoBO". */
	A3DInt32 m_iFontSize; 								/*!< Size of the font. */
	A3DUTF8Char* m_pcExportValue;						/*!< Value used when field data is exported by Acrobat functions. */
	A3DBool m_bIsCheckedByDefault;						/*!< If true, this check box is selected when the PDF document is opened. */
} A3DPDFCheckBoxData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdffield_checkbox
\brief Function to create a CheckBox

The CheckBox is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsertCheckBox.

\param [in,out] pDoc The Document object to work with.
\param [in] pCheckBoxData The CheckBox parameters. The name is mandatory.
\param [out] ppCheckBox The CheckBox created.
\return \ref A3D_SUCCESS \n
\version 8.0
*/
A3D_API (A3DStatus, A3DPDFCheckBoxCreate, (A3DPDFDocument* pDoc, const A3DPDFCheckBoxData* pCheckBoxData, A3DPDFCheckBox** ppCheckBox));


/*!
\ingroup a3d_pdffield_checkbox
\brief Function to insert a CheckBox in a page

\param [in,out] pPage The Page object to work with.
\param [in] pCheckBox The CheckBox object to insert on the page.
\param [in] pRectData The rectangle to specify the position on the page.
\return \ref A3D_SUCCESS \n
\version 8.0
*/
A3D_API (A3DStatus, A3DPDFPageInsertCheckBox, (A3DPDFPage* pPage, A3DPDFCheckBox* pCheckBox, const A3DPDFRectData* pRectData));


/*!
\defgroup a3d_pdffield_radiobutton Radio Button Module
\ingroup a3d_pdf_widget_module
\brief <b>(HOOPS Publish Advanced) </b>Module to access and define Radio Button field

This module describes the functions and structures that allow you to define a Radio Button.
A Radio Button is an Acrobat Field.
*/
/*!
\ingroup a3d_pdffield_radiobutton
\brief Structure to define a radio button field
\version 8.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;							// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcName;								/*!< Name of the field (it serves as the field identifier, it should be unique in the document). */
	A3DUTF8Char* m_pcTooltip;							/*!<
															Displayed text that the hesitant user may find helpful in filling in the form field.
															Tooltips appear when the pointer hovers briefly over the form field. */
	A3DPDFEFormField m_eFormField;						/*!< Specifies whether the form field can be seen, either on screen or in print. */
	A3DPDFETextOrientation m_eTextOrientation;			/*!< Orientation of the text inside the field. */
	A3DBool m_bIsReadOnly;								/*!< Defines if the field is editable. */
	A3DBool m_bIsRequired;								/*!< Defines if the field is required. */
	A3DBool m_bIsLocked;								/*!< If locked, prevents any further changes to any form field properties (in Acrobat 'Properties' dialog box). */
	A3DBool m_bHasBorder;								/*!< If true, there is a border around the field. */
	A3DPDFRgbColorData m_sBorderColor;					/*!< Color of the border of the field (if any). */
	A3DPDFEThicknessBorder m_eThicknessBorder;			/*!< Thickness of the border. */
	A3DPDFELineStyleBorder m_eLineStyleBorder;			/*!< Line style of the border. */
	A3DBool m_bHasFillColor;							/*!< If true, the following field (m_sFillColor) is used. */
	A3DPDFRgbColorData m_sFillColor;					/*!< Color which the inside of the field will be filled with (if m_bHasFillColor is true). */
	A3DPDFRgbColorData m_sTextColor;					/*!< Color of the text. */
	A3DUTF8Char* m_pcFontName;							/*!<
															Font name of the text.
		It should be the name of the font as appearing in Acrobat font list, with style separated with a comma.
		For example, 'Calibri Bold Italic' should be specified as "Calibri,BoldItalic".
		Also, specific values exist for Adobe default fonts:
		eTimesRoman is "TiRo" ; eTimesItalic is  "TiIt" ; eTimesBold is "TiBo" ; eTimesBoldItalic is "TiBI" ;
		eHelvetica is "Helv" ; eHelveticaOblique is "HeOb" ; eHelveticaBold is "HeBo" ; eHelveticaBoldOblique is "HeBO" ;
		eCourier is "Cour" ; eCourierOblique is "CoOb" ; eCourierBold is "CoBo" ; eCourierBoldOblique is "CoBO". */
	A3DInt32 m_iFontSize; 								/*!< Size of the font. */
	A3DUTF8Char* m_pcExportValue;						/*!< Value used when field data is exported by Acrobat functions. */
	A3DBool m_bIsCheckedByDefault;						/*!< If true, this radio button is selected when the PDF document is opened. */
	A3DBool m_bRadiosInUnison;							/*!< If true, radio buttons with the same name and choice are selected in unison. */
} A3DPDFRadioButtonData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdffield_radiobutton
\brief Function to create a RadioButton

The RadioButton is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsertRadioButton.

\param [in,out] pDoc The Document object to work with.
\param [in] pRadioButtonData The RadioButton parameters. The name is mandatory.
\param [out] ppRadioButton The RadioButton created.
\return \ref A3D_SUCCESS \n
\version 8.0
*/
A3D_API (A3DStatus, A3DPDFRadioButtonCreate, (A3DPDFDocument* pDoc, const A3DPDFRadioButtonData* pRadioButtonData, A3DPDFRadioButton** ppRadioButton));


/*!
\ingroup a3d_pdffield_radiobutton
\brief Function to insert a RadioButton in a page

\param [in,out] pPage The Page object to work with.
\param [in] pRadioButton The RadioButton object to insert on the page.
\param [in] pRectData The rectangle to specify the position on the page.
\return \ref A3D_SUCCESS \n
\version 8.0
*/
A3D_API (A3DStatus, A3DPDFPageInsertRadioButton, (A3DPDFPage* pPage, A3DPDFRadioButton* pRadioButton, const A3DPDFRectData* pRectData));

/*!
\defgroup a3d_pdffield_listbox ListBox Module
\ingroup a3d_pdf_widget_module
\brief <b>(HOOPS Publish Advanced) </b>Module to access and define ListBox field

This module describes the functions and structures that allow you to define a ListBox.
A ListBox is an Acrobat Field.
*/

/*!
\ingroup a3d_pdffield_listbox
\brief Structure to define a list box field
\version 8.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;							// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcName;								/*!< Name of the field (it serves as the field identifier, it should be unique in the document). */
	A3DUTF8Char* m_pcTooltip;							/*!<
															Displayed text that the hesitant user may find helpful in filling in the form field.
															Tooltips appear when the pointer hovers briefly over the form field. */
	A3DPDFEFormField m_eFormField;						/*!< Specifies whether the form field can be seen, either on screen or in print. */
	A3DPDFETextOrientation m_eTextOrientation;			/*!< Orientation of the text inside the field. */
	A3DBool m_bIsReadOnly;								/*!< Defines if the field is editable. */
	A3DBool m_bIsRequired;								/*!< Defines if the field is required. */
	A3DBool m_bIsLocked;								/*!< If locked, prevents any further changes to any form field properties (in Acrobat 'Properties' dialog box). */
	A3DBool m_bHasBorder;								/*!< If true, there is a border around the field. */
	A3DPDFRgbColorData m_sBorderColor;					/*!< Color of the border of the field (if any). */
	A3DPDFEThicknessBorder m_eThicknessBorder;			/*!< Thickness of the border. */
	A3DPDFELineStyleBorder m_eLineStyleBorder;			/*!< Line style of the border. */
	A3DBool m_bHasFillColor;							/*!< If true, the following field (m_sFillColor) is used. */
	A3DPDFRgbColorData m_sFillColor;					/*!< Color which the inside of the field will be filled with (if m_bHasFillColor is true). */
	A3DPDFRgbColorData m_sTextColor;					/*!< Color of the text. */
	A3DUTF8Char* m_pcFontName;							/*!<
															Font name of the text.
		It should be the name of the font as appearing in Acrobat font list, with style separated with a comma.
		For example, 'Calibri Bold Italic' should be specified as "Calibri,BoldItalic".
		Also, specific values exist for Adobe default fonts:
		eTimesRoman is "TiRo" ; eTimesItalic is  "TiIt" ; eTimesBold is "TiBo" ; eTimesBoldItalic is "TiBI" ;
		eHelvetica is "Helv" ; eHelveticaOblique is "HeOb" ; eHelveticaBold is "HeBo" ; eHelveticaBoldOblique is "HeBO" ;
		eCourier is "Cour" ; eCourierOblique is "CoOb" ; eCourierBold is "CoBo" ; eCourierBoldOblique is "CoBO". */
	A3DInt32 m_iFontSize; 								/*!< Size of the font. */
	A3DBool m_bMultipleSelection;						/*!< If true, several items can be selected. */
} A3DPDFListBoxData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdffield_listbox
\brief Function to create a ListBox

The ListBox is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsertListBox.
List items can be added using the function \ref A3DPDFPageFieldListAddItem, after the call to \ref A3DPDFPageInsertListBox.

\param [in,out] pDoc The Document object to work with.
\param [in] pListBoxData The ListBox parameters. The name is mandatory.
\param [out] ppListBox The ListBox created.
\return \ref A3D_SUCCESS \n
\version 8.0
*/
A3D_API (A3DStatus, A3DPDFListBoxCreate, (A3DPDFDocument* pDoc, const A3DPDFListBoxData* pListBoxData, A3DPDFListBox** ppListBox));


/*!
\ingroup a3d_pdffield_listbox
\brief Function to insert a ListBox in a page

\param [in,out] pPage The Page object to work with.
\param [in] pListBox The ListBox object to insert on the page.
\param [in] pRectData The rectangle to specify the position on the page.
\return \ref A3D_SUCCESS \n
\version 8.0
*/
A3D_API (A3DStatus, A3DPDFPageInsertListBox, (A3DPDFPage* pPage, A3DPDFListBox* pListBox, const A3DPDFRectData* pRectData));


/*!
\defgroup a3d_pdffield_dropdownlist Drop down List Module
\ingroup a3d_pdf_widget_module
\brief <b>(HOOPS Publish Advanced) </b>Module to access and define Drop down List field

This module describes the functions and structures that allow you to define a Drop down List.
A Drop down List is an Acrobat Field.
*/
/*!
\ingroup a3d_pdffield_dropdownlist
\brief Structure to define a drop down list field (combo box)
\version 8.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;							// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcName;								/*!< Name of the field (it serves as the field identifier, it should be unique in the document). */
	A3DUTF8Char* m_pcTooltip;							/*!<
															Displayed text that the hesitant user may find helpful in filling in the form field.
															Tooltips appear when the pointer hovers briefly over the form field. */
	A3DPDFEFormField m_eFormField;						/*!< Specifies whether the form field can be seen, either on screen or in print. */
	A3DPDFETextOrientation m_eTextOrientation;			/*!< Orientation of the text inside the field. */
	A3DBool m_bIsReadOnly;								/*!< Defines if the field is editable. */
	A3DBool m_bIsRequired;								/*!< Defines if the field is required. */
	A3DBool m_bIsLocked;								/*!< If locked, prevents any further changes to any form field properties (in Acrobat 'Properties' dialog box). */
	A3DBool m_bHasBorder;								/*!< If true, there is a border around the field. */
	A3DPDFRgbColorData m_sBorderColor;					/*!< Color of the border of the field (if any). */
	A3DPDFEThicknessBorder m_eThicknessBorder;			/*!< Thickness of the border. */
	A3DPDFELineStyleBorder m_eLineStyleBorder;			/*!< Line style of the border. */
	A3DBool m_bHasFillColor;							/*!< If true, the following field (m_sFillColor) is used. */
	A3DPDFRgbColorData m_sFillColor;					/*!< Color which the inside of the field will be filled with (if m_bHasFillColor is true). */
	A3DPDFRgbColorData m_sTextColor;					/*!< Color of the text. */
	A3DUTF8Char* m_pcFontName;							/*!< Font name of the text.
		It should be the name of the font as appearing in Acrobat font list, with style separated with a comma.
		For example, 'Calibri Bold Italic' should be specified as "Calibri,BoldItalic".
		Also, specific values exist for Adobe default fonts:
		eTimesRoman is "TiRo" ; eTimesItalic is  "TiIt" ; eTimesBold is "TiBo" ; eTimesBoldItalic is "TiBI" ;
		eHelvetica is "Helv" ; eHelveticaOblique is "HeOb" ; eHelveticaBold is "HeBo" ; eHelveticaBoldOblique is "HeBO" ;
		eCourier is "Cour" ; eCourierOblique is "CoOb" ; eCourierBold is "CoBo" ; eCourierBoldOblique is "CoBO". */
	A3DInt32 m_iFontSize; 								/*!< Size of the font. */
	A3DBool m_bAllowUserToEnterCustomText;				/*!< If true, the user can enter custom text. */
	A3DBool m_bCheckSpelling;							/*!< If true, checks the spelling. */
	A3DBool m_bCommitImmediate;							/*!< If true, commits the selected value immediately. */
} A3DPDFDropDownListData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdffield_dropdownlist
\brief Function to create a DropDownList (combo box)

The DropDownList is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsertDropDownList.
List items can be added using the function \ref A3DPDFPageFieldListAddItem, after the call to \ref A3DPDFPageInsertDropDownList.

\param [in,out] pDoc The Document object to work with.
\param [in] pDropDownListData The DropDownList parameters. The name is mandatory.
\param [out] ppDropDownList The DropDownList created.
\return \ref A3D_SUCCESS \n
\version 8.0
*/
A3D_API (A3DStatus, A3DPDFDropDownListCreate, (A3DPDFDocument* pDoc, const A3DPDFDropDownListData* pDropDownListData, A3DPDFDropDownList** ppDropDownList));


/*!
\ingroup a3d_pdffield_dropdownlist
\brief Function to insert a DropDownList (combo box) in a page

\param [in,out] pPage The Page object to work with.
\param [in] pDropDownList The DropDownList object to insert on the page.
\param [in] pRectData The rectangle to specify the position on the page.
\return \ref A3D_SUCCESS \n
\version 8.0
*/
A3D_API (A3DStatus, A3DPDFPageInsertDropDownList, (A3DPDFPage* pPage, A3DPDFDropDownList* pDropDownList, const A3DPDFRectData* pRectData));



/*!
\defgroup a3d_pdfwidget_carousel View Carousel Module
\ingroup a3d_pdf_widget_module
\brief <b>(HOOPS Publish Advanced) </b>Module to access and define View Carousel widget

This module describes the functions and structures that allow you to define a View Carousel.
A View Carousel is a high level widget to display a set of 3D views.
The view list is displayed on a set of buttons, each button icon shows the poster related to the view,
and the icon has the view name as label.
The user can scroll the buttons set with previous and next buttons.
*/

/*!
\ingroup a3d_pdfwidget_carousel
\brief Function to create a 'standard' view carousel and position it in the PDF document.
\deprecated This function is deprecated. Please use the \ref A3DPDF3DViewCarousel widget and \ref A3DPDFDataTable to implement the same behaviour
(see \ref a3d_pdf_datamodel_module module).

The PDF document must contain the buttons for the views and the buttons to go to the next and the previous view.
\param [in,out] pDoc The document which will contain the carousel.
\param [in] pPage The page where the carousel will be put.
\param [in] pAnnot The annotation which contains the views of the carousel.
\param [in] pModelFile The model file corresponding with the annotation.
\param [in] iNbButtons The number of buttons in the ppButtonsNames array.
\param [in] ppButtonsNames The names of the view buttons in the pdf file. The size of this array should be iNbButtonsRows*iNbButtonsCols.
\param [in] pPreviousButtonName The name of the button to press to scroll the carousel to previous views.
\param [in] pNextButtonName The name of the button to press to scroll the carousel to next views.
\param [in] iNbViews If equal to 0, all the views of the model file will be used, in the order they are found in the model file.
If superior than 0, the number of the views of the next parameter.
\param [in] ppViews If NULL, all the views of the model file will be used.
If not NULL, ppViews must contain the views that will be used for the carousel.
\param [in] ppImages If NULL, the images are automatically created from the views and the following applies: <p> \DRIVER_WARNING </p>
If not NULL, ppImages must contain the images that will be used for the views. In that case, the size of the array
should be the same as the size of ppViews parameter.
\param [in] iScrollStep This is the number of buttons icons scrolled.
If = iNbButtons, the carousel scrolls the icons by page (all the icons are replaced in the carousel).
If = the number of rows, the icons are scrolled by rows.
If = the number of columns, the icons are scrolled by columns.
If = 1, the carousel scrolls the icons one by one (all the icons are shifted of 1 increment in the carousel).
\return \ref A3D_SUCCESS \n
\version 8.0
*/
A3D_API (A3DStatus, A3DPDFDefineViewCarousel, (A3DPDFDocument* pDoc,
	A3DPDFPage* pPage,
	A3DPDF3DAnnot* pAnnot,
	A3DAsmModelFile* pModelFile,
	A3DInt32 iNbButtons,
	A3DUTF8Char** ppButtonsNames,
	A3DUTF8Char* pPreviousButtonName,
	A3DUTF8Char* pNextButtonName,
	A3DInt32 iNbViews,
	A3DPDFView** ppViews,
	A3DPDFImage** ppImages,
	A3DInt32 iScrollStep));

/*!
\ingroup a3d_pdfwidget_carousel
\brief Structure to define a view carousel widget.

The PDF document must contain the buttons for the views and the buttons to go to the next and the previous view.
All buttons should be on the same page.
The buttons should have been primarily created with \ref A3DPDFButtonCreate and positioned on the page with \ref A3DPDFPageInsertButton,
or they might already exist on the page.

\return \ref A3D_SUCCESS \n
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;							// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_iNbButtons;								/*!< Size of the following array. */
	A3DUTF8Char** m_ppcButtonsNames;					/*!< List of buttons names. */
	A3DUTF8Char* m_pcPreviousButtonName;				/*!< Button name for 'previous' button. */
	A3DUTF8Char* m_pcNextButtonName;					/*!< Button name for 'next' button. */
	A3DPDFEDirection m_eScrollDirection;				/*!< Reserved for future use. */
	A3DInt32 m_iScrollStep;								/*!< Scroll step (how much buttons icons are scrolled when pushing previous or next).
														If m_iScrollStep = m_iNbButtons, the carousel scrolls the icons by page (all the icons are replaced in the carousel).
														If m_iScrollStep = the number of rows, the icons are scrolled by rows.
														If m_iScrollStep = the number of columns, the icons are scrolled by columns.
														If m_iScrollStep = 1, the carousel scrolls the icons one by one (all the icons are shifted of 1 increment in the carousel).
														*/
	A3DBool m_bTransitionActive;						/*!< Unused. Reserved for future use. */
} A3DPDF3DViewCarouselData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdfwidget_carousel
\brief Function to create a view carousel widget.

The view carousel widget is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsert3DViewCarousel.
Ultimately, the widget should be linked to a data table with \ref A3DPDF3DViewCarouselBindToTable.

\param [in,out] pDoc The Document object to work with.
\param [in] p3DViewCarouselData The view carousel parameters. The name is mandatory.
\param [out] pp3DViewCarousel The view carousel created.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDF3DViewCarouselCreate, (A3DPDFDocument* pDoc, const A3DPDF3DViewCarouselData* p3DViewCarouselData, A3DPDF3DViewCarousel** pp3DViewCarousel));

/*!
\ingroup a3d_pdfwidget_carousel
\brief Function to insert a view carousel widget in a page.

The page must be the same as the page of the underlying buttons.

\param [in,out] pPage The Page object to work with.
\param [in] pCarousel The button object to insert on the page.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDFPageInsert3DViewCarousel, (A3DPDFPage* pPage, A3DPDF3DViewCarousel* pCarousel));



/*!
\defgroup a3d_pdfwidget_scrolltable Scroll Table Module
\ingroup a3d_pdf_widget_module
\brief <b>(HOOPS Publish Advanced) </b>Module to access and define Scroll Table widget

This module describes the functions and structures that allow you to define a Scroll Table.
A Scroll Table is a high level widget.
*/

/*!
\ingroup a3d_pdfwidget_scrolltable
\brief Function to create a slide table. This is a table with a fixed size, and two buttons to scroll the rows of the table.
\deprecated This function is deprecated. Please use the \ref A3DPDFScrollTable widget and \ref A3DPDFDataTable to implement the same behaviour
(see \ref a3d_pdf_datamodel_module module).

The frame of the table is specified through html definition and printed as-is on the page. Then the text data is populated
dynamically from an array of texts specified.
The table MUST be of simple shape, with the same number of rows for each columns. Optionally, a header can figure in the
table data at first row.

Please note that the table implementation uses an add-on to HOOPS Publish (TableToPDF) which is provided for free by Tech Soft 3D.
The add-on is using components that are licensed under GNU LGPL terms.
<b>Consequently, the usage of tables using HOOPS Publish add-on requires our customer's application to comply
with LGPL requirements.</b>
TableToPDF can be downloaded at http://developer.techsoft3d.com/add-ons/tabletopdf/
The deployment process is simple and just requires to copy the provided DLLs in HOOPS Publish binaries folder.

The text data defines the text content, as well as text format attributes (font, font size, and text color). All rows must
have the same text attributes, so that only the first row of text data is used internally to get the text format.
\param [in,out] pDoc The document which will contain the slide table.
\param [in,out] pPage The page where the slide table will be put.
\param [in] iPosLeft The x coordinate of the insertion point of the table. The insertion point is the top left corner of the table. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\param [in] iPosTop The y coordinate of the insertion point of the table. The insertion point is the top left corner of the table. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\param [in] pcHtmlFrameTable String for definition of the frame table in a HTML format. See example definition in \ref a3d_pdf_table_module.
\param [in] pcHtmlFrameStyle String for definition of the frame table style in a CSS format. See example definition in \ref a3d_pdf_table_module.
\param [in] pPreviousButtonName The name of the button to press to go to the previous page.
\param [in] pNextButtonName The name of the button to press to go to the next page.
\param [in] iNbTextRows the number of rows in the matrix of text data specified in ppTextData.
\param [in] iNbTextCols the number of columns in the matrix of text data specified in ppTextData. It should be the same number as of the number of columns specified in the table frame.
\param [in] ppTexts must contain the text data that will be used to dynamically populate the table.
It is a matrix of iNbTextRows*iNbTextCols elements of type (A3DPDFTextField*). These elements should be created with A3DPDFTextFieldCreate.
The first row might contain the header for the table. In that case, specify bHasHeader to TRUE, then the first row will
not be scrolled and the formatting specific to this row will be applied.
The following members of A3DPDFTextFieldData structure are used:
<ul>
<li>the text content is specified in member m_pcDefaultValue</li>
<li>the font name is specified in member m_pcFontName</li>
<li>the font size is specified in member m_iFontSize</li>
<li>the text color is specified in member m_sTextColor</li>
<li>the background color is specified in member m_sFillColor if member m_bHasFillColor is TRUE</li>
<li>the text alignment is specified in member m_eTextAlignment</li>
<li>the text orientation is specified in member m_eTextOrientation</li>
<li>the readonly attribute is specified in member m_bReadOnly</li>
<li>the multiline attribute is specified in member m_bMultiLine</li>
<li>the donotscroll attribute is specified in member m_bDoNotScroll</li>
<li>the formfield attribute is specified in member m_eFormField</li>
</ul>
Only first row is used to get attributes.
\param [in] bHasHeader If true, the table header is described at first row of ppTexts. This row will remain visible and won't
be scrolled when pushing the prev/next buttons.
If false, the table is considered without header, then the whole rows will be scrolled when pushing the prev/next buttons.
\return \ref A3D_SUCCESS \n
\return \ref A3DPDF_SLIDETABLE_NBCOLUMNS_ERROR if iNbTextCols differs from the number of columns in table frame.\n
\return \ref A3DPDF_SLIDETABLE_TEXTDATA_ERROR if ppTexts has wrong data (typically null pointer).\n
\version 7.2
*/
A3D_API (A3DStatus, A3DPDFDefineSlideTable, (A3DPDFDocument* pDoc, A3DPDFPage* pPage,
	const A3DInt32 iPosLeft, const A3DInt32 iPosTop,
	const A3DUTF8Char* pcHtmlFrameTable, const A3DUTF8Char* pcHtmlFrameStyle,
	const A3DUTF8Char* pPreviousButtonName,
	const A3DUTF8Char* pNextButtonName,
	const A3DInt32 iNbTextRows,
	const A3DInt32 iNbTextCols,
	const A3DPDFTextField* const * const* ppTexts,
	const A3DBool bHasHeader));


/*!
\ingroup a3d_pdfwidget_scrolltable
\brief Function to create a slide table. This is a table with a fixed size, and a scroll bar to scroll the rows of the table.
\deprecated This function is deprecated. Please use the \ref A3DPDFScrollTable widget and \ref A3DPDFDataTable to implement the same behaviour
(see \ref a3d_pdf_datamodel_module module).

This function differs from \ref A3DPDFDefineSlideTable in that it uses a scroll bar to scroll the rows instead of previous and next buttons.
The frame of the table is specified through html definition and printed as-is on the page. Then the text data is populated
dynamically from an array of texts specified.
The table MUST be of simple shape, with the same number of rows for each columns. Optionally, a header can figure in the
table data at first row.

Please note that the table implementation uses an add-on to HOOPS Publish (TableToPDF) which is provided for free by Tech Soft 3D.
The add-on is using components that are licensed under GNU LGPL terms.
<b>Consequently, the usage of tables using HOOPS Publish add-on requires our customer's application to comply
with LGPL requirements.</b>
TableToPDF can be downloaded at http://developer.techsoft3d.com/add-ons/tabletopdf/
The deployment process is simple and just requires to copy the provided DLLs in HOOPS Publish binaries folder.

The text data defines the text content, as well as text format attributes (font, font size, and text color). All rows must
have the same text attributes, so that only the first row of text data is used internally to get the text format.
\param [in,out] pDoc The document which will contain the slide table.
\param [in,out] pPage The page where the slide table will be put.
\param [in] iPosLeft The x coordinate of the insertion point of the table. The insertion point is the top left corner of the table. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\param [in] iPosTop The y coordinate of the insertion point of the table. The insertion point is the top left corner of the table. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\param [in] pcHtmlFrameTable String for definition of the frame table in a HTML format. See example definition in \ref a3d_pdf_table_module.
\param [in] pcHtmlFrameStyle String for definition of the frame table style in a CSS format. See example definition in \ref a3d_pdf_table_module.
\param [in] iSliderWidth The width of the scroll bar (in points). Important: The scrollbar is positioned on the right of the table, outside of the frame.
\param [in] iNbTextRows the number of rows in the matrix of text data specified in ppTextData.
\param [in] iNbTextCols the number of columns in the matrix of text data specified in ppTextData. It should be the same number as of the number of columns specified in the table frame.
\param [in] ppTexts must contain the text data that will be used to dynamically populate the table.
It is a matrix of iNbTextRows*iNbTextCols elements of type (A3DPDFTextField*). These elements should be created with A3DPDFTextFieldCreate.
The first row might contain the header for the table. In that case, specify bHasHeader to TRUE, then the first row will
not be scrolled and the formatting specific to this row will be applied.
The following members of A3DPDFTextFieldData structure are used:
<ul>
<li>the text content is specified in member m_pcDefaultValue</li>
<li>the font name is specified in member m_pcFontName</li>
<li>the font size is specified in member m_iFontSize</li>
<li>the text color is specified in member m_sTextColor</li>
<li>the background color is specified in member m_sFillColor if member m_bHasFillColor is TRUE</li>
<li>the text alignment is specified in member m_eTextAlignment</li>
<li>the text orientation is specified in member m_eTextOrientation</li>
<li>the readonly attribute is specified in member m_bReadOnly</li>
<li>the multiline attribute is specified in member m_bMultiLine</li>
<li>the donotscroll attribute is specified in member m_bDoNotScroll</li>
<li>the formfield attribute is specified in member m_eFormField</li>
</ul>
Only first row is used to get attributes.
\param [in] bHasHeader If true, the table header is described at first row of ppTexts. This row will remain visible and won't
be scrolled when using the scroll bar.
If false, the table is considered without header, then the whole rows will be scrolled when using the scroll bar.
\param [out] ppcSTName The identifier of the slide table as a string. It can be useful to add Javascript instructions to work on this specific slide table.
\return \ref A3D_SUCCESS \n
\return \ref A3DPDF_SLIDETABLE_NBCOLUMNS_ERROR if iNbTextCols differs from the number of columns in table frame.\n
\return \ref A3DPDF_SLIDETABLE_TEXTDATA_ERROR if ppTexts has wrong data (typically null pointer).\n
\version 8.1
*/
A3D_API (A3DStatus, A3DPDFDefineSlideTable2, (A3DPDFDocument* pDoc, A3DPDFPage* pPage,
	const A3DInt32 iPosLeft, const A3DInt32 iPosTop,
	const A3DUTF8Char* pcHtmlFrameTable,
	const A3DUTF8Char* pcHtmlFrameStyle,
	const A3DInt32 iSliderWidth,
	const A3DInt32 iNbTextRows,
	const A3DInt32 iNbTextCols,
	const A3DPDFTextField* const * const* ppTexts,
	const A3DBool bHasHeader,
	A3DUTF8Char** ppcSTName));

/*!
\ingroup a3d_pdfwidget_scrolltable
\brief Function to link 3D nodes to a slide table.
\deprecated This function is deprecated. Please use the \ref A3DPDFScrollTable and \ref A3DPDF3DNodeScene widgets and \ref A3DPDFDataTable
to implement the same behaviour (see \ref a3d_pdf_datamodel_module module).

Each row of the slide table are linked to a set of 3D nodes. Thus, when the user clicks on a row, the 3D nodes are
automatically highlighted; and vice versa: when a 3D node is clicked in the 3D, the row is automatically selected.
\param [in,out] pDoc The document which contains the slide table
\param [in,out] pPage The page where the slide table is
\param [in] pcSTName The identifier of the slide table as a string
\param [in] p3DAnnot The 3D annotation
\param [in] sHighlightRowColor The color for the highlighted row
\param [in] iNbRows The size of the following array
\param [in] p3DNodesReferences The set of nodes for each row
\return \ref A3D_SUCCESS \n
\version 9.1
*/
A3D_API (A3DStatus, A3DPDFSlideTableLinkTo3DNodes, (A3DPDFDocument* pDoc, A3DPDFPage* pPage,
	const A3DUTF8Char* pcSTName,
	const A3DPDF3DAnnot* p3DAnnot,
	const A3DPDFRgbColorData sHighlightRowColor,
	const A3DInt32 iNbRows,
	const A3DPDF3DNodesReferencesData * const p3DNodesReferences
	));


/*!
\ingroup a3d_pdfwidget_scrolltable
\brief Structure to define a ScrollTable widget.

The frame of the scroll table is defined with a grid using parameters \ref m_iGridNbRows, \ref m_iGridNbCols, \ref m_iGridHeightCells, \ref m_piGridWidthCells.

The table header must be defined outside of this function if the layout requires one.

Rich text can be specified in a scroll table. This allows you to apply styling information to portions of the text, such as bold or italic.
To do this, the boolean \ref A3DPDFTextFieldData.m_bAllowRichText must be set to TRUE in m_pCellFormat.
If m_pCellFormat specifies rich text formatting, then ALL the texts in the table bound to this scrollbar MUST BE defined as
rich texts, that is to say they need to be formatted as in the following example.
\include exrichtext.html
Text fields first take the format specified in \ref m_pCellFormat, then attributes might be overloaded with the rich text formatting.
For example, "text-align" specified in xml style attribute overrides the alignement specified in A3DPDFTextFieldData.m_eTextAlignment.

\return \ref A3D_SUCCESS \n
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;				// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DInt32 m_iGridNbRows;					/*!< The number of rows in the frame. */
	A3DInt32 m_iGridNbCols;					/*!< The number of columns in the frame. */
	A3DInt32 m_iGridHeightCells;			/*!< The height of cells in the frame. */
	A3DInt32* m_piGridWidthCells;			/*!< Array of the widths of the cells. The array must be of size m_iGridNbCols. */
	A3DInt32 m_iSliderWidth;				/*!< The width of the scroll bar (in points). Important: The scrollbar is positioned on the right of the table, outside of the frame. Specify 0 to not have a scroll bar. */
	A3DPDFTextFieldData* m_pCellFormat;		/*!< Optional. Defines the text field format for scroll table cells. If NULL, default format is used (black centered Arial text of font size 10, on white background). */
	A3DPDFRgbColorData* m_pHighlightColor;	/*!< Optional. Color to highlight a row. If NULL, default color is used (same color as Acrobat fields highlighting). */
	A3DPDFEColumnType* m_peColumnTypes;		/*!< Array of the types of the cells. The array must be of size m_iGridNbCols. If NULL, all cells are of \ref kA3DPDFTextContent type. */
} A3DPDFScrollTableData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdfwidget_scrolltable
\brief Function to create a ScrollTable widget.

The ScrollTable is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsertScrollTable.
Ultimately, the widget should be linked to a data table with \ref A3DPDFScrollTableBindToTable.

\param [in,out] pDoc The Document object to work with.
\param [in] pScrollTableData The ScrollTable parameters. The name is mandatory.
\param [out] ppScrollTable The ScrollTable created.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDFScrollTableCreate, (A3DPDFDocument* pDoc, const A3DPDFScrollTableData* pScrollTableData, A3DPDFScrollTable** ppScrollTable));

/*!
\ingroup a3d_pdfwidget_scrolltable
\brief Function to insert a ScrollTable widget in a page.

\param [in,out] pPage The Page object to work with.
\param [in] pScrollTable The ScrollTable object to insert on the page.
\param [in] iPosLeft The x coordinate of the insertion point of the text. The insertion point is the top left corner of the text. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\param [in] iPosTop The y coordinate of the insertion point of the text. The insertion point is the top left corner of the text. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\return \ref A3DPDF_CANNOT_INITIALIZE_RESOURCES if the resources directory is not properly defined.
\return \ref A3DPDF_CANNOT_LOAD_TABLETOPDF_DLL if the tabletopdf dll cannot be loaded.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDFPageInsertScrollTable, (A3DPDFPage* pPage, A3DPDFScrollTable* pScrollTable, const A3DInt32 iPosLeft, const A3DInt32 iPosTop));



/*!
\defgroup a3d_pdf_datamodel_module Data Model Module
\ingroup a3d_pdf_interactivity_module
\brief <b>(HOOPS Publish Advanced) </b>Module to modelize data tables and interactivity.

This module describes the functions and structures that allow you to define tables, relationships and widgets on a PDF page,
as well as describing interactivity between widgets.

The process to define a data model binding is as follows:
<ul>
<li> Tables and Relationships must be created with \ref A3DPDFDataTableCreate and \ref A3DPDFDataRelationshipCreate.
</li>
<li> The widgets are primarily created by other API functions, or with Acrobat Forms. Widgets can be Acrobat fields (text field, button, list),
or a high level widget (ScrollTable, ViewCarousel ...)
<li> Use A3DPDFXXXBindToTable functions to bind a widget to a data table.</li>
<li> Use \ref A3DPDFWidgetSetTargetBehaviour functions to define the interactivity between widgets. </li>
</ul>

Init states are defined as follows (also used when unselecting a widget):
 . Text fields are empty
 . List fields display the whole data table
 . 3D annot is defined in the PRC

*/



/*!
\defgroup a3d_pdf_datamodel_table_module DataTable Module
\ingroup a3d_pdf_datamodel_module
\brief Module to define a Data table.

\version 10.0
*/


/*!
\ingroup a3d_pdf_datamodel_table_module
\brief Structure to define a DataTable
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_iNbRows;					/*!< Number of rows in the table. */
	A3DUns32 m_iNbCols;					/*!< Number of columns in the table. */
	A3DUTF8Char** m_ppcTexts;			/*!< Array of texts, ordered first by row : [row0col0, row0col1, row0col2, row1col0, row1col1, row1col2, ...]
		Table cells types can be text or image. The widget bound to the table must support the good type.
		The cells of type image must contain the icon id of the image.
		Icon images must be previously created and stored in the document with \ref A3DPDFDocumentAddImageAsIcon2, which returns the icon id. */
} A3DPDFDataTableData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdf_datamodel_table_module
\brief Function to create a DataTable.

\param [in] pDoc The Document object to work with.
\param [in] pDataTableData The DataTable parameters.
\param [out] ppDataTable The DataTable object created.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDFDataTableCreate, (
	A3DPDFDocument* pDoc,
	const A3DPDFDataTableData* pDataTableData,
	A3DPDFDataTable** ppDataTable));

/*!
\ingroup a3d_pdf_datamodel_table_module
\brief Structure to define a DataTable for 3D Views
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_iNbViews;		/*!< size of 3 following arrays. */
	A3DInt32* m_piViewIndexes;	/*!< Array of indexes of views as ordered in 3d annot.
								List of views can be primarily obtained using A3DPDF3DArtworkGetViews.
								The array must be of size m_iNbViews.  */
	A3DUTF8Char** m_ppcViewLabels; /*!< Array of labels of views if they are provided
								List of views can be primarily obtained using A3DPDF3DArtworkGetViews.
								The array must be of size m_iNbViews.
								If m_ppcViewLabels is NULL or an item is NULL, the label is the internal name of the view. */
	A3DPDFImage** m_ppImages;	/*!< Array of images of views if they are provided.
								The array must be of size m_iNbViews.
								If ppImages is NULL and mode iModeFlags is kA3DPDFTableFor3DViewsCustom | kA3DPDFTableFor3DViewsComputePosters, all posters are automatically computed.
								If iModeFlags is kA3DPDFTableFor3DViewsCustom, no posters are computed. This is good for no carousel usage. */
	A3DPDFDataTableData* m_pDataTableData; /*!< Additional table view data. It should define a table of columns for each view.
								m_pDataTableData->m_iNbRows must be equal to m_iNbViews.
								m_pDataTableData->m_ppDataTableRows must be set to additional columns. */
} A3DPDFTable3DViewsData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdf_datamodel_table_module
\brief Function to create a table for 3D views.

The table generated is derived from the list obtained with A3DPDF3DArtworkGetViews: it has the same size, and each row has the following columns:
<ul>
<li>column 0 is for the index in 3D annot internal views (internally assigned).</li>
<li>column 1 is the internal name of the view (internally assigned).</li>
<li>column 2 is the label of the view to be displayed in carousel buttons (assigned using A3DPDFTable3DViewsData.m_ppcViewLabels parameter).</li>
<li>column 3 is the icon name for the view (internally assigned from icons generation for carousel and A3DPDFTable3DViewsData.m_ppImages parameter)</li>
<li>additional columns are defined in pTable3DViewsData in m_pDataTableData member.
</li>
</ul>

<p><strong>Important:</strong> If iModeFlags is set to \ref kA3DPDFTableFor3DViewsComputePosters, the following applies: \DRIVER_WARNING </p>

\param [in] pDoc The Document object to work with.
\param [in] p3DArtwork Artwork object, used to get view definitions to compute posters.
\param [in] pModelFile Used to compute posters - must be the modelfile as stored into p3DArtwork.
\param [in] iModeFlags indicate how views are specified. see \ref a3d_pdf_TableFor3DViewsBit. For information about graphics driver selection for poster generation, please the details section above.
\param [in] pTable3DViewsData This allows specification of custom data in addition to internal data.
			Only used if kA3DPDFTableFor3DViewsCustom is specified in iModeFlags.
\param [out] ppDataTable The DataTable3DViews object created.
\return \ref A3D_SUCCESS \n
\return \ref A3DPDF_INVALID_VIEW_INDEX if an index specified in pTable3DViewsData.m_piViewIndexes is out of view array defined in p3DArtwork\n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDFDataTable3DViewsCreate, (
	A3DPDFDocument* pDoc,
	A3DPDF3DArtwork* p3DArtwork,
	A3DAsmModelFile* pModelFile,
	const int iModeFlags,
	const A3DPDFTable3DViewsData* pTable3DViewsData,
	A3DPDFDataTable3DViews** ppDataTable));

/*!
\ingroup a3d_pdf_datamodel_table_module
\brief Function to create a Object DataTable.

An Ojbect DataTable is a specific data table that contain objects instead of just text.
An Object define a set of properties and related values ; it is formatted as follows : { property1: valueproperty1, property2: valueproperty2, ... }
<br>
Supported properties are:
<ul>
<li>caption : the text of the field</li>
<li>icon : the icon id to store on the field when the field is defined as image holder (button)</li>
<li>actionurl : an url to be launched when the user clicks on the cell</li>
<li>actionjs : a javascript code to be launched when the user clicks on the cell</li>
<li>actionjscallback : the name of a javascript function stored on the document. This callback function is called when the user clicks on the cell, with the arguments : tablerowindex, tablecolindex, userdata (see actionjscallbackdata)</li>
<li>actionjscallbackdata : the third argument provided to the callback function actionjscallback</li>
</ul>
Properties actionurl, actionjs, or actionjscallback can be used to define an action on a table cell.
If an action is defined on a cell: when the user clicks on the cell, the row selected is unhighlighted and the action is launched.
If no action is defined on the cell: when the user clicks on the cell, the row selected is highlighted and the datamodel action linked to the row is launched (typically invoking other widgets on the page bound on linked tables).

\param [in] pDoc The Document object to work with.
\param [in] pDataTableData The DataTable parameters.
\param [out] ppDataTable The DataTable object created.
\return \ref A3D_SUCCESS \n
\version 11.2
*/
A3D_API(A3DStatus, A3DPDFObjectDataTableCreate, (
	A3DPDFDocument* pDoc,
	const A3DPDFDataTableData* pDataTableData,
	A3DPDFObjectDataTable** ppDataTable));

/*!
\ingroup a3d_pdf_datamodel_table_module
\brief Function to set the property iInitIndex on DataTable object.

By default, a -1 value is defined.
If it is defined here, the table row at index iInitIndex is 'selected' at init time (when the document is opened),
or when the table is reset. A table is typically reset when a bound widget is unselected.
A widget bound to this table have to be defined with a NULL source and \ref kA3DPDFDataSelect behaviour
to be automatically selected at init time (see \ref A3DPDFWidgetSetTargetBehaviour).

\param [in] pDataTable The DataTable object.
\param [in] iInitIndex The index to set.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDFDataTableSetInitIndex, (
	A3DPDFDataTable* pDataTable,
	A3DInt32 iInitIndex));


/*!
\defgroup a3d_pdf_datamodel_table_rship DataRelationship Module
\ingroup a3d_pdf_datamodel_module
\brief Module to define a relationship between DataTables.

\version 10.0
*/
/*!
\ingroup a3d_pdf_datamodel_table_rship
\brief Structure to define a Relationship map
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DInt32 m_aiIndexes[2];	/*!< Array of indexes between pDataTableSource and pDataTableTarget.
								Each row of the array has 2 elements: the index in table source (starting from 0),
								and the index in table target (starting from 0). */
} A3DPDFMapIndexData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_datamodel_table_rship
\brief Structure to define a Data Relationship
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;				// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_iSizeMap;					/*!< Number of entries in the map. */
	A3DPDFMapIndexData *m_pMapIndexes;		/*!< Array of indexes between pDataTableSource and pDataTableTarget. */
	A3DPDFDataTable* m_pDataTableSource;	/*!< Data Table source of the relationship. */
	A3DPDFDataTable* m_pDataTableTarget;	/*!< Data Table target of the relationship. */
} A3DPDFDataRelationshipData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_datamodel_table_rship
\brief Function to create a Relationship object

\param [in] pDoc The Document object to work with.
\param [in] pRelationshipData The Relationship parameters.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDFDataRelationshipCreate, (
	A3DPDFDocument* pDoc,
	const A3DPDFDataRelationshipData* pRelationshipData));




/*!
\ingroup a3d_pdf_datamodel_module
\brief Function to get a widget filter object from an existing drop down field

Filter widgets are specific.
A filter widget enables the user to apply a filter to a table related to the table bound to this widget.
Hence, the table bound to the filter widget should have a relationship with another table on which apply the filter.
Each entry of the filter widget isolate a set fo data onto the related table.
There's a specific entry which might be defined as first row to disable all filtering (hence, displaying all data). \sa A3DPDFDataFilterWidgetBindToTable.

\param [in] pPage The Page object on which is the field.
\param [in] pcFieldId Unique name for the drop down field.
\param [out] ppDataFilter The A3DPDFDataFilter object created.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDFPageGetDataFilterFromDropDownListField, (
	A3DPDFPage* pPage,
	const A3DUTF8Char* pcFieldId,
	A3DPDFDataFilter** ppDataFilter));


/*!
\ingroup a3d_pdf_datamodel_module
\brief Function to get a widget object for the list of 3D nodes from an existing 3D annot.

\param [in] p3DAnnot The Annot 3D previously created with \ref A3DPDF3DAnnotCreate.
\param [out] pp3DAnnotUIListNodes The widget object created.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDF3DAnnotGet3DNodeScene, (
	A3DPDF3DAnnot* p3DAnnot,
	A3DPDF3DNodeScene** pp3DAnnotUIListNodes));

/*!
\ingroup a3d_pdf_datamodel_module
\brief Function to get a widget object for the list of 3D views from an existing 3D annot list views UI.

\param [in] p3DAnnot The Annot 3D previously created with \ref A3DPDF3DAnnotCreate.
\param [out] pp3DAnnotUIListViews The widget object created.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDF3DAnnotGet3DViewList, (
	A3DPDF3DAnnot* p3DAnnot,
	A3DPDF3DViewList** pp3DAnnotUIListViews));


/*!
\ingroup a3d_pdf_datamodel_module
\brief Function to bind a widget text field to a table.

\param [in] pTextField The Widget text field to bind to a table.
\param [in] pDataTable The Data Table object to bind the widget to.
\param [in] iMappedColumn index (starting from 0) of column mapped to this widget.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDFTextFieldBindToTable, (
	A3DPDFTextField* pTextField,
	A3DPDFDataTable* pDataTable,
	const A3DInt32 iMappedColumn
	));

/*!
\ingroup a3d_pdf_datamodel_module
\brief Function to bind a widget scroll table to a table.

\param [in] pScrollTable The scroll table object to bind to a table.
\param [in] pDataTable The Data Table object to bind the widget to.
\param [in] iSizeMap Size of the following array.
\param [in] piMapColumns Array of indexes (starting from 0) of column mapped to this widget.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDFScrollTableBindToTable, (
	A3DPDFScrollTable* pScrollTable,
	A3DPDFDataTable* pDataTable,
	const A3DInt32 iSizeMap,
	const A3DInt32* piMapColumns
	));

/*!
\ingroup a3d_pdf_datamodel_module
\brief Function to bind a widget list box to a table.

\param [in] pListBox The widget list box to bind to a table.
\param [in] pDataTable The Data Table object to bind the widget to.
\param [in] iMappedColumn index (starting from 0) of column mapped to this widget.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDFListBoxBindToTable, (
	A3DPDFListBox* pListBox,
	A3DPDFDataTable* pDataTable,
	const A3DInt32 iMappedColumn
	));

/*!
\ingroup a3d_pdf_datamodel_module
\brief Function to bind a widget dropdown list to a table.

\param [in] pDropDownList The widget dropdown list to bind to a table.
\param [in] pDataTable The Data Table object to bind the widget to.
\param [in] iMappedColumn index (starting from 0) of column mapped to this widget.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDFDropDownListBindToTable, (
	A3DPDFDropDownList* pDropDownList,
	A3DPDFDataTable* pDataTable,
	const A3DInt32 iMappedColumn
	));

/*!
\ingroup a3d_pdf_datamodel_module
\brief Function to bind a widget object for the list of 3D nodes, to a table.

\param [in] p3DAnnotUIListNodes The Widget object to bind to a table.
\param [in] pDataTable The Data Table object to bind the widget to.
\param [in] iMappedColumn index (starting from 0) of column mapped to this widget.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDF3DNodeSceneBindToTable, (
	A3DPDF3DNodeScene* p3DAnnotUIListNodes,
	A3DPDFDataTable* pDataTable,
	const A3DInt32 iMappedColumn
	));

/*!
\ingroup a3d_pdf_datamodel_module
\brief Function to bind a widget object for the list of 3D views, to a table.

\param [in] p3DAnnotUIListViews The Widget object to bind to a table.
\param [in] pDataTable The Data Table object to bind the widget to.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDF3DViewListBindToTable, (
	A3DPDF3DViewList* p3DAnnotUIListViews,
	A3DPDFDataTable3DViews* pDataTable
	));


/*!
\ingroup a3d_pdf_datamodel_module
\brief Function to bind a widget view carousel to a table.

\param [in] p3DViewCarousel The view carousel object to bind to a table.
\param [in] pDataTable The Data Table object to bind the widget to.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDF3DViewCarouselBindToTable, (
	A3DPDF3DViewCarousel* p3DViewCarousel,
	A3DPDFDataTable3DViews* pDataTable
	));

/*!
\ingroup a3d_pdf_datamodel_module
\brief Function to bind a filter widget to a table.

\param [in] pDataFilter The filter widget object to bind to a table.
\param [in] pDataTable The Data Table object to bind the widget to.
\param [in] iSizeMap Size of the following array
\param [in] piMapColumns Array of indexes (starting from 0) of column mapped to this widget.
\param [in] pcValueNoFilterApplied If the filter widget is a drop down list, pcNoFilterApplied is the value to add as first row of the widget.
	If pcValueNoFilterApplied is specified, selecting the first row of the widget disables the filters.
	If pcValueNoFilterApplied is not specified, a filter is always applied (there is no 'nofilter' state ; default is first row).
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API (A3DStatus, A3DPDFDataFilterWidgetBindToTable, (
	A3DPDFDataFilter* pDataFilter,
	A3DPDFDataTable* pDataTable,
	const A3DInt32 iSizeMap,
	const A3DInt32* piMapColumns,
	const A3DUTF8Char* pcValueNoFilterApplied
	));

/*!
\ingroup a3d_pdf_datamodel_module
\brief Function to bind a widget button to a table.

The button can be bound to two columns types: index 0 is for the icon id for button. index 1 is optional and is for the label of the button.

\param [in] pButton The Widget text field to bind to a table.
\param [in] pDataTable The Data Table object to bind the widget to.
\param [in] iSizeMap Size of the following array.
\param [in] piMapColumns Array of indexes (starting from 0) of column mapped to this widget.
\return \ref A3D_SUCCESS \n
\version 10.2
*/
A3D_API(A3DStatus, A3DPDFButtonBindToTable, (
	A3DPDFButton* pButton,
	A3DPDFDataTable* pDataTable,
	const A3DInt32 iSizeMap,
	const A3DInt32* piMapColumns
	));


/*!
\ingroup a3d_pdf_datamodel_module
\brief Function to define an interaction between two widgets.

An interaction acts as follows: if the user clicks on the widget source, then a table joint is calculated using the
relationship between the table associated from pWidgetSource to the table associated to pWidgetTarget.
The result of the joint is a set of indexes in the table related to pWidgetTarget.
The widget target is ultimately filled from this result set, depending on the eBehaviourOnTarget value:
<ul>
<li> kA3DPDFDataIsolate: only the rows in result set are displayed in the widget.</li>
<li> kA3DPDFDataHighlight: the widget displays all the rows from its binded table, but only the rows in result set are highlighted.</li>
<li> kA3DPDFDataSelect: for this, the result set must contain only one value. Then in this case, the target widget acts as if the user selected the row indexed by this value.</li>
<li> kA3DPDFDataIsolateAndSelect: the widget is populated as with \ref kA3DPDFDataIsolate. Then, it acts as if the user selected the first item. This behaviour is valid only for list widgets.</li>
</ul>

A widget can be specifed as target without a source, to be automatically populated whenever the table bound is selected, whatever source widget triggered this selection.
For this, use A3DPDFWidgetSetTargetBehaviour(NULL, WidgetTarget);<br>
A widget that has to be populated at init time (when the file is opened) should have a NULL source specified, and the table bound should be set for init time with \ref A3DPDFDataTableSetInitIndex.<br>
A widget not connected to any other widget have automatically the source specified to NULL, and then is automatically populated with all rows of the data table when the file is opened.<br>
A widget connected to a source widget with kA3DPDFDataIsolate behaviour always remains empty at init time.

\param [in] pWidgetSource Widget source of interaction. Can be NULL to automatically populate the target widget at init time
\param [in] pWidgetTarget Widget to be refreshed.
\param [in] eBehaviourOnTarget Values allowed for this kind of widget target: see \ref A3DPDFEWidgetTargetBehaviour
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API(A3DStatus, A3DPDFWidgetSetTargetBehaviour, (
	A3DPDFWidget* pWidgetSource,
	A3DPDFWidget* pWidgetTarget,
	A3DPDFEWidgetTargetBehaviour eBehaviourOnTarget
	));


/*!
\ingroup a3d_pdf_datamodel_module
\brief Function to activate the PMI cross highlight and select behaviour.
With this, some JavaScript code is generated so that when the user clicks on a markup,
the associated surface is highlighted, and then the solid node containing the surface is selected.
Only works with Acrobat 11 and later.
Warning: If this behaviour is activated, when the node is selected, the PMI originally selected if unselected.
If datamodel connection is linked to a PMI node, the connectino will be resetted as if the PMI node is unselected.
\param [in] p3DAnnot The Annot 3D to work on.
\param [in] bActivatePMICrossHighlightAndSelect If true, activate the PMI cross highlight and select behaviour.
\return \ref A3D_SUCCESS \n
\version 10.1
*/
A3D_API(A3DStatus, A3DPDF3DAnnotActivatePMICrossHighlightAndSelect, (
	A3DPDF3DAnnot* p3DAnnot,
	A3DBool bActivatePMICrossHighlightAndSelect));


/*!
\defgroup a3d_pdf_Tetra4D_module Tetra 4D Enrich Templates Module
\ingroup a3d_pdf_interactivity_module
\brief <b>(HOOPS Publish Advanced) </b>Functions to enrich Tetra 4D documents.

This module describes the functions and structures useful to enrich PDF documents generated by Tetra4D solutions.
*/

/*!
\ingroup a3d_pdf_Tetra4D_module
\brief Function to automatically update Publish data into a document generated by Tetra 4D Enrich.

Updated data are carousels, slide tables, attributes, and text fields.
During update, following operations are performed:
<ul>
<li>The 3D object from the specified PDF template is replaced by the CAD data stored in model file,</li>
<li>The external information defined by XML files if any (attributes, title block) are read</li>
<li>The Tetra4D Enrich widgets present in the template (Carousel of Views, Table) are updated based on the new 3D data and the read XML attributes file,</li>
<li>The generic actions (text output on part selection, change of rendering mode...), if any, are updated,</li>
<li>The text fields (title block) are updated based on the specified title block XML file if any, and on the new 3D data.</li>
</ul>

\param [in] pDoc The Document object to work with. The document should be a PDF file that has been generated by Tetra 4D Enrich.
It is also necessary to primarily use the function A3DPDFDocumentCreateFromPDFFile to create the document, so that all necessary information are kept (javascript and all internal data stored in the document).
\param [in]  pcAnnot3DUID Identifier of the 3D annot to update.
If not provided, use idxPage and idx3dAnnot to identify the annotation to update.
\param [in]  idxPage Identifier of the 3D annot to update: index of the page in the document (starting from 0).
If -1, looks in all the document for the first annot 3D found
\param [in]  idx3dAnnot Identifier of the 3D annot to update: index of the annotation in the page (starting from 0).
If -1, looks in all pages for the first annot 3D found.
\param [in] pModelFile The model file to work with.
\param [in] pParamsExportData The PRC export parameters that are used to store the model file into the PDF.
\param [in,out] ppPrcWriteHelper Used to store the model file into the PDF.
ppPrcWriteHelper is filled by this function and can be later used  to get PRC data such as unique identifiers of PRC nodes
\param [in] p3DArtworkData The 3D Artwork parameters.
\param [in] p3DAnnotData The 3D Annot parameters.
\param [in] bAddStandardViews Add six standard views: Left Top Front Right Bottom Back.
\param [in] pDefaultViewData If provided, the parameters of this view are used to create a new automatic view (the view associated to the 'home' icon in Adobe Reader).
If not provided, the automatic view is created with HOOPS Publish strategy: it is duplicated from the existing default view if there is one in the model file definition; if not, a new 'automatic' view is created with hard coded parameters.
\param [in] pcIn3dAttribsFile XML file containing part attributes. If some of the meta-data to be put in the PDF document are managed by another application (such as ERP, PLM...), so not present in the CAD files, this function enables to import them through an XML file and map them to the 3D.
Please refer to the section section <a href="publish_enrich.html#attribs3dxml">Parts attributes definition XML file</a> in our programming guide for more details.
\param [in] pcInTextFieldDataFile XML file defining text information to populate text fields (title-block) in the PDF document.
Please refer to the section section <a href="publish_enrich.html#textfieldsxml">Text fields values definition XML file</a> in our programming guide for more details.

\return \ref A3D_SUCCESS \n
\version 9.1
*/
A3D_API(A3DStatus, A3DPDFDocumentUpdateData, (
	A3DPDFDocument* pDoc,
	// 3Dannot identifier:
	const A3DUTF8Char* pcAnnot3DUID,
	const A3DInt32 idxPage,
	const A3DInt32 idx3dAnnot,
	// modelfile to use to update 3D data
	A3DAsmModelFile* pModelFile,
	const A3DRWParamsExportPrcData* pParamsExportData,
	A3DRWParamsPrcWriteHelper** ppPrcWriteHelper,
	// options to create the replaced 3D annot
	const A3DPDF3DArtworkData2* p3DArtworkData,
	const A3DPDF3DAnnotData* p3DAnnotData,

	const A3DBool bAddStandardViews,
	const A3DPDFViewData* pDefaultViewData,

	const A3DUTF8Char* pcIn3dAttribsFile,
	const A3DUTF8Char* pcInTextFieldDataFile
	));



/*!
\defgroup a3d_pdf_layer_module Layer Module
\ingroup a3d_pdf_layout_module
\brief <b>(HOOPS Publish Advanced) </b>Module to define PDF layers.
*/
/*!
\brief Structure to define a layer.
\ingroup a3d_pdf_layer_module
\version 10.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16		m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char*	m_pcName;			/*!< The name of the layer. */
	A3DBool			m_bIsVisible;		/*!< The initial state of the layer. */
} A3DPDFLayerData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdf_layer_module
\brief Creates a layer in a document

\param [in] pDoc The document to work with.
\param [in] pData The layer data parameters.
\param [out] ppLayer The newly-created layer.

\return \ref A3D_SUCCESS \n
\version 10.1
*/
A3D_API(A3DStatus, A3DPDFLayerCreate, (A3DPDFDocument* pDoc, const A3DPDFLayerData *pData, A3DPDFLayer** ppLayer));

/*!
\ingroup a3d_pdf_layer_module
\brief Populates the \ref A3DPDFLayerData structure with data from an \ref A3DPDFLayer entity

\param [in] pLayer The layer to determine data from.
\param [out] pData The layer data parameters.

\return \ref A3D_SUCCESS \n
\version 10.1
*/
A3D_API(A3DStatus, A3DPDFLayerGet, (const A3DPDFLayer* pLayer, A3DPDFLayerData* pData));

/*!
\ingroup a3d_pdf_layer_module
\brief Function to get the number of layers in the document

\param [in] pDoc The document to work with.
\param [out] piNbLayers The number of layers in the document.

\return \ref A3D_SUCCESS \n
\version 10.1
*/
A3D_API(A3DStatus, A3DPDFDocumentGetNumberLayers, (const A3DPDFDocument* pDoc, A3DInt32* piNbLayers));

/*!
\ingroup a3d_pdf_layer_module
\brief Get a layer object by its index in the document.

\param [in] pDoc The document to work with.
\param [in] iNumLayer The index of the layer. The first index is 0.
\param [out] ppLayer The Layer object.

\return \ref A3D_SUCCESS \n
\version 10.1
*/
A3D_API(A3DStatus, A3DPDFDocumentGetLayer, (const A3DPDFDocument* pDoc, const A3DInt32 iNumLayer, A3DPDFLayer** ppLayer));

/*!
\ingroup a3d_pdf_layer_module
\brief Get a layer object by its name in the document.
\param [in] pDoc The document to work with.
\param [in] pcName The name of the layer to look for.
\param [out] ppLayer The Layer object.

\return \ref A3D_SUCCESS \n
\version 10.1
*/
A3D_API(A3DStatus, A3DPDFDocumentGetLayerByName, (const A3DPDFDocument* pDoc, const A3DUTF8Char* pcName, A3DPDFLayer** ppLayer));

/*!
\ingroup a3d_pdf_layer_module
\brief Define a "radio button" behaviour for a group of layers.
That is, the state of at most one layer in the group can be activated at a time

\param [in] pDoc The document to work with.
\param [in] iNbLayers The size of the following array of layers.
\param [in] ppLayers The array of pointer on layers objects.

\return \ref A3D_SUCCESS \n
\version 10.1
*/
A3D_API(A3DStatus, A3DPDFDocumentSetLayersRBGroup, (const A3DPDFDocument* pDoc, A3DUns32 iNbLayers, A3DPDFLayer** ppLayers));

/*!
\ingroup a3d_pdf_layer_module
\brief Adds a field into a layer

\param [in] pField the field to be added
\param [in] pLayer The layer in which the field must be added.

\return \ref A3D_SUCCESS \n
\version 10.1
*/
A3D_API(A3DStatus, A3DPDFFieldSetLayer, (A3DPDFField* pField, A3DPDFLayer* pLayer));

/*!
\ingroup a3d_pdf_layer_module
\brief Adds an image into a layer

\param [in] pImage the image to be added
\param [in] pLayer The layer in which the image must be added.

\return \ref A3D_SUCCESS \n
\version 10.1
*/
A3D_API(A3DStatus, A3DPDFImageSetLayer, (A3DPDFImage* pImage, A3DPDFLayer* pLayer));


/*!
\ingroup a3d_pdf_layer_module
\brief Function to insert a table in a page within a layer

Warning1: The positioning is not defined as in the other insertion functions (for texts or images). Here, the position is specified from
the top of the page.
Warning2: This function must be used instead of \ref A3DPDFPageInsertTable if the table is to be placed in a layer.

\param [in,out] pPage The Page object to work with.
\param [in] pTable The Table object to insert on the page.
\param [in] iPosLeft The x coordinate of the insertion point of the table. The insertion point is the top left corner of the table. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\param [in] iPosTop The y coordinate of the insertion point of the table. The insertion point is the top left corner of the table. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\param [in] pLayer The layer in which the table must be placed.
\return \ref A3D_SUCCESS \n
\version 10.1
*/
A3D_API(A3DStatus, A3DPDFPageInsertTableAndSetLayer, (A3DPDFPage* pPage, A3DPDFTable* pTable, const A3DInt32 iPosLeft, const A3DInt32 iPosTop, A3DPDFLayer *pLayer));

/*!
\ingroup a3d_pdf_layer_module
\brief Function to insert a text line in a page within a layer.
Warning: This function must be used instead of \ref A3DPDFPageInsertText if the text is to be placed in a layer.

\param [in,out] pPage The Page object to work with.
\param [in] pText The Text object to insert on the page.
\param [in] iPosLeft The x coordinate of the insertion point of the text. The insertion point is the bottom left corner of the text. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\param [in] iPosBottom The y coordinate of the insertion point of the text. The insertion point is the bottom left corner of the text. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\param [in] pLayer The layer in which the text must be placed
\return \ref A3D_SUCCESS \n
\version 10.1
*/
A3D_API(A3DStatus, A3DPDFPageInsertTextAndSetLayer, (A3DPDFPage* pPage, A3DPDFText* pText, const A3DInt32 iPosLeft, const A3DInt32 iPosBottom, A3DPDFLayer *pLayer));

/*!
\ingroup a3d_pdf_layer_module
\brief Function to insert a ScrollTable widget in a page.

\param [in,out] pPage The Page object to work with.
\param [in] pScrollTable The ScrollTable object to insert on the page.
\param [in] iPosLeft The x coordinate of the insertion point of the text. The insertion point is the top left corner of the text. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\param [in] iPosTop The y coordinate of the insertion point of the text. The insertion point is the top left corner of the text. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\param [in] pLayer The layer in which the ScrollTable must be placed
\return \ref A3DPDF_CANNOT_INITIALIZE_RESOURCES if the resources directory is not properly defined.
\return \ref A3DPDF_CANNOT_LOAD_TABLETOPDF_DLL if the tabletopdf dll cannot be loaded.
\return \ref A3D_SUCCESS \n
\version 10.1
*/
A3D_API(A3DStatus, A3DPDFPageInsertScrollTableAndSetLayer, (A3DPDFPage* pPage, A3DPDFScrollTable* pScrollTable, const A3DInt32 iPosLeft, const A3DInt32 iPosTop, A3DPDFLayer* pLayer));

/*!
\ingroup a3d_read
\brief Function to retrieve all 3D streams embedded in a PDF document, this function export native and PDF views (defined using Acrobat and not in the origin native CAD file). It also manages secured PDF.

The stream is the raw binary data stored as a char* stream. A PRC stream can be interpreted with the function A3DAsmModelFileLoadFromPrcStream.
A U3D stream needs to be written as a physical file before being read with classical A3DAsmModelFileLoadFromFile function.
\param [in] pcFileName References the path to the PDF file
\param [in] view flags
\param [out] ppStreamData Array of stream data
\param [out] piNumStreams Number of streams

If pcFileName is NULL, *ppStreamData will be freed if *piNumStreams is non-null. A3DPDFGetStreams(NULL, ppStreamData, piNumStreams) to
release *ppStreamData.

\return \ref A3D_LOAD_CANNOT_ACCESS_CADFILE \n
\return \ref A3D_SUCCESS \n

\version 12.0
*/
A3D_API(A3DStatus, A3DPDFGetStreams, (const A3DUTF8Char* pcFileName, A3DUns32 iFlags, A3DStream3DPDFData** ppStreamData, A3DInt32* piNumStreams));

#endif
