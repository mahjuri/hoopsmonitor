/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for feature holes enum
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifndef __A3DPRCFEATUREHOLEENUMS_H__
#define __A3DPRCFEATUREHOLEENUMS_H__


/*!
\enum EA3DFRMEnumValue_Hole
\brief Enumerate the possible types of hole shape types <br/>
This allow to specify the type of hole, and so the Definition you should expect to have under the #kA3DFRMFeatureDefinitionType_Hole.

\image html frm_holeshape_types.jpg
\image html frm_holeshape_type_ByElement.jpg

\ingroup a3d_feature_hole_module
\version 10.2
*/
typedef enum
{
	kA3DFRMEnumValue_HoleShape_None = 0,				/*!< Invalid Hole Shape Type. */
	kA3DFRMEnumValue_HoleShape_Simple,					/*!< <b>Simple Hole</b>: regular hole. Expect to NOT have Bore, Sunk or Tapered Definition */
	kA3DFRMEnumValue_HoleShape_Bore,					/*!< <b>Counter Bore Hole</b>: Expect to have a BoreDefinition. */
	kA3DFRMEnumValue_HoleShape_Sunk,					/*!< <b>Counter Sunk Hole</b>: Expect to have a SunkDefinition. */
	kA3DFRMEnumValue_HoleShape_Tapered,					/*!< <b>Counter Tapered Hole</b>: Expect to have a TaperedDefinition. */
	kA3DFRMEnumValue_HoleShape_Sketch,					/*!< Hole shape define with a <b>Sketch</b>: Not Implemented Yet. */
	kA3DFRMEnumValue_HoleShape_General,					/*!< Complex Hole Shape, that combine other shapes: Expect to have a combination of Bore, Sunk and Tapered Definitions. */
	kA3DFRMEnumValue_HoleShape_Standard,					/*!< . */
	kA3DFRMEnumValue_HoleShape_StandardClearance,			/*!< . */
	kA3DFRMEnumValue_HoleShape_ByElement,				/*!< Hole that is a sequence of common elements, such as Counter Bore or Tapered. Each element has a shape, a depth, and might have a thread. */
}
EA3DFRMEnumValue_Hole;

/*!
\enum EA3DFRMEnumValue_DepthLevel
\brief Enumerate the possible types of depth level types <br/>
Enumerate the possible semantic depth position in a hole. This can be used to specify:
  - Hole Chamfer position: possible values = <b>Start / Neck / End </b>
  - For a Tapered, where tapered diameter is applied: possible values = <b>Start / VStart</b> (define rather the min or max tapered diameter)

\image html frm_depthlevel_types.jpg

\ingroup a3d_feature_hole_module
\version 10.2
*/
typedef enum
{
	kA3DFRMEnumValue_DepthLevel_None = 0,				/*!< Invalid Depth Level Type. */
	kA3DFRMEnumValue_DepthLevel_Start,					/*!< Start of the hole. */
	kA3DFRMEnumValue_DepthLevel_Neck,					/*!< In case of a Counter Bore, The neck correspond to the Bore part depth, just before the regular hole part. */
	kA3DFRMEnumValue_DepthLevel_VStart,					/*!< In case of tipped Hole, VStart correspond to the hole bottom without considering the tip part. */
	kA3DFRMEnumValue_DepthLevel_End,					/*!< End of the hole. */
	kA3DFRMEnumValue_DepthLevel_Profile,				/*!< Position of the profile. This could be different than hole Start for example in case of Hole with DepthFrom value. */
}
EA3DFRMEnumValue_DepthLevel;


/*!
\enum EA3DFRMDefinitionHoleType
\brief Enumerate the possible types of definition hole types <br/>
Enumerate the specific types of Definition Feature that can be found under a #kA3DFRMFeatureDefinitionType_Hole.
\ingroup a3d_feature_hole_module
\version 10.2
*/
typedef enum
{
	kA3DFRMDefinitionHoleType_None = 0,				   /*!< Invalid Hole Type. */
	kA3DFRMDefinitionHoleType_RectangularDefinition,   /*!< Type of Definition containing regular hole information common to all Hole Shape types <b>except HoleShape_Sketch</b>:
													   - Parameter Data:
														 - <i>DoubleData Diameter</i>: hole diameter
														 - <i>Value Angle</i>: tip angle <i>(optional)</i> */
	kA3DFRMDefinitionHoleType_ChamferDefinition,	   /*!< Type of Definition containing information specific to Hole Chamfer:
													   - Parameter Specification:
														 - <i>EnumData DepthLevel</i>: depth level where chamfer is applied. see #EA3DFRMEnumValue_DepthLevel
													   - Parameter Data:
													     - <i>Value Angle</i>: chamfer angle
													     - <i>Value Offset</i>: chamfer offset */
	kA3DFRMDefinitionHoleType_CboreDefinition,		   /*!< Type of Definition containing information specific to Counter Bore Hole:
													   - Parameter Data:
													     - <i>DoubleData Depth</i>: bore depth
													     - <i>DoubleData Diameter</i>: bore diameter
													     - <i>IntegerData Boolean</i>: isBoreOutside: true if the bore is outside the hole, on the other side of the profile <i>(specific to Catia)</i> */
	kA3DFRMDefinitionHoleType_SunkDefinition,		   /*!< Type of Definition containing information specific to Sunk Hole:
													   - Parameter Data:
													     - <i>Value Diameter</i>: sunk diameter
													     - <i>Value Angle</i>: sunk angle */
	kA3DFRMDefinitionHoleType_TaperedDefinition, 	   /*!< Type of Definition containing information specific to Tapered Hole:
													   - Parameter Specification:
														 - <i>EnumData DepthLevel</i>: depth level where tapered angle is applied. see #EA3DFRMEnumValue_DepthLevel
													   - Parameter Data:
													     - <i>Value Angle</i>: tapered angle */
	kA3DFRMDefinitionHoleType_StandardDefinition, 	   /*!< Type of Definition containing information specific to Hole Standard. */
	kA3DFRMDefinitionHoleType_ElementDefinition		/*!< Type of Definition containing information specific to Hole defined by elements:
														- Parameter Data:
														 - <i>Depth</i>: Depth of the element.
														 - <i>Hole Shape</i>: Shape of the element.
														 - <i>Thread</i>: Thread of the element  <i>(optional)</i>*/
}
EA3DFRMDefinitionHoleType;


#endif	/*	__A3DPRCFEATUREHOLEENUMS_H__ */
