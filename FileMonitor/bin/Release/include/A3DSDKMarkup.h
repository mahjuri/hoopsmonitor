/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for the markup module.
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifdef A3DAPI_LOAD
#  undef __A3DPRCMARKUP_H__
#endif
#ifndef __A3DPRCMARKUP_H__
#define __A3DPRCMARKUP_H__
#ifndef A3DAPI_LOAD
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKInitializeFunctions.h>
#  include <A3DSDKTessellation.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD

/*!
\defgroup a3d_markup_module Markup Module
\ingroup a3d_entitiesdata_module
\brief Creates and accesses markup entities.

Entity type is \ref kA3DTypeMkpMarkup.

There may be a tessellation in the markup and in each leader.
The following sample code shows how to retrieve the tessellation.
\par Sample code
\include Markup.cpp
\sa a3d_tessmarkup
*/

/*!
\defgroup a3d_markupdefinition_module Markup definition
\ingroup a3d_markup_module
\version 4.0
*/

/*!
\defgroup a3d_markupleader Markup Leader
\ingroup a3d_markup_module
Entity type is \ref kA3DTypeMkpLeader.

There may a tessellation in the leader.
Use the sample code in \ref a3d_markup_module to retrieve the tessellation.
\sa a3d_tessmarkup
*/

/*!
\brief Markup Leader structure
\ingroup a3d_markupleader
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DMiscMarkupLinkedItem* m_pLinkedItem;		/*!< Represents the link with an entity. It must be one of the markup array of leaders (see A3DMkpMarkupData). It is not mandatory. */
	A3DTessMarkup* m_pTessellation;				/*!< Tessellation of leader. May be NULL. */
} A3DMkpLeaderData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMkpLeaderData structure
\ingroup a3d_markupleader
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpLeaderGet,(	const A3DMkpLeader* pLeader,
													A3DMkpLeaderData* pData));

/*!
\brief Creates an \ref A3DMkpLeader from an \ref A3DMkpLeaderData structure
\ingroup a3d_markupleader
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpLeaderCreate,(const A3DMkpLeaderData* pData,
													A3DMkpLeader** ppLeader));



/*!
\defgroup a3d_annotationentity Annotations Entity
\ingroup a3d_markup_module
An annotation entity can be one of the following types:
\li \ref A3DMkpAnnotationItem
\li \ref A3DMkpAnnotationSet
\li \ref A3DMkpAnnotationReference
*/

/*!
\defgroup a3d_annotationitem Annotation Item
\brief Usage of a markup
\ingroup a3d_annotationentity

Entity type is \ref kA3DTypeMkpAnnotationItem.

An annotation item denotes the usage of a single markup.
It enables the sharing of markups. For instance, a markup of type "datum" can be re-used in several annotation sets
to denote different tolerances.
*/

/*!
\brief Annotation Item structure
\ingroup a3d_annotationitem
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DMkpMarkup* m_pMarkup;		/*!< The markup held by the annotation item. */
} A3DMkpAnnotationItemData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMkpAnnotationItemData structure
\ingroup a3d_annotationitem
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpAnnotationItemGet,(const A3DMkpAnnotationItem* pAnnotation,
															A3DMkpAnnotationItemData* pData));

/*!
\brief Creates an \ref A3DMkpAnnotationItem from an \ref A3DMkpAnnotationItemData structure
\ingroup a3d_annotationitem
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpAnnotationItemCreate,(const A3DMkpAnnotationItemData* pData,
																A3DMkpAnnotationItem** ppAnnotation));


/*!
\defgroup a3d_annotationset Annotation Set
\brief Group of annotation entities
\ingroup a3d_annotationentity
Entity type is \ref kA3DTypeMkpAnnotationSet.

An annotation set is a group of annotation items or subsets.
For example, a tolerance defined by a datum and a feature control frame are described by an annotation set
with two annotation items, where the items point respectively
to a markup of type "datum" and a markup of type "feature control frame".

*/

/*!
\brief Annotation Set structure
\ingroup a3d_annotationset
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_uiAnnotationsSize;				/*!< Size of m_ppAnnotations array. */
	A3DMkpAnnotationEntity** m_ppAnnotations;	/*!< Annotation entities stored under current annotation. */
} A3DMkpAnnotationSetData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMkpAnnotationSetData structure
\ingroup a3d_annotationset
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpAnnotationSetGet,(	const A3DMkpAnnotationSet* pAnnotation,
															A3DMkpAnnotationSetData* pData));

/*!
\brief Creates an \ref A3DMkpAnnotationSet from an \ref A3DMkpAnnotationSetData structure
\ingroup a3d_annotationset
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_ANNOTATIONSET_WITH_NULL_ELEMENT \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpAnnotationSetCreate,(	const A3DMkpAnnotationSetData* pData,
																A3DMkpAnnotationSet** ppAnnotation));


/*!
\defgroup a3d_annotationreference Annotation Reference
\brief Logical grouping of other annotations
\ingroup a3d_annotationentity
Entity type is \ref kA3DTypeMkpAnnotationReference.

An annotation reference stores explicit combinations of markup data with modifiers that can then be used
to define other annotations (feature control frame).

*/

/*!
\brief Annotation Reference structure
\ingroup a3d_annotationreference
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_uiLinkedItemsSize;				/*!< The size of \ref m_ppLinkedItems. */
	A3DMiscMarkupLinkedItem** m_ppLinkedItems;	/*!< Array of \ref A3DMiscMarkupLinkedItem. */
} A3DMkpAnnotationReferenceData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMkpAnnotationReferenceData structure
\ingroup a3d_annotationreference
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpAnnotationReferenceGet,(	const A3DMkpAnnotationReference* pAnnotation,
																	A3DMkpAnnotationReferenceData* pData));

/*!
\brief Creates an \ref A3DMkpAnnotationReference from an \ref A3DMkpAnnotationReferenceData structure
\ingroup a3d_annotationreference
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpAnnotationReferenceCreate,(	const A3DMkpAnnotationReferenceData* pData,
																		A3DMkpAnnotationReference** ppAnnotation));


/*!
\defgroup a3d_annots_view View
\ingroup a3d_markup_module
Entity type is \ref kA3DTypeMkpView.

A view is either a grouping of markups or a particular representation of the 3D scene, depending on the
value of the \ref A3DMkpViewData::m_bIsAnnotationView member.
If \ref A3DMkpViewData::m_bIsAnnotationView is \c TRUE,
the view contains only an array of annotation entities.
If it is \c FALSE, it can also redefine other view properties such scene display parameters,
entity visibilities, and positions on representation items and markups

*/

/*!
\brief View structure
\ingroup a3d_annots_view
\version 2.0

The type of children must be \ref kA3DTypeMkpAnnotationSet and must point to existing annotation entities.
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;									// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_uiAnnotationsSize;								/*!< Size of the m_ppAnnotations array. */
	A3DMkpAnnotationEntity** m_ppAnnotations;					/*!< Contains references to annotations. In an assembly context, if the annotation belongs to a child level of the assembly it is necessary to use linked items to redefine visibility (see \ref m_ppLinkedItems below). */
	A3DSurfPlane* m_pPlane;										/*!< View plane. */
	A3DGraphSceneDisplayParameters* m_pSceneDisplayParameters;	/*!< Scene display parameters. Reserved for future use. */
	A3DBool m_bIsAnnotationView;								/*!< This view is used only to group markups. */
	A3DBool m_bIsDefaultView;									/*!< If true, the parameters of this view are used when loading the 3D scene. */
	A3DUns32 m_uiLinkedItemsSize;								/*!< Size of m_ppLinkedItems array. */
	A3DMiscMarkupLinkedItem** m_ppLinkedItems;					/*!< Array of pointers to \ref A3DMiscMarkupLinkedItem entities, each of which redefines special display parameters for the View. Elements in this array are not owned by the View -- they are only referenced by the View, and the A3DMiscMarkupLinkedItemData member \ref A3DMiscMarkupLinkedItemData::m_pTargetProductOccurrence "m_pTargetProductOccurrence" indicates which Product Occurrence actually owns the linked entity. */
	A3DUns32 m_uiDisplayFilterSize;								/*!< Size of m_ppDisplayFilters array. */
	A3DAsmFilter** m_ppDisplayFilters;							/*!< Filters for display settings. Several filters are possible, but only one filter is active at a time. */
} A3DMkpViewData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMkpViewData structure
\ingroup a3d_annots_view
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpViewGet,(	const A3DMkpView* pView,
												A3DMkpViewData* pData));

/*!
\brief Creates an \ref A3DMkpView from an \ref A3DMkpViewData structure
\ingroup a3d_annots_view
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpViewCreate,(	const A3DMkpViewData* pData,
													A3DMkpView** ppView));

/*!
\brief The set of activated components in a View.

In a model, a view may consist in different components:

- Camera positioning
- PMI Filtering
- Geometry Filtering
- Cross-section
- View explosion
- Combined State

When traversing an \ref A3DMkpView instance, you can check whether theses
components are used or not by calling \ref A3DMkpViewGetFlags.
The returned structure is a set of boolean members which indicates the state of
each component.

## Application of the component

A view's component is applied by HOOPS Exchange if:
- It is present in the view's data,
- The corresponding field in \ref A3DMkpViewFlagsData is `A3DTrue`.

Otherwise, the component of the current state is used if it exists, whatever the
value of the field in \ref A3DMkpViewFlagsData is.

The following array summarizes when a specific component is applied and which
one:

| **Flag value** | **Defined in view** | **Defined in current state** | **Component**     |
|----------------|---------------------|------------------------------|-------------------|
| `A3DTrue`      | no                  | no                           | None              |
| `A3DTrue`      | no                  | *yes*                        | None              |
| `A3DTrue`      | *yes*               | no                           | View's component  |
| `A3DTrue`      | *yes*               | *yes*                        | View's component  |
| `A3DFalse`     | no                  | no                           | None              |
| `A3DFalse`     | no                  | *yes*                        | Current component |
| `A3DFalse`     | *yes*               | no                           | None              |
| `A3DFalse`     | *yes*               | *yes*                        | Current component |

For example, if within a given view, `m_bIsCrossSectionSet == A3DTrue` and
cross-section data are present in the view then this cross-section will be used
by HOOPS Exchange. This example corresponds to both third and fourth line of the
table.

On the other hand, if `m_bIsCrossSectionSet == A3DFalse` the component used will be
the one defined in the current state if any. This is the case shown by the two last
rows of this table.

\ingroup a3d_annots_view
\version 7.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;									// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool  m_bIsCameraSet;                  /*!< A Camera is flagged on.  */
	A3DBool  m_bIsPMIFilteringSet;            /*!< PMI filtering is flagged on.  */
	A3DBool  m_bIsGeomFilteringSet;           /*!< Geometry filtering is flagged on.  */
	A3DBool  m_bIsCrossSectionSet;            /*!< A Cross-section is flagged on.  */
	A3DBool  m_bIsExplosionSet;               /*!< A view explosion is flagged on.  */
	A3DBool  m_bIsCombineState;               /*!< The view is in a combined state  */
} A3DMkpViewFlagsData;
#endif // A3DAPI_LOAD


/*!
\brief Returns the set of components for a view using a \ref A3DMkpViewFlagsData
\ingroup a3d_annots_view
\version 7.1
\param in pView The view to get components info from.
\param out pFlags The set of flags to write in.
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpViewGetFlags, (const A3DMkpView* pView, A3DMkpViewFlagsData* pFlags));


/*!
\defgroup a3d_markuplinkeditem Markup Linked Item
\ingroup a3d_markup_module
Entity type is \ref kA3DTypeMiscMarkupLinkedItem.

The linked item contains data that can be accessed using the \ref A3DMiscEntityReferenceGet function.
\sa a3d_entity_reference
*/

/*!
\brief Markup Linked Item structure
\ingroup a3d_markuplinkeditem
\version 2.0

The \ref m_bMarkupShowControl and \ref m_bMarkupDeleteControl members apply only to linked items in markups
(\ref A3DMkpMarkupData).
Similarly, the \ref m_bLeaderShowControl and \ref m_bLeaderDeleteControl members apply only to linked items in leaders
(\ref A3DMkpLeaderData).
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;								// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bMarkupShowControl;							/*!< Show/hide markup follows pointed entity. */
	A3DBool m_bMarkupDeleteControl;							/*!< Delete markup follows pointed entity. */
	A3DBool m_bLeaderShowControl;							/*!< Show leader follows pointed entity. */
	A3DBool m_bLeaderDeleteControl;							/*!< Delete leader follows pointed entity. */
	A3DAsmProductOccurrence* m_pTargetProductOccurrence;	/*!< If non-null, this member references a remote product occurrence that contains the reference. */
	A3DEntity* m_pReference;								/*!< Pointer on the referenced entity. Only A3DRiRepresentationItem, A3DAsmProductOccurrence, A3DMiscReferenceOnTopology and A3DMkpMarkup are accepted \version 2.2 */
} A3DMiscMarkupLinkedItemData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMiscMarkupLinkedItemData structure
\ingroup a3d_markuplinkeditem
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMiscMarkupLinkedItemGet,(const A3DMiscMarkupLinkedItem* pLinkedItem,
																A3DMiscMarkupLinkedItemData* pData));

/*!
\brief Creates an \ref A3DMiscMarkupLinkedItem from an \ref A3DMiscMarkupLinkedItemData structure
\ingroup a3d_markuplinkeditem
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE if A3DMiscMarkupLinkedItemData::m_pReference is not of a valid type \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMiscMarkupLinkedItemCreate,(const A3DMiscMarkupLinkedItemData* pData,
																	A3DMiscMarkupLinkedItem** ppLinkedItem));





/*!
\brief Markup structure
\ingroup a3d_markup_module
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DEMarkupType m_eType;						/*!< Markup type. */
	A3DEMarkupSubType m_eSubType;				/*!< Markup subtype. */
	A3DUns32 m_uiLeadersSize;					/*!< The size of m_ppLeaders. */
	A3DMkpLeader** m_ppLeaders;					/*!< Array of \ref A3DMkpLeader. */
	A3DUns32 m_uiLinkedItemsSize;				/*!< The size of \ref m_ppLinkedItems. */
	A3DMiscMarkupLinkedItem** m_ppLinkedItems;	/*!< Array of \ref m_pTessellation. */
	A3DTessMarkup* m_pTessellation;				/*!< Tessellation of markup. May be NULL. Does not contain the leaders' tessellation. */
} A3DMkpMarkupData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMkpMarkupData structure
\ingroup a3d_markup_module
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpMarkupGet,(	const A3DMkpMarkup* pMarkup,
													A3DMkpMarkupData* pData));
/*!
\brief Creates the \ref A3DMkpMarkup from \ref A3DMkpMarkupData structure
\ingroup a3d_markup_module
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpMarkupCreate,(	const A3DMkpMarkupData* pData,
														A3DMkpMarkup** ppMarkup));

/*!
\brief Get links corresponding to the \ref A3DMkpMarkup references
This function allocates and returns an array of \ref A3DMiscMarkupLinkedItem. To free the allocated array, the function
must be called again with pMarkup as NULL.
\ingroup a3d_markup_module
\version 6.0

\param[in] pMarkup The \ref A3DMkpMarkup entity to get linked item list from. If NULL, pppLinkedItems is freed instead of being allocated.
\param[out] puiLinkedItemsSize The number of \ref A3DMiscMarkupLinkedItem in the resulting array.
\param[out] ppLinkedItems The resulting array.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpLinkForMarkupReferenceGet,(const A3DMkpMarkup* pMarkup,
											A3DUns32* puiLinkedItemsSize,
											A3DMiscMarkupLinkedItem*** ppLinkedItems));
/*!
\brief Get links corresponding to the \ref A3DMkpMarkup additional references such as element defining the toleranced line for line profile.
This function allocates and returns an array of \ref A3DMiscMarkupLinkedItem. To free the allocated array, the function
must be called again with pMarkup as NULL.
\ingroup a3d_markup_module
\version 6.0

\param[in] pMarkup The \ref A3DMkpMarkup entity to get linked item list from. If NULL, pppLinkedItems is freed instead of being allocated.
\param[out] puiLinkedItemsSize The number of \ref A3DMiscMarkupLinkedItem in the resulting array.
\param[out] pppLinkedItems The resulting array.

\ingroup a3d_markup_module
\version 9.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DMkpLinkForAdditionalMarkupReferenceGet, (const A3DMkpMarkup* pMarkup,
											A3DUns32* puiLinkedItemsSize,
											A3DMiscMarkupLinkedItem*** pppLinkedItems));
/*!
\defgroup a3d_markup_rtf RTF parsing
\ingroup a3d_markup_module
Set of functions used to parse RTF strings. \n
RTF strings can be present in \ref A3DMDFCFDraftingRowData, \ref A3DMarkupGDTData, \ref A3DMarkupRichTextData

\par Sample code
\include MarkupRTF.cpp
*/

#ifndef A3DAPI_LOAD
typedef void A3DMkpRTFField;

/*!
\brief Structure used to parse RTF strings
\ingroup a3d_markup_rtf
\version 4.2
*/
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_usGetFieldIndex;         /*!< Reserved; used internally to parse RTF string. */
	A3DUTF8Char* m_pcText;				/*!< Text extracted from the RTF. Only one caracter if the text is a symbol. */
	A3DUTF8Char* m_pcFamilyName;	    /*!< Font family name extracted from the RTF. */
	A3DEMarkupSymbol m_eSymbol;         /*!< Set if m_pcText is a symbol. \ref A3DEMarkupSymbol */
	A3DInt32 m_aRGB[3];					/*!< Text color extracted from the RTF. */
	A3DFloat m_fHeight;		            /*!< Text size as millimeter. */
	A3DInt8 m_cAttributes;			    /*!< Text attributes. See \ref a3d_fontattribdef. */
} A3DMkpRTFFieldData;

//! \deprecated This type is deprecated. Please use the \ref A3DRTFFieldData to implement the same behaviour.
typedef A3DMkpRTFFieldData A3DRTFFieldData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_markup_rtf
\brief Initialize RTF data for parsing
Previously A3DMkpRTFInit.
\version 10.1

\return \ref A3D_ERROR \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpRTFFieldCreate,(
	const A3DUTF8Char* pRTFString,
	A3DMkpRTFField** pRTFField));

/*!
\ingroup a3d_markup_rtf
\brief Initialize RTF data for parsing
\version 4.2
\deprecated This function is deprecated. Please use the \ref A3DMkpRTFFieldCreate to implement the same behaviour.

\return \ref A3D_ERROR \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpRTFInit,(	const A3DUTF8Char* pRTF, A3DVoid** pRTFData));
/*!
\ingroup a3d_markup_rtf
\brief Get each independent field of a RTF data
Previously A3DMkpRTFGetField.
\version 10.1

\return \ref A3D_ERROR \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpRTFFieldGet,(
	 const A3DMkpRTFField* pRTFField,
	 A3DMkpRTFFieldData* pRTFFieldData));

/*!
\ingroup a3d_markup_rtf
\brief Get each independent field of a RTF data
\version 4.2
\deprecated This function is deprecated. Please use the \ref A3DMkpRTFFieldGet to implement the same behaviour.

\return \ref A3D_ERROR \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMkpRTFGetField,(
		 A3DVoid* pRTFData,
		 A3DRTFFieldData *pRTFField));

/*!
\ingroup a3d_markup_rtf
\brief delete RTF data created by \ref A3DMkpRTFInit
Previously A3DMkpRTFDelete.
\version 10.1

\return \ref A3D_ERROR \n
\return \ref A3D_SUCCESS \n
*/

A3D_API (A3DStatus, A3DMkpRTFFieldDelete,(const A3DMkpRTFField* pRTFField));

/*!
\ingroup a3d_markup_rtf
\brief delete RTF data created by \ref A3DMkpRTFInit
\version 4.2
\deprecated This function is deprecated. Please use the \ref A3DMkpRTFFieldDelete to implement the same behaviour.

\return \ref A3D_ERROR \n
\return \ref A3D_SUCCESS \n
*/

A3D_API (A3DStatus, A3DMkpRTFDelete,(A3DVoid* pRTFData));



/*!
\defgroup a3d_fonts Fonts
\ingroup a3d_markup_module
When creating a markup tessellation that uses specific fonts,
store fonts with font keys (\ref A3DFontKeyData)
 by invoking the \ref A3DGlobalFontKeyCreate function.

When parsing a markup tessellation that uses specific fonts,
use the \ref A3DGlobalFontKeyGet function
to obtain fonts information from font keys (\ref A3DFontKeyData).

\sa a3d_tessmarkup
*/



/*!
\brief Font key structure
\ingroup a3d_fonts
\version 2.0

This structure stores the information of a font key.
Font keys are used by markup tessellation.

\sa a3d_tessmarkupfontkeydef
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DInt32 m_iFontFamilyIndex;	/*!< Font family index. */
	A3DInt32 m_iFontStyleIndex;		/*!< Font style index. */
	A3DInt32 m_iFontSizeIndex;		/*!< Font size index. */
	A3DInt8 m_cAttributes;			/*!< Font attributes. See \ref a3d_fontattribdef. */
} A3DFontKeyData;

/*!
\brief Font structure
\ingroup a3d_fonts
\version 2.0

This structure stores the information of a font.
*/
typedef struct
{
	A3DUns16 m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcFamilyName;	/*!< Font family name. */
	A3DUTF8Char* m_pcFontFilePath;	/*!< Font file path. */
	A3DECharSet m_eCharset;			/*!< Font character set. */
	A3DUns32 m_uiSize;				/*!< Font size. */
	A3DInt8 m_cAttributes;			/*!< Font attributes. See \ref a3d_fontattribdef. */
} A3DFontData;
#endif // A3DAPI_LOAD

/*!
\brief Retrieves font information (\ref A3DFontData) from the \ref A3DFontKeyData structure.
This function is useful when reading markup tessellation.
\ingroup a3d_fonts
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_MARKUP_CANNOT_ACCESS_FONT \n
\return \ref A3D_MARKUP_INVALID_FONTKEY \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DGlobalFontKeyGet,(	const A3DFontKeyData* pFontKeyData,
														A3DFontData* pFontData));

/*!
\brief Gets text bounding box according to the font
\ingroup a3d_fonts
\version 5.2

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_MARKUP_CANNOT_ACCESS_FONT \n
\return \ref A3D_MARKUP_INVALID_FONTKEY \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DGlobalFontTextBoxGet, (	const A3DFontKeyData* psFontKeyData,
												 A3DUTF8Char* pcText,
												 A3DDouble* pdLength,
												 A3DDouble* pdHeight));

/*!
\brief Gets text bounding box and scale according to the font
\ingroup a3d_fonts
\version 7.2

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_MARKUP_CANNOT_ACCESS_FONT \n
\return \ref A3D_MARKUP_INVALID_FONTKEY \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DGlobalFontTextBoxAndScaleGet, (	const A3DFontKeyData* psFontKeyData,
														A3DUTF8Char* pcText,
														A3DDouble* pdLength,
														A3DDouble* pdHeight,
														A3DDouble* pdScale));
/*!
\brief Creates the \ref A3DFontKeyData structure from the \ref A3DFontData structure
\ingroup a3d_fonts
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_MARKUP_CANNOT_CREATE_FONTKEY \n
\return \ref A3D_MARKUP_INVALID_FONTKEY \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DGlobalFontKeyCreate,(	const A3DFontData* pFontData,
															A3DFontKeyData* pFontKeyData));

/*!
\brief Gets text tessellation as wireframe
\ingroup a3d_fonts
\version 6.1

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_MARKUP_CANNOT_ACCESS_FONT \n
\return \ref A3D_MARKUP_INVALID_FONTKEY \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DGlobalFontTextTessellationGet, (	const A3DFontKeyData* psFontKeyData,
														 const A3DUTF8Char* pcOneChar,
														 A3DRiSet** ppset,
														 A3DDouble* pdCharWidth));


#endif	/*	__A3DPRCMARKUP_H__ */
