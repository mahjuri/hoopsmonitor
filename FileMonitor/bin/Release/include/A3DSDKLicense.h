/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized 
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the 
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header of <b>GOLD license for A3DLIBS</b>. 
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

/*!
\mainpage A3DLIBS V2.3 Gold option

*/

#ifdef A3DAPI_LOAD
#  undef __A3DSDKLICENSE_H__
#endif
#ifndef __A3DSDKLICENSE_H__
#define __A3DSDKLICENSE_H__
#ifndef A3DAPI_LOAD
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKInitializeFunctions.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD

/*!
\defgroup a3d_license_module License module
\brief Methods and structures dedicated to setting the license
\ingroup a3d_base_module
*/

/*!
\brief Sets the license for the current instance of the library based on the Techsoft3d unified key
\ingroup a3d_license_module
\param [in] pcUnifiedKey	The license key.

\return \ref A3D_INVALID_LICENSE \n
\return \ref A3D_INVALID_DATA \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DLicPutUnifiedLicense, ( const A3DUTF8Char* pcUnifiedKey));

#endif	/*	__A3DSDKLICENSE_H__ */
