/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for the read-write module
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifdef A3DAPI_LOAD
#  undef __A3DSDKREADWRITE_H__
#endif
#ifndef __A3DSDKREADWRITE_H__
#define __A3DSDKREADWRITE_H__
#ifndef A3DAPI_LOAD
#  include <A3DCommonReadWrite.h>
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKInitializeFunctions.h>
#  include <A3DSDKStructure.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD

/*!
\addtogroup a3d_readwrite_module Load / Export Module
\brief Methods and structures dedicated to reading and writing capabilities

This module describes the functions and structures that allow you to read and write 3D model files.
*/

/*!
\defgroup a3d_read Load Module
\ingroup a3d_readwrite_module
\brief Loads the model file with information from the input CAD file.

Please refer to \REF_SUPPORTED_FORMATS documentation for list of available formats.
*/


/*!
\ingroup a3d_read
\brief Returns the format of a physical file.

\param [in] pcFileName References the path to the CAD file
\param [out] peModellerType References the format of the input CAD file

\return \ref A3D_INVALID_LICENSE \n
\return \ref A3D_LOAD_CANNOT_ACCESS_CADFILE \n
\return \ref A3D_LOAD_CANNOT_LOAD_MODEL \n
\return \ref A3D_LOAD_CANNOT_LOAD_MULTIENTRY \n
\return \ref A3D_LOAD_EMPTY_MULTI_MODEL \n
\return \ref A3D_LOAD_INVALID_FILE_FORMAT \n
\return \ref A3D_LOAD_MISSING_COMPONENTS \n
\return \ref A3D_LOAD_MULTI_MODELS_CADFILE \n
\return \ref A3D_LOAD_READER_NOT_IMPLEMENTED \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_WRITER_NOT_IMPLEMENTED \n
\return \ref A3D_SUCCESS \n

\version 3.0
*/
A3D_API (A3DStatus, A3DGetFileFormat, (
	const A3DUTF8Char* pcFileName,
	A3DEModellerType *peModellerType));


/*!
\ingroup a3d_read
\brief Checks the format of a physical file.

\param [in] pcFileName References the path to the CAD file
\param [in] eCadTypeWanted References the format that the input CAD file is going to be checked against

\return \ref A3D_INVALID_LICENSE \n
\return \ref A3D_LOAD_CANNOT_ACCESS_CADFILE \n
\return \ref A3D_LOAD_CANNOT_LOAD_MODEL \n
\return \ref A3D_LOAD_CANNOT_LOAD_MULTIENTRY \n
\return \ref A3D_LOAD_EMPTY_MULTI_MODEL \n
\return \ref A3D_LOAD_INVALID_FILE_FORMAT \n
\return \ref A3D_LOAD_MISSING_COMPONENTS \n
\return \ref A3D_LOAD_MULTI_MODELS_CADFILE \n
\return \ref A3D_LOAD_READER_NOT_IMPLEMENTED \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_WRITER_NOT_IMPLEMENTED \n
\return \ref A3D_SUCCESS \n

\version 6.0
*/
A3D_API (A3DStatus, A3DCheckFileFormat, (
		 const A3DUTF8Char* pcFileName,
		 A3DEModellerType eCadTypeWanted));

/*!
\ingroup a3d_read
\brief Thumbnail data

\version 8.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns8 *m_pcBuffer;						/*!< Thumbnail buffer. */
	A3DEPictureDataFormat m_eFormat;			/*!< References the format of the thumbnail */
	A3DUns32 m_iLength;							/*!< Length of the thumbnail buffer. */
}A3DThumbnailData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_read
\brief Extract thumbnail stored in a CAD file.

\param [in] pcFileName References the path to the CAD file
\param [out] pThumbnailData References the thumbnail data

\return \ref A3D_INVALID_LICENSE \n
\return \ref A3D_LOAD_CANNOT_ACCESS_CADFILE \n
\return \ref A3D_LOAD_CANNOT_LOAD_MODEL \n
\return \ref A3D_LOAD_CANNOT_LOAD_MULTIENTRY \n
\return \ref A3D_LOAD_EMPTY_MULTI_MODEL \n
\return \ref A3D_LOAD_INVALID_FILE_FORMAT \n
\return \ref A3D_LOAD_MISSING_COMPONENTS \n
\return \ref A3D_LOAD_MULTI_MODELS_CADFILE \n
\return \ref A3D_LOAD_READER_NOT_IMPLEMENTED \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_WRITER_NOT_IMPLEMENTED \n
\return \ref A3D_SUCCESS \n

\warning This function is only available for Catia V5 and Solidworks inputs.

\version 8.2
*/
A3D_API(A3DStatus, A3DExtractFileThumbnail, (
	const A3DUTF8Char* pcFileName,
	A3DThumbnailData *pThumbnailData));

/*!
\ingroup a3d_read
\brief File

\version 8.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DEModellerType m_eModellerType;			/*!< Modeller type. */
	A3DUTF8Char *m_pcModelName;					/*!< Model name. */
	A3DUTF8Char *m_pcSoftwareVersion;			/*!< Software release */
	A3DUTF8Char *m_pcAuthor;					/*!< Author */
	A3DUTF8Char *m_pcOrganization;				/*!< Organization */
	A3DInt32	m_iTimeStamp;					/*!< TimeStamp. */
}A3DFileInformationData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_read
\brief Get file information a CAD file.

\param [in] pcFileName References the path to the CAD file
\param [out] pFileInformationData References the information of the input CAD file

\return \ref A3D_INVALID_LICENSE \n
\return \ref A3D_LOAD_CANNOT_ACCESS_CADFILE \n
\return \ref A3D_LOAD_CANNOT_LOAD_MODEL \n
\return \ref A3D_LOAD_CANNOT_LOAD_MULTIENTRY \n
\return \ref A3D_LOAD_EMPTY_MULTI_MODEL \n
\return \ref A3D_LOAD_INVALID_FILE_FORMAT \n
\return \ref A3D_LOAD_MISSING_COMPONENTS \n
\return \ref A3D_LOAD_MULTI_MODELS_CADFILE \n
\return \ref A3D_LOAD_READER_NOT_IMPLEMENTED \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_WRITER_NOT_IMPLEMENTED \n
\return \ref A3D_SUCCESS \n

\warning This function is only available for Catia V5 and Solidworks inputs. For unsupported formats, the function returns `A3D_LOAD_CANNOT_ACCESS_CADFILE`.

\version 8.2
*/
A3D_API(A3DStatus, A3DGetFileInformation, (
	const A3DUTF8Char* pcFileName,
	A3DFileInformationData *pFileInformationData));





/*!
\defgroup a3d_write Export Module
\ingroup a3d_readwrite_module
\brief Structures to write the ModelFile defined through this API.

Please refer to \REF_SUPPORTED_FORMATS documentation for list of available formats.
*/


/*!
\ingroup a3d_write
\brief Extract file paths from a model file.

This function reads the \ref A3DAsmModelFile extracts the file path of all the sub-assemblies, part and missing files
of the \ref A3DAsmModelFile.\n
Sub-assemblies are files referencing other files (part files or other sub-assemblies files).\n
Part files are final files with no reference to an other depending file.\n
If a file has reference on both types of file, the file will be considered as a sub-assembly file and will not be
referenced in the part file path array.\n
Missing files path are sub-files that cannot be loaded for some reasons: bad paths, file not found...

If pA3DAsmModelFile is NULL, the A3DUTF8Char arrays will be freed if the corresponding unsigned int parameters are
non-null: A3DAsmGetFilesPathFromModelFile(NULL, [...]) to delete the arrays.

\param [in] pA3DAsmModelFile The input model file. Can be "tree only" A3DAsmModelFile.
\param [out] uNbPartFiles The number of parts detected. (must not be NULL)
\param [out] ppPartFilesPaths The file names of detected parts. (must not be NULL)
\param [out] uNbAssemblyFiles The number of sub-assemblies detected. (can be NULL)
\param [out] ppAssemblyFilesPaths The file names of detected sub-assemblies. (can be NULL)
\param [out] uNbMissingFiles The number of missing files. (can be NULL)
\param [out] ppMissingFilesPaths The file names of detected missing files. (can be NULL)

\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_SUCCESS \n

\version 5.0
*/
A3D_API ( A3DStatus, A3DAsmGetFilesPathFromModelFile, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	A3DUns32* uNbPartFiles,
	A3DUTF8Char*** ppPartFilesPaths,
	A3DUns32* uNbAssemblyFiles,
	A3DUTF8Char*** ppAssemblyFilesPaths,
	A3DUns32* uNbMissingFiles,
	A3DUTF8Char*** ppMissingFilesPaths));


/*!
\ingroup a3d_write
\brief A structure that specifies the file contexts
It is used to by:
\ref A3DAsmGetFilesContextsFromModelFile \n

\version 8.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16		m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char*	m_pcFileName;						/*!< Name of the concerned file. */
	A3DUns32		m_uiNbContexts;						/*!< Number of contexts for this file. */
	A3DUTF8Char**	m_apcContexts;						/*!< Contexts for this file. */
} A3DFileContextData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_write
\brief Extract all the files contexts (RefSet, Configs...) from a model file.

\param [in] pA3DAsmModelFile The input model file. Can be "tree only" A3DAsmModelFile.
\param [out] uNbFilesContexts The number of files contexts detected. (must not be NULL)
\param [out] ppFilesContexts The file contexts. (must not be NULL)

\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_SUCCESS \n

\version 8.1
*/
A3D_API ( A3DStatus, A3DAsmGetFilesContextsFromModelFile, (
		 const A3DAsmModelFile* pA3DAsmModelFile,
		 A3DUns32* uNbFilesContexts,
		 A3DFileContextData*** ppFilesContexts));

/*!
\ingroup a3d_write
\brief This function is used to free memory allocated by following functions:
\ref A3DAsmGetFilesContextsFromModelFile \n

\version 8.1

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DFileContextDelete, (A3DFileContextData* pData));




/*!
\ingroup a3d_write
\brief Creates a model file from multiple PRC files + a PRC assembly file + a map for file names.

This function inserts all the parts and product occurrences from a file list into a single PRC assembly file loaded in "assembly NO Dependency" mode
and outputs a reassembled model file.

\param [in] pcRootPrcFilePath The file name of the top assembly file. This model file is loaded as assembly tree only and saved as a PRC file.
\param [in] uNbFile The number of file names to plug into the assembly.
\param [in] ppPRCFilePath The equivalent PRC file paths for parts. Length must equal uNbFile.
\param [in] ppCADFilePath The native part file paths. Length must equal uNbFile.
\param [in] pParamsLoadData The parameters to use when reading the model files. Configuration must be set to read the default entry or must only specify a single entry to read.
\param [out] pA3DAsmModelFile The final reassembled model file.

\return \ref A3D_ERROR \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA \n
\return \ref A3D_LOAD_INVALID_FILE_NAME \n
\return \ref A3D_SUCCESS \n

\version 7.1
*/
A3D_API ( A3DStatus, A3DAsmModelFileLoadFromPRCFiles, (	const A3DUTF8Char* pcRootPrcFilePath,
														const A3DUns32 uNbFile,
														const A3DUTF8Char** ppPRCFilePath,
														const A3DUTF8Char** ppCADFilePath,
														const A3DRWParamsLoadData* pParamsLoadData,
														A3DAsmModelFile** pA3DAsmModelFile));


/*!
\ingroup a3d_write
\brief Creates a model file from multiple PRC files + an PRC assembly file + a map for file names.

This function replugs all the parts from a list from a PRC file loaded with an "assembly tree only" mode.

\param [in] pcFileName The file name of the top assembly file. Conceived for model file loaded as assembly tree only and saved as a PRC file.
\param [in] uNbPart The number of file names to plug.
\param [in] ppCADFilePath The native part file paths. Length must equal uNbPart.
\param [in] ppPRCFilePath The equivalent PRC file paths for parts. Length must equal uNbPart.
\param [in] pParamsLoadData The read/write parameters use the model file. Configurations must be set (default or numEntries = 1).
\param [out] pA3DAsmModelFile The final reassembled model file.

\return \ref A3D_SUCCESS \n

\version 5.0
*/
A3D_API ( A3DStatus, A3DAsmModelFileLoadFromMultiplePrcFile, (
	const A3DUTF8Char* pcFileName,
	const A3DUns32 uNbPart,
	const A3DUTF8Char** ppCADFilePath,
	const A3DUTF8Char** ppPRCFilePath,
	const A3DRWParamsLoadData* pParamsLoadData,
	A3DAsmModelFile** pA3DAsmModelFile));

/*!
\ingroup a3d_write
\brief A structure that specifies parameters used to write the model file to Parasolid format.
\version 3.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bSaveSolidsAsFaces;		/*!<
										Specifies how solids are written.
										\li With a value of \c TRUE, solids are written as independent faces.
										\li With a value of \c FALSE, solid topology is kept. */
	A3DBool m_bWriteTessellation;		/*!< If \c TRUE tessellations will be converted to bodies. If \c FALSE tessellations will be ignored. */
	A3DBool m_bWriteBlankedEntities;	/*!< Write blanked entities (hidden entities become visible). */
	A3DUTF8Char* m_pcApplication;	/*!< Application name (will be reported in the output file). */
	A3DUTF8Char* m_pcVersion;		/*!< Application version (will be reported in the output file). */
	A3DBool m_bBStrictAssemblyStructure;	/*!< If \c TRUE, HOOPS Exchange will attempt to build a Parasolid assembly tree as closely as possible to how it was in the original software (the goal is to remove the effects of the intermediate PRC assembly, which is not equivalent to the Parasolid assemblies). This is achieved by heavily modifying the assembly tree and attributes in the exported Parasolid. Names of bodies will be a concatenation of the several assembly levels, and may not match the original names in Parasolid.*/
	A3DBool m_bExplodeMultiBodies;		/*!< Explode multi-connexes B-reps into multiple bodies. \version 9.0 */
	A3DBool m_bMakePointsWithCoordinateSystems; /*!< Write Coordinate Systems as Points (There's no equivalent in Parasolid format). */
} A3DRWParamsExportParasolidData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_write
\brief Writes a model file to a physical file in Parasolid format.

This function writes the \ref A3DAsmModelFile entity to a physical file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [in] pcCADFileName References the path of the file into which the function stores the model file.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 3.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToParasolidFile, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportParasolidData* pParamsExportData,
	const A3DUTF8Char* pcCADFileName));


/*!
\ingroup a3d_write
\brief A structure that contains chained buffers.
It is used to by:
\ref A3DAsmModelFileExportToParasolidBuffer \n
\ref A3DRepresentationItemExportToParasolidBuffer \n
\ref A3DAsmModelFileExportToAcisBuffer \n
\ref A3DRepresentationItemExportToAcisBuffer \n
\ref A3DChainedBufferFree \n
This structure can be used directly when loading a file from a buffer in Parasolid.

\version 4.0
*/
#ifndef A3DAPI_LOAD
typedef struct A3DChainedBuffer_t
{
	struct A3DChainedBuffer_t*	m_pNext;
	size_t				m_sSize;
	const char*			m_pBuffer;
} A3DChainedBuffer;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_write
\brief This function is used to free memory allocated by following functions:
\ref A3DAsmModelFileExportToParasolidBuffer \n
\ref A3DRepresentationItemExportToParasolidBuffer \n
\ref A3DAsmModelFileExportToAcisBuffer \n
\ref A3DRepresentationItemExportToAcisBuffer \n
\ref A3DChainedBufferFree \n

\version 4.0
*/
A3D_API(A3DStatus, A3DChainedBufferFree, (A3DChainedBuffer* pBuffer));


/*!
\ingroup a3d_write
\brief Writes a model file to a buffer in Parasolid format.

This function writes the \ref A3DAsmModelFile entity to a buffer.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [out] pBuffer References the chained buffer to be filled.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 4.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToParasolidBuffer, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportParasolidData* pParamsExportData,
	A3DChainedBuffer** pBuffer));


/*!
\ingroup a3d_write
\brief Writes a representation item to a file in Parasolid format.

This function writes the \ref A3DAsmModelFile entity to a file.

\param [in] pRepItem References the \ref A3DRiRepresentationItem to be written out.
\param [in,out] pcTempFileName References the output file to be written.
\param [in] dUnit References the wanted unit for the RI.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 4.0
*/
A3D_API(A3DStatus, A3DRepresentationItemExportToParasolidFile, (
	const A3DRiRepresentationItem* pRepItem,
	const A3DUTF8Char* pcTempFileName,
	A3DDouble dUnit));


/*!
\ingroup a3d_write
\brief Writes a representation item to a buffer in Parasolid format.

This function writes the \ref A3DRiRepresentationItem entity to a buffer.

\param [in] pRepItem References the \ref A3DRiRepresentationItem to be written out.
\param [in] pBuffer References the chained buffer to be filled.
\param [in] dUnit References the wanted unit for the RI.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 4.0
*/
A3D_API(A3DStatus, A3DRepresentationItemExportToParasolidBuffer, (const A3DRiRepresentationItem* pRepItem,
	A3DChainedBuffer** pBuffer,
	double dUnit));


/*!
\ingroup a3d_write
\brief A structure that specifies parameters used to translate the model file or a representation item into an open Parasolid session.
\version 8.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;										// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DETranslateToPkPartsHealing m_eHealing;						/*!< Enum to control healing */
	A3DETranslateToPkPartsAccurate m_eComputeAccurateEdges;			/*!< Enum to control accurate edge computation. */
	A3DBool m_bDisjoinIfFaceFaceError;								/*!< If \c TRUE bodies with face_face error will be disjoined. */
	A3DBool m_bSew;													/*!< Sew the geometry. */
	A3DDouble m_dSewingTolerance;									/*!< Enables the sewing tolerance in meters. */
	A3DETranslateToPkPartsSimplifyGeometry m_eSimplifyGeometry;		/*!< Simplify geometry and topology. */
	A3DETranslateToPkPartsMergeEntities m_eMergeEntities;			/*!< Merge: remove redundant entities (edges/surfaces). */
	A3DUTF8Char* m_pcPSBodyShopPath;								/*!< Path to PSBodyshop DLL. Necessary for simplify and merge options. */
	A3DMiscPKMapper** m_pMapper;									/*!< Mapper. This pointer is set by Exchange. If set to null, the mapper will be allocated and populated for the first time. If the mapper has already been allocated and the pointer isn't null, the mapper will be filled with new elements. It can be deleted by \ref A3DEntityDelete().\version 9.0 */
	A3DBool m_bUseColour2Attribute;									/*!< Use SDL/TYSA_COLOUR_2 Parasolid attribute. \version 9.1 */
	A3DBool m_bUseUNameAttribute;									/*!< Use SDL/TYSA_UNAME Parasolid attribute. \version 9.1 */
	A3DBool m_bUseLayerAttribute;									/*!< Use SDL/TYSA_LAYER Parasolid attribute. \version 9.1 */
	A3DUns32 m_uiNbProc;											/*!< MultiProcess operation. Use in \ref A3DAsmModelFileTranslateToPkParts \version 11.0 */
	A3DBool m_bExportNormalsWithTessellation;						/*!< When exporting tessellation (with Parasolid v28.1 and beyond), setting this option ensures normals aren't translated. This can lead to some large files being translated in much less time. \version 13.2 */
} A3DRWParamsTranslateToPkPartsData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_write
\brief Writes a model file to parts in the current Parasolid session.

This function writes the \ref A3DAsmModelFile entity to parts in the current Parasolid session.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [in] pParamsTranslateToPkPartsData References the parameters to export in the Parasolid session. \version 8.1
\param [out] pNbPkParts References the number of resulting parts.
\param [out] pPkParts References the array of resulting parts. Memory for the pPkParts array should be allocated and freed in Parasolid.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return `A3D_PARASOLID_MEMORY_ERROR` \n
\return \ref A3D_SUCCESS \n

\version 8.0
*/
A3D_API(A3DStatus, A3DAsmModelFileTranslateToPkParts, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportParasolidData* pParamsExportData,
	const A3DRWParamsTranslateToPkPartsData* pParamsTranslateToPkPartsData,
	int* pNbPkParts,
	int** pPkParts));

/*!
\ingroup a3d_write
\brief Writes a representation item to parts in the current Parasolid session.

This function writes the \ref A3DRiRepresentationItem entity to parts in the current Parasolid session.

\param [in] pRepItem References the \ref A3DRiRepresentationItem to be written out.
\param [in] pParamsTranslateToPkPartsData References the parameters to export in the Parasolid session. \version 8.1
\param [in] dUnit References the wanted unit for the RI.
\param [out] pNbPkParts References the number of resulting parts.
\param [out] pPkParts References the array of resulting parts.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return `A3D_PARASOLID_MEMORY_ERROR` \n
\return \ref A3D_SUCCESS \n

\version 8.0
*/
A3D_API(A3DStatus, A3DRepresentationItemTranslateToPkParts, (
	const A3DRiRepresentationItem* pRepItem,
	const A3DRWParamsTranslateToPkPartsData* pParamsTranslateToPkPartsData,
	double dUnit,
	int* pNbPkParts,
	int** pPkParts));


/*!
\ingroup a3d_write
\brief Writes a representation item to parts in the current Parasolid session in multi-process context.

This function writes the \ref A3DRiRepresentationItem entity to parts in the current Parasolid session.

\param [in]	uiNbProc References the number of processes on which to run
\param [in] pParamsTranslateToPkPartsData References the parameters to export in the Parasolid session. \version 8.1
\param [in] uiNbRepItem References the number of \ref A3DRiRepresentationItem to be written out.
\param [in] pRepItem References the array of \ref A3DRiRepresentationItem to be written out.
\param [in] padUnit References the array of wanted units for the RI.
\param [out] pNbPkParts References the array of numbers of resulting parts.
\param [out] pPkParts References the array of arrays of resulting parts.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return `A3D_PARASOLID_MEMORY_ERROR` \n
\return \ref A3D_SUCCESS \n

\version 9.2
*/
A3D_API(A3DStatus, A3DRepresentationItemsTranslateToPkPartsMultiProcess, (
	A3DUns32 uiNbProc,
	A3DRWParamsTranslateToPkPartsData const * pParamsTranslateToPkPartsData,
	A3DUns32 uiNbRepItem,
	A3DRiRepresentationItem const ** pRepItem,
	A3DDouble const *padUnit,
	int** pNbPkParts,
	int*** pPkParts));

/*!
\ingroup a3d_write
\brief Heal parts in the current Parasolid session.


\param [in] pNbPkParts References the number of resulting parts.
\param [in,out] pPkParts References the array of resulting parts.
\param [in] pParamsTranslateToPkPartsData References the parameters to export in the Parasolid session. \version 8.1

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_SUCCESS \n

\version 9.1
*/
A3D_API(A3DStatus, A3DHealPkParts, (
	int* pNbPkParts,
	int** pPkParts,
	const A3DRWParamsTranslateToPkPartsData* pParamsTranslateToPkPartsData));

/*!
\ingroup a3d_write
\brief Get Parasolid entities from HOOPS Exchange entity

\param [in] pMapper Topology entities mapper
\param [in] pA3DEntity The HOOPS Exchange entity
\param [out] piNbPKEntities Number of Parasolid entities in the following array
\param [out] ppPKEntities Array of Parasolid entities. This array is internal to the A3DMiscPKMapper and should not be modified (changing it will compromise the A3DMiscPKMapper). The lifetime of this array is the same as the lifetime of the A3DMiscPKMapper.

\return \ref A3D_SUCCESS \n

\version 9.0
*/
A3D_API(A3DStatus, A3DMiscPKMapperGetPKEntitiesFromA3DEntity, (
	const A3DMiscPKMapper * pMapper,
	const A3DEntity* pA3DEntity,
	int* piNbPKEntities,
	int** ppPKEntities));

/*!
\ingroup a3d_write
\brief Get HOOPS Exchange entities from Parasolid entity

\param [in] pMapper Topology entities mapper
\param [in] pPKEntity The Parasolid entity
\param [out] piNbA3DEntities Number of HOOPS Exchange entities in the following array
\param [out] ppA3DEntities Array of HOOPS Exchange entities. This array is internal to the A3DMiscPKMapper and shouldn't be modified (changing it will compromise the A3DMiscPKMapper). The lifetime of this array is the same as the lifetime of the A3DMiscPKMapper.

\return \ref A3D_SUCCESS \n

\version 9.0
*/
A3D_API(A3DStatus, A3DMiscPKMapperGetA3DEntitiesFromPKEntity, (
	const A3DMiscPKMapper * pMapper,
	int pPKEntity,
	int* piNbA3DEntities,
	A3DEntity*** ppA3DEntities));


/*!
\ingroup a3d_write
\brief Writes a model file to a physical file in Parasolid format .

This function writes the \ref A3DAsmModelFile entity to a physical file. This function uses the PK bodies already converted and stored
in the pMapper with the \ref A3DAsmModelFile structure in order to export them as a Parasolid file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pMapper Topology entities mapper
\param [in] pParamsExportData References the parameters for export.
\param [in] pcCADFileName References the path of the file into which the function stores the model file.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 10.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToParasolidFileWithMapper, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	A3DMiscPKMapper* pMapper,
	const A3DRWParamsExportParasolidData* pParamsExportData,
	const A3DUTF8Char* pcCADFileName));

/*!
\ingroup a3d_read
\brief Translate Parasolid Parts from parasolid session into an A3DModelFile with a mapper.

This function creates an \ref A3DAsmModelFile entity from parts of parasolid session and creates a mapper to get links between entities.

\param [in] pNbPkParts References the number of parts.
\param [in] pPkParts References the array of parts.
\param [in] pLoadParam Load parameters.
\param [out] ppA3DAsmModelFile References the \ref A3DAsmModelFile to be created.
\param [out] pMapper Topology entities mapper

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_LOAD_CANNOT_ACCESS_CADFILE \n
\return \ref A3D_LOAD_READER_NOT_IMPLEMENTED \n
\return \ref A3D_LOAD_CANNOT_LOAD_MODEL \n
\return \ref A3D_LOAD_CANNOT_LOAD_MULTIENTRY \n
\return \ref A3D_LOAD_EMPTY_MULTI_MODEL \n
\return \ref A3D_LOAD_MISSING_COMPONENTS \n
\return \ref A3D_LOAD_MULTI_MODELS_CADFILE if the file contains multiple entries (see A3DRWParamsMultiEntriesData). \n
\return \ref A3D_LOAD_INVALID_FILE_FORMAT \n
\return \ref A3D_HEPB_MISSING \n
\return \ref A3D_INVALID_DATA_NULL \n
\return \ref A3D_SUCCESS \n
\version 10.2
*/
A3D_API(A3DStatus, A3DPkPartsTranslateToA3DAsmModelFile, (
	int pNbPkParts,
	int* pPkParts,
	A3DRWParamsLoadData* pLoadParam,
	A3DAsmModelFile** ppA3DAsmModelFile,
	A3DMiscPKMapper** pMapper));

/*!
\ingroup a3d_write
\brief Writes a model file to a buffer in Parasolid format .

This function writes the \ref A3DAsmModelFile entity to a physical file. This function uses the PK bodies already converted and stored
in the pMapper with the \ref A3DAsmModelFile structure in order to export them as a Parasolid file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pMapper Topology entities mapper
\param [in] pParamsExportData References the parameters for export.
\param [out] pBuffer Buffer.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 11.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToParasolidBufferWithMapper, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	A3DMiscPKMapper* pMapper,
	const A3DRWParamsExportParasolidData* pParamsExportData,
	A3DChainedBuffer** pBuffer));



/*!
\ingroup a3d_write
\brief Writes a model file to a stream buffer in a PRC format.

This function writes the \ref A3DAsmModelFile entity to a PRC stream buffer.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [out] pcStream Buffer containing the PRC content.
\param [out] uLength Length of the pcStream.
\param [in,out] ppPrcWriteHelper Used to get PRC data such as unique identifiers of PRC nodes.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 4.0
*/
A3D_API ( A3DStatus, A3DAsmModelFileExportToPrcStream, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportPrcData* pParamsExportData,
	A3DUTF8Char** pcStream,
	A3DUns32* uLength,
	A3DRWParamsPrcWriteHelper** ppPrcWriteHelper));

/*!
\ingroup a3d_write
\brief A structure that specifies parameters used to write the model file to ACIS format.
\version 4.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bSaveAsMillimeter;				/*!< Save data in millimeter instead of current model unit. */
	A3DBool m_bSaveAsBinary;					/*!< Save in binary file (.sab). */
	A3DBool m_bForceSurfaceDuplication;			/*!< Force surface duplication - \ref A3DSurfBase are not shared between \ref A3DTopoFace.*/
} A3DRWParamsExportAcisData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_write
\brief Writes a model file to a physical file in ACIS format.

This function writes the \ref A3DAsmModelFile entity to a physical file.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [in] pcCADFileName References the path of the file into which the function stores the model file.

\return \ref A3D_INVALID_LICENSE \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 5.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToAcisFile, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportAcisData* pParamsExportData,
	const A3DUTF8Char* pcCADFileName));

/*!
\ingroup a3d_write
\brief Writes a model file to a buffer in binary ACIS format.

This function writes the \ref A3DAsmModelFile entity to a buffer.

\param [in] pA3DAsmModelFile References the \ref A3DAsmModelFile to be written out.
\param [in] pParamsExportData References the parameters for export.
\param [out] pBuffer Buffer.

\return \ref A3D_INVALID_LICENSE \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 5.0
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToAcisBuffer, (
	const A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportAcisData* pParamsExportData,
	A3DChainedBuffer** pBuffer));

/*!
\ingroup a3d_write
\brief Writes a representation item to a buffer in binary ACIS format.

This function writes the \ref A3DRiRepresentationItem entity to a buffer.

\param [in] pRepItem References the \ref A3DRiRepresentationItem to be written out.
\param [in] pBuffer Buffer.
\param [in] dScale Scale applied to the representation item.

\return \ref A3D_INVALID_LICENSE \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_WRITE_INVALID_FILE_NAME \n
\return \ref A3D_WRITE_ERROR \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER \n
\return \ref A3D_SUCCESS \n

\version 7.2
*/
A3D_API(A3DStatus, A3DRepresentationItemExportToAcisBuffer, (
	const A3DRiRepresentationItem* pRepItem,
	A3DChainedBuffer** pBuffer,
	double dScale));

/*!
\ingroup a3d_read
\brief Loads an \ref A3DAsmModelFile from a XML file and a set of native cad files.

This function loads an \ref A3DAsmModelFile from a XML file and a set of native cad files.

\param [in] pcFileName References the path to the file containing the XML description of the assembly.
\param [in] pLoadParametersData References the parameters for reading the native parts (does not affect the reading of the XML file).
\param [out] ppModelFile References a pointer into which should be stored the location
of the model file. Set this pointer to null before calling the function.

\return `A3D_FILE_TOO_OLD` \n
\return `A3D_FILE_TOO_RECENT` \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_LOAD_CANNOT_ACCESS_CADFILE \n
\return \ref A3D_LOAD_CANNOT_LOAD_MODEL \n
\return \ref A3D_LOAD_CANNOT_LOAD_MULTIENTRY \n
\return \ref A3D_LOAD_EMPTY_MULTI_MODEL \n
\return \ref A3D_LOAD_FILE_TOO_OLD \n
\return \ref A3D_LOAD_FILE_TOO_RECENT \n
\return \ref A3D_LOAD_INVALID_FILE_FORMAT \n
\return \ref A3D_LOAD_MISSING_COMPONENTS \n
\return \ref A3D_LOAD_MULTI_MODELS_CADFILE if the file contains multiple entries (see A3DRWParamsMultiEntriesData). \n
\return \ref A3D_LOAD_READER_NOT_IMPLEMENTED \n
\return \ref A3D_SUCCESS \n

Only a subset of what is exported is supported on import:
\code{.xml}
<!-- Root: mandatory -->
<Root>
  <!-- ModelFile: mandatory
    Name: optional; default value is none
    Unit: optional, 64-bit floating point value (C double); > 0; default value is 1
  -->
  <ModelFile Name="My model file" Unit="10">
    <!-- ProductOccurrence:
      Id: mandatory; 32-bit signed integer > 0; must be unique in the file; ids are not required to be consecutive or ordered
      PersistentId: optional; 32-bit signed integer (C int); default value is 0
      Name: optional
      FilePath: optional; mutually exclusive with Children; can be absolute, relative or UNC; Windows or Linux style; path to any file format supported by HOOPS Exchange except XML; actually loads a native part only if the node has no InstanceRef (i.e. prototype), no InstanceRefExt and no Children
      InstanceRef: prototype id; the prototype ProductOccurrence must have defined before; optional; 32-bit signed integer (C int); > 0; default value is -1 (no prototype)
      InstanceRefExt: external id; the external ProductOccurrence must have defined before; optional; 32-bit signed integer (C int); > 0; default value is -1 (no external)
      Children: optional; array of 32-bit integer values (C int); each value should be a valid ProductOccurrence id; the ProductOccurrence must have defined before; mutually exclusive with FilePath
      Unit: optional; 64-bit floating point value (C double); > 0; default value is 1 (mm?)
      Color: optional; array of 4 32-bit signed integers (C int) describing the RGBA; from 0 to 255 - default value is 255; Alpha from 0 (transparent) to 255 (opaque); applies only to nodes loading a native part
    -->
    <!-- leaf node loading a file, used as a prototype -->
    <ProductOccurrence Id="10" PersistentId="34" Name="file_1_" FilePath="file_1.prc"/>

    <!-- internal node referencing the prototype of Id 10; the FilePath is ignored -->
    <ProductOccurrence Id="20" PersistentId="34" Name="file_1" InstanceRef="10" FilePath="file_1.prc">
      <!-- Transformation: matrix 4 x 4; optional; array of 16 64-bit floating point values (C doubles); format: r00 r01 r02 0 r10 r11 r12 0 r20 r21 r22 0 x y z 1 -->
      <Transformation RelativeTransfo="1 0 0 0 0 1 0 0 0 0 0 1 100 0 0 1"/>
    </ProductOccurrence>

    <!-- leaf node loading a file, used as a prototype -->
    <ProductOccurrence Id="3" PersistentId="17" Name="file_2_" FilePath="D:\temporaire\file_2.prc"/>

    <!-- internal node referencing a prototype and overloading its color -->
    <ProductOccurrence Id="4" PersistentId="17" Name="file_2" InstanceRef="3" Color="255 0 0 255">
      <Transformation RelativeTransfo="0 0.70711 0.70711 0 -1 0 0 0 0 -0.70711 0.70711 0 0 100 0 1"/>
    </ProductOccurrence>

    <!-- leaf node loading a file, used as a prototype -->
    <ProductOccurrence Id="500" PersistentId="11" Name="file_3_" FilePath="d:/temporaire/file_3.prc"/>

    <!-- internal node referencing a prototype -->
    <ProductOccurrence Id="600" PersistentId="11" Name="file_3" InstanceRef="500">
      <Transformation RelativeTransfo="1 0 0 0 0 1 0 0 0 0 0 1 0 0 0 100 1"/>
    </ProductOccurrence>

    <!-- leaf node loading a file, used as a prototype -->
    <ProductOccurrence Id="7" PersistentId="13" Name="file_4_" FilePath="sub_directory/file_4.prc"/>

    <!-- internal node referencing a prototype -->
    <ProductOccurrence Id="8" PersistentId="13" Name="file_4" InstanceRef="7">
      <Transformation RelativeTransfo="1 0 0 0 0 1 0 0 0 0 1 0 100 100 100 1"/>
    </ProductOccurrence>

    <!-- root node with four children; the Color specification is ignored -->
    <ProductOccurrence Id="11" PersistentId="1011" Name="assembly" Children="20 4 600 8" Color="255 255 0 128"/>

  </ModelFile>
</Root>
\endcode

\version 7.0
*/
A3D_API ( A3DStatus, A3DAsmModelFileLoadFromXMLFile, (
	const A3DUTF8Char* pcFileName,
	const A3DRWParamsLoadData* pLoadParametersData,
	A3DAsmModelFile** ppModelFile));

/*!
\ingroup a3d_read
\brief Loads an \ref A3DAsmModelFile from a XML buffer and a set of native cad files.

This function loads an \ref A3DAsmModelFile from a XML buffer and a set of native cad files.

\param [in] pcBuffer References null terminated buffer containing the XML description of the assembly.
\param [in] pLoadParametersData References the parameters for reading the native parts (does not affect the reading of the XML file).
\param [out] ppModelFile References a pointer into which should be stored the location
of the model file. Set this pointer to null before calling the function.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_LOAD_CANNOT_ACCESS_CADFILE \n
\return \ref A3D_LOAD_READER_NOT_IMPLEMENTED \n
\return \ref A3D_LOAD_CANNOT_LOAD_MODEL \n
\return \ref A3D_LOAD_CANNOT_LOAD_MULTIENTRY \n
\return \ref A3D_LOAD_EMPTY_MULTI_MODEL \n
\return \ref A3D_LOAD_MISSING_COMPONENTS \n
\return \ref A3D_LOAD_MULTI_MODELS_CADFILE if the file contains multiple entries (see A3DRWParamsMultiEntriesData). \n
\return \ref A3D_LOAD_INVALID_FILE_FORMAT \n
\return \ref A3D_SUCCESS \n

Supported elements and examples: see \ref A3DAsmModelFileLoadFromXMLFile.

\version 11.2
*/
A3D_API(A3DStatus, A3DAsmModelFileLoadFromXMLStream, (
	const A3DUTF8Char* pcBuffer,
	const A3DRWParamsLoadData* pLoadParametersData,
	A3DAsmModelFile** ppModelFile));

#endif	/*	__A3DSDKREADWRITE_H__ */
