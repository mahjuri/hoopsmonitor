/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Top-level header file of the \COMPONENT_A3D_API
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/
#ifndef A3DSDKEXPORTS_HPP
#define A3DSDKEXPORTS_HPP

#define A3D_DLL_MAJORVERSION 13
#define A3D_DLL_MINORVERSION 0
#define A3D_DLL_NAME "A3DLIB"
#define A3D_DLL_COPYRIGHT "Copyright (c) 2019 by Tech Soft 3D, Inc. All rights reserved."

#ifdef __APPLE__
# include "TargetConditionals.h"
# if TARGET_IPHONE_SIMULATOR
#  define A3DLIB_STATIC 1
# elif TARGET_OS_IPHONE
#  define A3DLIB_STATIC 1
# endif
#endif

// Force static according to platform
#ifdef __APPLE__
# if TARGET_IPHONE_SIMULATOR
#  define A3DLIB_STATIC 1
# elif TARGET_OS_IPHONE
#  define A3DLIB_STATIC 1
# endif
#endif

// Merge INITIALIZE_A3D_API_STATIC and INITIALIZE_A3D_API+A3DLIB_STATIC
#ifdef INITIALIZE_A3D_API_STATIC
#  ifndef INITIALIZE_A3D_API
#    define INITIALIZE_A3D_API
#  endif
#  ifndef A3DLIB_STATIC
#    define A3DLIB_STATIC
#  endif
#else
#  ifdef INITIALIZE_A3D_API
#    ifdef A3DLIB_STATIC
#      define INITIALIZE_A3D_API_STATIC
#    endif
#  endif
#endif

#ifndef FALSE
/*!
\brief Boolean standard false value.
*/
# define FALSE  0
#endif

#ifndef TRUE
/*!
\brief Boolean standard true value.
*/
# define TRUE   1
#endif

#ifndef A3D_FALSE
/*!
\brief Boolean standard false value.
*/
# define A3D_FALSE  0
#endif

#ifndef A3D_TRUE
/*!
\brief Boolean standard true value.
*/
# define A3D_TRUE   1
#endif


// Possible client configuration available, either:
// - Define nothing for Android build
// - Define nothing for Exchange only
// - Define HOOPS_PRODUCT_PUBLISH_STANDARD for Standard Publish only
// - Define HOOPS_PRODUCT_PUBLISH_ADVANCED for Advanced Publish only
// - Define HOOPS_PRODUCT_PUBLISH_STANDARD and HOOPS_PRODUCT_PUBLISHPLUSEXCHANGE for Standard Publish and Exchange
// - Define HOOPS_PRODUCT_PUBLISH_ADVANCED and HOOPS_PRODUCT_PUBLISHPLUSEXCHANGE for Advanced Publish and Exchange
#ifdef HOOPS_PRODUCT_PUBLISH
#  define HOOPS_PRODUCT_PUBLISH_ADVANCED
#endif
#ifdef TF_A3DLIBS
#  ifndef HOOPS_PRODUCT_PUBLISHPLUSEXCHANGE
#    define HOOPS_PRODUCT_PUBLISHPLUSEXCHANGE
#  endif
#  ifndef HOOPS_PRODUCT_PUBLISH_ADVANCED
#    define HOOPS_PRODUCT_PUBLISH_ADVANCED
#  endif
#  ifndef HOOPS_PRODUCT_PUBLISH_STANDARD
#    define HOOPS_PRODUCT_PUBLISH_STANDARD
#  endif
#endif
#ifdef HOOPS_PRODUCT_PUBLISHPLUSEXCHANGE
#  ifndef HOOPS_EXCHANGE
#    define HOOPS_EXCHANGE
#  endif
#else
#  if ! (defined HOOPS_PRODUCT_PUBLISH_ADVANCED) && !(defined HOOPS_PRODUCT_PUBLISH_STANDARD)
#    ifndef HOOPS_EXCHANGE
#      define HOOPS_EXCHANGE
#    endif
#  endif
#endif
#ifdef HOOPS_PRODUCT_PUBLISH_ADVANCED
#  ifndef HOOPS_PRODUCT_PUBLISH_STANDARD
#    define HOOPS_PRODUCT_PUBLISH_STANDARD
#  endif
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/// Function directives
///
/// Define these do that the clients use functions with the same calling
/// conventions as the implementation.
///
/// A3D_C_API_ATTRS - Before the return type in declarations.
/// A3D_C_API_CALL  - After the return type in declration.
/// A3D_C_API_PTR   - Between the '(' and '*' in function pointer.
///
/// Function declaration: A3D_C_API_ATTRS return_type A3D_C_API_CALL fname(params...);
/// Function pointer:     typedef void (A3D_C_API_PTR *PF_fname)(params...);

#define A3D_C_API_ATTRS extern "C"
#define A3D_C_API_CALL
#define A3D_C_API_PTR

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#ifndef A3D_C_EXTERN_PFN
# define A3D_C_EXTERN_PFN(RET,NAME,PARAMS) typedef RET (A3D_C_API_PTR *PF##NAME) PARAMS; extern PF##NAME NAME;
#endif

#ifndef A3D_C_DECL_FN
# define A3D_C_DECL_FN(RET,NAME,PARAMS) A3D_C_API_ATTRS RET A3D_C_API_CALL NAME PARAMS;
#endif

#undef A3D_API
#ifdef A3DLIB_STATIC
#  define A3D_API(RET,NAME,PARAMS) A3D_C_DECL_FN(RET,NAME,PARAMS)
#else
#  ifdef TF_A3DLIBS
#    define A3D_API(RET,NAME,PARAMS) A3D_C_DECL_FN(RET,NAME,PARAMS)
#  else
#    define A3D_API(RET,NAME,PARAMS) A3D_C_EXTERN_PFN(RET,NAME,PARAMS)
#  endif
#endif

#ifdef TF_A3DLIBS
#  define A3DAPI_INTERNAL
#endif

/*!
\brief Maximum length for character string.
*/
#define A3D_MAX_BUFFER 2048

#endif
