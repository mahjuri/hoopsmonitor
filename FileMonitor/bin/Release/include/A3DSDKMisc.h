/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for tools used with structure and geometry
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/
#ifdef A3DAPI_LOAD
#  undef __A3DPRCMISC_H__
#endif
#ifndef __A3DPRCMISC_H__
#define __A3DPRCMISC_H__
#ifndef A3DAPI_LOAD
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKGeometry.h>
#  include <A3DSDKInitializeFunctions.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD


/*!
\defgroup a3d_misc_module Miscellaneous Module
\ingroup a3d_entitiesdata_module
\brief Tools and common structures

This module describes tools and common structures.
*/

/*!
\defgroup a3d_misc_trsf Transformations Module
\ingroup a3d_misc_module
\brief Creates and accesses transformation entities
	that can be applied to PRC entities containing geometry

This module describes the transformation entities that can be applied
to PRC entities containing geometry.

The abstract base entity for this module (\ref A3DMiscTransformation)
appears in structure members as a pointer that references
either an \ref A3DMiscCartesianTransformation or an \ref A3DMiscGeneralTransformation entity.
There are no functions to separately create or access an \ref A3DMiscTransformation entity.

To determine the type of transformation entity referenced by an \ref A3DMiscTransformation pointer,
use the \ref A3DEntityGetType function.
*/

/*!
\defgroup a3d_generaltransfo3d General 3D Transformation (using 4x4 transformation matrices)
\ingroup a3d_misc_trsf
\brief Functions and structures for defining 4x4 transformation matrices that translate, scale, and rotate

Entity type is \ref kA3DTypeMiscGeneralTransformation.

An \ref A3DMiscGeneralTransformation entity describes a 3D general transformation
that translates, scales and rotates the associated geometric entity.

@{
*/

/*!
\brief Structure that specifies a 4x4 matrix for use in the \ref A3DMiscGeneralTransformation entity
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble m_adCoeff[16];		/*!< Matrix[4][4]. */
} A3DMiscGeneralTransformationData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMiscGeneralTransformationData structure
\ingroup a3d_generaltransfo3d
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE\n
\return \ref A3D_INVALID_DATA_STRUCT_NULL\n
\return \ref A3D_INVALID_ENTITY_NULL\n
\return \ref A3D_INVALID_ENTITY_TYPE\n
\return \ref A3D_SUCCESS\n
*/
A3D_API (A3DStatus, A3DMiscGeneralTransformationGet,(	const A3DMiscGeneralTransformation* pGeneralTransformation3d,
																		A3DMiscGeneralTransformationData* pData));

/*!
\brief Creates an \ref A3DMiscGeneralTransformation from an \ref A3DMiscGeneralTransformationData structure.
\ingroup a3d_generaltransfo3d
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n

*/

A3D_API (A3DStatus, A3DMiscGeneralTransformationCreate,(	const A3DMiscGeneralTransformationData* pData,
																			A3DMiscGeneralTransformation** ppGeneralTransformation3d));
/*!
@} <!-- end of module a3d_generaltransfo3d -->
*/

/*!
\defgroup a3d_cartesiantransfo3d Cartesian Transformation
\ingroup a3d_misc_trsf
\brief Functions and structures for defining combinations of transformations (without the use of a matrix)

Entity type is \ref kA3DTypeMiscCartesianTransformation.

This entity describes combinations of transformations such as
relocation to a new 3D Cartesian coordinate system, scaling, and rotation.
The \ref A3DMiscCartesianTransformationData structure provides parameters for the transformation.
The behaviour mask specified by the \ref A3DMiscCartesianTransformationData::m_ucBehaviour member
specifies the transformations that apply.
Those transformations must be consistent with the actual data provided in other fields as described here:

\li If the \c m_sOrigin member is null vector, translation is disabled.
\li If the \c m_sXVector member is (1,0,0) and \c m_sYVector member is (0,1,0), rotation is disabled.
\li If the \c m_sScale member is (1,1,1), scaling is disabled.
\li If the \c m_sXVector and \c m_sYVector members are not perpendicular or if they are not unit vectors,
	the data is invalid.

\par Sample code
\include CartesianTransformation3d.cpp

\version 2.3
\warning
Some particular formats do not support Cartesian transformations directly, but define general
ones instead. When possible, the associated readers try to make cartesian tranformations out
of them, setting this Behaviour flag. In all cases, except for Mirror, direct use of
transformation members is valid, where behavior is just informational.
*/

/*!
\brief Structure for defining an \ref A3DMiscCartesianTransformation entity
\ingroup a3d_cartesiantransfo3d
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DVector3dData	m_sOrigin;		/*!< 3D point - Center of axis system. */
	A3DVector3dData	m_sXVector;		/*!< 3D point - X axis of axis system. */
	A3DVector3dData	m_sYVector;		/*!< 3D point - Y axis of axis system. */
	A3DVector3dData	m_sScale;		/*!< 3D point - Scale of axis system. */
	A3DUns8			m_ucBehaviour;	/*!< Behavior of \ref A3DMiscCartesianTransformationData. \sa a3d_transformationbit */
} A3DMiscCartesianTransformationData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMiscCartesianTransformationData structure
\ingroup a3d_cartesiantransfo3d
\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE\n
\return \ref A3D_INVALID_DATA_STRUCT_NULL\n
\return \ref A3D_INVALID_ENTITY_NULL\n
\return \ref A3D_INVALID_ENTITY_TYPE\n
\return \ref A3D_SUCCESS\n
*/
A3D_API (A3DStatus, A3DMiscCartesianTransformationGet,(	const A3DMiscCartesianTransformation* pCartesianTransformation3d,
																			A3DMiscCartesianTransformationData* pData));

/*!
\brief Creates an \ref A3DMiscCartesianTransformation from an \ref A3DMiscCartesianTransformationData structure.
\ingroup a3d_cartesiantransfo3d

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_POINTSET_BADSIZE \n
\return \ref A3D_TRANSFORMATION3D_INCONSISTENT \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMiscCartesianTransformationCreate,(	const A3DMiscCartesianTransformationData* pData,
																				A3DMiscCartesianTransformation** ppCartesianTransformation3d));

/*!
\defgroup a3d_entity_reference Entity Reference
\ingroup a3d_misc_module
\brief Creates and accesses references to other PRC entities
\version 2.0

Entity type is \ref kA3DTypeMiscEntityReference.

An \ref A3DMiscEntityReference references an entity containing
structure data, representation items, topology data, markup data or scene display data.
All of these entities can be referenced from other entities.
For example, the \ref A3DAsmProductOccurrence entity (\ref a3d_productoccurrence) can include an \ref A3DMiscEntityReference entity
that references another product occurrence.
In another example, an \ref A3DAsmFilter entity (\ref a3d_filter) can include an entity filter
for which multiple \ref A3DMiscEntityReference entities
specify the entities to be inclusively or exclusively filtered.

The \ref A3DMiscEntityReference can supply modifiers that affect the referenced entity nominal definition.
All entity references can include a coordinate system, which is applied to the referenced item.
Topology entity references can also include global index items, such as color and texture.

To create references for non-topological entities listed in \ref a3d_eligibleforreference, perform these steps:
<ol>
	<li>Create an \ref A3DMiscEntityReferenceData structure that references the non-topological entity.</li>
	<li>Set the coordinate system transformation in the \ref A3DMiscEntityReferenceData structure as needed. </li>
	<li>Create an \ref A3DMiscEntityReference that references the \ref A3DMiscEntityReferenceData structure.</li>
</ol>

To create references for topological entities listed in \ref a3d_eligiblefortopologicalreference, perform these steps:
<ol>
	<li>Create an \ref A3DMiscReferenceOnTopologyData structure that references the topological entity.</li>
	<li>Set global index items in the \ref A3DMiscReferenceOnTopologyData structure as needed. </li>
	<li>Create an \ref A3DMiscReferenceOnTopology entity, providing the \ref A3DMiscReferenceOnTopologyData structure.</li>
	<li>Create an \ref A3DMiscEntityReferenceData structure that references the \ref A3DMiscReferenceOnTopology entity.</li>
	<li>Set the coordinate system transformation in the \ref A3DMiscEntityReferenceData structure as needed. </li>
	<li>Create an \ref A3DMiscEntityReference that references the \ref A3DMiscEntityReferenceData structure.</li>
</ol>

See the referenceable types in the modules \ref a3d_eligibleforreference
and \ref a3d_eligiblefortopologicalreference.

*/

/*!
\ingroup a3d_entity_reference
\brief Structure that identifies the referenced entity and that provides a new coordinate system
\version 2.0

If the type of the referenced entity is topology,
\ref m_pEntity is an \ref A3DMiscReferenceOnTopology object of type \ref kA3DTypeMiscReferenceOnTopology,
which contains the reference to the topological body.
In addition to containing a reference to the topology entity,
the \ref A3DMiscReferenceOnTopology entity defines the topology item type
and includes indexes to items in the global settings, such as color and layer.

If the type of the referenced entity is \e not topology,
the \ref m_pEntity member can directly reference an object that has one of the types
defined in  \ref a3d_eligibleforreference. (\ref m_pEntity can also be NULL.)<BR>
To get the type of the entity referenced by the \ref m_pEntity member, use the \ref A3DEntityGetType function.<BR>

*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DEntity* m_pEntity;						/*!< Referenced entity. It can be NULL. */
	A3DRiCoordinateSystem* m_pCoordinateSystem;	/*!< Optional location. */
} A3DMiscEntityReferenceData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMiscEntityReferenceData structure
\ingroup a3d_entity_reference
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMiscEntityReferenceGet,(	const A3DMiscEntityReference* pEntityReference,
																A3DMiscEntityReferenceData* pData));

/*!
\brief Creates an \ref A3DMiscEntityReference from an \ref A3DMiscEntityReferenceData structure
\ingroup a3d_entity_reference
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_ENTITYREFERENCE_INCONSISTENT_REFERENCE \n
\return \ref A3D_SUCCESS\n
*/
A3D_API (A3DStatus, A3DMiscEntityReferenceCreate,(	const A3DMiscEntityReferenceData* pData,
																	A3DMiscEntityReference** ppEntityReference));

/*!
\brief Function to set the entity reference from \ref A3DMiscEntityReferenceData structure in a previously created object
\ingroup a3d_entity_reference
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_ENTITYREFERENCE_INCONSISTENT_REFERENCE \n
\return \ref A3D_SUCCESS\n
*/
A3D_API (A3DStatus, A3DMiscEntityReferenceSet,(	A3DMiscEntityReference* pEntityReference,
																const A3DMiscEntityReferenceData* pData));


/*!
\defgroup a3d_reference_on_topo Reference on Topology/Tessellation
\ingroup a3d_entity_reference
\brief Creates and accesses entities that reference topology entities
	and that specify globally-defined attributes
\version 2.0

Entity type is \ref kA3DTypeMiscReferenceOnTopology.


Reference topology entity are retrieved by indexes on father elements.

For a \ref A3DTopoConnex, it needs:
\li the \ref A3DTopoConnex index on the current \ref A3DTopoBrepData.

For a \ref A3DTopoShell, it needs:
\li the \ref A3DTopoShell index on the current \ref A3DTopoBrepData. It is a global index to the "BrepData. In the case of several \ref A3DTopoConnex, it must take into account the number of shells present in the preceding \ref A3DTopoConnex.
\code
	A3DUns32 uiShellIndex = 0;
	for all pConnex on A3DTopoBrepData
	{
		for all pShell on pConnex
		{
			if pShell is wanted
				return uiShellIndex;
			uiShellIndex++;
		}
	}
\endcode

For a \ref A3DTopoFace, it needs:
\li the \ref A3DTopoFace index on the current \ref A3DTopoBrepData. It is a global index to the "BrepData. In the case of several \ref A3DTopoConnex, it must take into account the number of faces present in the preceding \ref A3DTopoConnex.
\code
	A3DUns32 uiFaceIndex = 0;
	for all pConnex on A3DTopoBrepData
	{
		for all pShell on pConnex
		{
			for all pFace on pShell
			{
				if pFace is wanted
					return uiFaceIndex;
				uiFaceIndex++;
			}
		}
	}
\endcode

For a \ref A3DTopoEdge or \ref A3DTopoCoEdge, it needs:
\li the \ref A3DTopoFace index on the current \ref A3DTopoBrepData (see index need for \ref A3DTopoFace reference)..
\li the \ref A3DTopoLoop index on the current \ref A3DTopoFace.
\li the \ref A3DTopoCoEdge index on the current \ref A3DTopoLoop.

For a \ref A3DTopoVertex, it needs:
\li the \ref A3DTopoFace index on the current \ref A3DTopoBrepData (see index need for \ref A3DTopoFace reference).
\li the \ref A3DTopoLoop index on the current \ref A3DTopoFace.
\li the \ref A3DTopoEdge index on the current \ref A3DTopoLoop.
\li the \ref A3DTopoVertex index on the current \ref A3DTopoEdge. Is set to 0 for the start vertex, and 1 for the end vertex.

*/

/*!
\ingroup a3d_reference_on_topo
\brief Structure that identifies the referenced topology entity and that specifies globally-defined attributes
\version 2.0

\ref m_puiAdditionalIndexes contains index on different topology elements (see \ref a3d_reference_on_topo for explanation)

For reference on \ref A3DTopoEdge or A3DTopoCoEdge, \ref m_uiSize must be set to 3 and
\li \ref m_puiAdditionalIndexes[0] is the \ref A3DTopoFace index in the \ref A3DTopoBrepData
\li \ref m_puiAdditionalIndexes[1] is the \ref A3DTopoLoop index in the \ref A3DTopoFace
\li \ref m_puiAdditionalIndexes[2] is the \ref A3DTopoEdge index in the \ref A3DTopoLoop


For reference on \ref A3DTopoVertex, \ref m_uiSize must be set to 4 and
\li \ref m_puiAdditionalIndexes[0] is the \ref A3DTopoFace index in the \ref A3DTopoBrepData
\li \ref m_puiAdditionalIndexes[1] is the \ref A3DTopoLoop index in the \ref A3DTopoFace
\li \ref m_puiAdditionalIndexes[2] is the \ref A3DTopoEdge index in the \ref A3DTopoLoop
\li \ref m_puiAdditionalIndexes[3] is the \ref A3DTopoVertex index in the \ref A3DTopoEdge.

*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DEEntityType m_eTopoItemType;		/*!< Restricted to the type listed in in \ref a3d_eligiblefortopologicalreference. */
	A3DTopoBrepData* m_pBrepData;		/*!< Body. Filled by A3DMiscReferenceOnTopologyGet, Can be NULL.
												 With A3DMiscReferenceOnTopologyCreate it can't be NULL and must be a A3DTopoBrepData.*/
	A3DUns32 m_uiSize;					/*!< The size of \ref m_puiAdditionalIndexes. */
	A3DUns32* m_puiAdditionalIndexes;	/*!< Indices to retrieve the topological entity. */
} A3DMiscReferenceOnTopologyData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMiscReferenceOnTopologyData structure
\ingroup a3d_reference_on_topo
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMiscReferenceOnTopologyGet,(const A3DMiscReferenceOnTopology* pReferenceOnTopoItem,
														A3DMiscReferenceOnTopologyData* pData));

/*!
\brief Structure that identifies the referenced tesselation entity and that specifies globally-defined attributes
\ingroup a3d_reference_on_topo
\version 9.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DEEntityType m_eTopoItemType;		/*!< kA3DTypeTessFace, kA3DTypeTessEdge, kA3DTypeTessVertex. */
	A3DRiPolyBrepModel* m_pPolyBrepModel;/*!< PolyBrepModel. Filled by A3DMiscReferenceOnTessGet. Can be NULL only if the \ref A3DMiscReferenceOnTessGet fails. */
	A3DUns32 m_uiSize;					/*!< The size of \ref m_puiAdditionalIndexes. */
	A3DUns32* m_puiAdditionalIndexes;	/*!< Indices to retrieve the tessellated entity. */
	A3DAsmProductOccurrence* m_pTargetProductOccurrence;	/*!< This member references the product occurrence that contains the m_pPolyBrepModel. */
} A3DMiscReferenceOnTessData;
#endif // A3DAPI_LOAD


/*!
\brief Populates the \ref A3DMiscReferenceOnTessData structure for PolyBrepModel
\ingroup a3d_reference_on_topo
\version 9.2

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DMiscReferenceOnTessGet,(const A3DMiscReferenceOnTess* pReferenceOnTess,
	A3DMiscReferenceOnTessData* pData));

/*!
\brief Creates an \ref A3DMiscReferenceOnTess from an \ref A3DMiscReferenceOnTessData structure
\ingroup a3d_reference_on_topo
\version 11.0

If \ref m_eTopoItemType is kA3DTypeTessEdge, \ref m_uiSize must be 3 and
\li \ref m_puiAdditionalIndexes[0] is the face index in the \ref m_pPolyBrepModel
\li \ref m_puiAdditionalIndexes[1] is the loop index in the face
\li \ref m_puiAdditionalIndexes[2] is the edge index in the loop


If \ref m_eTopoItemType is kA3DTypeTessVertex, \ref m_uiSize must be 4 and
\li \ref m_puiAdditionalIndexes[0] is the face index in the \ref m_pPolyBrepModel
\li \ref m_puiAdditionalIndexes[1] is the loop index in the face
\li \ref m_puiAdditionalIndexes[2] is the edge index in the loop
\li \ref m_puiAdditionalIndexes[3] is the vertex index in the edge


\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_ENTITYREFERENCE_INCONSISTENT_REFERENCE \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS\n
*/
A3D_API(A3DStatus, A3DMiscReferenceOnTessCreate, (const A3DMiscReferenceOnTessData* pData,
	A3DMiscReferenceOnTess** ppReferenceOnTess));

/*!
\brief Creates an \ref A3DMiscReferenceOnTopology from an \ref A3DMiscReferenceOnTopologyData structure
\ingroup a3d_reference_on_topo
\version 2.0

\return \ref A3D_INVALID_ENTITY_NULL if pData->m_pBrepData is NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE if pData->m_pBrepData is not a A3DTopoBrepData \n
\return Until version 2018 it was possible to pass A3DRiPolyBrepModel for pData->m_pBrepData which was not valid. \n
\return Use A3DMiscReferenceOnTessCreate for that case (see CreatePRCCubes sample).  \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_ENTITYREFERENCE_INCONSISTENT_REFERENCE \n
\return \ref A3D_SUCCESS\n
*/
A3D_API (A3DStatus, A3DMiscReferenceOnTopologyCreate,(const A3DMiscReferenceOnTopologyData* pData,
																		A3DMiscReferenceOnTopology** ppReferenceOnTopoItem));


/*!
\defgroup a3d_eligibleforreference Referenceable Non-Topological Entities
\ingroup a3d_entity_reference
\brief Listing of the non-topological entities that can be referenced from the \ref A3DMiscEntityReference

\li \ref kA3DTypeMiscEntityReference
\li \ref kA3DTypeMiscMarkupLinkedItem

\li \ref kA3DTypeRiBrepModel
\li \ref kA3DTypeRiCurve
\li \ref kA3DTypeRiDirection
\li \ref kA3DTypeRiPlane
\li \ref kA3DTypeRiPointSet
\li \ref kA3DTypeRiPolyBrepModel
\li \ref kA3DTypeRiPolyWire
\li \ref kA3DTypeRiSet
\li \ref kA3DTypeRiCoordinateSystem

\li \ref kA3DTypeAsmProductOccurrence
\li \ref kA3DTypeAsmPartDefinition
\li \ref kA3DTypeAsmFilter

\li \ref kA3DTypeMkpView
\li \ref kA3DTypeMkpMarkup
\li \ref kA3DTypeMkpLeader
\li \ref kA3DTypeMkpAnnotationItem
\li \ref kA3DTypeMkpAnnotationSet
\li \ref kA3DTypeMkpAnnotationReference

\li \ref kA3DTypeGraphStyle
\li \ref kA3DTypeGraphMaterial
\li \ref kA3DTypeGraphTextureApplication
\li \ref kA3DTypeGraphTextureDefinition
\li \ref kA3DTypeGraphTextureTransformation
\li \ref kA3DTypeGraphLinePattern
\li \ref kA3DTypeGraphDottingPattern
\li \ref kA3DTypeGraphHatchingPattern
\li \ref kA3DTypeGraphSolidPattern
\li \ref kA3DTypeGraphVPicturePattern
\li \ref kA3DTypeGraphCamera
\li \ref kA3DTypeGraphAmbientLight
\li \ref kA3DTypeGraphPointLight
\li \ref kA3DTypeGraphDirectionalLight
\li \ref kA3DTypeGraphSpotLight
\li \ref kA3DTypeGraphCamera
*/

/*!
\defgroup a3d_eligiblefortopologicalreference Referenceable Topological Entities
\ingroup a3d_entity_reference
\brief Listing of the topological entities that can be referenced from the \ref A3DMiscReferenceOnTopology entity

\li \ref kA3DTypeTopoMultipleVertex
\li \ref kA3DTypeTopoUniqueVertex
\li \ref kA3DTypeTopoWireEdge
\li \ref kA3DTypeTopoEdge
\li \ref kA3DTypeTopoLoop
\li \ref kA3DTypeTopoFace
\li \ref kA3DTypeTopoShell
\li \ref kA3DTypeTopoConnex

Currently, not all topological entities are supported.
	See \ref a3d_reference_on_topo for limitations.
*/

/*!
\defgroup a3d_reference_on_csys_sub_cmpnt Reference on coordinate system subcomponent
\ingroup a3d_entity_reference
\version 6.0
\brief Creates and accesses entities that reference coordinate system subcomponent

Entity type is \ref kA3DTypeMiscReferenceOnCsysItem.

Reference on coordinate system subcomponent are retrieved by the index.
m_uiIndex = 1 plane (x,y)
m_uiIndex = 2 plane (y,z)
m_uiIndex = 3 plane (x,z)
m_uiIndex = 4 axis x
m_uiIndex = 5 axis y
m_uiIndex = 6 axis z

*/

/*!
\struct A3DMiscReferenceOnCsysItemData
\brief Reference on coordinate system item
\ingroup a3d_reference_on_csys_sub_cmpnt
\version 6.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;				// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DRiCoordinateSystem* m_pCoordinateSystem;		/*!< Coordinate system */
	A3DUns16 m_uiIndex;								/*!< Index defining sub-component. */
} A3DMiscReferenceOnCsysItemData;
#endif // A3DAPI_LOAD


/*!
\brief Populates the \ref A3DMiscReferenceOnCsysItemData structure
\ingroup a3d_reference_on_csys_sub_cmpnt
\version 6.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMiscReferenceOnCsysItemGet,(const A3DMiscReferenceOnCsysItem* pReferenceOnCSYSItem,
		 A3DMiscReferenceOnCsysItemData* pData));

/*!
\brief Creates an \ref A3DMiscReferenceOnCsysItem from an \ref A3DMiscReferenceOnCsysItemData structure
\ingroup a3d_reference_on_csys_sub_cmpnt
\version 6.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_ENTITYREFERENCE_INCONSISTENT_REFERENCE \n
\return \ref A3D_SUCCESS\n
*/
A3D_API (A3DStatus, A3DMiscReferenceOnCsysItemCreate,(const A3DMiscReferenceOnCsysItemData* pData,
		 A3DMiscReferenceOnCsysItem** ppReferenceOnCSYSItem));


/*!
\defgroup a3d_UTF8_utilities UTF-8 Conversion Utilities
\ingroup a3d_misc_module
\brief Functions for converting between UTF-8 and UTF-16 strings on Windows.
@{
*/

/*!
\brief Converts UTF-8 encoded characters to UTF-16 strings on Windows.
\version 2.0

This function returns a UTF-16 string on Windows.

You must cast the string to a wide char pointer (\c wchar_t*) depending on the target computer's characteristics.

\warning On Linux, this API simply copies the input \c A3DUTF8Char* string into the output \c A3DUTF8Char* string.

\return \ref A3D_SUCCESS \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
*/
A3D_API (A3DStatus, A3DMiscUTF8ToUnicode,(const A3DUTF8Char* pcInputBuffer,
														A3DUTF8Char acOutputBuffer[]));

/*!
\brief Converts UTF-16 strings to an array UTF-8 encoded characters on Windows.
\version 2.0

This function returns an array of UTF-8 encoded characters.

The input buffer is cast and interpreted as a Unicode string (\c wchar_t*).

\warning On Linux, this API simply copies the input \c A3DUTF8Char* string into the output \c A3DUTF8Char* string.

\return \ref A3D_SUCCESS \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
*/
A3D_API (A3DStatus, A3DMiscUnicodeToUTF8,(const A3DUTF8Char* pcInputBuffer,
														A3DUTF8Char acOutputBuffer[]));

/*!
\brief Converts a UTF-8 string to a UTF-16 string on Windows.
\version 7.0

This function returns a UTF-16 string.

\warning On Linux, this API simply returns A3D_ERROR.

\return \ref A3D_SUCCESS \n
\return \ref A3D_ERROR \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
*/
A3D_API (A3DStatus, A3DMiscUTF8ToUTF16,(const A3DUTF8Char* pcInputBuffer,
														A3DUniChar acOutputBuffer[]));

/*!
\brief Converts a UTF-16 string to a UTF-8 string on Windows.
\version 7.0

This function returns a UTF-8 string.

\warning On Linux, this API simply returns A3D_ERROR.

\return \ref A3D_SUCCESS \n
\return \ref A3D_ERROR \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INITIALIZE_NOT_CALLED \n
*/
A3D_API (A3DStatus, A3DMiscUTF16ToUTF8,(const A3DUniChar* pcInputBuffer,
														A3DUTF8Char acOutputBuffer[]));
/*!
@} <!-- end of a3d_UTF8_utilities -->
*/

 /*!
 \defgroup a3d_error_debug_utilities Debug Utilities
\ingroup a3d_misc_module
\brief Functions to aid debugging.
@{
 */

/*!
\brief Returns the text description of an error code.

This text is stored in a global array used also by A3DMiscGetEntityTypeMsg.
This function returns a pointer to the global array and therefore it must not be desallocated.
Each time the function is called, the global array is updated.
For version 2.1, this method declaration was changed to
use the same convention applied to all other \COMPONENT_A3D_API declarations.
More specifically, the declaration was changed
to <code>A3D_API (A3DUTF8Char*, A3DMiscGetErrorMsg,(A3DInt32));</code>.
You will not be able to see this change because this reference
simplifies all declarations to the more readable format shown in the declaration frame.

\remark Because this function does not return A3DStatus the internal thread safety policy is to wait for the availability of the API.

\version 2.0
*/

A3D_API (const A3DUTF8Char*, A3DMiscGetErrorMsg,(A3DStatus));

/*!
\brief Returns the name of the entity type.

This text is stored in a global array used also by A3DMiscGetErrorMsg.
This function returns a pointer to the global array and therefore it must not be desallocated.
Each time the function is called, the global array is updated.
For version 2.1, this method declaration was changed to
use the same convention applied to all other \COMPONENT_A3D_API declarations.
For more information, see the description for \ref A3DMiscGetErrorMsg.

\remark Because this function does not return A3DStatus the internal thread safety policy is to wait for the availability of the API.

\version 2.0
*/
A3D_API (const A3DUTF8Char*, A3DMiscGetEntityTypeMsg,(A3DEEntityType eType));
/*!
@} <!-- end of a3d_error_msg_utilities module -->
*/

/*!
\struct A3DMiscPhysicMaterialData
\brief Physical properties. contains a union of the differents .
\ingroup a3d_misc_module
\version 9.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble m_dDensity;		/*!< Density in kg/m3. Equal to -1.0 if not set */
} A3DMiscPhysicMaterialData;
#endif // A3DAPI_LOAD

/*!
\struct A3DMiscMaterialFiberData
\brief Fiber physical properties.
\ingroup a3d_misc_module
\version 9.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble m_dYoungModulus_X;				/*! Longitudinal Young Modulus */
	A3DDouble m_dYoungModulus_Y;                /*! Transverse Young Modulus */
	A3DDouble m_dPoissonRatio_XY;               /*! Poisson Ratio in XY plane */
	A3DDouble m_dShearModulus_XY;               /*! Shear Modulus in XY plane */
	A3DDouble m_dShearModulus_YZ;               /*! Shear Modulus in XZ plane */
	A3DDouble m_dThermalExpansion_X;            /*! Longitudinal Thermal Expansion */
	A3DDouble m_dThermalExpansion_Y;            /*! Transverse Thermal Expansion */
	A3DDouble m_dTensileStressLimit_X;          /*! Transverse Tensile Stress Limit */
	A3DDouble m_dCompressiveStressLimit_X;      /*! Transverse Compressive Stress Limit */
	A3DDouble m_dTensileStressLimit_Y;          /*! Transverse Tensile Strain Limit */
	A3DDouble m_dCompressiveStressLimit_Y;      /*! Transverse Compressive Strain Limit */
	A3DDouble m_dShearStressLimit_XY;           /*! Shear Stress Limit in XY plane */
	A3DDouble m_dShearStressLimit_YZ;           /*! Shear Stress Limit in XZ plane */

} A3DMiscMaterialFiberData;
#endif // A3DAPI_LOAD

/*!
\struct A3DMiscMaterialHoneyCombData
\brief HoneyComb physical properties.
\ingroup a3d_misc_module
\version 9.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble m_dYoungModulus;					/*! Normal Young Modulus */
	A3DDouble m_dShearModulus_XZ;				/*! Shear Modulus in XY plane */
	A3DDouble m_dShearModulus_YZ;				/*! Shear Modulus in XZ plane */
	A3DDouble m_dShearStressLimit_XZ;			/*! Shear Stress Limit in XY plane*/
	A3DDouble m_dShearStressLimit_YZ;			/*! Shear Stress Limit in XZ plane*/
	A3DDouble m_dThermalExpansion;				/*! Normal Thermal Expansion */

} A3DMiscMaterialHoneyCombData;
#endif // A3DAPI_LOAD

/*!
\struct A3DMiscMaterialIsotropicData
\brief Isotropic physical properties.
\ingroup a3d_misc_module
\version 9.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble m_dYoungModulus;                  /*! Young Modulus */
	A3DDouble m_dPoissonRatio;                  /*! Poisson Ratio */
	A3DDouble m_dShearModulus;                  /*! Shear Modulus */
	A3DDouble m_dThermalExpansion;              /*! Thermal Expansion */

} A3DMiscMaterialIsotropicData;
#endif // A3DAPI_LOAD

/*!
\struct A3DMiscMaterialOrthotropic2DData
\brief Orthotropic2D physical properties.
\ingroup a3d_misc_module
\version 9.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble m_dYoungModulus_X;                /*! Longitudinal Young Modulus */
	A3DDouble m_dYoungModulus_Y;                /*! Transverse Young Modulus */
	A3DDouble m_dPoissonRatio_XY;               /*! Poisson Ratio in XY plane*/
	A3DDouble m_dShearModulus_XY;               /*! Shear Modulus in XY plane */
	A3DDouble m_dShearModulus_XZ;               /*! Shear Modulus in XZ plane */
	A3DDouble m_dShearModulus_YZ;               /*! Shear Modulus in YZ plane */
	A3DDouble m_dTensileStressLimit_X;          /*! Longitudinal Tensile Stress Limit */
	A3DDouble m_dCompressiveStressLimit_X;      /*! Longitudinal Compressive Stress Limit */
	A3DDouble m_dTensileStressLimit_Y;          /*! Transverse Tensile Stress Limit */
	A3DDouble m_dCompressiveStressLimit_Y;      /*! Transverse Compressive Stress Limit */
	A3DDouble m_dThermalExpansion_X;            /*! Longitudinal Thermal Expansion */
	A3DDouble m_dThermalExpansion_Y;            /*! Transverse Thermal Expansion */
	A3DDouble m_dTensileStrainLimit_X;          /*! Longitudinal Tensile Strain Limit */
	A3DDouble m_dCompressiveStrainLimit_X;      /*! Longitudinal Compressive Strain Limit */
	A3DDouble m_dTensileStrainLimit_Y;          /*! Transverse Tensile Strain Limit */
	A3DDouble m_dCompressiveStrainLimit_Y;      /*! Transverse Compressive Strain Limit */
	A3DDouble m_dShearStressLimit_XY;           /*! Shear Stress Limit in XY plane*/

} A3DMiscMaterialOrthotropic2DData;
#endif // A3DAPI_LOAD

/*!
\struct A3DMiscMaterialOrthotropic3DData
\brief Orthotropic3D physical properties.
\ingroup a3d_misc_module
\version 9.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble m_dYoungModulus_X;				/*! Longitudinal Young Modulus */
	A3DDouble m_dYoungModulus_Y;				/*! Transverse Young Modulus */
	A3DDouble m_dYoungModulus_Z;				/*! Normal Young Modulus */
	A3DDouble m_dPoissonRatio_XY;				/*! Poisson Ratio in XY plane */
	A3DDouble m_dPoissonRatio_XZ;				/*! Poisson Ratio in XZ plane */
	A3DDouble m_dPoissonRatio_YZ;				/*! Poisson Ratio in YZ plane */
	A3DDouble m_dShearModulus_XY;				/*! Shear Modulus in XY plane */
	A3DDouble m_dShearModulus_XZ;				/*! Shear Modulus in XZ plane */
	A3DDouble m_dShearModulus_YZ;				/*! Shear Modulus in YZ plane */
	A3DDouble m_dThermalExpansion_X;			/*! Longitudinal Thermal Expansion */
	A3DDouble m_dThermalExpansion_Y;			/*! Transverse Thermal Expansion */
	A3DDouble m_dThermalExpansion_Z;			/*! Normal Thermal Expansion */
	A3DDouble m_dTensileStressLimit_X;			/*! Transverse Tensile Stress Limit */
	A3DDouble m_dCompressiveStressLimit_X;		/*! Transverse Compressive Stress Limit */
	A3DDouble m_dTensileStressLimit_Y;			/*! Transverse Tensile Strain Limit */
	A3DDouble m_dCompressiveStressLimit_Y;		/*! Transverse Compressive Strain Limit */
	A3DDouble m_dShearStressLimit_XY;			/*! Longitudinal Shear Stress Limit */
	A3DDouble m_dShearStressLimit_XZ;			/*! Transverse Shear Stress Limit */
	A3DDouble m_dShearStressLimit_YZ;			/*! Normal Shear Stress Limit */

} A3DMiscMaterialOrthotropic3DData;
#endif // A3DAPI_LOAD

/*!
\struct A3DMiscMaterialAnisotropicData
\brief Anisotropic physical properties.
\ingroup a3d_misc_module
\version 9.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble m_dShearModulus_XX;				/*! Longitudinal Shear Modulus */
	A3DDouble m_dShearModulus_XY;				/*! Shear Modulus in XY plane */
	A3DDouble m_dShearModulus_XZ;				/*! Shear Modulus in XZ plane */
	A3DDouble m_dShearModulus_YY;				/*! Transverse Shear Modulus */
	A3DDouble m_dShearModulus_YZ;				/*! Shear Modulus in YZ plane */
	A3DDouble m_dShearModulus_ZZ;				/*! Normal Shear Modulus */
	A3DDouble m_dThermalExpansion_X;			/*! Longitudinal Thermal Expansion */
	A3DDouble m_dThermalExpansion_Y;			/*! Transverse Thermal Expansion */
	A3DDouble m_dThermalExpansion_Z;			/*! Normal Thermal Expansion */
	A3DDouble m_dTensileStressLimit;			/*! Tensile Stress Limit */
	A3DDouble m_dCompressiveStressLimit;		/*! Compressive Stress Limit */
	A3DDouble m_dShearStressLimit;				/*! Shear Stress Limit */

} A3DMiscMaterialAnisotropicData;
#endif // A3DAPI_LOAD

/*!
\struct A3DMiscMaterialPropertiesData
\brief Material properties, like density
\ingroup a3d_misc_module
\version 8.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16		m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble		m_dDensity;			/*!< Density in kg/m3. Equal to -1.0 if not set */

	A3DUTF8Char*	m_pcMaterialName;	/*!< Name of the material. \version 9.0 */
	A3DMaterialPhysicType m_ePhysicType; /*!< Physical Type: use for extract properties from the union of physical properties.	\version 9.0 */
	union
	{
		A3DMiscMaterialFiberData		    m_sFiber;			/*!< Fiber Material properties			\version 9.0 */
		A3DMiscMaterialHoneyCombData		m_sHoneyComb;       /*!< Honey Comb Material properties		\version 9.0 */
		A3DMiscMaterialIsotropicData		m_sIsotropic;       /*!< Isotropic Material properties		\version 9.0 */
		A3DMiscMaterialOrthotropic2DData	m_sOrthotropic2D;	/*!< Orthotropic Material properties	\version 9.0 */
		A3DMiscMaterialOrthotropic3DData	m_sOrthotropic3D;	/*!< Orthotropic 3D Material properties \version 9.0 */
		A3DMiscMaterialAnisotropicData		m_sAnisotropic;     /*!< Anisotropic Material properties	\version 9.0 */
	};

} A3DMiscMaterialPropertiesData;
#endif // A3DAPI_LOAD
/*!
\brief Get density in kg/m3 read from the file. Equal to -1.0 if not set
\ Can call it on product occurrences, part, and representation item
\ingroup a3d_misc_module
\version 8.2
*/
A3D_API(A3DStatus, A3DMiscGetMaterialProperties, (const A3DEntity* pEntity, A3DMiscMaterialPropertiesData *pMaterialPropertiesData));




/*!
\brief	Read the axis-aligned bounding box (AABB) of the given \ref A3DEntity
				directly from the CAD data.
\ingroup a3d_misc_module
\version 8.2

\param[in] pEntity The Entity to get the bounding box from. It can be any of
									\ref A3DAsmModelFile, \ref A3DAsmProductOccurrence,
									\ref A3DAsmPartDefinition or \ref A3DRiSet.
\param[out] pAABB A pointer to a valid \ref A3DBoundingBoxData which will be
									filled in with the found bounding box. If no AABB is found
									in the CAD data, \ref A3DBoundingBoxData::m_sMin and
									\ref A3DBoundingBoxData::m_sMax will be set to zero vectors
									({0.0, 0.0, 0.0}).

\return \ref A3D_SUCCESS
\return \ref A3D_BASE_BAD_ENTITY_TYPE
\return \ref A3D_INITIALIZE_BAD_VALUES

\warning	This function does *not* compute any bounding box. To do so please check
					\ref A3DMiscComputeBoundingBox.
*/
A3D_API(A3DStatus, A3DMiscGetBoundingBox, (const A3DEntity* pEntity, A3DBoundingBoxData *pAABB));

/*!
\brief	Use the tessellation to compute the axis-aligned bounding box (AABB) of
				the given entity.
\ingroup a3d_misc_module
\version 11.1

\param [in] pEntity The Entity to get the bounding box from. It can be any of
									\ref A3DAsmProductOccurrence, \ref A3DAsmPartDefinition
									or \ref A3DRiSet.
\param [in] pOptPlacement Reserved for future use. Must be set to 0.
\param [out] pAABB A pointer to a valid \ref A3DBoundingBoxData which will be
						filled in with the computed bounding box. If not possible an invalid
						bounding box is filled in (see below).

\return \ref A3D_INVALID_DATA_STRUCT_SIZE
\return \ref A3D_INVALID_DATA_STRUCT_NULL
\return \ref A3D_BASE_BAD_ENTITY_TYPE
\return \ref A3D_SUCCESS

The bounding box computation is done on visible entities only.
The unit used is the same as the given entity.

An invalid bounding box corresponds to those values:
- \ref A3DBoundingBoxData::m_sMin is `{1.0, 0.0, 0.0}`
- \ref A3DBoundingBoxData::m_sMax is `{-1.0, 0.0, 0.0}`

It can occur under following conditions:
- The computation is done on infinite elements.
- The CAD data do not contain any tessellation.
- \ref pEntity is of type \ref A3DRiPlane.

\warning	This function *computes* the bounding box using tessellation data. If
					you want to read the actual bounding box from CAD data,
					see \ref A3DMiscGetBoundingBox.
*/
A3D_API(A3DStatus, A3DMiscComputeBoundingBox, (
	const A3DEntity* pEntity,
	A3DDouble const * pOptPlacement,
	A3DBoundingBoxData *pAABB));


#endif	/*	__A3DPRCMISC_H__ */
