/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/
#ifndef A3DSDKLOADER_H
#define A3DSDKLOADER_H

#ifndef A3DSDKINCLUDEFILE
#  define A3DSDKINCLUDEFILE <A3DSDKIncludes.h>
#endif

/*!
\defgroup a3d_library_loader Tools Module
\ingroup a3d_Exchange
*/

/*!
\defgroup a3d_library_loader_internal Tools Module
\ingroup a3d_library_loader
*/

/*!
\defgroup a3d_library_loader_win32 Tools Module
\ingroup a3d_library_loader
*/

/*!
\defgroup a3d_library_loader_unix Tools Module
\ingroup a3d_library_loader
*/


/******************************************************************************/
/******************************************************************************/
/// PLATFORM REQUIREMENTS
/// On Windows machines, A3DModule_T expands to HMODULE.
/// Else it's void*
#if defined(_MSC_VER) && !defined(WINCE)
#  ifndef WIN32_LEAN_AND_MEAN
#    define WIN32_LEAN_AND_MEAN 1
#  endif
#  include <windows.h>
#  include <wchar.h>
typedef HMODULE A3DModule_T;
#else
#  include <dlfcn.h>
#  include <stdio.h>
#  include <string.h>
typedef void* A3DModule_T;
#endif

#ifndef _MAX_PATH
#  define _MAX_PATH 1000
#endif

/**
 * Handle to the currently loaded libray.
 * Null if the library is not loaded.
 * \ingroup a3d_library_loader
 */
static A3DModule_T st_A3DSDKLibraryHandle = NULL;

/**
 * Flags used to customize functions initializations
 */
enum A3DAPIFlags
{
	A3D_API_NOFLAG = 0,          /*!< No flag set */
	A3D_API_SERIALIZED = 1 << 0, /*!< The API functions are serialized: they cannot be called in parallel */
	A3D_API_TRACE = 1 << 1       /*!< All API called are reported using a callback function */
};
static A3DUns32    st_A3DAPIFlags = A3D_API_SERIALIZED;

/******************************************************************************/
/******************************************************************************/
/// INCLUDE API
/// This first include is a standard one : only declare function pointers
/// as externs  (or as function declarations if A3DLIB_STATIC is set)
#include A3DSDKINCLUDEFILE

#ifdef A3DLIB_STATIC

/******************************************************************************/
/**
 * \brief      StaticLib version of library loader. Does nothing. Only succeeds.
 * \ingroup    a3d_library_loader
 *
 * \param[in]  <unnamed>  Unused parameter.
 *
 * \return     A3D_TRUE.
 */
static A3DBool A3DSDKLoadLibraryA(const char*)
{
  return A3D_TRUE;
}


/******************************************************************************/
/**
 * \brief      StaticLib version of library initialization. Does nothing. Only succeeds.
 * \ingroup    a3d_library_loader
 *
 * \param[in]  <unnamed>  Unused parameter.
 *
 * \return     A3D_TRUE.
 */
static A3DBool A3DSDKInitializeLibrary(A3DModule_T)
{
  return A3D_TRUE;
}

/******************************************************************************/
/**
 * \brief      StaticLib version of library loader. Does nothing. Only succeeds.
 * \ingroup    a3d_library_loader
 *
 * \param[in]  <unnamed>  Unused parameter.
 *
 * \return     A3D_TRUE.
 */
static A3DBool A3DSDKLoadLibraryW(const wchar_t*)
{
  return A3D_TRUE;
}

/******************************************************************************/
/**
 * \brief      StaticLib version of library unload. Does nothing.
 * \ingroup    a3d_library_loader
 *
 * \param[in]  <unnamed>  Unused parameter.
 *
 */
static void A3DSDKUnloadLibrary()
{
  // EMPTY
}

#else

/******************************************************************************/
/******************************************************************************/
/// DEFINE API
/// Define functions pointers (previously declared) by re-including all
/// our API with a new definition for A3D_API.
#undef A3D_API
#define A3D_API(RET,NAME,PARAMS) PF##NAME NAME = NULL;

#define A3DAPI_LOAD
#include A3DSDKINCLUDEFILE
#undef A3DAPI_LOAD

#undef A3DSDKEXPORTS_HPP
#include <A3DSDK.h>


#include <stdio.h> // Because we output load errors.

/******************************************************************************/
/******************************************************************************/
// PRIVATE FUNCTIONS
// Don't call them directly, they're kinda shy.

/**
 * __A3DGetProcAddress is the only function exported by the API.
 * \ingroup a3d_library_loader_internal
 */
typedef void*(*PF_A3DGetProcAddress)(const char*, A3DUns32);
#ifdef A3DAPI_GETPROCADDRESS
PF_A3DGetProcAddress A3DGetProcAddress = NULL;
#endif

/******************************************************************************/
/******************************************************************************/
/**
 * \brief      Reset all API functions to NULL.
 * \ingroup a3d_library_loader_internal
 */
static void __A3DSDKDisposeLibraryFunctions()
{
  #ifdef A3DAPI_GETPROCADDRESS
  A3DGetProcAddress = NULL;
  #endif


  // Reinclude all the API with a new definition for A3D_API
  #undef A3D_API
  #define A3D_API(RET,NAME,PARAMS) NAME = NULL;
  #define A3DAPI_LOAD
  #include A3DSDKINCLUDEFILE
  #undef A3DAPI_LOAD
}

/******************************************************************************/
/******************************************************************************/
/**
 * \brief      Initialize all API function pointers.
 * \ingroup a3d_library_loader_internal
 *
 * \param[in]  __A3DGetProcAddress Library function used to load the other
 * functions.
 *
 * In case of failure, \ref __A3DSDKDisposeLibraryFunctions is automatically
 * called.
 *
 * \return     A3D_TRUE if the initialization succeeds. A3D_FALSE otherwise.
 */
static A3DBool __A3DSDKInitializeLibraryFunctions(PF_A3DGetProcAddress __A3DGetProcAddress)
{
  if (__A3DGetProcAddress == NULL)
  {
    return A3D_FALSE;
  }

  unsigned uiNullFunctions = 0;

  #undef A3D_API
#ifdef __cplusplus
  #define A3D_API(RET,NAME,PARAMS) \
    NAME = reinterpret_cast<PF##NAME>(__A3DGetProcAddress(#NAME, st_A3DAPIFlags)); \
    if(NAME == NULL) {fprintf(stderr, "ERROR: function %s is null\n", #NAME); ++uiNullFunctions;}
#else
#define A3D_API(RET,NAME,PARAMS) \
    NAME = (PF##NAME)(__A3DGetProcAddress(#NAME, st_A3DAPIFlags)); \
    if(NAME == NULL) {fprintf(stderr, "ERROR: function %s is null\n", #NAME); ++uiNullFunctions;}
#endif

  #define A3DAPI_LOAD
  #include A3DSDKINCLUDEFILE
  #undef A3DAPI_LOAD

    /// Reset A3D_API
  #undef A3DSDKEXPORTS_HPP
  #include <A3DSDK.h>

  if (uiNullFunctions > 0)
  {
    fprintf(stderr, "ERROR: %d null functions\n", uiNullFunctions);
    __A3DSDKDisposeLibraryFunctions();
    return A3D_FALSE;
  }

  return A3D_TRUE;
}


#ifdef _MSC_VER

/******************************************************************************/
/******************************************************************************/
/**
 * \brief      Unload the library resetting all API
 *  functions pointers to NULL and freeing the DLL.
 * \ingroup a3d_library_loader_win32
 */
static void A3DSDKUnloadLibrary()
{
  if (st_A3DSDKLibraryHandle)
  {
    __A3DSDKDisposeLibraryFunctions();

    FreeLibrary(st_A3DSDKLibraryHandle);
    st_A3DSDKLibraryHandle = NULL;
  }
}

/******************************************************************************/
/******************************************************************************/
/**
 * \brief      Initialize (or overrides) all API fonction pointers.
 * \ingroup a3d_library_loader_win32
 *
 * This function may be use when the DLL is already loaded by external process
 * and the API still needs to be initialized. The function won't check if the
 * API is already initialized and thus will override their value. It is then up
 * to the caller to ensure this function is called when the API is not
 * initialized.
 * If at least one fonction initialize leads to a NULL pointer, all API
 * functions are reset to NULL and the initialization is considered as to a
 * failure.
 *
 * \param[in]  inLibraryModule  The DLL handle
 *
 * \return     A3D_TRUE if all functions are initialized, A3D_FALSE otherwise.
 */
static A3DBool A3DSDKInitializeLibrary(A3DModule_T inLibraryModule)
{
  if(inLibraryModule == NULL)
  {
    return A3D_FALSE;
  }

#ifdef __cplusplus
  PF_A3DGetProcAddress __A3DGetProcAddress = reinterpret_cast<PF_A3DGetProcAddress>(GetProcAddress(inLibraryModule, "A3DGetProcAddress"));
#else
  PF_A3DGetProcAddress __A3DGetProcAddress = (PF_A3DGetProcAddress)(GetProcAddress(inLibraryModule, "A3DGetProcAddress"));
#endif
  #ifdef A3DAPI_GETPROCADDRESS
  A3DGetProcAddress = __A3DGetProcAddress;
  #endif

  if(__A3DGetProcAddress == NULL)
  {
    return A3D_FALSE;
  }

  if (__A3DSDKInitializeLibraryFunctions(__A3DGetProcAddress) == A3D_FALSE)
  {
    return A3D_FALSE;
  }

  return A3D_TRUE;
}

/******************************************************************************/
/******************************************************************************/
/**
 * \brief      Load A3DLIBS by getting the DLL object and initializing API
 * functions.
 * \ingroup a3d_library_loader_win32
 *
 * The caller provides a path to the directory directly containing the A3DLIBS
 * dynamic library. The path may omit the trailing slash '/'.
 * The load is successful only if the library is correctly loaded and the API
 * function are all initalized. Any other ending leads to a failure and the
 * function will return A3D_FALSE. In that case \ref st_A3DSDKLibraryHandle and all
 * function pointers are set to NULL.
 *
 * \param[in]  pSDKDirectory  The sdk directory.
 *
 * \return     A3D_TRUE if the load suceeds, A3D_FALSE otherwise.
 */
static A3DBool A3DSDKLoadLibraryW(const wchar_t* pSDKDirectory)
{
  wchar_t sSDKPath[_MAX_PATH];
  sSDKPath[0] = 0;
  if (pSDKDirectory != NULL)
  {
	size_t iLen = wcslen(pSDKDirectory);
    wcsncpy_s(sSDKPath, _MAX_PATH, pSDKDirectory, iLen);

    if (iLen > 1 && sSDKPath[iLen - 1] != L'\\')
    {
      wcsncat_s(sSDKPath, _MAX_PATH, L"\\", 2);
    }
  }

  wcsncat_s(sSDKPath, _MAX_PATH, L"A3DLIBS.dll", 11);
  st_A3DSDKLibraryHandle = LoadLibraryExW(sSDKPath, NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
  if (!st_A3DSDKLibraryHandle)
  {
    return A3D_FALSE;
  }

  if(A3DSDKInitializeLibrary(st_A3DSDKLibraryHandle) == A3D_FALSE)
  {
    A3DSDKUnloadLibrary();
    return A3D_FALSE;
  }

  return A3D_TRUE;
}

/******************************************************************************/
/******************************************************************************/
/**
 * \brief      Load A3DLIBS by getting the DLL object and initializing API
 * functions.
 * \ingroup a3d_library_loader_win32
 *
 * The caller provides a path to the directory directly containing the A3DLIBS
 * dynamic library. The path may omit the trailing slash '/'.
 * The load is successful only if the library is correctly loaded and the API
 * function are all initalized. Any other ending leads to a failure and the
 * function will return A3D_FALSE. In that case \ref st_A3DSDKLibraryHandle and all
 * function pointers are set to NULL.
 *
 * \param[in]  pSDKDirectory  The sdk directory.
 *
 * \return     A3D_TRUE if the load suceeds, A3D_FALSE otherwise.
 */
static A3DBool A3DSDKLoadLibraryA(const char* pSDKDirectory)
{
  wchar_t sSDKPath[_MAX_PATH];
  sSDKPath[0] = 0;
  if (pSDKDirectory != NULL)
  {
    int iLen = MultiByteToWideChar(
      CP_UTF8,                      // _In_      UINT   CodePage,
      0,                            // _In_      DWORD  dwFlags,
      pSDKDirectory,                // _In_      LPCSTR lpMultiByteStr,
      (int)strlen(pSDKDirectory),   // _In_      int    cbMultiByte,
      sSDKPath,                     // _Out_opt_ LPWSTR lpWideCharStr,
	  _MAX_PATH                     // _In_      int    cchWideChar
    );
    sSDKPath[iLen] = 0;
    if (iLen > 1 && sSDKPath[iLen - 1] != L'\\')
    {
      wcsncat_s(sSDKPath, _MAX_PATH, L"\\", 2);
    }
  }

  return A3DSDKLoadLibraryW(sSDKPath);
}

#else // _MSC_VER

static const char* ODALibNames[] = {
	"libTD_Alloc.so",
    "liblibcrypto.so",
    "libTD_Root.so",
    "libTD_Ge.so",
    "libTD_SpatialIndex.so",
    "libTD_Gi.so",
    "libTD_DbRoot.so",
	"libTD_DbCore.so",
    "SCENEOE.tx",
    "ACCAMERA.tx",
    "ISM.tx",
    "TD_DbEntities.tx",
    "AcMPolygonObj15.tx",
    "WipeOut.tx",
    "ATEXT.tx",
    "RText.tx",
    "libTD_Db.so",
    "TD_DbIO.tx",
    "RecomputeDimBlock.tx" ,
    "AcIdViewObj.tx",
    "TD_TfCore.tx",
    "liboless.so",
    "libTD_Zlib.so",
    "libTD_BrepBuilder.so",
    "libTD_Br.so",
    "libOdBrepModeler.so",
    "ThreadPool.tx",
    "TB_Common.tx",
    "TB_Base.tx",
    "TB_Geometry.tx",
    "TB_GeometryUtils.tx",
    "TB_Database.tx",
    "TB_HostObj.tx",
    "TB_Family.tx",
    "TB_MEP.tx",
    "TB_Architecture.tx",
    "TB_Analytical.tx",
    "TB_StairsRamp.tx",
    "TB_Main.tx",
    "libTD_BrepRenderer.so",
    "TB_ModelerGeometry.tx",
    "RasterProcessor.tx",
    "TD_BrepBuilderFiller.tx",
    "libTD_AcisBuilder.so",
    "ModelerGeometry.tx",
    "liblibBuffer.so",
    "libTD_Gs.so",
    "OdOleSsItemHandler.tx",
    "TB_ExLabelUtils.tx",
    "TB_LoaderBase.tx",
    "TB_FormatCommonClasses.tx",
    "TB_Format2019Classes.tx",
    "TB_Format2019Writers.tx",
    "TB_Format2018Classes.tx",
    "TB_Format2017Classes.tx",
    "TB_Format2016Classes.tx",
    "TB_Format2015Classes.tx",
    "TB_Format2014Classes.tx",
    "TB_Format2013Classes.tx",
	"TB_Format2012Classes.tx",
	"TB_Format2011Classes.tx",
	"TB_FormatCommonReaders.tx",
	"TB_Format2019Readers.tx",
	"TB_Format2018Readers.tx",
	"TB_Format2017Readers.tx",
	"TB_Format2016Readers.tx",
	"TB_Format2015Readers.tx",
	"TB_Format2014Readers.tx",
	"TB_Format2013Readers.tx",
	"TB_Format2012Readers.tx",
	"TB_Format2011Readers.tx",
	"TB_Loader.tx",
  "RxRasterServices.tx",
  "TD_FtFontEngine.tx",

  };


static void* stODALibs[ sizeof(ODALibNames) / sizeof(ODALibNames[0])] = {0};
static int stODAnum = 0;

/******************************************************************************/
/******************************************************************************/
/**
 * \brief      Unload the library resetting all API
 *  functions pointers to NULL and freeing the DLL.
 * \ingroup a3d_library_loader_unix
 */
static void A3DSDKUnloadLibrary()
{
  if (st_A3DSDKLibraryHandle)
  {
    __A3DSDKDisposeLibraryFunctions();
    while(stODAnum)
	    if(stODALibs[--stODAnum])
			{dlclose(stODALibs[stODAnum]); stODALibs[stODAnum]=NULL;}
    dlclose(st_A3DSDKLibraryHandle);
  }
}

/******************************************************************************/
/******************************************************************************/
/**
 * \brief      Initialize (or overrides) all API fonction pointers.
 * \ingroup a3d_library_loader_win32
 *
 * This function may be use when the DLL is already loaded by external process
 * and the API still needs to be initialized. The function won't check if the
 * API is already initialized and thus will override their value. It is then up
 * to the caller to ensure this function is called when the API is not
 * initialized.
 * If at least one fonction initialize leads to a NULL pointer, all API
 * functions are reset to NULL and the initialization is considered as to a
 * failure.
 *
 * \param[in]  inLibraryModule  The DLL handle
 *
 * \return     A3D_TRUE if all functions are initialized, A3D_FALSE otherwise.
 */
static A3DBool A3DSDKInitializeLibrary(A3DModule_T inLibraryModule)
{
  if(inLibraryModule == NULL)
  {
    return A3D_FALSE;
  }

#ifdef __cplusplus
  PF_A3DGetProcAddress __A3DGetProcAddress = reinterpret_cast<PF_A3DGetProcAddress>(dlsym(inLibraryModule, "A3DGetProcAddress"));
#else
  PF_A3DGetProcAddress __A3DGetProcAddress = (PF_A3DGetProcAddress)(dlsym(inLibraryModule, "A3DGetProcAddress"));
#endif
#ifdef A3DAPI_GETPROCADDRESS
  A3DGetProcAddress = __A3DGetProcAddress;
#endif

  if(__A3DGetProcAddress == NULL)
  {
    return A3D_FALSE;
  }

  if (__A3DSDKInitializeLibraryFunctions(__A3DGetProcAddress) == A3D_FALSE)
  {
    return A3D_FALSE;
  }

  return A3D_TRUE;
}

/******************************************************************************/
/******************************************************************************/
/**
 * \brief      Load A3DLIBS by getting the DLL object and initializing API
 * functions.
 * \ingroup a3d_library_loader_win32
 *
 * The caller provides a path to the directory directly containing the A3DLIBS
 * dynamic library. The path may omit the trailing slash '/'.
 * The load is successful only if the library is correctly loaded and the API
 * function are all initalized. Any other ending leads to a failure and the
 * function will return A3D_FALSE. In that case \ref st_A3DSDKLibraryHandle and all
 * function pointers are set to NULL.
 *
 * \param[in]  pSDKDirectory  The sdk directory.
 *
 * \return     A3D_TRUE if the load suceeds, A3D_FALSE otherwise.
 */
static A3DBool A3DSDKLoadLibraryA(const A3DUTF8Char *pSDKDirectory)
{
  A3DUTF8Char sSDKPath[_MAX_PATH];
  sSDKPath[0] = 0;
  if (pSDKDirectory != NULL)
  {
    strncpy(sSDKPath, pSDKDirectory, _MAX_PATH);
    size_t iLen = strlen(sSDKPath);
    if (iLen > 1 && sSDKPath[iLen - 1] != '/')
      strncat(sSDKPath, "/", _MAX_PATH - 1);
  }

#ifdef __APPLE__
  strncat(sSDKPath, "libA3DLIBS.dylib", _MAX_PATH);
#else // Apple

  {
    // try to load ODA libs if present in the folder of Exchange.
    // If not present, no error is splited
    // If present, then the lib is preloaded so Exchange find it immediatly
    stODAnum = sizeof(ODALibNames) / sizeof(ODALibNames[0]);
    memset(stODALibs, 0, stODAnum*sizeof(void*));
    // try to load the 1st one...
    A3DUTF8Char sODAlib[_MAX_PATH];
    strncpy(sODAlib, sSDKPath, _MAX_PATH);
    strncat(sODAlib, ODALibNames[0], _MAX_PATH);
    stODALibs[0] = dlopen(sODAlib, RTLD_LAZY |RTLD_GLOBAL);
	if (!stODALibs[0] && !sSDKPath[0])
	{
		// second try with ./
		strncpy(sSDKPath, "./", _MAX_PATH);
		strncat(sODAlib, ODALibNames[0], _MAX_PATH);
		stODALibs[0] = dlopen(sODAlib, RTLD_LAZY | RTLD_GLOBAL);
		if (!stODALibs[0])
			sSDKPath[0] = 0;
	}

    if(stODALibs[0]) {
      // ODA present, load...
      for (int i=1; i<stODAnum; i++) {
        strncpy(sODAlib, sSDKPath, _MAX_PATH);
        strncat(sODAlib, ODALibNames[i], _MAX_PATH);
        stODALibs[i] = dlopen(sODAlib, RTLD_LAZY |RTLD_GLOBAL);
        //if(!stODALibs[i])
         // printf("WARNING: %s while preloading ODA libs(%s)\n", dlerror(), sODAlib);
      }
    }
  }

  strncat(sSDKPath, "libA3DLIBS.so", _MAX_PATH - 1);
#endif  //
  st_A3DSDKLibraryHandle = dlopen(sSDKPath, RTLD_LAZY);

  if (!st_A3DSDKLibraryHandle)
  {
    printf("ERROR: %s\n", dlerror());
    return A3D_FALSE;
  }

  if(A3DSDKInitializeLibrary(st_A3DSDKLibraryHandle) == A3D_FALSE)
  {
    A3DSDKUnloadLibrary();
    return A3D_FALSE;
  }

  return A3D_TRUE;
}

#endif // _MSC_VER

#endif // A3DLIB_STATIC

// Defines A3DSDKLoadLibrary to A or W version according to platform and unicode support
#ifdef _MSC_VER
#  ifdef _UNICODE
#    define A3DSDKLoadLibrary A3DSDKLoadLibraryW
#  else
#    define A3DSDKLoadLibrary A3DSDKLoadLibraryA
#  endif
#else
#  define A3DSDKLoadLibrary A3DSDKLoadLibraryA
#endif

#ifndef NO_CONVERTER
#	include <A3DSDKInternalConvert.hxx>
#endif

#endif // ifndef A3DSDKLOADER_H

#ifdef TF_A3DLIBS
# error This is an interface only header
#endif
