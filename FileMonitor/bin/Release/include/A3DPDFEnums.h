/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      A3D SDK Enumerations and static values
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifndef __A3DSDKPDFENUMS_H__
#define __A3DSDKPDFENUMS_H__



/*!
\defgroup a3d_publish_widget_types_enum PDF Widget Entity Constants
\ingroup a3d_publish_widget_types

@{
*/
#define kA3DTypePDFWidgetRoot					0									/*!< This type does not correspond to any widget. */
#define kA3DTypePDFField						( kA3DTypePDFWidgetRoot +  1 )		/*!< Abstract Types for fields. */
#define kA3DTypePDFHighLevelWidget				( kA3DTypePDFWidgetRoot +  20 )		/*!< Abstract Types for High Level widgets. */

/*!
\brief Enumerations for PDF Widget Types
This enumeration defines a unique type for each PDF widget.
\version 10.0
*/
typedef enum
{
  kA3DTypePDFWidgetUnknown = -1,

  kA3DTypeFieldText = kA3DTypePDFField + 1,				/*!< Acrobat Field Text. \sa a3d_pdffield_text */
  kA3DTypeFieldButton = kA3DTypePDFField + 2,				/*!< Acrobat Field Button. \sa a3d_pdffield_button */
  kA3DTypeFieldCheckBox = kA3DTypePDFField + 3,			/*!< Acrobat Field Check Box. \sa a3d_pdffield_checkbox */
  kA3DTypeFieldRadioButton = kA3DTypePDFField + 4,		/*!< Acrobat Field Radio Button. \sa a3d_pdffield_radiobutton */
  kA3DTypeFieldListBox = kA3DTypePDFField + 5,			/*!< Acrobat Field List Box. \sa a3d_pdffield_listbox */
  kA3DTypeFieldDropDownList = kA3DTypePDFField + 6,		/*!< Acrobat Field Drop Down List (also named Combo Box). \sa a3d_pdffield_dropdownlist */
  kA3DTypeFieldDigitalSignature = kA3DTypePDFField + 7,	/*!< Acrobat Field Digital Signature. \sa a3d_pdffield_signature */

  kA3DType3DViewCarousel = kA3DTypePDFHighLevelWidget + 1,	/*!< High level widget for a View Carousel. \sa a3d_pdfwidget_carousel */
  kA3DTypeScrollTable = kA3DTypePDFHighLevelWidget + 2,		/*!< High level widget for a Scroll Table. \sa a3d_pdfwidget_scrolltable */
  kA3DTypeDataFilter = kA3DTypePDFHighLevelWidget + 3,		/*!< High level widget for a Data Filter. \sa a3d_pdf_datamodel_module */
  kA3DType3DAnnotListNodes = kA3DTypePDFHighLevelWidget + 4,	/*!< High level widget for a 3DAnnot list of nodes. \sa a3d_pdf_datamodel_module */
  kA3DType3DAnnotListViews = kA3DTypePDFHighLevelWidget + 5,	/*!< High level widget for a 3DAnnot list of views. \sa a3d_pdf_datamodel_module */
} A3DPDFEWidgetType;
/*!
@} <!-- end of a3d_publish_widget_types_enum -->
*/



/*!
\ingroup a3d_pdf_page_module
\brief Page size of the PDF document
\version 4.1
*/
typedef enum
{
  kA3DPDFPage11x17 = 0,		/*!< Standard 11*17 format. size in points: 792 * 1224. */
  kA3DPDFPageA3,			/*!< Standard A3 format. size in points: 842 * 1190. */
  kA3DPDFPageA4,			/*!< Standard A4 format. size in points: 595 * 842. */
  kA3DPDFPageA5,			/*!< Standard A5 format. size in points: 420 * 595. */
  kA3DPDFPageB4JIS,		/*!< Standard B4 JIS format. size in points: 728 * 1031. */
  kA3DPDFPageB5JIS,		/*!< Standard B5 JIS format. size in points: 515 * 728. */
  kA3DPDFPageExecutive,	/*!< Standard Executive format. size in points: 522 * 756. */
  kA3DPDFPageLegal,		/*!< Standard Legal format. size in points: 612 * 1008. */
  kA3DPDFPageLetter,		/*!< Standard Letter format. size in points: 612 * 792. */
  kA3DPDFPageTabloid,		/*!< Standard Tabloid format. size in points: 792 * 1224. */
  kA3DPDFPageB4ISO,		/*!< Standard B4 ISO format. size in points: 709 * 1001. */
  kA3DPDFPageB5ISO,		/*!< Standard B5 ISO format. size in points: 499 * 709. */
  kA3DPDFPageCustom		/*!< The page format must be defined using SetPageSize.  */
} A3DPDFEPageSize;

/*!
\ingroup a3d_pdf_page_module
\brief Page orientation of the PDF document
\version 4.1
*/
typedef enum
{
  kA3DPDFPagePortrait = 0,	/*!< Standard portrait orientation. */
  kA3DPDFPageLandscape		/*!< Standard landscape orientation. */
} A3DPDFEPageOrientation;


/*!
\defgroup a3d_pdf_documentpermissionbits Bit Field to indicate the document permissions.
\ingroup a3d_pdf_document_module
\version 7.2
@{
*/
#define kA3DPDFDocumentPermOpen		0x01 /*!< The user can open and decrypt the document. */
#define kA3DPDFDocumentPermSecure	0x02 /*!< The user can change the document's security settings. */
#define kA3DPDFDocumentPermPrint		0x04 /*!< The user can print the document.
Page Setup access is unaffected by this permission, since that affects Acrobat's preferences - not
the document's. In the Document Security dialog, this corresponds to the Printing entry. */
#define kA3DPDFDocumentPermEdit		0x08 /*!< The user can edit the document more than adding
or modifying text notes (see also kA3DPDFDocumentPermEditNotes). In the Document Security dialog,
this corresponds to the Changing the Document entry. */
#define kA3DPDFDocumentPermCopy		0x10 /*!< The user can copy information from the document
to the clipboard. In the document restrictions, this corresponds to the Content Copying or Extraction
entry. */
#define kA3DPDFDocumentPermEditNotes	0x20 /*!< The user can add, modify, and delete text notes
(see also kA3DPDFDocumentPermEdit). In the document restrictions, this corresponds to the Authoring
Comments and Form Fields entry. */
#define kA3DPDFDocumentPermSaveAs	0x40 /*!< The user can perform a Save As.... If both
kA3DPDFDocumentPermEdit and kA3DPDFDocumentPermEditNotes are disallowed, Save will be disabled but
Save As... will be enabled. The Save As... menu item is not necessarily disabled even if the user
is not permitted to perform	a Save As... */
#define kA3DPDFDocumentPermFillandSign	0x100 /*!< Overrides other kA3DPDFDocumentPerm bits. It allows
the user to fill in	or sign existing form or signature fields. */
#define kA3DPDFDocumentPermAccessible	0x200 /*!< Overrides kA3DPDFDocumentPermCopy to enable the
Accessibility API. If a document is saved in Rev2 format (Acrobat 4.0 compatible), only the
kA3DPDFDocumentPermCopy bit is checked to determine the Accessibility API state. */
#define kA3DPDFDocumentPermDocAssembly	0x400 /*!< Overrides various kA3DPDFDocumentPermEdit bits and
allows the following operations: page insert/delete/rotate and create bookmark and thumbnail. */
#define kA3DPDFDocumentPermHighPrint		0x800 /*!< This bit is a supplement to kA3DPDFDocumentPermPrint.
If it is clear (disabled) only low quality printing (Print As Image) is allowed. On UNIX platforms where
Print As Image doesn't exist, printing is disabled. */
#define kA3DPDFDocumentPermOwner		0x8000 /*!< The user is permitted to perform all operations,
regardless of the permissions specified by the document. Unless this permission is set, the document's
permissions will be reset to those in the document after a full save. */
#define kA3DPDFDocumentPermFormSubmit       0x10000 /*!< This should be set if the user can submit forms
outside of the browser. This bit is a supplement to kA3DPDFDocumentPermFillandSign. */
#define kA3DPDFDocumentPermFormSpawnTempl   0x20000 /*!< This should be set if the user can spawn
template pages. This bit will allow page template spawning even if kA3DPDFDocumentPermEdit
and kA3DPDFDocumentPermEditNotes are clear. */
#define kA3DPDFDocumentPermAll		0xFFFFFFFF /*!< All permissions. */
#define kA3DPDFDocumentPermSettable	(kA3DPDFDocumentPermPrint + kA3DPDFDocumentPermEdit + kA3DPDFDocumentPermCopy + kA3DPDFDocumentPermEditNotes)
/*!< The OR of all operations that can be set by the user in the
security restrictions (kA3DPDFDocumentPermPrint + kA3DPDFDocumentPermEdit + kA3DPDFDocumentPermCopy + kA3DPDFDocumentPermEditNotes). */
#define kA3DPDFDocumentPermUser		(kA3DPDFDocumentPermAll - kA3DPDFDocumentPermOpen - kA3DPDFDocumentPermSecure) /*!< All permissions. */
/*!< These values are to have all items unallowed, at least all items visible in Acrobat and Acrobat Reader dialog boxes. */
#define kA3DPDFDocumentPermNone		(kA3DPDFDocumentPermAll - kA3DPDFDocumentPermSettable - kA3DPDFDocumentPermAccessible - kA3DPDFDocumentPermFillandSign - kA3DPDFDocumentPermDocAssembly) /*!< All visible permissions unallowed. */
/*!
@} <!-- end of a3d_pdf_documentpermissionbits -->
*/


/*!
\defgroup a3d_pdf_saveflagsbits Bit Field to indicate the saving options.
\ingroup a3d_pdf_document_module
\version 9.2
@{
*/
#define kA3DPDFSaveFull				0x00 /*!< Basic level (equivalent to a call to \ref A3DPDFDocumentSave). */
#define kA3DPDFSaveOptimized		0x01 /*!< Adds misc optimizations for saving. */
#define kA3DPDFSaveOptimizeFonts	0x02 /*!< Adds optimization on fonts definition. This can involve substantial memory and CPU overhead. */
#define kA3DPDFSaveCopy				0x04 /*!< Save a copy of the document. If this flag is not used, the file is locked until A3DPDFDocumentClose is called. */
/*!
@} <!-- end of a3d_pdf_saveflagsbits -->
*/

/*!
\ingroup a3d_pdf_document_module
\brief Options to encrypt document content
\version 12.0
*/
typedef enum
{
	kA3DPDFEncryptAll,					/*!< Encrypt all document contents. */
	kA3DPDFEncryptAllExceptMetadata,	/*!< Encrypt all document contents except metadata. */
	kA3DPDFEncryptOnlyFileAttachments	/*!< Encrypt only file attachments. */
}  A3DPDFEEncryptContent;


/*!
\ingroup a3d_pdf_text_module
\brief Different predefined fonts.

The PDF format includes a set of 14 standard fonts that can be used without prior definition.
HOOPS Publish can set text using the base 14 fonts, even if the fonts do not exist on the local system.
These fonts are automatically supported by default by Adobe Reader. Nothing specific is required to display these fonts.
The font will be referenced (not embedded), which is an appropriate method for a Base14 font references. 
Any viewer should be able to display the resulting text, either in the referenced font or in a suitable substitute.

To have the set of characters supported by these fonts, please refer to the PDF norm annex D.2 for latin fonts and D.6 for symbol fonts.
Symbol and Zapfindgbats fonts have built-in encodings.
Other latin fonts (Times, Courier, Helvetica) are encoded with WinAnsiEncoding.
\version 4.1
*/
typedef enum
{
  kA3DPDFFontTimesRoman = 0, 		/*!< Adobe standard Times font, with a normal face. */
  kA3DPDFFontTimesItalic,			/*!< Adobe standard Times font, with a italic face. */
  kA3DPDFFontTimesBold,				/*!< Adobe standard Times font, with a bold face. */
  kA3DPDFFontTimesBoldItalic,		/*!< Adobe standard Times font, with a bold-italic face. */
  kA3DPDFFontHelvetica,				/*!< Adobe standard Helvetica font, with a normal face. */
  kA3DPDFFontHelveticaOblique,		/*!< Adobe standard Helvetica font, with a italic face. */
  kA3DPDFFontHelveticaBold,			/*!< Adobe standard Helvetica font, with a bold face. */
  kA3DPDFFontHelveticaBoldOblique,	/*!< Adobe standard Helvetica font, with a bold-italic face. */
  kA3DPDFFontCourier,				/*!< Adobe standard Courier font, with a normal face. */
  kA3DPDFFontCourierOblique,		/*!< Adobe standard Courier font, with a italic face. */
  kA3DPDFFontCourierBold,			/*!< Adobe standard Courier font, with a bold face. */
  kA3DPDFFontCourierBoldOblique,	/*!< Adobe standard Courier font, with a bold-italic face. */
  kA3DPDFFontSymbol,				/*!< Adobe standard Symbol font. */
  kA3DPDFFontZapfDingbats			/*!< Adobe standard ZapfDingbats font. */
} A3DPDFEFontName;


/*!
\ingroup a3d_pdf_text_module
\brief Languages supported for text strings.
\version 5.0
*/
typedef enum
{
  kA3DPDFASCII,				/*!< ASCII. */
  kA3DPDFEastEuropeanRoman,	/*!< East European Roman. */
  kA3DPDFCyrillic,			/*!< Cyrillic. */
  kA3DPDFGreek,				/*!< Greek. */
  kA3DPDFTurkish,				/*!< Turkish. */
  kA3DPDFHebrew,				/*!< Hebrew. */
  kA3DPDFArabic,				/*!< Arabic. */
  kA3DPDFBaltic,				/*!< Baltic. */
  kA3DPDFChineseTraditional,	/*!< Traditional Chinese. */
  kA3DPDFChineseSimplified,	/*!< Simplified Chinese. */
  kA3DPDFJapanese,			/*!< Japanese. */
  kA3DPDFKorean,				/*!< Korean. */
  kA3DPDFSymbol,				/*!< Symbols. */
  kA3DPDFLangAutoDetect			/*!< Automatic language detection depending on font used. */
}  A3DPDFELanguage;

/*!
\ingroup a3d_pdf_image_module
\brief Different image formats
\version 4.1
*/
typedef enum
{
  kA3DPDFImageFormatUnknown,			/*!< Undefined format. */
  kA3DPDFImageFormatBmp,				/*!< BMP format. */
  kA3DPDFImageFormatPng,				/*!< PNG format. */
  kA3DPDFImageFormatJpg,				/*!< JPEG format. */
  kA3DPDFImageFormatBitmapRgbByte,	/*!< Bitmap RGB format. */
  kA3DPDFImageFormatBitmapRgbaByte,	/*!< Bitmap RGBA format. */
  kA3DPDFImageFormatBitmapGreyByte,	/*!< Bitmap grey format. */
  kA3DPDFImageFormatBitmapGreyaByte,	/*!< Bitmap greya format. */
  kA3DPDFImageFormatEmf,				/*!< EMF format. */
  kA3DPDFImageFormatUrl,				/*!< URL format. */	 // m_acBinaryData contains the url (UTF-8)
  kA3DPDFImageFormatGif,				/*!< GIF format. */
  kA3DPDFImageFormatTif,				/*!< TIFF format. */
  kA3DPDFImageFormatPcx,				/*!< PCX format. */
  kA3DPDFImageFormatTga,				/*!< TGA format. */
  kA3DPDFImageFormatPpm,				/*!< PPM format. */
  kA3DPDFImageFormatIlbm,				/*!< ILBM format. */
  kA3DPDFImageFormatCel,				/*!< Cel format. */
  kA3DPDFImageFormatRgb,				/*!< RGB format. */
  kA3DPDFImageFormatPsd,				/*!< PSD format. */
  kA3DPDFImageFormatSoftimagepic		/*!< Softimagepic format. */
} A3DPDFEImageFormat;


/*!
\ingroup a3d_pdf_link_module
\brief Highlighting mode (the visual effect that shall be used when the mouse button is pressed or held down inside its active area)
\version 5.2
*/
typedef enum
{
  kA3DPDFLinkHighlightNone = 0,		/*!< No highlighting. */
  kA3DPDFLinkHighlightInvert,		/*!< Invert the contents of the link rectangle. */
  kA3DPDFLinkHighlightOutline,	/*!< Invert the link border. */
  kA3DPDFLinkHighlightPush		/*!< Display the link as if it were being pushed below the surface of the page. */
} A3DPDFELinkHighlightMode;

/*!
\ingroup a3d_pdf_3Dannot_module
\brief The circumstances under which the 3D Annot shall be activated
\version 4.1
*/
typedef enum
{
  kA3DPDFActivExplicitActivation = 0, 	/*!< The 3D Annot shall remain inactive until explicitly activated by a script or user action.*/
  kA3DPDFActivPageOpened,				/*!< The 3D Annot shall be activated as soon as the page containing the 3D Annot is opened. */
  kA3DPDFActivPageVisible				/*!< The 3D Annot shall be activated as soon as any part of the page containing the 3D Annot becomes visible. */
} A3DPDFEActivateWhen;

/*!
\ingroup a3d_pdf_3Dannot_module
\brief The circumstances under which the 3D Annot shall be deactivated
\version 4.1
*/
typedef enum
{
  kA3DPDFActivExplicitDesactivation = 0,	/*!< The 3D Annot shall remain active until explicitly deactivated by a script or user action. */
  kA3DPDFActivPageClosed,					/*!< The 3D Annot shall be deactivated as soon as the page is closed. */
  kA3DPDFActivPageNotVisible				/*!< The 3D Annot shall be deactivated as soon as the page containing the 3D Annot becomes invisible. */
} A3DPDFEDesactivateWhen;


/*!
\ingroup a3d_pdf_3Dannot_module
\brief Animation Style
\version 4.1
*/
typedef enum
{
  kA3DPDFAnimStyleNoAnimation = 0,	/*!< Animations shall not be driven directly by the conforming reader. This value shall be used by documents that are intended to drive animations through an alternate means, such as JavaScript. */
  kA3DPDFAnimStyleLoop,			/*!< This animation style results in a repetitive playthrough of the animation. */
  kA3DPDFAnimStyleBounce			/*!< This animation style results in a back-and-forth playing of the animation. */
} A3DPDFEAnimationStyle;

/*!
\ingroup a3d_pdf_3Dannot_module
\brief Lighting
\version 4.1
*/
typedef enum
{
  kA3DPDFLightArtworkCurrent = 0,	/*!< The light is defined from the 3D data (in the 3D Artwork). */
  kA3DPDFLightNone,				/*!< No light. */
  kA3DPDFLightWhite,				/*!< White light. */
  kA3DPDFLightDay,				/*!< Day light. */
  kA3DPDFLightBright,				/*!< Bright light. */
  kA3DPDFLightPrimaryColor,		/*!< Primary color light. */
  kA3DPDFLightNight,				/*!< Night light. */
  kA3DPDFLightBlue,				/*!< Blue light. */
  kA3DPDFLightRed,				/*!< Red light. */
  kA3DPDFLightCube,				/*!< Cube light. */
  kA3DPDFLightCADOptimized,		/*!< CAD Optimized light. */
  kA3DPDFLightHeadlamp			/*!< Headlamp light. */
} A3DPDFELighting;

/*!
\ingroup a3d_pdf_3Dannot_module
\brief Rendering Style
\version 4.1
*/
typedef enum
{
  kA3DPDFRenderingTransparentBoundingBox = 0,		/*!< Displays bounding boxes faces of each node, aligned with the axes of the local coordinate space for that node, with an added level of transparency. */
  kA3DPDFRenderingSolid,							/*!< Displays textured and lit geometric shapes. */
  kA3DPDFRenderingTransparent,					/*!< Displays textured and lit geometric shapes (triangles) with an added level of transparency. */
  kA3DPDFRenderingSolidWireframe,					/*!< Displays textured and lit geometric shapes (triangles) with an added level of transparency, with single color opaque edges on top of it. */
  kA3DPDFRenderingIllustration,					/*!< Displays silhouette edges with surfaces, removes obscured lines. */
  kA3DPDFRenderingSolidOutline,					/*!< Displays silhouette edges with lit and textured surfaces, removes obscured lines. */
  kA3DPDFRenderingShadedIllustration,				/*!< Displays silhouette edges with lit and textured surfaces and an additional emissive term to remove poorly lit areas of the artwork. */
  kA3DPDFRenderingBoundingBox,					/*!< Displays the bounding box edges of each node, aligned with the axes of the local coordinate space for that node. */
  kA3DPDFRenderingTransparentBoundingBoxOutline,	/*!< Displays bounding boxes edges and faces of each node, aligned with the axes of the local coordinate space for that node, with an added level of transparency. */
  kA3DPDFRenderingWireframe,						/*!< Displays only edges in a single color. */
  kA3DPDFRenderingShadedWireframe,				/*!< Displays only edges, though interpolates their color between their two vertices and applies lighting. */
  kA3DPDFRenderingTransparentWireframe,			/*!< Displays textured and lit geometric shapes (triangles) with an added level of transparency, with single color opaque edges on top of it. */
  kA3DPDFRenderingHiddenWireframe,				/*!< Displays edges in a single color, though removes back-facing and obscured edges. */
  kA3DPDFRenderingVertices,						/*!< Displays only vertices in a single color. */
  kA3DPDFRenderingShadedVertices					/*!< Displays only vertices, though uses their vertex color and applies lighting. */
} A3DPDFERenderingStyle;

/*!
\ingroup a3d_pdf_javascript_module
\brief Set of events that can trigger the execution of an action.

The kA3DPDFEventPageOpened and kA3DPDFEventPageClosed event on an annotation object have a similar function in the page object.
However, associating these triggers with annotations allows annotation objects to be self-contained.
The kA3DPDFEventPageVisible and kA3DPDFEventPageInvisible types allow a distinction between pages that are open
and pages that are visible. At any one time, while more than one page may be visible, depending on the page layout.
\version 9.0
*/
typedef enum
{
  kA3DPDFEventAutomatic = -1,		/*!< The action is performed when an event (automatically determined depending on the function called) is triggered. See documentation for function using this. */
  kA3DPDFEventPageOpened = 0,		/*!< An action that shall be performed when the page, or the page containing the annotation, is opened. */
  kA3DPDFEventPageClosed = 1,		/*!< An action that shall be performed when the page, or the page containing the annotation, is closed. */
  kA3DPDFEventPageVisible = 2,		/*!< An action that shall be performed when the page containing the annotation becomes visible. */
  kA3DPDFEventPageInvisible = 3,	/*!< An action that shall be performed when the page containing the annotation is no longer visible in the conforming reader’s user interface. */
  kA3DPDFEventFieldEnter = 4,		/*!< An action that shall be performed when the cursor enters the annotation’s active area. */
  kA3DPDFEventFieldExit = 5,		/*!< An action that shall be performed when the cursor exits the annotation’s active area. */
  kA3DPDFEventFieldMouseDown = 6,	/*!< An action that shall be performed when the mouse button is pressed inside the annotation’s active area. */
  kA3DPDFEventFieldMouseUp = 7,		/*!< An action that shall be performed when the mouse button is released inside the annotation’s active area. */
  kA3DPDFEventFieldFocusIn = 8,		/*!< An action that shall be performed when the annotation receives the input focus. */
  kA3DPDFEventFieldFocusOut = 9,	/*!< An action that shall be performed when the annotation loses the input focus. */
  kA3DPDFEventDocWillClose = 10,	/*!< An action that shall be performed before closing a document. */
  kA3DPDFEventDocWillSave = 11,		/*!< An action that shall be performed before saving a document. */
  kA3DPDFEventDocDidSave = 12,		/*!< An action that shall be performed after saving a document. */
  kA3DPDFEventDocWillPrint = 13,	/*!< An action that shall be performed before printing a document. */
  kA3DPDFEventDocDidPrint = 14,		/*!< An action that shall be performed after printing a document. */
  kA3DPDFEventFieldKeyStroke = 15	/*!< An action that shall be performed when the user modifies a character in a text field or combo box or modifies the selection in a scrollable list box. */
} A3DPDFEEventActionType;

/*!
\ingroup a3d_pdf_widget_module
\brief Field type
\version 6.0
*/
typedef enum
{
  kA3DPDFText = 0,			/*!< Field of type 'Text field'. */
  kA3DPDFButton = 1,		/*!< Field of type 'Button'. */
  kA3DPDFDropDown,		/*!< Field of type 'Drop-down list' (also named 'Combo box'). */
  kA3DPDFListBox,			/*!< Field of type 'List Box'. */
  kA3DPDFCheckBox,		/*!< Field of type 'Check Box'. */
  kA3DPDFRadioButton,		/*!< Field of type 'Radio Button'. */
  kA3DPDFDigitalSignature,/*!< Field of type 'Digital Signature'. */
  kA3DPDFBarCode			/*!< Field of type 'Barcode'. Not used by Publish. */
} A3DPDFEFieldType;


#define MAX_FIELD_NAME 1024


/*!
\ingroup ingroup a3d_publish_modelfile_module
\brief ModelFileNode type: type of node in a modelfile
\version 10.0
*/
typedef enum
{
  kA3DPDFNodeProductOccurrence = 0,		/*!< node for a Product occurence. */
  kA3DPDFNodeRepresentationItem = 1,	/*!< node for a mesh (node object that contains geometry). */
  kA3DPDFNodePMI = 2,					/*!< node for a PMI. Stored in the PDF model tree under a '3D PMI' node. */
  kA3DPDFNodeRiSet = 3,					/*!< node for a mesh set. Does not contain any geometry but has children who do.*/
  kA3DPDFNodeCamera = 4,				/*!< node for a camera.*/
  kA3DPDFNodeLight = 5,					/*!< node for a light.*/
} A3DPDFEModelFileNodeType;

/*!
\ingroup ingroup a3d_pdf_view_module
\brief Projection mode: type of projection
\version 6.0
*/
typedef enum
{
  kA3DPDFOrthographicMode = 0,		/*!< Orthographic projection. */
  kA3DPDFPerspectiveMode = 1,		/*!< Perspective projection. */
} A3DPDFEProjectionMode;


/*!
\defgroup a3d_pdf_animation_interpolbits Bit Field to indicate which data must be interpolated.
\ingroup a3d_pdf_animation_module
\version 6.1

@{
*/
#define kA3DPDFInterpolateTransformationMatrix		0x00000001	/*!< Transformation matrix is interpolated. */
#define kA3DPDFInterpolateAppearanceColor			0x00000002	/*!< Color is interpolated. */
#define kA3DPDFInterpolateAppearanceTransparency	0x00000004	/*!< Transparency is interpolated. */
#define kA3DPDFInterpolateCamera					0x00000008	/*!< Camera is interpolated. */
/*!
@} <!-- end of a3d_pdf_animation_interpolbits -->
*/


/*!
\ingroup a3d_pdf_widget_module
\brief Field flags
\version 7.2
*/
typedef enum
{
  kA3DPDFVisible,			/*!< The field is visible and printable. */
  kA3DPDFHidden,			/*!< The field is hidden and not printable. */
  kA3DPDFVisibleNoPrint,	/*!< The field is visible but not printable. */
  kA3DPDFHiddenPrintable,	/*!< The field is hidden but printable. */
} A3DPDFEFormField;

/*!
\ingroup a3d_pdf_widget_module
\brief Orientation of the text in a field
\version 7.2
*/
typedef enum
{
  kA3DPDFNormal,			/*!< The text has a standard orientation. */
  kA3DPDF90,				/*!< The button is turned 90&deg; from counter clockwise. */
  kA3DPDF180,				/*!< The button is turned 180&deg; from counter clockwise. */
  kA3DPDF270,				/*!< The button is turned 270&deg; from counter clockwise. */
} A3DPDFETextOrientation;

/*!
\ingroup a3d_pdf_widget_module
\brief Thickness of the border of a field
\version 7.2
*/
typedef enum
{
  kA3DPDFThin,			/*!< The border of the field is thin. */
  kA3DPDFMedium,			/*!< The border of the field is medium size. */
  kA3DPDFThick,			/*!< The border of the field is thin. */
} A3DPDFEThicknessBorder;

/*!
\ingroup a3d_pdf_widget_module
\brief Line style of the border of a field
\version 7.2
*/
typedef enum
{
	kA3DPDFSolid, /*!< Solid line. */
	kA3DPDFDashed, /*!< Dashed line. */
	kA3DPDFBeveled, /*!< Beveled line. */
	kA3DPDFInset, /*!< Inset line. */
	kA3DPDFUnderlined, /*!< Underlined. */
} A3DPDFELineStyleBorder;

/*!
\ingroup a3d_pdffield_button
\brief Position of the label of the button relative to its icon
\version 7.2
*/
typedef enum
{
	kA3DPDFLabelOnly, /*!< Only the label will be displayed. */
	kA3DPDFIconOnly, /*!< Only the icon will be displayed. */
	kA3DPDFIconTopLabelBottom, /*!< The label will be displayed below the icon. */
	kA3DPDFLabelTopIconBottom, /*!< The label will be displayed above the icon. */
	kA3DPDFIconLeftLabelRight, /*!< The label will be displayed to the right of the icon. */
	kA3DPDFLabelLeftIconRight, /*!< The label will be displayed to the left of the icon. */
	kA3DPDFLabelOverIcon, /*!< The label will be displayed on top of the icon. */
} A3DPDFELayoutTextIcon;

/*!
\ingroup a3d_pdffield_text
\brief Alignment of the text
\version 7.2
*/
typedef enum
{
  kA3DPDFLeft,		/*!< The text is left justified. */
  kA3DPDFCentered,	/*!< The button is centered. */
  kA3DPDFRight,		/*!< The button is right justified. */
} A3DPDFETextAlignment;


/*!
\ingroup a3d_pdf_2ddrawings_module
\brief Graphic types
\version 9.1
*/
typedef enum
{
  kA3DPDFGraphicLine = 0,		/*!< Graphic line. */
  kA3DPDFGraphicArc,			/*!< Graphic arc. */
  kA3DPDFGraphicBezierCurve	/*!< Graphic bezier curve. */
} A3DPDFEGraphicType;

/*!
\defgroup a3d_pdf_TableFor3DViewsBit Field to indicate the options for creating table for 3D views.
\ingroup a3d_pdf_datamodel_table_module
\brief These fields are used with function \ref A3DPDFDataTable3DViewsCreate to define its behavior.

Use \ref kA3DPDFTableFor3DViewsAll to automatically define a simple table for 3D views used only for 3D annotation widget.

Use \ref kA3DPDFTableFor3DViewsAll | \ref kA3DPDFTableFor3DViewsComputePosters to automatically define a table for 3D views used also for view carousel
(automatic here means that all views are stored and that all posters are automatically computed for view carousel usage).

Use \ref kA3DPDFTableFor3DViewsCustom to specify which views should be stored, and which posters images will be used.

Use \ref kA3DPDFTableFor3DViewsCustom | \ref kA3DPDFTableFor3DViewsComputePosters to specify which views should be stored and automatically compute posters for these.

\ingroup a3d_pdf_datamodel_module
\version 10.0

@{
*/
#define kA3DPDFTableFor3DViewsCustom			0x01 /*!< The detail of views are passed as an argument to the function. */
#define kA3DPDFTableFor3DViewsAll				0x02 /*!< All views are stored in the table. */
#define kA3DPDFTableFor3DViewsComputePosters	0x04 /*!< The posters for views are automatically generated during table creation. */
/*!
@} <!-- end of a3d_pdf_TableFor3DViewsBit -->
*/

/*!
\defgroup a3d_pdf_FilterViewsBit Field to indicate the options for filtering 3D views.
\brief These fields are used with function \ref A3DPDFGetStreams to define its behaviour.
\version 11.1

@{
*/
#define kA3DPDFNoFilter							0x00 /*!< All views, (the native views and the PDF views */
#define kA3DPDFFilterPDFDefaultView				0x01 /*!< The PDF default view is filtered using the kA3DGraphicsRemoved from A3DGraphicsData structure */
#define kA3DPDFFilterPDFViewsExceptDefault		0x02 /*!< The PDF views are filtered using the kA3DGraphicsRemoved from A3DGraphicsData structure, ony the default PDF is excluded */
#define kA3DPDFFilterNativeViews				0x04 /*!< Native views are filtered using the kA3DGraphicsRemoved from A3DGraphicsData structure */
#define kA3DPDFFilterSubAssemblyViews			0x08 /*!< The PDF default views not on the root assembly are filtered using the kA3DGraphicsRemoved from A3DGraphicsData structure */

/*!
@} <!-- end of a3d_pdf_FilterViewsBit -->
*/

/*!
\ingroup a3d_pdf_datamodel_module
\brief Types of behaviours for a widget when targetted from another widget
\version 9.2
*/
typedef enum
{
	kA3DPDFDataIsolate = 0,		/*!< Isolate the rows. */
	kA3DPDFDataHighlight,		/*!< Highlight the rows. */
	kA3DPDFDataSelect,			/*!< Select the rows. */
	kA3DPDFDataIsolateAndSelect	/*!< Isolate the rows, then selects the first item. */
} A3DPDFEWidgetTargetBehaviour;

/*!
\ingroup a3d_pdfwidget_carousel
\brief Direction of scrolling for a widget (usually View Carousel)
\version 10.0
*/
typedef enum
{
  kA3DPDFHorizontal = 0,	/*!< Horizontal direction. */
  kA3DPDFVertical			/*!< Vertical direction. */
} A3DPDFEDirection;

/*!
\ingroup a3d_pdfwidget_scrolltable
\brief Type of content of the column
\version 10.2
*/
typedef enum
{
  kA3DPDFTextContent = 0,		/*!< The contents of the column are texts. */
  kA3DPDFImageContent			/*!< The contents of the column are pictures. */
} A3DPDFEColumnType;

/*!
\ingroup a3d_publishhtml_module
\brief Ouput format flag when exporting for web
\version 12.0
*/
#define kA3DWebOutFormatHtml	0x01	/*!< Annotation 3D are exported into html files. */
#define kA3DWebOutFormatScs		0x02	/*!< Annotation 3D are exported into scs files. */
#define kA3DWebOutFormatPrc		0x04	/*!< Annotation 3D are exported into prc files (only needed to integrate Communicator in streaming mode). */
#define kA3DWebOutFormatALL		(kA3DWebOutFormatHtml|kA3DWebOutFormatScs|kA3DWebOutFormatPrc)	// Should not be documented : this is for internal purposes



/*!
\ingroup a3d_pdf_richmediaannot_module
\brief Type of skins for playback controls of rich media annot
\version 12.2
*/
typedef enum
{
	kA3DPDFSkinOverPlay = 0,				/*!< . */
	kA3DPDFSkinOverPlayMute,				/*!< . */
	kA3DPDFSkinOverPlaySeekMute,			/*!< . */
	kA3DPDFSkinOverPlaySeekStop,			/*!< . */
	kA3DPDFSkinOverPlayStopSeekMuteVol,		/*!< . */
	kA3DPDFSkinOverAllNoVolNoCaptionNoFull,	/*!< . */
	kA3DPDFSkinOverAllNoFullNoCaption,		/*!< . */
} A3DPDFEPlaybackControlsSkin;



#endif