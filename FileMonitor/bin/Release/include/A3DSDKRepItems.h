/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for representation item entities
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/
#ifdef A3DAPI_LOAD
#  undef __A3DPRCREPITEMS_H__
#endif
#ifndef __A3DPRCREPITEMS_H__
#define __A3DPRCREPITEMS_H__
#ifndef A3DAPI_LOAD
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKGeometry.h>
#  include <A3DSDKInitializeFunctions.h>
#  include <A3DSDKReadWrite.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD


/*!
\defgroup a3d_repitem Representation Items Module
\ingroup a3d_entitiesdata_module
\brief Creates and accesses entities that represent Representation items.

Representation item entities define individual objects that are present in a CAD file,
such as a bolt, wheel, or table leg.

Multiple representation items are contained within a part definition,
which scales and positions the representation items within a 3D space.
The result is a 3D part that can be assembled into higher level product occurrences,
such as an engine part, car, or table.

An \ref A3DRiRepresentationItem contains a local \ref A3DRiCoordinateSystem.
The \ref A3DRiCoordinateSystem coordinate system is expressed relative to the
parent product occurrence's coordinate system (\ref A3DAsmProductOccurrence).

All representation item entities inherit the \ref A3DRiRepresentationItem base class.
*/



/*!
\defgroup a3d_ricontent Representation Item Base
\ingroup a3d_repitem
*/
/*!
\brief Representation Item Structure
\ingroup a3d_ricontent
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16				m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DTessBase*			m_pTessBase;			/*!< Tessellation base. */
	A3DRiCoordinateSystem*	m_pCoordinateSystem;	/*!< Coordinate system. */
} A3DRiRepresentationItemData;
#endif // A3DAPI_LOAD
/*!
\brief Populates the \ref A3DRiRepresentationItemData structure
\ingroup a3d_ricontent
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiRepresentationItemGet,(const A3DRiRepresentationItem* pRi,
																A3DRiRepresentationItemData* pData));

/*!
\brief Sets a previously created \ref A3DRiRepresentationItem with an \ref A3DRiRepresentationItemData structure
\ingroup a3d_ricontent
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SET_INCONSISTENT_EMPTY \n
\return \ref A3D_SUCCESS \n
\note The \ref A3DRiRepresentationItem entity is an abstract class. You cannot directly create this entity.
Instead, use the appropriate derived function (such as \ref A3DRiSetCreate or \ref A3DRiPointSetCreate)
to create a specific representation item entity.
Then use this function to specify values in that entity base class.
For example, this function specifies a value for the \ref A3DRiRepresentationItemData::m_pCoordinateSystem member.
*/
A3D_API (A3DStatus, A3DRiRepresentationItemSet,(A3DRiRepresentationItem* pRi,
																const A3DRiRepresentationItemData* pData));

/*!
\brief Create a new representation item by making a deep copy.
\ingroup a3d_ricontent

\brief

\image html ri_deep_copy.jpg

Representation item data are copied, except coordinate system and surface are shared for optimization as for the rest of the model.
There are some limitations:
<ul>
<li> ID is reset to 0, but topological item ids are kept.
<li> Stored links are lost when a representation item is copied independently of the rest of the model. It&apos;s up to the user to create new ones.
	An entity reference sets to describe the link between a datum plane and a body plane face will be removed.
<li> Drawing block is not implemented.
<li> Feature data on TfSet are not copied.

</ul>

\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_ERROR \n
\return \ref A3D_SUCCESS \n
\version 11.2
*/

A3D_API(A3DStatus, A3DRiRepresentationItemDeepCopy, (const A3DRiRepresentationItem* pRi, A3DRiRepresentationItem** ppNewRi));

/*!
\brief Calculates the tessellation of a previously created \ref A3DRiRepresentationItem with an \ref A3DRWParamsTessellationData parameters
\ingroup a3d_ricontent
\version 4.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SET_INCONSISTENT_EMPTY \n
\return \ref A3D_SUCCESS \n
\note If the \ref A3DRiRepresentationItem entity has already been tessellated, the previous tessellation will be
replaced by the new one. This function works for brep models and curves only.
*/

A3D_API (A3DStatus, A3DRiRepresentationItemComputeTessellation,(A3DRiRepresentationItem* pRi,
																				const A3DRWParamsTessellationData* pTessellationParametersData));



/*!
\brief Function to release the tessellation stored on the representation item
\version 5.0
\ingroup a3d_ricontent
\par

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS\n
*/
A3D_API (A3DStatus, A3DRiReleaseTessellation,(A3DRiRepresentationItem* pRepresentationItem));

/*!
\brief Function to edit the coordinate system of a representation item
\version 9.2
\ingroup a3d_ricontent
\par

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS\n
*/
A3D_API(A3DStatus, A3DRiRepresentationItemEditCoordinateSystem, (A3DRiCoordinateSystem*	pNewCoordinateSystem,
																	A3DRiRepresentationItem* pRepresentationItem));





/*!
\defgroup a3d_riset Set Representation Item
\ingroup a3d_repitem
This entity is a logical grouping of other representation items.
No matrix for placement is attached to components,
and an \ref A3DRiSet entity can contain multiple child \ref A3DRiSet sets.

\warning A representation item cannot belong to more than one set.

\par Sample code
\include Set.cpp
*/
/*!
\brief Set structure
\ingroup a3d_riset
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16					m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32					m_uiRepItemsSize;	/*!< The size of \ref m_ppRepItems. */
	A3DRiRepresentationItem**	m_ppRepItems;		/*!< Array of representation items in the set. */
} A3DRiSetData;
#endif // A3DAPI_LOAD
/*!
\brief Populates the \ref A3DRiSetData structure
\ingroup a3d_riset
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiSetGet,(const A3DRiSet* pSet,
											A3DRiSetData* pData));

/*!
\brief Creates an \ref A3DRiSet from \ref A3DRiSetData structure
\ingroup a3d_riset
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SET_INCONSISTENT_EMPTY \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiSetCreate,(const A3DRiSetData* pData,
												A3DRiSet** ppSet));

/*!
\brief Replaces the old data of an \ref A3DRiSet with the new \ref A3DRiSetData structure
\ingroup a3d_riset
\version 10.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SET_INCONSISTENT_EMPTY \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DRiSetEdit, (const A3DRiSetData* pData, A3DRiSet* pSet));



/*!
\defgroup a3d_ripointset PointSet Representation Item
\ingroup a3d_repitem
An \ref A3DRiPointSet is a set of 3D points.

When the \ref A3DRiPointSetGet function populates an \ref A3DRiPointSetData structure,
it allocates arrays of coordinates.
When you no longer need the \ref A3DRiPointSetData structure, invoke the \ref A3DRiPointSetGet function
with the first argument set to NULL to free its allocated memory.
\par Sample code
\include PointSet.cpp
*/
/*!
\brief PointSet structure
\ingroup a3d_ripointset
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16			m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32			m_uiSize;		/*!< Number of points in the set. */
	A3DVector3dData*	m_pPts;			/*!< Array of 3D points. */
} A3DRiPointSetData;
#endif // A3DAPI_LOAD
/*!
\brief Populates the \ref A3DRiPointSetData structure
\ingroup a3d_ripointset
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiPointSetGet,(const A3DRiPointSet* pPointSet,
													A3DRiPointSetData* pData));

/*!
\brief Creates an \ref A3DRiPointSet from an \ref A3DRiPointSetData structure
\ingroup a3d_ripointset
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiPointSetCreate,(const A3DRiPointSetData* pData,
														A3DRiPointSet** ppPointSet));

/*!
\brief Replaces the old data of an \ref A3DRiPointSet with the new \ref A3DRiPointSetData structure
\ingroup a3d_ripointset
\version 10.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DRiPointSetEdit, (const A3DRiPointSetData* pData,
	A3DRiPointSet* pPointSet));




/*!
\defgroup a3d_ridirection Direction Representation Item
\ingroup a3d_repitem
\version 2.0

An \ref A3DRiDirection is defined by a point (optional) and a vector (mandatory).
*/
/*!
\brief Direction structure
\ingroup a3d_ridirection
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16		m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool			m_bHasOrigin;		/*!< If this boolean is false, m_sOrigin has no meaning. */
	A3DVector3dData	m_sOrigin;			/*!< Origin (optional). */
	A3DVector3dData	m_sDirection;		/*!< Direction. */
} A3DRiDirectionData;
#endif // A3DAPI_LOAD
/*!
\brief Populates the \ref A3DRiDirectionData structure
\ingroup a3d_ridirection
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiDirectionGet,(const A3DRiDirection* pDirection,
													A3DRiDirectionData* pData));

/*!
\brief Creates an \ref A3DRiDirection from an \ref A3DRiDirectionData structure
\ingroup a3d_ridirection
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
\todo Not yet implemented
*/
A3D_API (A3DStatus, A3DRiDirectionCreate,(const A3DRiDirectionData* pData,
														A3DRiDirection** ppDirection));

/*!
\brief Modifies an \ref A3DRiDirection from an \ref A3DRiDirectionData structure
\ingroup a3d_ridirection
\version 10.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_SUCCESS \n
\todo Not yet implemented
*/
A3D_API(A3DStatus, A3DRiDirectionEdit, (const A3DRiDirectionData* pData,
	A3DRiDirection* pDirection));


/*!
\defgroup a3d_ricoordinatesystem Coordinate System Representation Item
\ingroup a3d_repitem
\brief An axis system

An \ref A3DRiCoordinateSystem is an \ref A3DRiRepresentationItem that specifies a coordinate system.
The \ref A3DRiCoordinateSystemData structure contains an \ref A3DMiscCartesianTransformation
axis system. This axis system owns the geometric data: there is no particular
geometry attached, and the user is responsible for representation.

\note An \ref A3DRiCoordinateSystem is an \ref A3DRiRepresentationItem and not an \ref A3DMiscCartesianTransformation.
Being a representation item, a coordinate system can own a local coordinate system (on the RI level), which can also
own a local coordinate system, and so on...
*/
/*!
\brief Coordinate System structure
\ingroup a3d_ricoordinatesystem
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16					m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DMiscTransformation*		m_pTransformation;		/*!< Cartesian transformation 3D. Cannot be NULL. */
} A3DRiCoordinateSystemData;
#endif // A3DAPI_LOAD
/*!
\brief Populates the \ref A3DRiCoordinateSystemData structure
\ingroup a3d_ricoordinatesystem
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE\n
\return \ref A3D_INVALID_DATA_STRUCT_NULL\n
\return \ref A3D_INVALID_ENTITY_NULL\n
\return \ref A3D_INVALID_ENTITY_TYPE\n
\return \ref A3D_SUCCESS\n
*/
A3D_API (A3DStatus, A3DRiCoordinateSystemGet,(const A3DRiCoordinateSystem* pCoordinateSystem,
															A3DRiCoordinateSystemData* pData));

/*!
\brief Creates an \ref A3DRiCoordinateSystem from an \ref A3DRiCoordinateSystemData structure
\ingroup a3d_ricoordinatesystem
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SET_INCONSISTENT_EMPTY \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiCoordinateSystemCreate,(const A3DRiCoordinateSystemData* pData,
																A3DRiCoordinateSystem** ppCoordinateSystem));

/*!
\brief Modifies an \ref A3DRiCoordinateSystem from an \ref A3DRiCoordinateSystemData structure
\ingroup a3d_ricoordinatesystem
\version 10.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SET_INCONSISTENT_EMPTY \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DRiCoordinateSystemEdit, (const A3DRiCoordinateSystemData* pData,
	A3DRiCoordinateSystem* pCoordinateSystem));



/*!
\defgroup a3d_ricurve Curve Representation Item
\ingroup a3d_repitem
An \ref A3DRiCurve is a representation item that contains a geometrical curve.
\sa \ref a3d_crv.
*/
/*!
\brief RiCurve structure
\ingroup a3d_ricurve
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16				m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DTopoSingleWireBody*	m_pBody;				/*!< Body. */
} A3DRiCurveData;
#endif // A3DAPI_LOAD
/*!
\brief Populates the \ref A3DRiCurveData structure
\ingroup a3d_ricurve
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiCurveGet,(const A3DRiCurve* pRICrv,
												A3DRiCurveData* pData));

/*!
\brief Creates an \ref A3DRiCurve from an \ref A3DRiCurveData structure
\ingroup a3d_ricurve
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiCurveCreate,(const A3DRiCurveData* pData,
													A3DRiCurve** ppRICrv));

/*!
\brief Modifies an \ref A3DRiCurve from an \ref A3DRiCurveData structure
\ingroup a3d_ricurve
\version 10.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DRiCurveEdit, (const A3DRiCurveData* pData,
	A3DRiCurve* pRICrv));


/*!
\brief Get the geometrical element used to build the \ref A3DRiCurve
\ingroup a3d_ricurve
\version 6.0

\param[in] pRiCurve The \ref A3DRiCurve to query
\param[out] pLinkedItem The entity used to build \ref pRiCurve

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/

A3D_API (A3DStatus, A3DRiCurveSupportGet,(const A3DRiCurve* pRiCurve, A3DMiscMarkupLinkedItem** ppLinkedItem));


/*!
\defgroup a3d_riplane Plane Representation Item
\ingroup a3d_repitem
An \ref A3DRiPlane is a representation item that contains a \ref A3DTopoBrepData made of one single planar face.
*/
/*!
\brief RiPlane structure
\ingroup a3d_riplane
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16			m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DTopoBrepData*	m_pBrepData;			/*!< Topological entry for plane description. Made of one face, no trimming loops. */
} A3DRiPlaneData;
#endif // A3DAPI_LOAD
/*!
\brief Populates the \ref A3DRiPlaneData structure
\ingroup a3d_riplane
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiPlaneGet,(const A3DRiPlane* pRiPlane,
												A3DRiPlaneData* pData));

/*!
\brief Creates an \ref A3DSurfPlane from an \ref A3DRiPlaneData structure
\ingroup a3d_riplane
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiPlaneCreate,(const A3DRiPlaneData* pData,
													A3DRiPlane** ppRIPlane));


/*!
\brief Modifies an \ref A3DSurfPlane from an \ref A3DRiPlaneData structure
\ingroup a3d_riplane
\version 10.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DRiPlaneEdit, (const A3DRiPlaneData* pData,
	A3DRiPlane* pRIPlane));


/*!
\brief Get the geometrical element used to build the \ref A3DRiPlane
\ingroup a3d_riplane
\version 6.0

\param[in] pRiPlane The \ref A3DRiPlane to query
\param[out] ppLinkedItem The entity used to build \ref pRiPlane

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/

A3D_API (A3DStatus, A3DRiPlaneSupportGet,(const A3DRiPlane* pRiPlane, A3DMiscMarkupLinkedItem** ppLinkedItem));



/*!
\defgroup a3d_ribrepmodel BrepModel Representation Item
\ingroup a3d_repitem
An \ref A3DRiBrepModel is a representation item that contains a topological Boundary Representation.
*/
/*!
\brief BrepModel structure
\ingroup a3d_ribrepmodel
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bSolid;				/*!< Solid or Shell flag. */
	A3DTopoBrepData* m_pBrepData;	/*!< Topological description of the B-rep. */
} A3DRiBrepModelData;
#endif // A3DAPI_LOAD
/*!
\brief Populates the \ref A3DRiBrepModelData structure
\ingroup a3d_ribrepmodel
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiBrepModelGet,(const A3DRiBrepModel* pRIBrepModel,
													A3DRiBrepModelData* pData));

/*!
\brief Creates an \ref A3DRiBrepModel from an \ref A3DRiBrepModelData structure
\ingroup a3d_ribrepmodel
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiBrepModelCreate,(const A3DRiBrepModelData* pData,
														A3DRiBrepModel** ppRIBrepModel));

/*!
\brief Modifies an \ref A3DRiBrepModel from an \ref A3DRiBrepModelData structure
\ingroup a3d_ribrepmodel
\version 10.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DRiBrepModelEdit, (const A3DRiBrepModelData* pData,
	A3DRiBrepModel* pRIBrepModel));


/*!
\defgroup a3d_ripolybrepmodel PolyBrepModel Representation Item
\ingroup a3d_repitem
An \ref A3DRiPolyBrepModel is a representation item that contains only a tessellation of a solid or a surface.
\sa \ref A3DTessBaseGet
*/
/*!
\brief PolyBrepModel structure
\ingroup a3d_ripolybrepmodel
\version 2.0

This structure handles only a flag indicating whether the tessellation is closed (solid).
Tessellation must be recovered with base class function \ref A3DRiRepresentationItemGet.
\sa \ref a3d_tessellation_module
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16	m_usStructSize; 		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool		m_bIsClosed;			/*!< A value of true indicates the tessellation closed; otherwise, it is open. */
} A3DRiPolyBrepModelData;
#endif // A3DAPI_LOAD
/*!
\brief Populates the \ref A3DRiPolyBrepModelData structure
\ingroup a3d_ripolybrepmodel
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE\n
\return \ref A3D_INVALID_DATA_STRUCT_NULL\n
\return \ref A3D_INVALID_ENTITY_NULL\n
\return \ref A3D_INVALID_ENTITY_TYPE\n
\return \ref A3D_SUCCESS\n
*/
A3D_API (A3DStatus, A3DRiPolyBrepModelGet,(	const A3DRiPolyBrepModel* pRIPolyBrepModel,
															A3DRiPolyBrepModelData* pData));

/*!
\brief Creates an \ref A3DRiPolyBrepModel from an \ref A3DRiPolyBrepModelData structure
\ingroup a3d_ripolybrepmodel
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiPolyBrepModelCreate,(	const A3DRiPolyBrepModelData* pData,
																A3DRiPolyBrepModel** ppRIPolyBrepModel));

/*!
\brief Modifies an \ref A3DRiPolyBrepModel from an \ref A3DRiPolyBrepModelData structure
\ingroup a3d_ripolybrepmodel
\version 10.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DRiPolyBrepModelEdit, (const A3DRiPolyBrepModelData* pData,
	A3DRiPolyBrepModel* pRIPolyBrepModel));


/*!
\defgroup a3d_ripolywire PolyWire Representation Item
\ingroup a3d_repitem
An \ref A3DRiPolyWire is a representation item that contains a polywire.
See the description for the \ref A3DRiPolyBrepModelData structure
to understand how to access the data (tessellation definition).

\sa \ref A3DRiPolyWire
*/
/*!
\brief PolyWire structure
\ingroup a3d_ripolywire
\version 2.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize; // Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
} A3DRiPolyWireData;
#endif // A3DAPI_LOAD
/*!
\brief Populates the \ref A3DRiPolyWireData structure
\ingroup a3d_ripolywire
\version 2.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE\n
\return \ref A3D_INVALID_DATA_STRUCT_NULL\n
\return \ref A3D_INVALID_ENTITY_NULL\n
\return \ref A3D_INVALID_ENTITY_TYPE\n
\return \ref A3D_SUCCESS\n
*/
A3D_API (A3DStatus, A3DRiPolyWireGet,(	const A3DRiPolyWire* pRIPolyWire,
													A3DRiPolyWireData* pData));

/*!
\brief Creates an \ref A3DRiPolyWire from an \ref A3DRiPolyWireData structure
\ingroup a3d_ripolywire
\version 2.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SET_INCONSISTENT_EMPTY \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DRiPolyWireCreate,(	const A3DRiPolyWireData* pData,
														A3DRiPolyWire** ppRIPolyWire));


#endif	/*	__A3DPRCREPITEMS_H__ */
