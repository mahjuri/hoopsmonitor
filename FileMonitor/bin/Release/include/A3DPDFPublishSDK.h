/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for the Publish module
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifdef A3DAPI_LOAD
#  undef __A3DPDFPUBLISHSDK_H__
#endif
#ifndef __A3DPDFPUBLISHSDK_H__
#define __A3DPDFPUBLISHSDK_H__
#ifndef A3DAPI_LOAD
#  include <A3DCommonReadWrite.h>
#  include <A3DPDFEnums.h>
#  include <A3DPDFInitializeFunctions.h>
#  include <A3DSDK.h>
#  include <A3DSDKBase.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD



// (addtogroup because this module is defined in top doxygen file)
/*!
\addtogroup a3d_pdflib_functions \PDF_LIBRARY Session Functions
\brief Starts, configures, and terminates a session with the \PDF_LIBRARY.

The functions in this section start and terminate a session with the \PDF_LIBRARY.
WARNING: the initialization and termination functions must be called only once during the life of the application.
Attempting to initialize the \PDF_LIBRARY more than once in the application may cause errors or unpredictable behavior,
and is not supported. You are free to create multiple documents and/or multiple files within the run, but the initialization
and termination of the \PDF_LIBRARY is limited to one iteration of each.
*/

/*!
\ingroup a3d_pdflib_functions
\deprecated This method was deprecated in HOOPS Publish 5.1. Use \ref A3DPDFInitializePDFLibAndResourceDirectory instead.

This function initializes the Adobe PDF Library which is internally used by HOOPS Publish.
It should be called before calling any function working on the PDF document, and only once during the life of the application.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFInitializePDFLib, ());

/*!
\ingroup a3d_pdflib_functions
\brief Function to initialize the PDF Library and set the resource directory

This function initializes the Adobe PDF Library which is internally used by HOOPS Publish, and indicates where
the resource directory is located. The resource directory is used by the Adobe PDF Library to manipulate text data.
Customers should consider as a general rule that a complete resource directory (subfolders included) should always
exist in the distribution and should always be specified to the \ref A3DPDFInitializePDFLibAndResourceDirectory function.
Not providing a valid resource directory can lead to unexpected behavior and crashes. As an example, the Table
functionality fails if the resource directory doesn't exist or is not specified.
A minimal exception to this rule might be when the PDF document generated doesn't contain any text strings, whatever the source
(added with HOOPS Publish functions or even already defined in a provided PDF template).

The resource directory is provided with the HOOPS Publish package in the \\bin\\resource folder. Please also consider all subfolders as
part of the resources.

This function should be called before calling any function working on the PDF document, and only once during the life of the application.

Important: Before calling \ref A3DPDFInitializePDFLibAndResourceDirectory, you must call SETCURRENTDIRECTORY(IN_INSTALL_DIR) to set the current directory to the location of your binary files. After calling \ref A3DPDFInitializePDFLibAndResourceDirectory, you may change the current directory to another path.
Starting with 11.0 version, the current directory is automatically changed during the initialization and set back to the previous one. There's now no need for the customer to set current directory.

\param [in] pcResourceDirectory The path of an existing directory with full resource definition as provided in the HOOPS Publish package.
The path must be relative to the location of your binary files, or must be absolute.
Empty string ("") is valid but should be used very cautiously as not using resources can lead to unexpected behavior and crashes (see function description).
\return \ref A3D_SUCCESS \n
\version 5.0
*/
A3D_API (A3DStatus, A3DPDFInitializePDFLibAndResourceDirectory, (const A3DUTF8Char* pcResourceDirectory));

/*!
\ingroup a3d_pdflib_functions
\brief Function to terminate the PDF Library.
Terminates the PDF library. This function should only be called at the end of your application. Do not call this function and then continue to use either HOOPS Exchange or HOOPS Publish. Doing so will result in undefined behavior. It is important to call this function only once during the life of the application.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFTerminatePDFLib, ());

/*!
\ingroup a3d_pdflib_functions
\brief Function to check if the PDF Library has been initialized with \ref A3DPDFInitializePDFLibAndResourceDirectory.

\param [out] pbIsInitialized true if the PDF Library has been initialized, false if not.

\return \ref A3D_SUCCESS \n
\version 11.0
*/
A3D_API(A3DStatus, A3DPDFCheckPDFLibInitialization, (A3DBool* pbIsInitialized));



// (addtogroup because this module is defined in top doxygen file)
/*!
\addtogroup a3d_publishpdf_module PDF Module
*/



/*!
\defgroup a3d_pdfentitytypes_module PDF Entity Types
\ingroup a3d_publishpdf_module
\brief Description of different types of PDF entities.

This section describes the entity types specific to PDF entities such as document, page, fields and other widgets or static entities.
*/


/*!
\defgroup a3d_pdfentitytypes_def PDF Entity Type Definitions
\ingroup a3d_pdfentitytypes_module
\brief Definitions of type of PDF entities.

This section describes the entity types used for the PDF entities.
The types are grouped into the following modules in this reference (the \COMPONENT_A3D_API_REF).
\li Document / Page
\li Layout
\li 3D
\li Interactivity
*/
#ifndef A3DAPI_LOAD

/*!
\defgroup a3d_publish_doc_types Document Type Declarations
\ingroup a3d_pdfentitytypes_def
@{
*/
typedef void A3DPDFDocument;		/*!< PDF document entity. \sa a3d_pdf_document_module */
/*!
@} <!-- end of a3d_publish_doc_types -->
*/

/*!
\defgroup a3d_publish_layoutstatic_types Layout Static Type Declarations
\ingroup a3d_pdfentitytypes_def
@{
*/
typedef void A3DPDFPage;			/*!< PDF page entity. \sa a3d_pdf_page_module */
typedef void A3DPDFText;			/*!< PDF text entity. \sa a3d_pdf_text_module */
typedef void A3DPDFImage;			/*!< PDF image entity. \sa a3d_pdf_image_module */
typedef void A3DPDFLink;			/*!< PDF link entity. \sa a3d_pdf_link_module */
typedef void A3DPDFTable;			/*!< PDF table entity. \sa a3d_pdf_table_module */
typedef void A3DPDFLayer;			/*!< PDF layer entity. \sa a3d_pdf_layer_module */
typedef void A3DPDFRichMediaAnnot;	/*!< PDF rich media entity. \sa a3d_pdf_richmediaannot_module */

/*!
@} <!-- end of a3d_publish_layoutstatic_types -->
*/


/*!
\defgroup a3d_publish_widget_types Layout Widget Type Declarations
\ingroup a3d_pdfentitytypes_def
@{
*/
typedef void A3DPDFWidget;				/*!< PDF Widget. Abstract type. Widgets are low level Fields, or High Level widgets (scroll table, view carousel). \sa a3d_publish_widget_type_functions */
typedef void A3DPDFField;				/*!< Field. Abstract type. Fields are low level objects positioned on a PDF page. \sa a3d_publish_widget_type_functions */
typedef void A3DPDFButton;				/*!< Button field. \sa a3d_pdffield_button */
typedef void A3DPDFTextField;			/*!< Text field. \sa a3d_pdffield_text */
typedef void A3DPDFDigitalSignature;	/*!< Digital signature field. \sa a3d_pdffield_signature */
// next are for ADVANCED version only
typedef void A3DPDFCheckBox;			/*!< Check box field. \sa a3d_pdffield_checkbox */
typedef void A3DPDFRadioButton;			/*!< Radio button field. \sa a3d_pdffield_radiobutton */
typedef void A3DPDFListBox;				/*!< Listbox field. \sa a3d_pdffield_listbox */
typedef void A3DPDFDropDownList;		/*!< Drop down list field. \sa a3d_pdffield_dropdownlist */
typedef void A3DPDF3DViewCarousel;		/*!< 3D view carousel widget. High level object widget. \sa a3d_pdfwidget_carousel */
typedef void A3DPDFScrollTable;			/*!< Scroll table widget. High level object widget. \sa a3d_pdfwidget_scrolltable */
typedef void A3DPDFDataFilter;			/*!< Data Filter widget. High level object widget. \sa a3d_pdf_datamodel_module */
typedef void A3DPDF3DNodeScene;			/*!< Widget to interact with 3D nodes (via the 3D model tree or via the 3D annotation scene). \sa a3d_pdf_datamodel_module */
typedef void A3DPDF3DViewList;			/*!< Widget to interact with 3D views (via the 3D model tree or via the context menu in the 3D annotation scene). \sa a3d_pdf_datamodel_module */
/*!
@} <!-- end of a3d_publish_widget_types -->
*/

// see A3DPDFEnums.h
/*!
\addtogroup a3d_publish_widget_types_enum PDF Widget Entity Constants
\ingroup a3d_publish_widget_types
*/

/*!
\defgroup a3d_publish_3D_types 3D Type Declarations
\ingroup a3d_pdfentitytypes_def
@{
*/
typedef void A3DPDF3DAnnot;			/*!< 3D Annotation entity. \sa a3d_pdf_3Dannot_module */
typedef void A3DPDF3DStream;		/*!< 3D Stream entity. \sa a3d_pdf_3Dstream_module */
typedef void A3DPDF3DArtwork;		/*!< 3D Artwork entity. \sa a3d_pdf_3Dartwork_module */
typedef void A3DPDFView;			/*!< 3D View entity. \sa a3d_pdf_view_module */
typedef void A3DPDFAnimKeyFrame;	/*!< Key frame entity. \sa a3d_pdf_animation_module */
typedef void A3DPDFAnimMotion;		/*!< Animation motion entity. \sa a3d_pdf_animation_module */
typedef void A3DPDFAnimation;		/*!< Animation entity. \sa a3d_pdf_animation_module */
typedef void A3DPDFTargetEntity;	/*!< Animation target entity. \sa a3d_pdf_animation_module */
/*!
@} <!-- end of a3d_publish_3D_types -->
*/

/*!
\defgroup a3d_publish_interactivity_types Interactivity Type Declarations
\ingroup a3d_pdfentitytypes_def
@{
*/
typedef void A3DPDFAction;					/*!< Action. Abstract type. \sa a3d_pdfaction */
typedef void A3DPDFActionSetView;			/*!< Action of type SetView. \sa a3d_pdfaction */
typedef void A3DPDFActionStartAnimation;	/*!< Action of type StartAnimation. \sa a3d_pdfaction */
typedef void A3DPDFActionPauseAnimation;	/*!< Action of type PauseAnimation. \sa a3d_pdfaction */
typedef void A3DPDFActionResumeAnimation;	/*!< Action of type ResumeAnimation. \sa a3d_pdfaction*/
typedef void A3DPDFActionSetRenderingStyle;	/*!< Action of type SetRenderingStyle. \sa a3d_pdfaction*/
typedef void A3DPDFActionLaunchURL;			/*!< Action of type LaunchURL. \sa a3d_pdfaction*/
/*!
@} <!-- end of a3d_publish_interactivity_types -->
*/

/*!
\defgroup a3d_publish_interactivityadv_types Interactivity Advanced Type Declarations
\ingroup a3d_pdfentitytypes_def
@{
*/
typedef void A3DPDFDataTable;			/*!< Data Table. \sa a3d_pdf_datamodel_module */
typedef void A3DPDFObjectDataTable;	/*!< Data Table with objects. \sa a3d_pdf_datamodel_module */
typedef void A3DPDFDataTable3DViews;	/*!< Data Table for 3D views. \sa a3d_pdf_datamodel_module */
/*!
@} <!-- end of a3d_publish_interactivityadv_types -->
*/

#endif // A3DAPI_LOAD

/*!
\defgroup a3d_publish_widget_type_functions PDF Widget Type Determination
\ingroup a3d_publish_widget_types
\brief Determines the type of a PDF Widget
*/

/*!
\ingroup a3d_publish_widget_type_functions
\brief Gets the actual type of the PDF Widget

This function returns an integer that specifies a PDF Widget type.
The integer corresponds to one of the values described by the \ref A3DPDFEWidgetType enumeration.

\version 10.0

\param [in] pWidget The widget.
\param [out] peWidgetType The widget type.

\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n

\sa A3DPDFEWidgetType
*/
A3D_API(A3DStatus, A3DPDFWidgetGetType, (const A3DPDFWidget* pWidget, A3DPDFEWidgetType* peWidgetType));




/*!
\defgroup a3d_pdf_document_module Document Module
\ingroup a3d_publishpdf_module
\brief Module to define a PDF Document

This module describes the functions and structures that allow you to define a PDF Document.
*/

/*!
\defgroup a3d_pdf_layout_module Layout Module
\ingroup a3d_publishpdf_module
\brief Module to access or create entities on a PDF page.

This module describes the functions and structures that allow you to define a layout in a PDF Document.
*/

/*!
\defgroup a3d_pdf_3D_module 3D PDF Model Module
\ingroup a3d_publishpdf_module
\brief Module to define and store 3D data on a PDF page.

This module describes the functions and structures that allow you to define 3D in a PDF Document.
*/

/*!
\defgroup a3d_pdf_interactivity_module Interactivity Module
\ingroup a3d_publishpdf_module
\brief Module to define interactivity on a PDF document

This module describes the functions and structures that allow you to define Interactivity in a PDF Document.
*/

/*!
\defgroup a3d_pdf_exportimage_module Export Image Module
\ingroup a3d_publishpdf_module
\brief Module to export a PDF document to image formats

This module describes the functions and structures that allow you to export a PDF document to image formats.
*/




/*!
\ingroup a3d_pdf_layout_module
\brief A3DPDFRgbColorData structure

A color is defined in RGB color space. Red, Green, and Blue values vary from 0.0 to 1.0.
A value of (0.0, 0.0, 0.0) is for black. (1.0, 1.0, 1.0) is for white.
\version 4.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble m_dRed;			/*!< [0..1] Red value. */
	A3DDouble m_dGreen;			/*!< [0..1] Green value. */
	A3DDouble m_dBlue;			/*!< [0..1] Blue value. */
} A3DPDFRgbColorData;
#endif // A3DAPI_LOAD


/*!
\defgroup a3d_pdf_page_module Page Module
\ingroup a3d_pdf_layout_module
\brief Module to define and access a Page in a document.

This module describes the functions and structures that allow you to define a PDF Page in the document.
*/


/*!
\ingroup a3d_pdf_page_module
\brief A3DPDFPageData structure
\deprecated This structure is deprecated. Please use \ref A3DPDFPageData2 instead.
\version 4.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DPDFEPageSize m_ePageSize;				/*!< Size of the page. */
	A3DPDFEPageOrientation m_ePageOrientation;	/*!< Orientation of the page (landscape/portrait). */
} A3DPDFPageData;
#endif // A3DAPI_LOAD



/*!
\ingroup a3d_pdf_page_module
\brief A3DPDFPageData2 structure
\version 7.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DPDFEPageSize m_ePageSize;				/*!< Size of the page. */
	A3DPDFEPageOrientation m_ePageOrientation;	/*!< Orientation of the page (landscape/portrait). */
	int m_iPageWidth ;							/*!< Defines the page Width when the page m_ePageSize == kA3DPDFPageCustom */
	int m_iPageHeight;							/*!< Defines the page Height when Size m_ePageSize == kA3DPDFPageCustom */
} A3DPDFPageData2;
#endif // A3DAPI_LOAD


/*!
\ingroup a3d_pdf_page_module
\brief A3DPDFRectData structure: structure to define a rectangle placement in the PDF page
\version 4.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DInt32 m_iLeft;			/*!< The x coordinate of the bottom left corner of the rectangle. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DInt32 m_iBottom;			/*!< The y coordinate of the bottom left corner of the rectangle. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DInt32 m_iRight;			/*!< The x coordinate of the top right corner of the rectangle. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DInt32 m_iTop;			/*!< The y coordinate of the top right corner of the rectangle. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
} A3DPDFRectData;
#endif // A3DAPI_LOAD


/*!
\ingroup a3d_pdf_page_module
\brief A3DPDFRectDData structure: structure to define a rectangle placement in the PDF page, in double precision.
\version 9.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble m_dLeft;			/*!< The x coordinate of the bottom left corner of the rectangle. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DDouble m_dBottom;		/*!< The y coordinate of the bottom left corner of the rectangle. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DDouble m_dRight;			/*!< The x coordinate of the top right corner of the rectangle. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DDouble m_dTop;			/*!< The y coordinate of the top right corner of the rectangle. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
} A3DPDFRectDData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_document_module
\brief Function to create an empty PDF document

This function creates a document with no page. Subsequently, some pages must be added with the append
functions \ref A3DPDFDocumentAppendNewPage2 or \ref A3DPDFDocumentAppendPageFromPDFFileAndSuffixFields.

\param [out] ppDoc The document created.

\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFDocumentCreateEmpty, (A3DPDFDocument** ppDoc));

/*!
\ingroup a3d_pdf_document_module
\brief Function to create a document from a PDF file

This function creates a document from a PDF input file. Starting with HOOPS Publish 6.0 the input PDF can contain several pages.
Pages can be added to the document with append functions \ref A3DPDFDocumentAppendNewPage2 or \ref A3DPDFDocumentAppendPageFromPDFFileAndSuffixFields.

\param [in] pcFileName The input file name.
\param [out] ppDoc The document created.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFDocumentCreateFromPDFFile, (const A3DUTF8Char* pcFileName, A3DPDFDocument** ppDoc));

/*!
\ingroup a3d_pdf_page_module
\deprecated This method was deprecated in HOOPS Publish 1.10. Use \ref A3DPDFDocumentAppendNewPage2 instead.

This function adds a unique page to an empty PDF document.
This function can only be used on a file opened with \ref A3DPDFDocumentCreateEmpty. It can't be used on a file which already contains a page.
\param [in,out] pDoc The current document on which the new page will be added.
\param [in] pPageData The page data parameters.
\param [out] ppPage The newly-created page.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFDocumentCreateUniquePage, (A3DPDFDocument* pDoc, const A3DPDFPageData* pPageData, A3DPDFPage** ppPage));


/*!
\ingroup a3d_pdf_page_module
\brief This function adds a unique page to an empty PDF document.
\deprecated This method is deprecated. Use \ref A3DPDFDocumentAppendNewPage2 instead.

This function can only be used on a file opened with \ref A3DPDFDocumentCreateEmpty. It can't be used on a file which already contains a page.
\param [in,out] pDoc The current document on which the new page will be added.
\param [in] pPageData The page data parameters.
\param [out] ppPage The newly-created page.
\return \ref A3D_SUCCESS \n
\version 7.2
*/
A3D_API (A3DStatus, A3DPDFDocumentCreateUniquePage2, (A3DPDFDocument* pDoc, const A3DPDFPageData2* pPageData, A3DPDFPage** ppPage));


/*!
\ingroup a3d_pdf_page_module
\brief Function to get the unique page of the document
\deprecated This method was deprecated in HOOPS Publish 6.0. Use \ref A3DPDFDocumentGetPage instead.

This function only works on a document which contains a single page. It is designed to be called after \ref A3DPDFDocumentCreateFromPDFFile to get the Page object.

\param [in] pDoc The Document object to work with.
\param [out] ppPage The Page object.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFDocumentGetUniquePage, (const A3DPDFDocument* pDoc, A3DPDFPage** ppPage));

/*!
\ingroup a3d_pdf_page_module
\brief Function to get the number of pages in the document

\param [in] pDoc The Document object to work with.
\param [out] piNbPages The number of pages. Remember to subtract 1 from this value if you are going to pass it to a
method that takes a zero-based page number.
\return \ref A3D_SUCCESS \n
\version 6.0
*/
A3D_API (A3DStatus, A3DPDFDocumentGetNumberPages, (const A3DPDFDocument* pDoc, A3DInt32* piNbPages));

/*!
\ingroup a3d_pdf_page_module
\brief Function to get a page in the document

\param [in] pDoc The Document object to work with.
\param [in] iNumPage The index of the page. The first page is 0.
\param [out] ppPage The Page object.
\return \ref A3D_SUCCESS \n
\version 6.0
*/
A3D_API (A3DStatus, A3DPDFDocumentGetPage, (const A3DPDFDocument* pDoc, const A3DInt32 iNumPage, A3DPDFPage** ppPage));

/*!
\ingroup a3d_pdf_page_module
\brief Function to remove pages in the document

\param [in,out] pDoc The Document object to work with.
\param [in] iFirstPage The index of the first page to remove. The first page is 0.
\param [in] iLastPage The index of the last page to remove.
\return \ref A3D_SUCCESS \n
\version 6.0
*/
A3D_API (A3DStatus, A3DPDFDocumentRemovePages, (const A3DPDFDocument* pDoc, const A3DInt32 iFirstPage, const A3DInt32 iLastPage));




/*!
\ingroup a3d_pdf_page_module
\brief Appends a new empty page to a document
\deprecated Use \ref A3DPDFDocumentAppendNewPage2 instead.

The page is added at the end of the document. It is a new page created with the options set in the pPageData argument.
\param [in,out] pDoc The Document object to work with.
\param [in] pPageData The page parameters.
\param [out] ppPage The Page object for the new page.
\return \ref A3D_SUCCESS \n
\version 4.2
*/
A3D_API (A3DStatus, A3DPDFDocumentAppendNewPage, (A3DPDFDocument* pDoc, const A3DPDFPageData* pPageData, A3DPDFPage** ppPage));


/*!
\ingroup a3d_pdf_page_module
\brief Appends a new empty page to a document

The page is added at the end of the document. It is a new page created with the options set in the pPageData argument.
\param [in,out] pDoc The Document object to work with.
\param [in] pPageData The page parameters.
\param [out] ppPage The Page object for the new page.
\return \ref A3D_SUCCESS \n
\version 7.2
*/
A3D_API (A3DStatus, A3DPDFDocumentAppendNewPage2, (A3DPDFDocument* pDoc, const A3DPDFPageData2* pPageData, A3DPDFPage** ppPage));

/*!
\ingroup a3d_pdf_page_module
\deprecated Function deprecated, Please use \ref A3DPDFDocumentAppendPageFromPDFFileAndSuffixFields.

Appends new pages from a PDF template to a document. Starting with HOOPS Publish 6.0 the input PDF can contain several pages.
The pages are added at the end of the document. All pages are copied from the PDF template file specified with the pcFileName argument.
If the imported pages contain form fields with names that already exist on other pages of the document, the function will fail.
In this situation, use \ref A3DPDFDocumentAppendPageFromPDFFileAndSuffixFields to rename all form fields of the imported pages.
\param [in,out] pDoc The Document object to work with.
\param [in] pcFileName The input file name of the PDF template.
\param [out] ppPage The Page object for the last page inserted.
\return \ref A3D_SUCCESS \n
\version 4.2
*/
A3D_API (A3DStatus, A3DPDFDocumentAppendPageFromPDFFile, (A3DPDFDocument* pDoc, const A3DUTF8Char* pcFileName, A3DPDFPage** ppPage));

/*!
\ingroup a3d_pdf_page_module

Appends new pages from a PDF template to a document, with the possibility to rename all form fields to avoid PDF conflicts.
This method supersedes \ref A3DPDFDocumentAppendPageFromPDFFile() in A3DLIBS 4.3 and later.
Starting from HOOPS Publish 6.0 the input PDF can contain several pages.
The pages are added at the end of the document. All pages are copied from the PDF template file specified with the pcFileName argument.
If the imported pages contain form fields with names that already exist on other pages of the document, the function will fail.
In this situation, set the bRenameFields parameter to true to rename all form fields of the imported pages.
<p><b>Warning:</b> All references to the renamed form fields have to be modified to reflect the new names. This has to be done for example for JavaScript code
that could work with these fields. Please refer to the section 'Field Names' in programming guide for a description of specific cases with field names.
</p>
\param [in,out] pDoc The Document object to work with.
\param [in] pcFileName The input file name of the PDF template.
\param [in] bRenameFields If true, the form fields of the template pages are renamed, appending the position of the pages in the
final document to every fields. Note that the index of the first page is 1.
For example, a field 'MyTitle' will be renamed 'MyTitle_1" if the page is inserted as the first page of the document.
If false, the form fields are not renamed and the function will fail if some fields already exist on the document with the same name.
\param [out] ppPage The Page object for the last page inserted.
\return \ref A3D_SUCCESS \n
\version 4.3
*/
A3D_API(A3DStatus, A3DPDFDocumentAppendPageFromPDFFileEx, (A3DPDFDocument* pDoc, const A3DUTF8Char* pcFileName, const A3DBool bRenameFields, A3DPDFPage** ppPage));


/*!
\ingroup a3d_pdf_page_module

<p>Appends new pages from a PDF template to a document, with the possibility to rename all form fields to avoid PDF conflicts.
This method supersedes \ref A3DPDFDocumentAppendPageFromPDFFile() in A3DLIBS 11.1 and later.
</p>
<p>Starting from HOOPS Publish 6.0 the input PDF can contain several pages.
The pages are added at the end of the document. All pages are copied from the PDF template file specified with the pcFileName argument.
</p>
<p>If the imported pages contain form fields with names that already exist on other pages of the document, the function will fail.
In this situation, set the pcAppendStringToFieldNames parameter to rename all form fields of the imported pages.
</p>
<p><b>Warning:</b> All references to the renamed form fields have to be modified to reflect the new names. This has to be done for example for JavaScript code
that could work with these fields. Please refer to the section 'Field Names' in programming guide for a description of specific cases with field names.
</p>
\param [in,out] pDoc The Document object to work with.
\param [in] pcFileName The input file name of the PDF template.
\param [in] pcAppendStringToFieldNames The string that is appended to all form fields. Please see the \ref Details "details section" below for more information.
\param [out] ppPage The Page object for the last page inserted.
\return \ref A3D_SUCCESS \n
\version 11.1

\section Details
<p>When using pcAppendStringToFieldNames to modify field names, please be aware of the following:
	<ul>
		<li style="margin-bottom:10px;">If the string is not empty, the field name is suffixed with it.
		For example, a field 'MyTitle' with pcAppendStringToFieldNames set to 'abcd' will be renamed 'MyTitle_abcd'.
		Please refer to the section 'Field Names' in programming guide for a description of specific cases with field names.
		<p>If the field has been copied/pasted in Acrobat, then the field is renamed 'fieldname_appendstring.index'.</p>
		<p>If the field belongs to a group of fields, then the field is renamed 'groupname_appendstring.fieldname'.</p>
		<p>If the field is a radio button, then the field is renamed 'radiobuttongroupname_appendstring.index'.</p>
		</li>
		<li style="margin-bottom:10px;">If pcAppendStringToFieldNames is NULL or empty, the form fields are not renamed and the function will fail if fields already exist in the document with the same name.
		</li>
	</ul>
</p>
*/
A3D_API (A3DStatus, A3DPDFDocumentAppendPageFromPDFFileAndSuffixFields, (A3DPDFDocument* pDoc, const A3DUTF8Char* pcFileName, const A3DUTF8Char* pcAppendStringToFieldNames, A3DPDFPage** ppPage));

/*!
\ingroup a3d_pdf_document_module
\brief A3DPDFDocumentInformationData structure
\version 4.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;	// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcTitle;		/*!< Title of the document. */
	A3DUTF8Char* m_pcAuthor;	/*!< Author of the document. */
	A3DUTF8Char* m_pcSubject;	/*!< Subject of the document. */
	A3DUTF8Char* m_pcCreator;	/*!< The product which created the document (customer application). */
} A3DPDFDocumentInformationData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_document_module
\brief Function to set information on the document. These information are visible in the Adobe Reader on the File Properties menu.

\param [in,out] pDoc The Document object to work with.
\param [in] pInformationData The information parameters.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFDocumentSetInformation, (A3DPDFDocument* pDoc, const A3DPDFDocumentInformationData* pInformationData));

/*!
\ingroup a3d_pdf_document_module
\brief Function to set custom property on the document. These information are visible in the Adobe Reader on the File Properties menu, on the Custom thumbnail.

\param [in,out] pDoc The Document object to work with.
\param [in] pcPropName The property name.
\param [in] pcPropValue The property value.
\return \ref A3D_SUCCESS \n
\version 12.2
*/
A3D_API(A3DStatus, A3DPDFDocumentSetCustomProperty, (A3DPDFDocument* pDoc, const A3DUTF8Char* pcPropName, const A3DUTF8Char* pcPropValue));

/*!
\ingroup a3d_pdf_document_module
\brief Function to add user and owner passwords on a document.
\deprecated This method is deprecated because it allows generation of a PDF file with an owner password and no permissions, which is not a good practice.
Use \ref A3DPDFDocumentSetPasswordSecurity instead.

These passwords are natively supported by Adobe Reader. If a user password is specified, Adobe Reader will ask for a password when opening the document.
The owner password  is the password used to set document restrictions on the document. It does not restrict the opening of a PDF file, only what can be done once opened.
Document restrictions can include printing, changing the document, document assembly, content copying, content copying for accessibility, page extraction, commenting,
filling of form fields, signing, and creation of template pages.

\param [in,out] pDoc The Document object to work with.
\param [in] pcUserPassword The password to protect the document at opening. Also protects against document modification and printing.
If an empty string is specified, no user password is required by Adobe Reader or Acrobat.
\param [in] pcOwnerPassword Lets you require a password when changing the security features of the PDF, not when opening it.
If an empty string is specified, no password will be required in Acrobat to change security settings.

\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFDocumentSetPassword, (A3DPDFDocument* pDoc, const A3DUTF8Char* pcUserPassword, const A3DUTF8Char* pcOwnerPassword));

/*!
\ingroup a3d_pdf_document_module
\brief Function to set permissions on a document.
\deprecated This method is deprecated because it allows to generate a PDF file with permission and no owner password, which is not a good practice.
Use \ref A3DPDFDocumentSetPasswordSecurity instead.

\param [in,out] pDoc The Document object to work with.
\param [in] iPermissions The permissions, composed of bit fields detailed in \ref a3d_pdf_documentpermissionbits.

\return \ref A3D_SUCCESS \n
\version 7.2
*/
A3D_API (A3DStatus, A3DPDFDocumentSetDocumentPermissions, (A3DPDFDocument* pDoc, int iPermissions));

/*!
\ingroup a3d_pdf_document_module
\brief Function to set permissions and passwords on a document.

<p>The passwords are natively supported by Adobe Reader. If a user password is specified, Adobe Reader will ask for a password when opening the document.
</p>
<p>The owner password is the password used to set document restrictions on the document. It does not restrict the opening of a PDF file, only what can be done once opened.
Document restrictions can include printing, changing the document, document assembly, content copying, content copying for accessibility, page extraction, commenting,
filling of form fields, signing, and creation of template pages.
</p>
<p>Please note that the owner password is respected by Adobe reader and Acrobat products, but might not be respected by other vendors.
However, the user password offers more complete protection of the PDF file and can only be broken by brute-force attacks.
To ensure maximum password security, it's good practice to define passwords with a long set of characters that are not based on simple patterns of characters or common words.
</p>
<p>The password encryption implemented is the AES 128-bit algorithm, which is compatible with Acrobat 7 and later versions.
</p>
\param [in,out] pDoc The Document object to work with.
\param [in] pcUserPassword The password to protect the document at opening.
If NULL or an empty string is specified, no user password is required by Adobe Reader or Acrobat.
\param [in] pcOwnerPassword Lets you require a password when changing the security features of the PDF.
An owner password is necessary to define permissions.
If NULL or an empty string is specified, all permissions are allowed and no password will be required in Acrobat to change security settings.
\param [in] iPermissions The permissions, composed of bit fields detailed in \ref a3d_pdf_documentpermissionbits.
A permission can only be defined if an owner password is defined. Otherwise, all permissions are allowed.
Typical values are as follows:
<ul>
<li>kA3DPDFDocumentPermDocAssembly is for inserting, deleting, and rotating page</li>
<li>kA3DPDFDocumentPermFillandSign is for filling in form fields and signing signature fields</li>
<li>kA3DPDFDocumentPermFillandSign+kA3DPDFDocumentPermEditNotes is for commenting, filling in form fields, and signing signature fields</li>
</ul>
\param [in] eEncryptContentOptions Specifies the content to encrypt. See \ref A3DPDFEEncryptContent description.

\return \ref A3D_SUCCESS \n
\version 12.0
*/
A3D_API(A3DStatus, A3DPDFDocumentSetPasswordSecurity, (
	A3DPDFDocument* pDoc,
	const A3DUTF8Char* pcUserPassword,
	const A3DUTF8Char* pcOwnerPassword,
	A3DUns32 iPermissions,
	A3DPDFEEncryptContent eEncryptContentOptions));

/*!
\ingroup a3d_pdf_document_module
\brief Function to attach a file to the document.

Attachments are visible on the Adobe Reader with the specific navigation pane. In Adobe Reader X version, the Attachments pane can be activated with the menu View / Show/Hide / Navigation Panes / Attachments.

\param [in,out] pDoc The Document object to work with.
\param [in] pcFileName The name of the file to attach.
\param [in] pcDescription The description of the attached file.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFDocumentAddFileAttachment, (A3DPDFDocument* pDoc, const A3DUTF8Char* pcFileName, const A3DUTF8Char* pcDescription));


/*!
\ingroup a3d_pdf_document_module
\brief Function to save the PDF document.
\deprecated This function is now deprecated. Use \ref A3DPDFDocumentSaveEx with flags \ref kA3DPDFSaveFull to have the same basic behaviour.

\param [in] pDoc The Document object to save.
\param [in] pcFileName The file name where to save the document. The path (absolute or relative) must be provided (use ".\\" or "./" for the current directory).
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFDocumentSave, (A3DPDFDocument* pDoc, const A3DUTF8Char* pcFileName));

/*!
\ingroup a3d_pdf_document_module
\brief Function to save the PDF document, using some flags for optimizing the size of the resulting PDF.

\param [in] pDoc The Document object to save, using some flags for optimizing the resulting PDF.
\param [in] iSaveFlags The saving flags, composed of bit fields detailed in \ref a3d_pdf_saveflagsbits.
\param [in] pcFileName The file name where to save the document. The path (absolute or relative) must be provided (use ".\\" or "./" for the current directory).
\return \ref A3D_SUCCESS \n
\version 9.2
*/
A3D_API (A3DStatus, A3DPDFDocumentSaveEx, (A3DPDFDocument* pDoc, const int iSaveFlags, const A3DUTF8Char* pcFileName));

/*!
\ingroup a3d_pdf_document_module
\brief Function to close the document and free all memory.

\param [in,out] pDoc The Document object to work with.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFDocumentClose, (A3DPDFDocument* pDoc));

/*!
\ingroup a3d_pdf_document_module
\brief Function to embed a font in a pdf document

The font must be embedded before using it in the document.
The document must contain at least one page before calling this function.

\param [in] pDoc The document in which the font will be embedded.
\param [in] pcFontName The name of the font

\return \ref A3D_SUCCESS \n
\return \ref A3DPDF_CANNOT_FIND_FONT \n
\return \ref A3DPDF_CANNOT_CREATE_ENCODING \n
\return \ref A3DPDF_CANNOT_CREATE_FONT \n
\return \ref A3DPDF_CANNOT_EMBED_FONT \n
\return \ref A3DPDF_EMPTY_DOCUMENT \n

\version 11.1
*/
A3D_API(A3DStatus, A3DPDFDocumentEmbedFont,(const A3DPDFDocument* pDoc, const A3DUTF8Char* pcFontName, const char *pcEncodingName, A3DPDFELanguage eLanguage));

/*!
\defgroup a3d_pdf_font_module Font Module
\ingroup a3d_pdf_layout_module
\brief Module for fonts on a PDF document.

This module describes the functions and structures that allow you to specify fonts in a document.
*/

/*!
\ingroup a3d_pdf_font_module
\brief Function to check the existence of a PDF font on the local system.

\param [in] pcFontName The name of the font
\param [out] pbFontSupported true if the font can be used, false if not.
\param [out] ppcFontPSName The internal font name as used in PDF. This name should be used in Javascript code to set a font with function textFont.

\return \ref A3D_SUCCESS \n

\version 12.2
*/
A3D_API(A3DStatus, A3DPDFFontCheck, (const A3DUTF8Char* pcFontName, A3DBool* pbFontSupported, A3DUTF8Char** ppcFontPSName));

/*!
\defgroup a3d_pdf_layout_static_module Static Layout Entities Module
\ingroup a3d_pdf_layout_module
\brief Module for static entities on a PDF Page.

This module describes the functions and structures that allow you to add static entities in a page.
Static entities are entites such as text, image, or 2D graphics, which are positionned on a page
at the construction of the PDF, but can't be dynamically modified.
Static entities can only be added on a page, and cannot be retrieved from an existing page.
*/


/*!
\defgroup a3d_pdf_text_module Text Module
\ingroup a3d_pdf_layout_static_module
\brief Adds text lines in the page

This module describes the functions and structures that allow you to add Text lines in the page.
Text lines can only be added on a page, and cannot be retrieved from an existing page.
*/
/*!
\ingroup a3d_pdf_text_module
\brief A3DPDFTextData structure.

Structure to specify a text line: content and formatting options (color, font used and font size).
Specific to this function, the text provided is not in utf8, but is a char* with values 0x00 to 0xFF.
The encoding of characters is specified as described in PDF norm annex D ("Character Sets and Encodings").
Latin fonts (Times, Helvetica, Courier families) are encoded with WinAnsiEncoding; character set is described in annex D.2.
Symbol and ZapfDingbats have a built-in encoding; character set is described in annex D.5 and D.6.

\version 4.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcTextString;	/*!< Content of the text string. Note this is encoded as char* and not utf8. It should be a single line text string (no carriage return supported). */
	A3DPDFEFontName m_eFontName;	/*!< Font name of the text. */
	A3DInt32 m_iFontSize; 			/*!< Size of the font. */
	A3DPDFRgbColorData m_sColor;	/*!< Color of the text. */
} A3DPDFTextData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_text_module
\brief A3DPDFTextDataEx structure.

Structure to specify a text line with extended fonts and languages. The font needs to be available in C:\\Windows\\Fonts directory on the machine
where HOOPS Publish is run, or in the folder specified to A3DPDFInitializePDFLibAndResourceDirectory.
Depending on the characters used in the string, the font must be chosen, as well as the according language. 
The value kA3DPDFLangAutoDetect should be used for m_eLanguage to avoid to specify language and encoding.


\version 5.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcTextString;		/*!< Content of the text string. It should be a single line text string (no carriage return supported). */
	A3DUTF8Char* m_pcFontName;			/*!< Font name of the text. */
	A3DUTF8Char* m_pcEncodingName;		/*!< Set null to keep default treatment. Supported encoding are "WinAnsiEncoding",
										"StandardEncoding", "MacRomanEncoding", "MacExpertEncoding", or "PDFDocEncoding"
										(see PDF specifications for more details).
										If NULL, the encoding is tentatively retrieved from m_eLanguage. */
	A3DPDFELanguage m_eLanguage;		/*!< Language used for the text string. kA3DPDFLangAutoDetect value should preferably be used to automatically detect the correct language and encoding. */
	A3DInt32 m_iFontSize;				/*!< Size of the font. */
	A3DPDFRgbColorData m_sColor;		/*!< Color of the text. */
	A3DBool m_bEmbedFontInPDF;			/*!< If true, the font will be subsetted in the PDF document to only support the characters used in the page, no need to have the font on the computer for
												the one who will open the document.
												For the best behaviour, m_bEmbedFontInPDF should always be set to true. */
} A3DPDFTextDataEx;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdf_text_module
\brief Function to create a text object

The text object is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsertText.

\param [in,out] pDoc The Document object to work with.
\param [in] pTextData The text parameters.
\param [out] ppText The Text object created.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFTextCreate, (A3DPDFDocument* pDoc, const A3DPDFTextData* pTextData, A3DPDFText** ppText));


/*!
\ingroup a3d_pdf_text_module
\brief Function to create a text object with extended fonts and languages.

The text object is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsertText.
If the font can't be found in the directory C:\\Windows\\Fonts, then the function returns an A3DPDF_CANNOT_FIND_FONT (-1000016) error.
WARNING: the initialization function \ref A3DPDFInitializePDFLibAndResourceDirectory must be called before using this function.

\param [in] pDoc The Document object to work with.
\param [in] pTextDataEx The text parameters.
\param [out] ppText The Text object created.
\return \ref A3D_SUCCESS \n
\version 5.0
*/
A3D_API (A3DStatus, A3DPDFTextCreateEx, (A3DPDFDocument* pDoc, const A3DPDFTextDataEx* pTextDataEx, A3DPDFText** ppText));


/*!
\ingroup a3d_pdf_text_module
\brief Function to determine the width of a text string.

The width is returned in same units as the page.
It may be horizontal or vertical, depending on the writing style.
Thus the returned width has both a horizontal and vertical component.
Please note that vertical size is not the heigth of the text, but the size for a vertical writing type.
For most of fonts, the pdSizeV is zero.

\param [in] pText The Text object to calculate the size of.
\param [out] pdSizeH The horizontal size of the text. Is used if writing style is horizontal.
\param [out] pdSizeV The vertical size of the text. Is used if writing style is vertical.
\return \ref A3D_SUCCESS \n
\version 12.0
*/
A3D_API(A3DStatus, A3DPDFTextGetWidth, (A3DPDFText* pText, A3DDouble* pdSizeH, A3DDouble* pdSizeV));

/*!
\ingroup a3d_pdf_text_module
\brief Function to insert a text in a page

\param [in,out] pPage The Page object to work with.
\param [in] pText The Text object to insert on the page.
\param [in] iPosLeft The x coordinate of the insertion point of the text. The insertion point is the bottom left corner of the text. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\param [in] iPosBottom The y coordinate of the insertion point of the text. The insertion point is the bottom left corner of the text. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\return \ref A3DPDF_CANNOT_EMBED_FONT if the font is protected and cannot be embedded.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFPageInsertText, (A3DPDFPage* pPage, A3DPDFText* pText, const A3DInt32 iPosLeft, const A3DInt32 iPosBottom));

/*!
\ingroup a3d_pdf_text_module
\brief Function to insert a text in a table

\param [in,out] pPage The Page object to work with.
\param [in] pTable The Table object to insert in the table. \sa a3d_pdf_table_module.
\param [in] pText The Text object to insert in the table.
\param [in] iRowIndex The index of the row of the table's cell to insert the button (start from 1)
\param [in] iColumnIndex The index of the column of the table's cell to insert the button (start from 1)
\return \ref A3D_SUCCESS \n
\version 7.2
*/
A3D_API (A3DStatus, A3DPDFPageInsertTextInTable, (A3DPDFPage* pPage, A3DPDFTable* pTable, A3DPDFText* pText, A3DInt32 iRowIndex, A3DInt32 iColumnIndex));



/*!
\defgroup a3d_pdf_image_module Image Module
\ingroup a3d_pdf_layout_static_module
\brief Add Images in the page

This module describes the functions and structures that allow you to add images in the page.
These images can only be added on a page, and cannot be retrieved from an existing page.

An image can be used as a static image to be inserted on a page at a specific position. Use \ref A3DPDFPageInsertImage2 for positionning such an image on a page.
Also, an image can be stored as an icon in the document, and then be used for interactivity.
*/
/*!
\ingroup a3d_pdf_image_module
\brief A3DPDFImageData structure
\deprecated This structure is deprecated and Image objects should be created using A3DPDFImageCreateFromFile or \ref A3DPDFImageCreateFromStream functions.
\version 4.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcFileName;		/*!< File name of the image file. */
	A3DInt32 m_iWidth;				/*!< Width in pixels. Only used to insert an image on a page with \ref A3DPDFPageInsertImage. */
	A3DInt32 m_iHeight;				/*!< Height in pixels. Only used to insert an image on a page with \ref A3DPDFPageInsertImage. */
	A3DPDFEImageFormat m_eFormat;	/*!< Format of the image. */
} A3DPDFImageData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdf_image_module
\brief Function to create a picture image object
\deprecated This method is deprecated. Please use \ref A3DPDFImageCreateFromFile or \ref A3DPDFImageCreateFromStream instead.

The image object is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsertImage.

\param [in,out] pDoc The Document object to work with.
\param [in] pImageData The image parameters.
\param [out] ppImage The Image object created.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFImageCreate, (A3DPDFDocument* pDoc, const A3DPDFImageData* pImageData, A3DPDFImage** ppImage));

/*!
\ingroup a3d_pdf_image_module
\brief Function to create an image object from a file

The image object is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsertImage2.

\param [in,out] pDoc The Document object to work with.
\param [in] pcFileName File name of the image file.
\param [in] eFormat Format of the image. If kA3DPDFImageFormatUnknown, the format is deducted from file extension.
\param [out] ppImage The Image object created.
\return \ref A3D_SUCCESS \n
\version 10.2
*/
A3D_API(A3DStatus, A3DPDFImageCreateFromFile, (A3DPDFDocument* pDoc, const A3DUTF8Char* pcFileName, const A3DPDFEImageFormat eFormat, A3DPDFImage** ppImage));

/*!
\ingroup a3d_pdf_image_module
\brief Function to create an image object from a file

The image object is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsertImage2.

\param [in,out] pDoc The Document object to work with.
\param [in] pcStream The buffer.
\param [in] iLengthStream The length of the buffer.
\param [in] eFormat Format of the image. It is mandatory to be specified for a buffer.
\param [out] ppImage The Image object created.
\return \ref A3D_SUCCESS \n
\version 10.2
*/
A3D_API(A3DStatus, A3DPDFImageCreateFromStream, (A3DPDFDocument* pDoc, const A3DUTF8Char *pcStream, const A3DInt32 iLengthStream, const A3DPDFEImageFormat eFormat, A3DPDFImage** ppImage));

/*!
\ingroup a3d_pdf_image_module
\brief Function to insert a static image in a page. With this, an image is positionned on a page but can not be used for interactivity.
\deprecated This function is deprecated. Please use \ref A3DPDFPageInsertImage2 instead.

\param [in,out] pPage The Page object to work with.
\param [in] pImage The Image object to insert on the page.
\param [in] iPosLeft The x coordinate of the insertion point of the image. The insertion point is the bottom left corner of the image. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\param [in] iPosBottom The y coordinate of the insertion point of the image. The insertion point is the bottom left corner of the image. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFPageInsertImage, (A3DPDFPage* pPage, A3DPDFImage* pImage, const A3DInt32 iPosLeft, const A3DInt32 iPosBottom));

/*!
\ingroup a3d_pdf_image_module
\brief Function to insert a static image in a page. With this, an image is positionned on a page but can not be used for interactivity.

\param [in,out] pPage The Page object to work with.
\param [in] pImage The Image object to insert on the page.
\param [in] pRectData The rectangle to specify the position on the page.
\return \ref A3D_SUCCESS \n
\version 10.2
*/
A3D_API(A3DStatus, A3DPDFPageInsertImage2, (A3DPDFPage* pPage, A3DPDFImage* pImage, const A3DPDFRectData* pRectData));




/*!
\defgroup a3d_pdf_link_module Link Module
\ingroup a3d_pdf_layout_static_module
\brief Adds Links in the page

This module describes the functions and structures that allow you to add links in the page.
A link is a 'hot zone' on which is assigned an action. When the user clicks on the zone, the action is executed.
The link can be invisible, or drawn as a rectangle with a border. The border width and color can be specified.
*/
/*!
\ingroup a3d_pdf_link_module
\brief A3DPDFLinkData structure
Since 2017, m_pcJavascriptString can only be defined with a HOOPS Publish Advanced version.
With HOOPS Publish Standard version, you should use A3DPDFLinkAddAction.
\version 5.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DInt32 m_iBorderWidth;						/*!< Border width in points. 0 for an invisible border. */
	A3DPDFRgbColorData m_sColor;					/*!< Border color. */
	A3DPDFELinkHighlightMode m_eHighlightingMode;	/*!< Highlighting mode (the visual effect that shall be used when the mouse button is pressed or held down inside its active area). */
	A3DUTF8Char* m_pcJavascriptString;				/*!< JavaScript action to be executed when the link is clicked. Only available with HOOPS Publish Advanced version. */
} A3DPDFLinkData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdf_link_module
\brief Function to create a link object

The link object is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsertLink.

\param [in,out] pDoc The Document object to work with.
\param [in] pLinkData The link parameters.
\param [out] ppLink The Link object created.
\return \ref A3D_SUCCESS \n
\version 5.2
*/
A3D_API (A3DStatus, A3DPDFLinkCreate, (A3DPDFDocument* pDoc, const A3DPDFLinkData* pLinkData, A3DPDFLink** ppLink));

/*!
\ingroup a3d_pdf_link_module
\brief Function to insert a link in a page

\param [in] pPage The Page object to work with.
\param [in] pLink The Link object to insert on the page.
\param [in] pRectData The rectangle to specify the position on the page.
\return \ref A3D_SUCCESS \n
\version 5.2
*/
A3D_API (A3DStatus, A3DPDFPageInsertLink, (A3DPDFPage* pPage, A3DPDFLink* pLink, const A3DPDFRectData* pRectData));


/*!
\ingroup a3d_pdf_link_module
\brief Function to insert a link in a table

\param [in,out] pPage The Page object to work with.
\param [in] pTable The Table object to insert in the table. \sa a3d_pdf_table_module.
\param [in] pLink The Link object to insert in the table.
\param [in] iRowIndex The index of the row of the table's cell to insert the link (start from 1)
\param [in] iColumnIndex The index of the column of the table's cell to insert the link (start from 1)
\return \ref A3D_SUCCESS \n
\version 7.2
*/
A3D_API (A3DStatus, A3DPDFPageInsertLinkInTable, (A3DPDFPage* pPage, A3DPDFTable* pTable, A3DPDFLink* pLink, A3DInt32 iRowIndex, A3DInt32 iColumnIndex));



/*!
\defgroup a3d_pdf_table_module Table Module
\ingroup a3d_pdf_layout_static_module
\brief Module to create tables on a PDF page

This module describes the functions and structures that allow you to define tables on a PDF page.
The table implementation uses an add-on to HOOPS Publish (TableToPDF) which is provided for free by Tech Soft 3D.
The add-on is using components that are licensed under GNU LGPL terms.
<b>Consequently, the usage of tables using HOOPS Publish add-on requires our customer's application to comply
with LGPL requirements.</b>
TableToPDF can be downloaded at http://developer.techsoft3d.com/add-ons/tabletopdf/
The deployment process is simple and just requires to copy the provided DLLs in HOOPS Publish binaries folder.

With HOOPS Publish, tables are defined in a HTML format using the function \ref A3DPDFTableCreate. Two parameters
define the table: the table content definition, as well as a style definition. Then, the table is positioned on
the PDF page using the function \ref A3DPDFPageInsertTable.

The HTML and style definition is out of the scope of this guide. HOOPS Publish and TableToPDF support the
latest HTML5 and CSS3 specifications which are widely documented on the Internet. A lot of guides and samples
can be consulted.

As an example, the following simple table
\image html sampletable.png

is defined with HOOPS Publish by specifying the members of \ref A3DPDFTableData :

The member \ref A3DPDFTableData::m_pcHtmlTable member takes the following value:
\include extable.html

The member \ref A3DPDFTableData::m_pcHtmlStyle member takes the following value:
\include exstyle.html

*/
/*!
\ingroup a3d_pdf_table_module
\brief Structure to define a table
\version 5.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcHtmlTable;					/*!< String for definition of the table in a HTML format. See example in \ref a3d_pdf_table_module. */
	A3DUTF8Char* m_pcHtmlStyle;					/*!< String for definition of the table style in a CSS format. See example in \ref a3d_pdf_table_module. */
} A3DPDFTableData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_table_module
\brief Function to create a table on a PDF page

The table object is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsertTable.
WARNING: the initialization function \ref A3DPDFInitializePDFLibAndResourceDirectory must be called before using this function.
Also, the binaries of the free add-on TableToPDF must have been copied in the same directory as HOOPS Publish binaries.

\param [in,out] pDoc The Document object to work with.
\param [in] pTableData The table parameters.
\param [out] ppTable The Table object created.
\return \ref A3D_SUCCESS \n
\version 5.1
*/
A3D_API (A3DStatus, A3DPDFTableCreate, (A3DPDFDocument* pDoc, const A3DPDFTableData* pTableData, A3DPDFTable **ppTable));


/*!
\ingroup a3d_pdf_table_module
\brief Function to insert a table in a page

Warning: The positioning is not defined as in the other insertion functions (for texts or images). Here, the position is specified from
the top of the page.

\param [in,out] pPage The Page object to work with.
\param [in] pTable The Table object to insert on the page.
\param [in] iPosLeft The x coordinate of the insertion point of the table. The insertion point is the top left corner of the table. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\param [in] iPosTop The y coordinate of the insertion point of the table. The insertion point is the top left corner of the table. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
\return \ref A3D_SUCCESS \n
\version 5.1
*/
A3D_API (A3DStatus, A3DPDFPageInsertTable, (A3DPDFPage* pPage, A3DPDFTable* pTable, const A3DInt32 iPosLeft, const A3DInt32 iPosTop));




/*!
\defgroup a3d_pdf_widget_module Interactive Layout Entities Module
\ingroup a3d_pdf_layout_module
\brief Module for interactive widgets on a PDF page.

This module describes the functions and structures that allow you to define or retrieve widgets on a PDF Page.
Widgets are interactive entitities that can be modified with JavaScript coding at runtime.
Functions exist to create widgets on a page, or retrieve widgets on an existing page.

Widgets are divided in two categories: the Fields, and the High Level widgets.
Fields are Acrobat Forms entities existing on a page. These Fields can be authored using Acrobat.
High Level widgets are HOOPS Publish entities. They usually are built from a set of Fields.
High Level widgets can only be authored by HOOPS Publish functions.

*/

/*!
\ingroup a3d_pdf_widget_module
\brief A3DPDFFieldData structure
\version 6.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;				// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char m_acName[MAX_FIELD_NAME];	/*!< Name of the field. This name can be used in Field functions (prefixed with 'A3DPDFPageField').
		<p>Note that a field can be in a tree of fields. This is the case, for example, if the field has been copied/pasted in Acrobat.
		In this case, the field has a name such as 'fieldname\#index' in the Acrobat forms tool, and a name 'fieldname.index' accessible by javascript.</p> */
	A3DPDFRectData m_sRect;					/*!< The rectangle that specifies the position on the page. */
	A3DPDFEFieldType m_eType;				/*!< Type of the field. */
} A3DPDFFieldData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdf_widget_module
\brief Function to retrieve the fields of a PDF page. The fields supported are only form fields created with Acrobat.
LiveCycle Designer fields (XFA) are not supported.

\param [in] pPage The Page object to work with.
\param [out] piNbFields The number of fields.
\param [out] pppFields The array of pointer on fields objects.
\return \ref A3D_SUCCESS \n
\version 6.0
*/
A3D_API(A3DStatus, A3DPDFPageGetFields, (A3DPDFPage* pPage, A3DInt32* piNbFields, A3DPDFField*** pppFields));

/*!
\ingroup a3d_pdf_widget_module
\brief Function to get a PDF Field from an existing form field on a page.

\param [in] pPage The Page object on which is the field.
\param [in] pcFieldName Unique name for existing field.
\param [out] ppField The Field object.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API(A3DStatus, A3DPDFPageGetField, (A3DPDFPage* pPage, const A3DUTF8Char* pcFieldName, A3DPDFField** ppField));

/*!
\ingroup a3d_pdf_widget_module
\brief Function to retrieve useful information regarding a field of a document

\param [in] pField The Field object to work with.
\param [out] pFieldData Data structure completed by the function. Contains the attributes of the field.
\version 6.0
*/
A3D_API(A3DStatus, A3DPDFFieldGetInformation, (A3DPDFField* pField, A3DPDFFieldData *pFieldData));

/*!
\ingroup a3d_pdf_widget_module
\brief Function to retrieve the position of a widget on a page.

Widgets are fields or more high-level entities.

\param [in] pWidget The Widget object to work with.
\param [out] pRect Data structure completed by the function. Contains the position of the widget.
\version 10.0
*/
A3D_API(A3DStatus, A3DPDFWidgetGetPosition, (A3DPDFWidget* pWidget, A3DPDFRectDData *pRect));

/*!
\ingroup a3d_pdf_widget_module
\brief Function to retrieve the name (identifier) of a widget

Widgets are fields or more high-level entities.

\param [in] pWidget The Widget object to work with.
\param [out] ppcWidgetId The identifier as a string.
\version 10.0
*/
A3D_API(A3DStatus, A3DPDFWidgetGetName, (A3DPDFWidget* pWidget, A3DUTF8Char** ppcWidgetId));


/*!
\ingroup a3d_pdf_widget_module
\brief Function to set the visibility of a form field

\param [in,out] pPage The Page object to work with.
\param [in] pcFieldName The name of the form field. Only Acroform fields are supported.
\param [in] bIsVisible Specifies if the field should be visible or not.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API(A3DStatus, A3DPDFPageFieldSetVisibility, (A3DPDFPage* pPage, const A3DUTF8Char* pcFieldName, const A3DBool bIsVisible));



/*!
\defgroup a3d_pdf_3Dannot_module 3D Annot Module
\ingroup a3d_pdf_3D_module
\brief Module to define a 3D Annotation

This module describes the functions and structures that allow you to define 3D data in the page.
*/


/*!
\defgroup a3d_pdf_3Dartwork_module 3D Artwork Module
\ingroup a3d_pdf_3D_module
\brief Module to define a 3D Artwork to add in a 3D Annotation

This module describes the functions and structures that allow you to define 3D data in the page.
*/
/*!
\ingroup a3d_pdf_3Dartwork_module
\brief A3DPDF3DArtworkData structure
\deprecated This structure is deprecated. Please use \ref A3DPDF3DArtworkData2 instead.
\version 4.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DPDF3DStream *m_pStream;					/*!< Specifies the 3D stream object created from the 3D data. */
	A3DUTF8Char* m_pcJavaScriptFileName;		/*!< A JavaScript file to store on the 3D Annot. */

	A3DBool m_bActivatePMICrossHighlight;		/*!<
													If true, some JavaScript code is generated so that when the user clicks on a markup, the associated surface is highlighted.
													Only works with Acrobat 11 and later. */
	A3DBool m_bAddPMISemanticInformation;		/*!< If true, the information on semantic PMI is added as nodes attributes on the model tree. */
	A3DBool m_bKeepNativeDefaultView;			/*!<
													If true, if there's a default view in the PRC modelfile: the default view in the resulting PDF will be that one, no matter
													if other default views are defined at PDF level.
													In other situations (no default view in the PRC modelfile; or the boolean is false): a default view is defined at the PDF level.
													A default view at the PDF level means a view programmatically created using \ref A3DPDFViewCreate, or a default view
													automatically created by HOOPS Publish if no default view is already defined.
													<br>This boolean is not used if the 3D stream is created with \ref A3DPDF3DStreamCreateFromFile. */
	A3DBool m_bUseRuntimeDisplaySettings;		/*!<
													This boolean controls which display settings (rendering style, background color and lighting) are applied to the 3D annotation
													when a view is activated.
													<ul>
													<li>If the boolean is true, all the views have initially the <b>default display settings</b>, then if the end-user manually changes
													the settings, this change affects all the views except the 'home' view (the view associated to the 'home' icon in Adobe Reader)
													for which it resets to the <b>default display settings</b>.
													The <b>default display settings</b> are the ones assigned to the default view: A3DPDF3DAnnotData values if the default view is a
													native or the automatic view, and A3DPDFViewData if the default view is a Publish view.
													</li>
													<li>If the boolean is false, activating a view resets the display settings as predefined programmatically on the view, whatever
													the user might manually change. The settings have A3DPDF3DAnnotData values and are all the same for native views or the automatic
													view; and they have A3DPDFViewData values for Publish views, which can be potentially different for each view.
													</li>
													</ul>
													Warning: all the display behaviors might be affected by a bug in Adobe Reader when the toolbar is not displayed. For a full control
													of display settings, it might be better to always display the toolbar. If not, unexpected rendering might be observed. */
	A3DPDFEAnimationStyle m_eAnimationStyle;	/*!< The animation style. */
	A3DUns32 m_iNumberOfAnimations;				/*!< Number of animations. \version 6.0 */
	A3DPDFAnimation** m_ppAnimations;			/*!< Animations. \version 6.0 */
	A3DBool m_bChangePMIColor;					/*!< If this value is true, the color of the markup in the artwork will be set to m_adRGB. If this value is false, the markup keeps the color it has in the CAD file, or the default color if it does not have a defined color. \version 8.0 */
	A3DPDFRgbColorData m_sPMIColor;				/*!< Color of the markup in the artwork if m_bChangePMIColor is set to true. \version 8.0 */
} A3DPDF3DArtworkData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_view_module
\brief Structure to define settings for the cross sections display.
These settings are used by HOOPS Publish at PDF creation on every native views that have a clipping plane defined.
These settings are not used at runtime with clipping plane created by Javascript.
\version 8.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bIsCuttingPlaneVisible;				/*!< Visibility of the cutting plane. */
	A3DDouble m_dOpacity;							/*!< Opacity of the cutting plane, between 0 (transparent) and 1 (opaque). */
	A3DPDFRgbColorData m_sCuttingPlaneColor;		/*!< Color of the cutting plane. */
	A3DBool m_bIsIntersectionVisible;				/*!< Visibility of the intersection of the cutting plane with any 3D geometry. */
	A3DPDFRgbColorData m_sIntersectionColor;		/*!< Color of the intersection of the cutting plane, if visible. */
	A3DBool m_bAddSectionCaps;						/*!< If true, show the capping faces. */
} A3DPDFSectionDisplayData;
#endif // A3DAPI_LOAD


/*!
\ingroup a3d_pdf_3Dartwork_module
\brief A3DPDF3DArtworkData2 structure
\version 8.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DPDF3DStream *m_pStream;						/*!< Specifies the 3D stream object created from the 3D data. */
	A3DUTF8Char* m_pcJavaScriptFileName;			/*!< A JavaScript file to store on the 3D Annot. */

	A3DBool m_bActivatePMICrossHighlight;			/*!<
														If true, some JavaScript code is generated so that when the user clicks on a markup, the associated surface is highlighted.
														Only works with Acrobat 11 and later. */
	A3DBool m_bAddPMISemanticInformation;			/*!< If true, the information on semantic PMI is added as nodes attributes on the model tree. */
	A3DBool m_bKeepNativeDefaultView;				/*!<
														If true, if there's a default view in the PRC modelfile: the default view in the resulting PDF will be that one, no matter
														if other default views are defined at PDF level.
														In other situations (no default view in the PRC modelfile; or the boolean is false): a default view is defined at the PDF level.
														A default view at the PDF level means a view programmatically created using \ref A3DPDFViewCreate, or a default view
														automatically created by HOOPS Publish if no default view is already defined.
														<br>This boolean is not used if the 3D stream is created with \ref A3DPDF3DStreamCreateFromFile. */
	A3DBool m_bUseRuntimeDisplaySettings;			/*!<
														This boolean controls which display settings (rendering style, background color and lighting) are applied to the 3D annotation
														when a view is activated.
														<ul>
														<li>If the boolean is true, all the views have initially the <b>default display settings</b>, then if the end-user manually changes
														the settings, this change affects all the views except the 'home' view (the view associated to the 'home' icon in Adobe Reader)
														for which it resets to the <b>default display settings</b>.
														The <b>default display settings</b> are the ones assigned to the default view: A3DPDF3DAnnotData values if the default view is a
														native or the automatic view, and A3DPDFViewData if the default view is a Publish view.
														</li>
														<li>If the boolean is false, activating a view resets the display settings as predefined programmatically on the view, whatever
														the user might manually change. The settings have A3DPDF3DAnnotData values and are all the same for native views or the automatic
														view; and they have A3DPDFViewData values for Publish views, which can be potentially different for each view.
														</li>
														</ul>
														Warning: all the display behaviors might be affected by a bug in Adobe Reader when the toolbar is not displayed. For a full control
														of display settings, it might be better to always display the toolbar. If not, unexpected rendering might be observed. */
	A3DPDFEAnimationStyle m_eAnimationStyle;		/*!< The animation style. */
	A3DUns32 m_iNumberOfAnimations;					/*!< Number of animations. \version 6.0 */
	A3DPDFAnimation** m_ppAnimations;				/*!< Animations. \version 6.0 */
	A3DBool m_bChangePMIColor;						/*!< If the boolean is true, the color of the markups in the artwork will be set to m_adRGB; otherwise, it will remain like it already was. \version 8.0 */
	A3DPDFRgbColorData m_sPMIColor;					/*!< Color of the markups in the artwork if m_bChangePMIColor is set to true. \version 8.0 */
	A3DPDFSectionDisplayData m_sDisplaySectionData;	/*!< Settings for the cross sections display. \version 8.1 */
	A3DBool m_bAddAdditionalGeometry;				/*!< If true, the curves related to edges highlighted by a markup will be computed (only if m_bActivatePMICrossHighlight is set to true. \version 9.1 */
	A3DBool m_bDisableJSForInitSetView;				/*!< If false, a javascript code is added to 3D annotation to generate a CameraEventHandler event when a defautl view is defined.
													This enables CameraEventHandler code to be launched at file is opening on the default view, and consequently to add specific additional development.
													If true, this javascript is not added. It is useful for PDF files that have to be edited with Acrobat to modify the default view. \version 11.1 */
} A3DPDF3DArtworkData2;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_3Dartwork_module
\brief Function to create the 3D Artwork of the 3D Annot.
\deprecated This function is deprecated. Please use \ref A3DPDF3DArtworkCreate2 instead.

The 3D Artwork object is primarily created with this function, and it should be stored on the 3D Annot through the structure \ref A3DPDF3DAnnotData.

\param [in,out] pDoc The Document object to work with.
\param [in] p3DArtworkData The 3D Artwork parameters.
\param [out] pp3DArtwork The 3D Artwork object created.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDF3DArtworkCreate, (A3DPDFDocument* pDoc, const A3DPDF3DArtworkData* p3DArtworkData, A3DPDF3DArtwork** pp3DArtwork));

/*!
\ingroup a3d_pdf_3Dartwork_module
\brief Function to create the 3D Artwork of the 3D Annot.

The 3D Artwork object is primarily created with this function, and it should be stored on the 3D Annot through the structure \ref A3DPDF3DAnnotData.

\param [in,out] pDoc The Document object to work with.
\param [in] p3DArtworkData The 3D Artwork parameters.
\param [out] pp3DArtwork The 3D Artwork object created.
\return \ref A3D_SUCCESS \n
\version 8.1
*/
A3D_API (A3DStatus, A3DPDF3DArtworkCreate2, (A3DPDFDocument* pDoc, const A3DPDF3DArtworkData2* p3DArtworkData, A3DPDF3DArtwork** pp3DArtwork));
/*!
\ingroup a3d_pdf_3Dannot_module
\brief A3DPDF3DAnnotData structure: options for the 3D Annot behavior in the PDF
\version 4.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DPDFEActivateWhen m_eActivateWhen;		/*!< Option to specify when the 3D Annot should be activated. */
	A3DPDFEDesactivateWhen m_eDesactivateWhen;	/*!< Option to specify when the 3D Annot should be deactivated. */
	A3DBool m_bShowToolbar;						/*!< Option to show or not show the 3D toolbar when the 3D Annot is activated. */
	A3DBool m_bOpenModelTree;					/*!< Option to show or not show the 3D model tree when the 3D Annot is activated. */
	A3DBool m_bDisableInteractivity;			/*!< Option to enable or disable the interactivity on the 3D scene. If disabled, only JavaScript actions can control the 3D Annot. */
	A3DInt32 m_iAppearanceBorderWidth;			/*!< Border width in points. */
	A3DBool m_bTransparentBackground;			/*!<
													If true, the background of the 3D window is transparent.
													Warning, a transparent background leads to Adobe bugs in Adobe Reader or Acrobat:
													the 3D Annot can't be printed, and the form fields are blinking when a field is triggered. */
	A3DPDFRgbColorData m_sBackgroundColor;		/*!<
													Display setting - background color of the 3D scene. Incompatible with \ref m_bTransparentBackground.
													See \ref A3DPDF3DArtworkData2::m_bUseRuntimeDisplaySettings to understand how this setting is applied. */
	A3DPDFELighting m_eLighting;				/*!< Display setting - lighting of the 3D scene. See \ref A3DPDF3DArtworkData2::m_bUseRuntimeDisplaySettings to understand how this setting is applied. */
	A3DPDFERenderingStyle m_eRenderingStyle;	/*!< Display setting - rendering style of the 3D scene. See \ref A3DPDF3DArtworkData2::m_bUseRuntimeDisplaySettings to understand how this setting is applied. */
	A3DPDF3DArtwork *m_p3DArtwork;				/*!< Specifies the 3D Artwork object which contains the 3D definition. */
	A3DPDFImage* m_pPosterImage;				/*!<
													Specifies a poster image to be displayed when the 3D Annot is not activated.
													If null, a poster is automatically created from the default view. To disable this automatic generation of poster,
													specify a blank image file.
													<p><strong>Important:</strong> \DRIVER_WARNING
													</p> */
	A3DUTF8Char* m_pcName;						/*!< Name of the 3D Annot. */
} A3DPDF3DAnnotData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_3Dannot_module
\brief Function to create a 3D Annotation object

The 3D Annotation object is primarily created with this function, and it should be inserted on the page with the functions \ref A3DPDFPageInsert3DAnnot or \ref A3DPDFPageFieldSet3DAnnot.

\param [in,out] pDoc The Document object to work with.
\param [in] p3DAnnotData The 3D Annot parameters.
\param [out] pp3DAnnot The 3D Annot object created.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDF3DAnnotCreate, (A3DPDFDocument* pDoc, const A3DPDF3DAnnotData* p3DAnnotData, A3DPDF3DAnnot** pp3DAnnot));

/*!
\ingroup a3d_pdf_3Dannot_module
\brief Function to insert a 3D Annot in a page

Insert a 3D Annot object on the page.
Several 3D Annots can be stored on a page.

\param [in,out] pPage The Page object to work with.
\param [in] p3DAnnot The 3D Annot to insert on the page.
\param [in] pRectData The rectangle to specify the position on the page.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFPageInsert3DAnnot, (A3DPDFPage* pPage, const A3DPDF3DAnnot* p3DAnnot, const A3DPDFRectData* pRectData));

/*!
\ingroup a3d_pdf_3Dannot_module
\brief Function to replace the form field specified with a 3D Annot object

Several 3D Annots can be stored on a page.

\param [in,out] pPage The Page object to work with.
\param [in] pcFieldName The name of the form field. Only Acroform fields are supported.
\param [in] p3DAnnot The 3D Annot to set.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFPageFieldSet3DAnnot, (A3DPDFPage* pPage, const A3DUTF8Char* pcFieldName, const A3DPDF3DAnnot* p3DAnnot));




/*!
\defgroup a3d_pdf_3Dstream_module 3D Stream Module
\ingroup a3d_pdf_3D_module
\brief Module to define a 3D Stream to add in a 3D Artwork

This module describes the functions and structures that allow you to add a 3D source in a 3D Annot.
*/


/*!
\ingroup a3d_pdf_3Dstream_module
\brief Function to create a PRC Stream from a Model File

The 3D Stream object is primarily created with this function, and it should be stored on the 3D Artwork through the structure \ref A3DPDF3DArtworkData2.
This function saves the modelfile in a PRC format, and creates a 3D Stream object from this data.

\param [in] pDoc The Document object to work with.
\param [in] pModelFile The Document object to work with.
\param [in] pParamsExportData The PRC export parameters.
\param [out] pp3DStream The 3DStream object created.
\param [in,out] ppPrcWriteHelper Used to get PRC data such as unique identifiers of PRC nodes.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDF3DStreamCreateFromModelFileAsPRC, (A3DPDFDocument* pDoc,
															 A3DAsmModelFile* pModelFile,
															 const A3DRWParamsExportPrcData* pParamsExportData,
															 A3DPDF3DStream** pp3DStream,
															 A3DRWParamsPrcWriteHelper** ppPrcWriteHelper));

/*!
\ingroup a3d_pdf_3Dstream_module
\brief Function to create a U3D Stream from Model File

The 3D Stream object is primarily created with this function, and it should be stored on the 3D Artwork through the structure \ref A3DPDF3DArtworkData2.
This function saves the modelfile in a U3D format, and creates a 3D Stream object from this data.

\param [in] pDoc The Document object to work with.
\param [in] pModelFile The Document object to work with.
\param [in] pParamsExportData The U3D export parameters.
\param [out] pp3DStream The 3DStream object created.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDF3DStreamCreateFromModelFileAsU3D, (A3DPDFDocument* pDoc,
															 A3DAsmModelFile* pModelFile,
															 const A3DRWParamsExportU3DData* pParamsExportData,
															 A3DPDF3DStream** pp3DStream));
/*!
\ingroup a3d_pdf_3Dstream_module
\brief Function to create a Stream from a file

\param [in] pDoc The Document object to work with.
\param [in] pcFileName The full file path of a PRC or U3D file.
\param [in] bIsPRC Specifies the format of the data (true is for PRC, false if for U3D).
\param [out] pp3DStream The 3DStream object created.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDF3DStreamCreateFromFile, (A3DPDFDocument* pDoc,
												   const A3DUTF8Char* pcFileName,
												   const A3DBool bIsPRC,
												   A3DPDF3DStream** pp3DStream));

/*!
\defgroup a3d_publish_modelfile_module ModelFile Module
\ingroup a3d_pdf_3D_module
\brief Module to get and compute information on modelfile.

This module describes the functions and structures that allow you to to get and compute information on a modelfile.
*/
/*!
\ingroup a3d_publish_modelfile_module
\brief A3DPDFSnapshotOptionsData structure: options for the snapshot (poster) generation
\version 4.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DInt32 m_iWidth;		/*!< Width in pixels. */
	A3DInt32 m_iHeight;		/*!< Height in pixels. */
	A3DPDFView* m_pView;	/*!< The view to be activated before the snapshot is generated. */
} A3DPDFSnapshotOptionsData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_publish_modelfile_module
\brief Function to create the poster for a view in a model file

<p>
<strong>Important:</strong> \DRIVER_WARNING
</p>

\param [in] pModelFile The modelfile where is defined the 3D data.
\param [in] pSnapshotOptionsData The snapshot parameters.
\param [in] pcOutputFilename Path to the file name to generate.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFMakeSnapshotFromModelFile, (A3DAsmModelFile* pModelFile,
													  const A3DPDFSnapshotOptionsData* pSnapshotOptionsData,
													  const A3DUTF8Char* pcOutputFilename));


/*!
\defgroup a3d_pdf_view_module View Module
\ingroup a3d_pdf_3D_module
\brief Module to access and create Views in a 3D Annot.

This module describes the functions and structures that allow you to define 3D views on a 3D Annot.
*/
/*!
\ingroup a3d_pdf_view_module
\brief Structure to define a Camera View
\version 4.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcInternalName;					/*!< Internal name of the view. */
	A3DUTF8Char* m_pcExternalName;					/*!< External name of the view. This name is used in JavaScript functions. */
	A3DVector3dData m_sPosition;					/*!< Camera position. */
	A3DVector3dData m_sTarget;						/*!< Target position where the camera is looking at. */
	A3DVector3dData m_sUpVector;					/*!<
														Camera up vector. It should be defined so that it is perpendicular to the vector created between the camera and target position.
														A non-perpendicular up vector results in a view which is tilting when it is activated in the Adobe PDF Reader. */
	A3DDouble m_dZoomFactor;						/*!<
														This parameter is only applicable for orthographic cameras.
														It specifies the part of the scene that can be visualized and inversely acts as a zoom factor with the following
														relationship: [zoomfactor=1/2*view_size], with view_size as the smallest size of the viewable part in the X or Y direction. */
	A3DBool m_bIsDefault;							/*!<
														Specifies that this view should be the default view (the view used when the 3D is activated).
														Only one view should be defined by default. */
	A3DPDFRgbColorData m_sViewBackgroundColor;		/*!< Display setting - background color for the view. */
	A3DPDFELighting m_eViewLighting;				/*!< Display setting - lighting for the view. */
	A3DPDFERenderingStyle m_eViewRenderingStyle;	/*!< Display setting - rendering style for the view. */
	A3DPDFEProjectionMode m_eProjectionMode;		/*!< Camera projection mode: orthographic or perspective. */
	A3DDouble m_dFieldOfView;						/*!< This parameter is only applicable for perspective cameras. It specifies the field of view as an angle in degrees. */
} A3DPDFViewData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_view_module
\brief Function to create a camera view

The View object is primarily created with this function, and it should be stored in the 3D Artwork with the function \ref A3DPDF3DArtworkAddView.

\param [in,out] pDoc The Document object to work with.
\param [in] pViewData The view parameters.
\param [out] ppView The View object created.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFViewCreate, (A3DPDFDocument* pDoc, const A3DPDFViewData* pViewData, A3DPDFView** ppView));

/*!
\ingroup a3d_pdf_view_module
\brief Adds a view on the 3D Annot

The view is a PDF object and can be activated with PDF actions.
\param [in,out] p3DArtwork The 3D Artwork object to work with.
\param [in] pView The View to be stored on the 3D Annot.

\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDF3DArtworkAddView, (A3DPDF3DArtwork* p3DArtwork, const A3DPDFView* pView));

/*!
\ingroup a3d_pdf_view_module
\brief Returns the views objects stored in the artwork.

The function returns an array of pointer on views objects.
The views returned can be issued from native CAD file, or could have been created with \ref A3DPDF3DArtworkAddView function.
Alternatively if no default view is defined, HOOPS Publish automatically creates one. This automatic view is returned at the end of the array.
Note that the views are created during the 3D annotation creation (\ref A3DPDF3DAnnotCreate),
and the automatic view during the 3D annotation insertion (\ref A3DPDFPageInsert3DAnnot).
Consequently, this function should be called after the call to \ref A3DPDFPageInsert3DAnnot to be sure to get the whole list of views.
\param [in] p3DArtwork The 3D Artwork object to work with.
\param [out] puiNbViews The size of the following array of views.
\param [out] pppViews The array of pointer on views objects created.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDF3DArtworkGetViews, (A3DPDF3DArtwork* p3DArtwork, A3DUns32* puiNbViews,  A3DPDFView*** pppViews));


/*!
\ingroup a3d_pdf_view_module
\brief Returns the identifier of the view

The identifier may be used by JavaScript or PDF actions to activate a view. It can also be used to generate poster.
The function returns a string.

\param [in] pView The View object to work with. This object can be retrieved using the function \ref A3DPDF3DArtworkGetViews
\param [out] ppcViewId The identifier as a string.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API (A3DStatus, A3DPDFViewGetExternalName, (A3DPDFView* pView, A3DUTF8Char** ppcViewId));

/*!
\ingroup a3d_pdf_3Dartwork_module
\brief Get the bounding sphere of the default view of the 3D Artwork

\param [in] p3DArtwork The 3D Artwork object to work with
\param [out] pdRadius The radius of the bounding sphere.
\param [out] pCenter The center of the bounding sphere.

\return \ref A3D_SUCCESS \n
\version 5.1
*/
A3D_API (A3DStatus, A3DPDF3DArtworkGetBoundingSphere, (A3DPDF3DArtwork* p3DArtwork, A3DDouble* pdRadius, A3DVector3dData* pCenter));

/*!
\defgroup a3d_pdf_animation_module Animation Module
\ingroup a3d_pdf_3D_module
\brief Module to define Animation

This module describes the functions and structures that allow you to define 3D animations on a 3D Annot.
*/

/*!
\ingroup a3d_pdf_animation_module
\brief Structure to define a transformation

\param [in] m_adMatrix[16] The 4x4 transformation matrix. The 0, 1, 2, 4, 5, 6, 8, 9 and 10 values create a 3x3 rotation matrix; the 12, 13 and 14 values are translation vector data; the 3, 7 and 11 values show the scale components; the 15 value is always 1.

\version 6.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble m_adMatrix[16];						/*!< Entity position, absolute coordinates. */
} A3DPDFAnimTransformationData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_animation_module
\brief Structure grouping the appearance characteristics
\version 6.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DPDFRgbColorData m_sColor;					/*!< Color of the entity. */
	A3DDouble m_dOpacity;							/*!< From 0.0 (transparent) to 1.0 (opaque) */
	A3DPDFERenderingStyle m_eRenderingStyle;		/*!< Rendering style. */
	A3DBool m_bResetAppearance;						/*!< Flag for resetting appearance */
} A3DPDFAnimAppearanceData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_animation_module
\brief Structure grouping the camera characteristics
\version 6.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DVector3dData m_sPosition;					/*!< Camera position. */
	A3DVector3dData m_sTarget;						/*!< Target position where the camera is looking at. */
	A3DVector3dData m_sUpVector;					/*!<
														Camera up vector. It should be defined so that it is perpendicular to the vector created between the camera and target position.
														A non-perpendicular up vector results in a view which is tilting when it is activated in the Adobe PDF Reader. */
	A3DDouble m_dZoomFactor;						/*!<
														This parameter is only applicable for orthographic cameras.
														It specifies the part of the scene that can be visualized and inversely acts as a zoom factor with the following
														relationship: [zoomfactor=1/2*view_size], with view_size as the smallest size of the viewable part in the X or Y direction. */
	A3DPDFEProjectionMode m_eMode;					/*!< Camera projection mode: orthographic or perspective. Assuming that it won't change during the animation. */
	A3DDouble m_dFieldOfView;						/*!< This parameter is only applicable for perspective cameras. It specifies the field of view as an angle in degrees. */
} A3DPDFAnimCameraData;
#endif // A3DAPI_LOAD



/*!
\ingroup a3d_pdf_animation_module
\brief Structure to define a keyframe

\param [in] m_iInterpolationMask Use \ref kA3DPDFInterpolateTransformationMatrix | \ref kA3DPDFInterpolateAppearanceColor | \ref kA3DPDFInterpolateAppearanceTransparency | \ref kA3DPDFInterpolateCamera

\version 6.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;								// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble m_dTime;										/*!< Time of the keyframe. Added to the time offset of the motion. */
	A3DPDFAnimTransformationData *m_psTransformationData;	/*!< Data concerning the transformation. */
	A3DPDFAnimAppearanceData *m_psAppearanceData;			/*!< Data concerning the appearance. */
	A3DPDFAnimCameraData *m_psCameraData;					/*!< Data concerning the camera. */
	A3DInt32 m_iInterpolationMask;							/*!< Bits which indicates which data must be interpolated.  \sa a3d_pdf_animation_interpolbits */
} A3DPDFAnimKeyFrameData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdf_animation_module
\brief Function to create a keyframe

\param [in] pKeyFrameData The keyframe parameters.
\param [out] ppKeyFrame The keyframe object created.
\return \ref A3D_SUCCESS \n
\version 6.1
*/
A3D_API (A3DStatus, A3DPDFAnimKeyFrameCreate, (const A3DPDFAnimKeyFrameData* pKeyFrameData, A3DPDFAnimKeyFrame** ppKeyFrame));

/*!
\ingroup a3d_pdf_animation_module
\brief Structure to define an animation motion
\deprecated This structure is deprecated. Please use \ref A3DPDFAnimMotionData2 instead.
\version 6.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DInt32 m_iNumTargets;							/*!< Number of targets. */
	A3DMiscMarkupLinkedItem** m_ppTargets;			/*!< Targets of the motion: product occurence, part, markup. */
	A3DInt32 m_iNumKeyFrames;						/*!< Number of keyframes. */
	A3DPDFAnimKeyFrame **m_ppKeyFrames;				/*!< Array of keyframes. */
	A3DDouble m_dTimeOffset;						/*!< Time to start the first keyframe. */
	A3DBool m_bRepeat;								/*!< If true, the motion is repeated infinitely. */
} A3DPDFAnimMotionData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_animation_module
\brief Structure to define a target entity
\version 7.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;									// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DEntity *m_pTargetEntity;									/*!< Entity: product occurrence, part, markup. */
	A3DAsmProductOccurrence *m_pFatherProductOccurrence;		/*!< Father Product Occurrence of the target entity, NULL if the target is a product occurrence. */
} A3DPDFTargetEntityData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdf_animation_module
\brief Structure to define a target entity of a motion
\return \ref A3DPDF_BAD_PARAMETERS if wrong parameters are given : see \ref A3DPDFTargetEntityData\n
\return \ref A3D_SUCCESS\n
\version 7.1
*/
A3D_API (A3DStatus, A3DPDFTargetEntityCreate, (const A3DPDFTargetEntityData* pTargetData, A3DPDFTargetEntity** ppTargetEntity));


/*!
\ingroup a3d_pdf_animation_module
\brief Utility function to retrieve the targets from their names.

\param [in] pModelFile The model file which contains the targets.
\param [in] iNamesSize The number of names.
\param [in] ppNames An array of names of size \ref iNamesSize.
\param [in] piIndexes If the targeted entity had sons, index of the son in the model file tree. Must be -1 if no sons are wanted.
\param [out] ppTargetEntities The targeted entities retrieved. If several entities in the model file tree have the same name,
all these entities are in the array.
\param [out] ppiDataSizes For each name, the number of found entities.
\return \ref A3D_ERROR if wrong parameters are given \n
\return \ref A3D_SUCCESS\n
\version 7.1
*/
A3D_API(A3DStatus, A3DPDFGetEntitiesFromName, (A3DAsmModelFile* pModelFile,
	A3DInt32 iNamesSize,
	A3DUTF8Char** ppNames,
	A3DInt32* piIndexes,
	A3DPDFTargetEntity**** ppTargetEntities,
	A3DInt32** ppiDataSizes));
/*!
\ingroup a3d_pdf_animation_module
\brief Structure to define an animation motion.
\version 7.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;						// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_iNumTargets;							/*!< Number of targets. */
	A3DPDFTargetEntity** m_ppTargets;				/*!< Targets of the motion: product occurrence, part, markup. */
	A3DUns32 m_iNumKeyFrames;						/*!< Number of keyframes. */
	A3DPDFAnimKeyFrame **m_ppKeyFrames;				/*!< Array of keyframes. */
	A3DDouble m_dTimeOffset;						/*!< Time to start the first keyframe. */
	A3DBool m_bRepeat;								/*!< If true, the motion is repeated infinitely. */
} A3DPDFAnimMotionData2;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdf_animation_module
\brief Function to create an animation motion
\deprecated This function is deprecated. Please use \ref A3DPDFAnimMotionCreate2 instead.

\param [in] pMotionData The motion parameters.
\param [out] ppAnimationMotion The motion object created.
\return \ref A3D_SUCCESS \n
\version 6.1
*/
A3D_API (A3DStatus, A3DPDFAnimMotionCreate, (const A3DPDFAnimMotionData* pMotionData, A3DPDFAnimMotion** ppAnimationMotion));

/*!
\ingroup a3d_pdf_animation_module
\brief Function to create an animation motion.

\param [in] pMotionData The motion parameters.
\param [out] ppAnimationMotion The motion object created.
\return \ref A3D_SUCCESS \n
\version 7.1
*/
A3D_API (A3DStatus, A3DPDFAnimMotionCreate2, (const A3DPDFAnimMotionData2* pMotionData, A3DPDFAnimMotion** ppAnimationMotion));
/*!
\ingroup a3d_pdf_animation_module
\brief Structure to define an animation
\version 6.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;							// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcName;								/*!< Name of the animation. */
	A3DUns32 m_iNumAnimationMotions;					/*!< Number of motions. */
	A3DPDFAnimMotion **m_ppAnimationMotions;			/*!< Array of motions. */
	A3DInt32 m_iFramesPerSecond;						/*!< Number of frames per second. */
} A3DPDFAnimationData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdf_animation_module
\brief Function to create an animation

\param [in] pAnimationData The animation parameters.
\param [out] ppAnimation The animation object created.
\return \ref A3D_SUCCESS \n
\return \ref A3DPDF_ANIMATION_NULL_MOTION if one or several motions are null.\n
\version 6.1
*/
A3D_API (A3DStatus, A3DPDFAnimationCreate, (const A3DPDFAnimationData* pAnimationData, A3DPDFAnimation** ppAnimation));


/*!
\ingroup a3d_pdf_animation_module
\brief Debug function to edit the JavaScript stream corresponding to the animation data in a file.

If bUseAnimationFile is set to false,
the 'standard' JavaScript of the animation will be created from the animation data. If true, the output file will be used instead of the animation data.
To use the new content, the code execution should be stopped after the call to this function, then the code modified, then the execution restarted.
This function must be called before A3DPDF3DAnnotCreate.
\param [in,out] p3DArtwork The artwork which contains the animation.
\param [in] pcJavascriptFile The path of the file in which the JavaScript will be outputted. The content of the file can be modified by the user.
\param [in] bUseAnimationFile If true, the modified content will be used to generate the final PDF file.
\return \ref A3D_SUCCESS \n
\version 6.1
*/
A3D_API (A3DStatus, A3DPDF3DArtworkEditAnimationJavascript, (A3DPDF3DArtwork* p3DArtwork, const A3DUTF8Char* pcJavascriptFile, A3DBool bUseAnimationFile));

/*!
\defgroup a3d_pdffield_button Button Module
\ingroup a3d_pdf_widget_module
\brief Module to access and define Button field

This module describes the functions and structures that allow you to define a Button.
A Button is an Acrobat Field.
*/

/*!
\ingroup a3d_pdffield_button
\brief Structure to define a button field
\version 7.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;							// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcName;								/*!< Name of the field (it serves as the field identifier, it should be unique in the document). */
	A3DUTF8Char* m_pcTooltip;							/*!<
															Displayed text that the hesitant user may find helpful in filling in the form field.
															Tooltips appear when the pointer hovers briefly over the form field. */
	A3DPDFEFormField m_eFormField;						/*!< Specifies whether the form field can be seen, either on screen or in print. */
	A3DPDFETextOrientation m_eTextOrientation;			/*!< Orientation of the text inside the field. */
	A3DBool m_bIsReadOnly;								/*!< Defines if the field is editable. */
	A3DBool m_bIsRequired;								/*!< Defines if the field is required. */
	A3DBool m_bIsLocked;								/*!< If locked, prevents any further changes to any form field properties (in Acrobat 'Properties' dialog box). */
	A3DBool m_bHasBorder;								/*!< If true, there is a border around the field. */
	A3DPDFRgbColorData m_sBorderColor;					/*!< Color of the border of the field (if any). */
	A3DPDFEThicknessBorder m_eThicknessBorder;			/*!< Thickness of the border. */
	A3DPDFELineStyleBorder m_eLineStyleBorder;			/*!< Line style of the border. */
	A3DBool m_bHasFillColor;							/*!< If true, the following field (m_sFillColor) is used. */
	A3DPDFRgbColorData m_sFillColor;					/*!< Color which the inside of the field will be filled with (if m_bHasFillColor is true). */
	A3DPDFRgbColorData m_sTextColor;					/*!< Color of the text. */
	A3DUTF8Char* m_pcFontName;							/*!<
															Font name of the text.
		It should be the name of the font as appearing in Acrobat font list, with style separated with a comma.
		For example, 'Calibri Bold Italic' should be specified as "Calibri,BoldItalic".
		Also, specific values exist for Adobe default fonts:
		eTimesRoman is "TiRo" ; eTimesItalic is  "TiIt" ; eTimesBold is "TiBo" ; eTimesBoldItalic is "TiBI" ;
		eHelvetica is "Helv" ; eHelveticaOblique is "HeOb" ; eHelveticaBold is "HeBo" ; eHelveticaBoldOblique is "HeBO" ;
		eCourier is "Cour" ; eCourierOblique is "CoOb" ; eCourierBold is "CoBo" ; eCourierBoldOblique is "CoBO". */
	A3DInt32 m_iFontSize; 								/*!< Size of the font. */
	A3DPDFELayoutTextIcon m_eLayoutTextIcon;			/*!< Position of the label of the button relative to its icon. */
	A3DPDFELinkHighlightMode m_eHighlightingMode;		/*!< Highlighting mode (the visual effect that shall be used when the mouse button is pressed or held down inside its active area). */
	A3DUTF8Char* m_pcLabel;								/*!< Label of the button. */
	A3DPDFImage* m_pImage;								/*!< The image to store on the button as icon. */
} A3DPDFButtonData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdffield_button
\brief Function to create a button

The button is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsertButton.

\param [in,out] pDoc The Document object to work with.
\param [in] pButtonData The button parameters. The name is mandatory.
\param [out] ppButton The button created.
\return \ref A3D_SUCCESS \n
\version 7.2
*/
A3D_API (A3DStatus, A3DPDFButtonCreate, (A3DPDFDocument* pDoc, const A3DPDFButtonData* pButtonData, A3DPDFButton** ppButton));


/*!
\ingroup a3d_pdffield_button
\brief Function to insert a button in a page

\param [in,out] pPage The Page object to work with.
\param [in] pButton The button object to insert on the page.
\param [in] pRectData The rectangle to specify the position on the page.
\return \ref A3D_SUCCESS \n
\version 7.2
*/
A3D_API (A3DStatus, A3DPDFPageInsertButton, (A3DPDFPage* pPage, A3DPDFButton* pButton, const A3DPDFRectData* pRectData));


/*!
\ingroup a3d_pdffield_button
\brief Function to insert a button in a table

\param [in,out] pPage The Page object to work with.
\param [in] pTable The Table object to insert in the table. \sa a3d_pdf_table_module.
\param [in] pButton The Button object to insert in the table.
\param [in] iRowIndex The index of the row of the table's cell to insert the button (starts from 1).
\param [in] iColumnIndex The index of the column of the table's cell to insert the button (starts from 1).
\return \ref A3D_SUCCESS \n
\version 7.2
*/
A3D_API (A3DStatus, A3DPDFPageInsertButtonInTable, (A3DPDFPage* pPage, A3DPDFTable* pTable, A3DPDFButton* pButton, A3DInt32 iRowIndex, A3DInt32 iColumnIndex));

/*!
\ingroup a3d_pdffield_button
\brief Function to set the label of a button form field

\param [in,out] pPage The Page object to work with.
\param [in] pcFieldName The name of the button form field. Only Acroform fields are supported.
\param [in] pcLabel The label to set to the field.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API(A3DStatus, A3DPDFPageFieldButtonSetLabel, (A3DPDFPage* pPage, const A3DUTF8Char* pcFieldName, const A3DUTF8Char* pcLabel));

/*!
\ingroup a3d_pdffield_button
\brief Function to set the label of a button form field

\param [in,out] pButton The Button object to work with.
\param [in] pcLabel The label to set to the field.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API(A3DStatus, A3DPDFButtonSetLabel, (A3DPDFButton* pButton, const A3DUTF8Char* pcLabel));

/*!
\ingroup a3d_pdffield_button
\brief Function to set the icon of a button form field

The image is defined from a \ref A3DPDFImage object. It is always scaled non-proportionally, which means always adapted and stretched
to the size of the field. That way, the members m_iWidth and m_iHeight of \ref A3DPDFImageData are not used by this function.

\param [in,out] pPage The Page object to work with.
\param [in] pcFieldName The name of the form field. Only Acroform fields are supported.
\param [in] pImage The image to store on the button field.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API(A3DStatus, A3DPDFPageFieldButtonSetIcon, (A3DPDFPage* pPage, const A3DUTF8Char* pcFieldName, const A3DPDFImage* pImage));

/*!
\ingroup a3d_pdffield_button
\brief Function to set the icon of a button form field

The image is defined from a \ref A3DPDFImage object. It is always scaled non-proportionally, which means always adapted and stretched
to the size of the field. That way, the members m_iWidth and m_iHeight of \ref A3DPDFImageData are not used by this function.

\param [in,out] pButton The Button object to work with.
\param [in] pImage The image to store on the button field.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API(A3DStatus, A3DPDFButtonSetIcon, (A3DPDFButton* pButton, const A3DPDFImage* pImage));



/*!
\defgroup a3d_pdffield_text Text Field Module
\ingroup a3d_pdf_widget_module
\brief Module to access and define Text field

This module describes the functions and structures that allow you to define a Text Field.
A Text Field is an Acrobat Field.
*/
/*!
\ingroup a3d_pdffield_text
\brief Structure to define a text field
\version 7.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;							// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcName;								/*!< Name of the field (it serves as the field identifier, it should be unique in the document). */
	A3DUTF8Char* m_pcTooltip;							/*!<
															Displayed text that the hesitant user may find helpful in filling in the form field.
															Tooltips appear when the pointer hovers briefly over the form field. */
	A3DPDFEFormField m_eFormField;						/*!< Specifies whether the form field can be seen, either on screen or in print. */
	A3DPDFETextOrientation m_eTextOrientation;			/*!< Orientation of the text inside the field. */
	A3DBool m_bIsReadOnly;								/*!< Defines if the field is editable. */
	A3DBool m_bIsRequired;								/*!< Defines if the field is required. */
	A3DBool m_bIsLocked;								/*!< If locked, prevents any further changes to any form field properties (in Acrobat 'Properties' dialog box). */
	A3DBool m_bHasBorder;								/*!< If true, there is a border around the field. */
	A3DPDFRgbColorData m_sBorderColor;					/*!< Color of the border of the field (if any). */
	A3DPDFEThicknessBorder m_eThicknessBorder;			/*!< Thickness of the border. */
	A3DPDFELineStyleBorder m_eLineStyleBorder;			/*!< Line style of the border. */
	A3DBool m_bHasFillColor;							/*!< If true, the following field (m_sFillColor) is used. */
	A3DPDFRgbColorData m_sFillColor;					/*!< Color which the inside of the field will be filled with (if m_bHasFillColor is true). */
	A3DPDFRgbColorData m_sTextColor;					/*!< Color of the text. */
	A3DUTF8Char* m_pcFontName;							/*!<
															Font name of the text.
		It should be the name of the font as appearing in Acrobat font list, with style separated with a comma.
		For example, 'Calibri Bold Italic' should be specified as "Calibri,BoldItalic".
		For rich texts (if m_bAllowRichText is true), it is particularly important to specify the font name as defined in system folder, including spaces.
		For example, SegoeUI font should really be specifuy for rich texts as 'Segoe UI'.
		Also, specific values exist for Adobe default fonts:
		eTimesRoman is "TiRo" ; eTimesItalic is  "TiIt" ; eTimesBold is "TiBo" ; eTimesBoldItalic is "TiBI" ;
		eHelvetica is "Helv" ; eHelveticaOblique is "HeOb" ; eHelveticaBold is "HeBo" ; eHelveticaBoldOblique is "HeBO" ;
		eCourier is "Cour" ; eCourierOblique is "CoOb" ; eCourierBold is "CoBo" ; eCourierBoldOblique is "CoBO". */
	A3DInt32 m_iFontSize; 								/*!< Size of the font. */
	A3DPDFETextAlignment m_eTextAlignment;				/*!< Alignment of the text inside the text field. */
	A3DUTF8Char* m_pcDefaultValue;						/*!< Default text field content. This is the text value to be used,
														but also the default value to which the field reverts when a reset-form action is executed.
														The default value can be in xml format for rich text. */
	A3DBool m_bCheckSpelling;							/*!< Defines if the text entered in the field must be spell-checked. */
	A3DBool m_bIsMultiLine;								/*!< Defines if the text field is multi line. */
	A3DBool m_bDoNotScroll;								/*!< Defines if the text field is scrollable or not. */
	A3DBool m_bAllowRichText;							/*!< If set, the value of this field shall be a rich text string. */
	A3DUTF8Char* m_pcRichTextValue;						/*!< If m_bAllowRichText is true, this value defines the field content in xml format
		following rich text conventions specified for the XML Forms Architecture (XFA) specification. */
} A3DPDFTextFieldData;
#endif // A3DAPI_LOAD


/*!
\ingroup a3d_pdffield_text
\brief Function to create a text field

The text field is primarily created with this function, and it should be positioned on the page with the function \ref A3DPDFPageInsertTextField.

\param [in,out] pDoc The Document object to work with.
\param [in] pTextFieldData The text field parameters.
\param [out] ppTextField The text field created.
\return \ref A3D_SUCCESS \n
\version 7.2
*/
A3D_API (A3DStatus, A3DPDFTextFieldCreate, (A3DPDFDocument* pDoc, const A3DPDFTextFieldData* pTextFieldData, A3DPDFTextField** ppTextField));


/*!
\ingroup a3d_pdffield_text
\brief Function to insert a text field in a page.

The text field must have a name before inserting it.

\param [in,out] pPage The Page object to work with.
\param [in] pTextField The Text field object to insert on the page.
\param [in] pRectData The rectangle to specify the position on the page.
\return \ref A3D_SUCCESS \n
\version 7.2
*/
A3D_API (A3DStatus, A3DPDFPageInsertTextField, (A3DPDFPage* pPage, A3DPDFTextField* pTextField, const A3DPDFRectData* pRectData));

/*!
\ingroup a3d_pdffield_text
\brief Function to insert a text field in a table

\param [in,out] pPage The Page object to work with.
\param [in] pTable The Table object to insert in the table. \sa a3d_pdf_table_module.
\param [in] pTextField The Text field object to insert in the table.
\param [in] iRowIndex The index of the row of the table's cell to insert the button (start from 1)
\param [in] iColumnIndex The index of the column of the table's cell to insert the button (start from 1)
\return \ref A3D_SUCCESS \n
\version 7.2
*/
A3D_API (A3DStatus, A3DPDFPageInsertTextFieldInTable, (A3DPDFPage* pPage, A3DPDFTable* pTable, A3DPDFTextField* pTextField, A3DInt32 iRowIndex, A3DInt32 iColumnIndex));

/*!
\ingroup a3d_pdffield_text
\brief Function to set the text of a text form field

\param [in,out] pPage The Page object to work with.
\param [in] pcFieldName The name of the form field. Only Acroform fields are supported.
\param [in] pcValue The value to set to the text field. Text fields set as multiline can contain carriage return characters, they must be '\\r'.
\return \ref A3D_SUCCESS \n
\version 4.1
*/
A3D_API(A3DStatus, A3DPDFPageFieldTextSetValue, (A3DPDFPage* pPage, const A3DUTF8Char* pcFieldName, const A3DUTF8Char* pcValue));

/*!
\ingroup a3d_pdffield_text
\brief Function to set the text of a text form field

\param [in,out] pTextField The Text field object to work with.
\param [in] pcValue The value to set to the text field. Text fields set as multiline can contain carriage return characters, they must be '\\r'.
\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API(A3DStatus, A3DPDFTextFieldSetValue, (A3DPDFTextField* pTextField, const A3DUTF8Char* pcValue));


/*!
\defgroup a3d_pdffield_signature Digital Signature Module
\ingroup a3d_pdf_widget_module
\brief Module to access and define Digital Signature field

This module describes the functions and structures that allow you to define a Digital Signature.
A Digital Signature is an Acrobat Field.
*/
/*!
\ingroup a3d_pdffield_signature
\brief Structure to define a digital signature field
\version 8.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;							// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char* m_pcName;								/*!< Name of the field (it serves as the field identifier, it should be unique in the document). */
	A3DUTF8Char* m_pcTooltip;							/*!<
															Displayed text that the hesitant user may find helpful in filling in the form field.
															Tooltips appear when the pointer hovers briefly over the form field. */
	A3DPDFEFormField m_eFormField;						/*!< Specifies whether the form field can be seen, either on screen or in print. */
	A3DPDFETextOrientation m_eTextOrientation;			/*!< Orientation of the text inside the field. */
	A3DBool m_bIsReadOnly;								/*!< Defines if the field is editable. */
	A3DBool m_bIsRequired;								/*!< Defines if the field is required. */
	A3DBool m_bIsLocked;								/*!< If locked, prevents any further changes to any form field properties (in Acrobat 'Properties' dialog box). */
	A3DBool m_bHasBorder;								/*!< If true, there is a border around the field. */
	A3DPDFRgbColorData m_sBorderColor;					/*!< Color of the border of the field (if any). */
	A3DPDFEThicknessBorder m_eThicknessBorder;			/*!< Thickness of the border. */
	A3DPDFELineStyleBorder m_eLineStyleBorder;			/*!< Line style of the border. */
	A3DBool m_bHasFillColor;							/*!< If true, the following field (m_sFillColor) is used. */
	A3DPDFRgbColorData m_sFillColor;					/*!< Color which the inside of the field will be filled with (if m_bHasFillColor is true). */
	A3DPDFRgbColorData m_sTextColor;					/*!< Color of the text. */
	A3DUTF8Char* m_pcFontName;							/*!<
															Font name of the text.
		It should be the name of the font as appearing in Acrobat font list, with style separated with a comma.
		For example, 'Calibri Bold Italic' should be specified as "Calibri,BoldItalic".
		Also, specific values exist for Adobe default fonts:
		eTimesRoman is "TiRo" ; eTimesItalic is  "TiIt" ; eTimesBold is "TiBo" ; eTimesBoldItalic is "TiBI" ;
		eHelvetica is "Helv" ; eHelveticaOblique is "HeOb" ; eHelveticaBold is "HeBo" ; eHelveticaBoldOblique is "HeBO" ;
		eCourier is "Cour" ; eCourierOblique is "CoOb" ; eCourierBold is "CoBo" ; eCourierBoldOblique is "CoBO". */
	A3DInt32 m_iFontSize; 								/*!< Size of the font. */
} A3DPDFDigitalSignatureData;
#endif // A3DAPI_LOAD
/*!
\ingroup a3d_pdffield_signature
\brief Function to create a DigitalSignature

The DigitalSignature is primarily created with this function, and it should be positionned on the page with the function \ref A3DPDFPageInsertDigitalSignature.

\param [in,out] pDoc The Document object to work with.
\param [in] pDigitalSignatureData The DigitalSignature parameters. The name is mandatory.
\param [out] ppDigitalSignature The DigitalSignature created.
\return \ref A3D_SUCCESS \n
\version 8.0
*/
A3D_API (A3DStatus, A3DPDFDigitalSignatureCreate, (A3DPDFDocument* pDoc, const A3DPDFDigitalSignatureData* pDigitalSignatureData, A3DPDFDigitalSignature** ppDigitalSignature));


/*!
\ingroup a3d_pdffield_signature
\brief Function to insert a DigitalSignature in a page

\param [in,out] pPage The Page object to work with.
\param [in] pDigitalSignature The DigitalSignature object to insert on the page.
\param [in] pRectData The rectangle to specify the position on the page.
\return \ref A3D_SUCCESS \n
\version 8.0
*/
A3D_API (A3DStatus, A3DPDFPageInsertDigitalSignature, (A3DPDFPage* pPage, A3DPDFDigitalSignature* pDigitalSignature, const A3DPDFRectData* pRectData));



/*!
\defgroup a3d_pdf_2ddrawings_module 2D Drawing Module
\ingroup a3d_pdf_layout_static_module
\brief Module to create 2D Drawings on a PDF page
*/

/*!
\ingroup a3d_pdf_2ddrawings_module
\brief Structure that defines a graphic dash pattern.
\version 12.2

The dash pattern controls the pattern of dashes and gaps used to stroke paths. It is specified by a dash array and a dash phase.
The dash array's elements are numbers that specify the lengths of alternating dashes and gaps; the numbers shall be nonnegative and not all zero. 
The dash phase specifies the distance into the dash pattern at which to start the dash.
Before beginning to stroke a path, the dash array is cycled through, adding up the lengths of dashes and gaps. 
When the accumulated length equals the value specified by the dash phase, stroking of the path begins, and the dash array is used cyclically from that point onward. 

Due to internal limitations, m_pdDashes length should not exceed 11.
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;       // Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_iDashesLength;		/*!< The size of \ref m_pdDashes. Maximum allowed is 11. */
	A3DDouble *m_pdDashes;			/*!< The dashes.*/
	A3DDouble m_dDashPhase;			/*!< The dash phase. */
} A3DPDFDashPatternData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_2ddrawings_module
\brief Structure that defines a graphic rectangle.
\version 9.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;       // Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DPDFRectDData m_sRectangleData;	/*!< The coordinates of the rectangle. */
	A3DPDFRgbColorData m_sLineColor;	/*!< The color of the rectangle contour. */
	A3DPDFRgbColorData *m_pFillColor;	/*!< Optional: the filling color. */
	A3DDouble m_dWidth;					/*!< The width of the rectangle. */
	A3DDouble m_dDashSize;				/*!< The size of the visible part of a dashed line in points. If O the line will be plain. */
	A3DDouble m_dGapSize;				/*!< The size of the invisible part of a dashed line in points. If O the line will be plain. */
	A3DPDFDashPatternData *m_pDashPattern;	/*!< Optional: the dash pattern. If m_pDashPattern is defined, it supersedes m_dDashSize and m_dGapSize.*/
} A3DPDFGraphicRectangleData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_2ddrawings_module
\brief Function to create a graphic line

\param [in,out] pPage The Page object to work with.
\param [in] pGraphicRectangleData The Graphic rectangle parameters.
\return \ref A3D_SUCCESS \n
\version 9.1
*/
A3D_API (A3DStatus, A3DPDFPageDrawRectangle,(A3DPDFPage* pPage, const A3DPDFGraphicRectangleData* pGraphicRectangleData));


/*!
\ingroup a3d_pdf_2ddrawings_module
\brief Structure that defines a graphic line.
\version 9.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;      // Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble m_dXStartPoint;			/*!< The x coordinate of start point of the line. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DDouble m_dYStartPoint;			/*!< The y coordinate of the start point of the line. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DDouble m_dXEndPoint;				/*!< The x coordinate of the end point of the line. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DDouble m_dYEndPoint;				/*!< The y coordinate of the end point of the line. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DPDFRgbColorData m_sLineColor;	/*!< The color of the line. */
	A3DDouble m_dWidth;					/*!< The width of the line. */
	A3DDouble m_dDashSize;				/*!< The size of the visible part of a dashed line in points. If O the line will be plain. */
	A3DDouble m_dGapSize;				/*!< The size of the invisible part of a dashed line in points. If O the line will be plain. */
	A3DPDFDashPatternData *m_pDashPattern;	/*!< Optional: the dash pattern. If m_pDashPattern is defined, it supersedes m_dDashSize and m_dGapSize.*/
} A3DPDFGraphicLineData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_2ddrawings_module
\brief Function to create a graphic line

\param [in,out] pPage The page object to work with.
\param [in] pGraphicLineData The Graphic line parameters.
\return \ref A3D_SUCCESS \n
\version 9.1
*/
A3D_API (A3DStatus, A3DPDFPageDrawLine, (A3DPDFPage* pPage, const A3DPDFGraphicLineData* pGraphicLineData));


/*!
\ingroup a3d_pdf_2ddrawings_module
\brief Structure that defines a graphic circle arc.
\version 9.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;      // Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble m_dXStartPoint;			/*!< The x coordinate of start point of the arc. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DDouble m_dYStartPoint;			/*!< The y coordinate of the start point of the arc. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DDouble m_dXEndPoint;				/*!< The x coordinate of the end point of the arc. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DDouble m_dYEndPoint;				/*!< The y coordinate of the end point of the arc. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DDouble m_dXCenterPoint;			/*!< The x coordinate of the center point of the arc. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DDouble m_dYCenterPoint;			/*!< The y coordinate of the center point of the arc. The coordinate origin (0, 0) is the bottom left of the page. The unit is point. */
	A3DPDFRgbColorData m_sArcColor;		/*!< The color of the arc. */
	A3DPDFRgbColorData *m_pFillColor;	/*!< Optional: the filling color. */
	A3DDouble m_dWidth;					/*!< The width of the line. */
	A3DDouble m_dDashSize;				/*!< The size of the visible part of a dashed line in points. If O the line will be plain. */
	A3DDouble m_dGapSize;				/*!< The size of the invisible part of a dashed line in points. If O the line will be plain. */
	A3DPDFDashPatternData *m_pDashPattern;	/*!< Optional: the dash pattern. If m_pDashPattern is defined, it supersedes m_dDashSize and m_dGapSize.*/
} A3DPDFGraphicArcData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_2ddrawings_module
\brief Function to create a graphic circle arc

\param [in,out] pPage The page object to work with.
\param [in] pGraphicArcData The Graphic arc parameters.
\return \ref A3D_SUCCESS \n
\version 9.1
*/
A3D_API (A3DStatus, A3DPDFPageDrawArc, (A3DPDFPage* pPage, const A3DPDFGraphicArcData* pGraphicArcData));


/*!
\ingroup a3d_pdf_2ddrawings_module
\brief Structure that defines a graphic bezier curve.
\version 9.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;        // Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_iNumberOfPoints;			/*!< The size of \ref m_pdPoints expressed in number of points. */
	A3DDouble *m_pdPoints;				/*!< The coordinates x, y of the points. The coordinate origin (0, 0) is the bottom left of the page. The unit is point.
											 They must be given as x1, y1, x2, y2, x3, y3, ..., xn, yn. So the size of the array will be 2*m_iNumberOfPoints.*/
	A3DPDFRgbColorData m_sCurveColor;	/*!< The color of the curve. */
	A3DPDFRgbColorData *m_pFillColor;	/*!< Optional: the filling color. */
	A3DDouble m_dWidth;					/*!< The width of the curve. */
	A3DDouble m_dDashSize;				/*!< The size of the visible part of a dashed curve in points. If O the curve will be plain. */
	A3DDouble m_dGapSize;				/*!< The size of the invisible part of a dashed curve in points. If O the curve will be plain. */
	A3DPDFDashPatternData *m_pDashPattern;	/*!< Optional: the dash pattern. If m_pDashPattern is defined, it supersedes m_dDashSize and m_dGapSize.*/
} A3DPDFGraphicBezierCurveData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_2ddrawings_module
\brief Function to create a graphic bezier curve

\param [in,out] pPage The page object to work with.
\param [in] pGraphicBezierCurveData The Graphic Bezier curve parameters.
\return \ref A3D_SUCCESS \n
\version 9.1
*/
A3D_API (A3DStatus, A3DPDFPageDrawBezierCurve, (A3DPDFPage* pPage, const A3DPDFGraphicBezierCurveData* pGraphicBezierCurveData));


/*!
\ingroup a3d_pdf_2ddrawings_module
\brief Structure that defines a graphic path.
\version 9.1

The elements are given ordered from the first to the last element to be drawn. For example, if there are 4 graphics, and m_peGraphicOrder is:
m_peGraphicOrder[0]=kA3DPDFGraphicArc, m_peGraphicOrder[1]=kA3DPDFGraphicLine, m_peGraphicOrder[2]=kA3DPDFGraphicArc, m_peGraphicOrder[3]=kA3DPDFGraphicLine,
m_iNumberOfLines is 2
m_iNumberOfArcs is 2
the first element to be drawn will be m_pArcsData[0]
the second will be m_pLinesData[0]
the third m_pArcsData[1]
the last m_pLinesData[1]
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;                // Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32 m_iNumberOfGraphics;						/*!< The total number of lines, arcs, bezier curves of the path. */
	A3DPDFEGraphicType*	m_peGraphicOrder;				/*!< The order to draw the following elements. The size of the array is \ref m_iNumberOfGraphics. */
	A3DUns32 m_iNumberOfLines;							/*!< The number of lines of the path. */
	A3DPDFGraphicLineData * m_pLinesData;				/*!< The data of the lines of the path, ordered from the first line to be drawn to the last. */
	A3DUns32 m_iNumberOfArcs;							/*!< The number of arcs of the path. */
	A3DPDFGraphicArcData * m_pArcsData;					/*!< The data of the arcs of the path, ordered from the first arc to be drawn to the last. */
	A3DUns32 m_iNumberOfBezierCurves;					/*!< The number of curves of the path. */
	A3DPDFGraphicBezierCurveData * m_pCurvesData;		/*!< The data of the curves of the path, ordered from the first curve to be drawn to the last. */
	A3DPDFRgbColorData m_sPathColor;					/*!< The path color, overrides the colors given in LinesData, ArcsData, CurvesData. */
	A3DPDFRgbColorData *m_pFillColor;					/*!< Optional: the filling color. Overrides the colors given in LinesData, ArcsData, CurvesData. */
	A3DDouble m_dWidth;									/*!< The width of the path. Overrides the colors given in LinesData, ArcsData, CurvesData. */
	A3DDouble m_dDashSize;								/*!< The size of the visible part of a dashed path in points. If O the path will be plain. Overrides the colors given in LinesData, ArcsData, CurvesData. */
	A3DDouble m_dGapSize;								/*!< The size of the invisible part of a dashed path in points. If O the path will be plain. Overrides the colors given in LinesData, ArcsData, CurvesData. */
	A3DPDFDashPatternData *m_pDashPattern;				/*!< Optional: the dash pattern. If m_pDashPattern is defined, it supersedes m_dDashSize and m_dGapSize.*/
} A3DPDFGraphicCompositePathData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_2ddrawings_module
\brief Function to create a graphic composite path

\param [in,out] pPage The page object to work with.
\param [in] pGraphicPathData The parameters of the elements to be drawn.
\return \ref A3D_SUCCESS \n
\version 9.1
*/
A3D_API (A3DStatus, A3DPDFPageDrawCompositePath, (A3DPDFPage* pPage, A3DPDFGraphicCompositePathData* pGraphicPathData));




/*!
\defgroup a3d_pdfaction HOOPS Publish Standard Actions Module
\ingroup a3d_pdf_interactivity_module
\brief Module to set pre-defined HOOPS Publish Standard actions on PDF entities.

This module describes the functions and structures that allow you to define actions.
*/
/*!
\brief Structure to define an action to activate a view in a 3D annotation.
\ingroup a3d_pdfaction
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16		m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DPDF3DAnnot*	m_p3DAnnot;				/*!< The 3D annotation that holds the views. */
	A3DUTF8Char*	m_pcViewName;			/*!< Name identifier of the view in the 3D annotation.
											The view is identified primarily by \ref m_pcViewName. If m_pcViewName is NULL, then
											the view is identified by its index (\ref m_iViewIndex). */
	A3DUns32		m_iViewIndex;			/*!< Index identifier of the view in the 3D annotation.
											This is the index in the list of views as returned by A3DPDF3DArtworkGetViews.
											Index starts from 0.
											The view is identified primarily by \ref m_pcViewName. If m_pcViewName is NULL, then
											the view is identified by its index (\ref m_iViewIndex). */
	A3DBool			m_bAnimate;				/*!< A value of true indicates that the view should be animated to when set. */
} A3DPDFActionSetViewData;
#endif // A3DAPI_LOAD
/*!
\brief Creates an \ref A3DPDFActionSetView from \ref A3DPDFActionSetViewData structure
\ingroup a3d_pdfaction
\version 10.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3DPDF_INVALID_VIEW_INDEX \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DPDFActionSetViewCreate, (const A3DPDFActionSetViewData* pData, A3DPDFActionSetView** ppAction));

/*!
\brief Structure to define an action to start an animation on a 3D annotation.

This action is to start an animation on the 3D annotation.
The animation can later be paused or resumed with other actions \ref A3DPDFActionPauseAnimation and \ref A3DPDFActionResumeAnimation.

The animation can be a native animation (typically in a U3D file), or an animation built with HOOPS Publish API
(see \ref a3d_pdf_animation_module). This should be set with \ref m_bIsNativeAnimation.
<ul>
<li> A native (U3D) animation is identified with \ref m_iAnimIndex, and [\ref m_dStartTime, \ref m_dEndTime].
Use \ref m_dStartTime=0. and \ref m_dEndTime=-1. to run all the animation.
</li>
<li> A HOOPS Publish animation is identified primarily by \ref m_pcAnimName.
If \ref m_pcAnimName is NULL, then the animation is identified by its index (\ref m_iAnimIndex).
If \ref m_iAnimIndex is -1, then the animation is run from \ref m_dStartTime to \ref m_dEndTime.
Use \ref m_dStartTime=0. and \ref m_dEndTime=-1. to run all the animation.
</li>
</ul>s
\ingroup a3d_pdfaction
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16		m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DPDF3DAnnot*	m_p3DAnnot;				/*!< The 3D annotation that holds the animations. */
	A3DUTF8Char*	m_pcAnimName;			/*!< Name identifier of the animation in the 3D annotation. */
	A3DInt32		m_iAnimIndex;			/*!< Index identifier of the animation in the 3D annotation. */
	A3DBool			m_bIsNativeAnimation;	/*!< True if the animation is natively stored into the Artwork.
												 The only situation currently supported for this scenario is a U3D file
												 with a U3D native animation embedded. */
	A3DDouble		m_dStartTime;			/*!< Time where to start the animation (in seconds). */
	A3DDouble		m_dEndTime;				/*!< Time where to end the animation (in seconds).
											If the value is -1., the animation runs until its end. */
} A3DPDFActionStartAnimationData;
#endif // A3DAPI_LOAD
/*!
\brief Creates an \ref A3DPDFActionStartAnimation from \ref A3DPDFActionStartAnimationData structure
\ingroup a3d_pdfaction
\version 10.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DPDFActionStartAnimationCreate, (const A3DPDFActionStartAnimationData* pData, A3DPDFActionStartAnimation** ppAction));

/*!
\brief Structure to define an action to pause the animation player on a 3D annotation.

This action is to pause the animation player on the 3Dannotation.
This is the same as selecting the Pause toolbar button or menu item.

\ingroup a3d_pdfaction
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16		m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DPDF3DAnnot*	m_p3DAnnot;				/*!< The 3D annotation that holds the animation. */
} A3DPDFActionPauseAnimationData;
#endif // A3DAPI_LOAD
/*!
\brief Creates an \ref A3DPDFActionPauseAnimation from \ref A3DPDFActionPauseAnimationData structure
\ingroup a3d_pdfaction
\version 10.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DPDFActionPauseAnimationCreate, (const A3DPDFActionPauseAnimationData* pData, A3DPDFActionPauseAnimation** ppAction));


/*!
\brief Structure to define an action to resume the animation player on a 3D annotation.

This action is to resume the animation player on the 3Dannotation.
This is the same as selecting the Play toolbar button or menu item.

\ingroup a3d_pdfaction
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16		m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DPDF3DAnnot*	m_p3DAnnot;				/*!< The 3D annotation that holds the animation. */
} A3DPDFActionResumeAnimationData;
#endif // A3DAPI_LOAD
/*!
\brief Creates an \ref A3DPDFActionResumeAnimation from \ref A3DPDFActionResumeAnimationData structure
\ingroup a3d_pdfaction
\version 10.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DPDFActionResumeAnimationCreate, (const A3DPDFActionResumeAnimationData* pData, A3DPDFActionResumeAnimation** ppAction));

/*!
\brief Structure to define an action to set the default rendering type for all objects in the scene on a 3D annotation.
\ingroup a3d_pdfaction
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16				m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DPDF3DAnnot*			m_p3DAnnot;			/*!< The 3D annotation. */
	A3DPDFERenderingStyle	m_eRenderingStyle;	/*!< Rendering style to set on the 3D scene. */
} A3DPDFActionSetRenderingStyleData;
#endif // A3DAPI_LOAD
/*!
\brief Creates an \ref A3DPDFActionSetRenderingStyle from \ref A3DPDFActionSetRenderingStyleData structure
\ingroup a3d_pdfaction
\version 10.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DPDFActionSetRenderingStyleCreate, (const A3DPDFActionSetRenderingStyleData* pData, A3DPDFActionSetRenderingStyle** ppAction));

/*!
\brief Structure to define an action to launch a URL in a browser window.
\ingroup a3d_pdfaction
\version 10.0
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16		m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char*	m_pcURL;			/*!< A string that specifies the URL to launch. */
} A3DPDFActionLaunchURLData;
#endif // A3DAPI_LOAD
/*!
\brief Creates an \ref A3DPDFActionLaunchURL from \ref A3DPDFActionLaunchURLData structure
\ingroup a3d_pdfaction
\version 10.0

\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_SUCCESS \n
*/
A3D_API(A3DStatus, A3DPDFActionLaunchURLCreate, (const A3DPDFActionLaunchURLData* pData, A3DPDFActionLaunchURL** ppAction));




/*!
\ingroup a3d_pdfaction
\brief Adds an action on a button

\param [in,out] pButton The button object to work with.
\param [in] pAction The action to be stored on the button.

\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API(A3DStatus, A3DPDFButtonAddAction, (A3DPDFButton* pButton, const A3DPDFAction* pAction));

/*!
\ingroup a3d_pdfaction
\brief Adds an action on a link

\param [in,out] pLink The link object to work with.
\param [in] pAction The action to be stored on the link.

\return \ref A3D_SUCCESS \n
\version 10.0
*/
A3D_API(A3DStatus, A3DPDFLinkAddAction, (A3DPDFLink* pLink, const A3DPDFAction* pAction));


/*!
\ingroup a3d_pdf_exportimage_module
\brief Export a PDF document to image, treating only one page or all pages.

This version is in beta mode and only supported on windows platform.

\param [in,out] pDoc The Document object to work with.
\param [in] iNumPage The index of the page. The first page is 0. A value of -1 is to convert all pages of the document.
If \ref iNumPage is specified (not -1), then one image file is generated with the exact name provided in \ref pcOutImgFileName.
If \ref iNumPage is -1, then \ref iNbPages image files are generated with names "pcOutImgFileName_i.extension", with 'i' = 0 to iNbPages-1
and 'extension' being the trigram corresponding to the image format ('bmp', 'jpg', 'png', ...).
Note that some file images could not exist in case of error. Please always check the existence of the file image before processing it.
\param [in] dScale Scale to be applied to the image. The image generated is of the size of the PDF page (for ex. 612*792 pixels for a 612*792 points page).
the scale here is applied to the size of the image.
\param [in] eFormatOut Format of the output image.
\param [in] pcOutImgFileName The file name where to save the document.
The path (absolute or relative) must be provided (use ".\\" or "./" for the current directory).
\param [out] piNbPages The number of pages in the document. It is useful if -1 is provided to \ref iNumPage to process all generated pages.

\return \ref A3D_SUCCESS \n
\version 12.0
*/
A3D_API(A3DStatus, A3DPDFDocumentExportAsImage, (A3DPDFDocument* pDoc, const A3DInt32 iNumPage, const A3DDouble dScale, const A3DPDFEImageFormat eFormatOut, const A3DUTF8Char* pcOutImgFileName, A3DInt32* piNbPages));


A3D_API(A3DStatus, A3DConvertPDFToImage, (const A3DUTF8Char* pcInPDFFilename, const A3DInt32 iNumPage, const A3DDouble dScale, const A3DPDFEImageFormat eFormatOut, const A3DUTF8Char* pcOutImgFileName, A3DInt32* piNbPages));



/*!
\defgroup a3d_pdf_richmediaannot_module Rich Media Module
\ingroup a3d_pdf_layout_static_module
\brief Adds rich media (video, sound) in the page

This module describes the functions and structures that allow you to add rich medias in the page.
A rich media is a video or a sound.
Supported file formats for video are : QuickTime Movie (.mov) ; MPEG (.mp4, .m4v) ; 3GPP Movie (.3gp, .3g2)
Different skins can be selected to control the video player.
Supported file formats for audio are : mp3
*/

/*!
\ingroup a3d_pdf_richmediaannot_module
\brief A3DPDFRichMediaAnnotData structure: options for the RichMedia annot behavior in the PDF
\version 12.2
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;					// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DPDFEActivateWhen m_eActivateWhen;		/*!< Option to specify when the RichMedia annot should be activated. */
	A3DPDFEDesactivateWhen m_eDesactivateWhen;	/*!< Option to specify when the RichMedia annot should be deactivated. */
	A3DInt32 m_iBorderWidth;					/*!< Border width in points. 0 for an invisible border. */
	A3DPDFEPlaybackControlsSkin m_ePlaybackControlsSkin;	/*!< Option to specify the controls that should be present for the RichMedia annot. */
	A3DPDFRgbColorData m_sPlaybackControlsColor;/*!< PlaybackControls color. */
	A3DInt32 m_iPlaybackControlsOpacity;		/*!< PlaybackControls opacity. Values are from 0 (transparent) to 100 (opaque). */
	A3DBool m_bPlaybackControlsAutohide;		/*!< PlaybackControls auto hiding. */
	A3DInt32 m_iPlayerVolume;					/*!< Playbacker volume. Values are from 0 (mute) to 100. */
	A3DPDFImage* m_pPosterImage;				/*!<
												Specifies a poster image to be displayed when the RichMedia Annot is not activated.
												Null can not be specified. */
	A3DUTF8Char* m_pcName;						/*!< Name of the RichMedia Annot. If NULL is specified, the name is automatically generated. */
} A3DPDFRichMediaAnnotData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_pdf_richmediaannot_module
\brief Function to create a RichMedia Annotation object

The RichMedia Annotation object is primarily created with this function, and it should be inserted on the page with the functions \ref A3DPDFPageInsertRichMediaAnnot.

\param [in,out] pDoc The Document object to work with.
\param [in] pRichMediaAnnotData The RichMedia Annot parameters.
\param [in] pcFileName The file name of the rich media input file. The file format is deducted from file extension. Supported file formats for video are : QuickTime Movie (.mov) ; MPEG (.mp4, .m4v) ; 3GPP Movie (.3gp, .3g2).
Supported file formats for audio are : mp3
\param [out] ppRichMediaAnnot The RichMedia Annot object created.
\return \ref A3D_SUCCESS \n
\version 12.2
*/
A3D_API(A3DStatus, A3DPDFRichMediaAnnotCreateFromFile, (A3DPDFDocument* pDoc, const A3DPDFRichMediaAnnotData* pRichMediaAnnotData, const A3DUTF8Char* pcFileName, A3DPDFRichMediaAnnot** ppRichMediaAnnot));

/*!
\ingroup a3d_pdf_richmediaannot_module
\brief Function to insert a RichMedia Annot in a page

Insert a RichMedia Annot object on the page.
Several RichMedia Annots can be stored on a page.

\param [in,out] pPage The Page object to work with.
\param [in] pRichMediaAnnot The RichMedia Annot to insert on the page.
\param [in] pRectData The rectangle to specify the position on the page.
\return \ref A3D_SUCCESS \n
\version 12.2
*/
A3D_API(A3DStatus, A3DPDFPageInsertRichMediaAnnot, (A3DPDFPage* pPage, const A3DPDFRichMediaAnnot* pRichMediaAnnot, const A3DPDFRectData* pRectData));


#endif
