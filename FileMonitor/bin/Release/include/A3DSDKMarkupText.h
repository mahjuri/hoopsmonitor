/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for the markup text module
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifdef A3DAPI_LOAD
#  undef __A3DPRCMarkupText_H__
#endif
#ifndef __A3DPRCMarkupText_H__
#define __A3DPRCMarkupText_H__
#ifndef A3DAPI_LOAD
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKInitializeFunctions.h>
#  include <A3DSDKMarkupDefinition.h>
#  include <A3DSDKTypes.h>
#endif // A3DAPI_LOAD






/*!
@} <!-- end of a3d_markuptext_enums -->
*/

/*!
\struct A3DMDTextPositionData
\brief Markup text position
\ingroup a3d_markuptext
\version 4.0

*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;		// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DVector3dData	m_sPosition;	/*!< Origin for text orientation. */
	A3DVector3dData	m_sBaseVector;	/*!< Horizontal vector for text orientation. */
	A3DVector3dData	m_sUpVector;	/*!< Vertical vector for text orientation. */
} A3DMDTextPositionData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMDTextPositionData structure
\ingroup a3d_markuptext
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMDTextPositionGet,(	const A3DMDTextPosition* pTextPosition,
		 A3DMDTextPositionData* pData));





/*!
\defgroup a3d_markuptext Markups based on simple text
\ingroup a3d_markupnote
\version 4.0
*/


/*!
\struct A3DMarkupTextData
\brief Note with simple text (m_ppcLines) and text properties like font, size, thickness...
\ingroup a3d_markuptext
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16						m_usStructSize;				// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUns32						m_uiLinesSize;				/*!< The size of \ref m_ppLines. */
	A3DUTF8Char**					m_ppLines;					/*!< Note lines. */
	A3DUns32						m_uiFileLinksSize;			/*!< Number of file links. */
	A3DUTF8Char**					m_ppFileLinks;				/*!< Array of file links. */
	A3DDouble						m_dWrappingWidth;			/*!< Wrapping width. */
	EA3DMarkupFrameType				m_eFrameType;				/*!< Frame type. */
	EA3DLeaderAlignementType		m_eLeaderAlignementType;	/*!< Precises the leader/note attach. */
	A3DMDTextProperties*			m_pTextProperties;			/*!< Pointer to the text properties. \sa A3DMDTextPropertiesGetData */
	A3DMDTextPosition*				m_psTextPosition;			/*!< The relative position of the text to the attach of the markup \ref A3DMDTextPropertiesGet. */
} A3DMarkupTextData;
#endif // A3DAPI_LOAD

/*!
\fn A3DStatus A3DMarkupTextGet( const A3DMarkupText* pMarkupText, A3DMarkupTextData* pData)
\brief Populates the \ref A3DMarkupTextData structure
\ingroup a3d_markuptext
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMarkupTextGet,( const A3DMarkupText* pMarkupText,
											A3DMarkupTextData* pData));





/*!
\struct A3DMarkupCoordinateData
\brief Markup text with additional data.
Use \ref A3DMarkupTextGet to access to markup text data.
\ingroup a3d_markuptext


*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16						m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DDouble						m_dLitDimensionedX;		/*!< Coordinate x. */
	A3DDouble						m_dLitDimensionedY;		/*!< Coordinate y. */
	A3DDouble						m_dLitDimensionedZ;		/*!< Coordinate z. */
	A3DBool							m_bType3D;				/*!< Specifies if it's a 2D or a 3D coordinate; if it's the latter, m_dLitDimensionedZis is initialized. */
	A3DMDTextPosition*				m_psTextPosition;		/*!< The relative position of the text to the attach of the markup. \sa A3DMDTextPositionGetData */
} A3DMarkupCoordinateData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMarkupCoordinateData structure
\ingroup a3d_markuptext
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMarkupCoordinateGet,( const A3DMarkupCoordinate* pMarkupCoordinate,
											A3DMarkupCoordinateData* pData));





/*!
\defgroup a3d_markuprichtext Markup note based on rich text
\ingroup a3d_markupnote
\version 4.0
*/

/*!
\struct A3DMarkupRichTextData
\brief Markup rich text data
\ingroup a3d_markuprichtext
\version 4.0
Entity type is \ref kA3DTypeMarkupRichText.
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16						m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DUTF8Char*					m_pcRichText;			/*!< Text with RTF format. See \ref a3d_markup_rtf. */
	A3DUns32						m_uLength;				/*!< Length of the rich text. */
	A3DUns32						m_uiFileLinksSize;		/*!< Number of file links. */
	A3DUTF8Char**					m_ppFileLinks;			/*!< Array of file links. */
	A3DDouble						m_dWrappingWidth;		/*!< Wrapping width. */
	EA3DMarkupFrameType				m_eFrameType;			/*!< Frame type. */
	EA3DLeaderAlignementType	m_eLeaderAlignementType;	/*!< Precises the leader/note attach. */
	A3DMDTextPosition*				m_psTextPosition;		/*!< The relative position of the text to the attach of the markup. \sa A3DMDTextPositionGetData */
} A3DMarkupRichTextData;
#endif // A3DAPI_LOAD

/*!
\brief Populates the \ref A3DMarkupRichTextData structure
\ingroup a3d_markuprichtext
\version 4.0

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_INVALID_DATA_STRUCT_SIZE \n
\return \ref A3D_INVALID_DATA_STRUCT_NULL \n
\return \ref A3D_INVALID_ENTITY_NULL \n
\return \ref A3D_INVALID_ENTITY_TYPE \n
\return \ref A3D_SUCCESS \n
*/
A3D_API (A3DStatus, A3DMarkupRichTextGet,( const A3DMarkupRichText* pMarkupRichText,
											A3DMarkupRichTextData* pData));





#endif	/*	__A3DPRCMarkupText_H__ */
