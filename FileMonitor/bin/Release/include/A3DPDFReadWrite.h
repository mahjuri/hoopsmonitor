/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for the read-write module
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par		Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

#ifdef A3DAPI_LOAD
#  undef __A3DPDFREADWRITE_H__
#endif
#ifndef __A3DPDFREADWRITE_H__
#define __A3DPDFREADWRITE_H__
#ifndef A3DAPI_LOAD
#  include <A3DCommonReadWrite.h>
#  include <A3DSDK.h>
#  include <A3DSDKEnums.h>
#  include <A3DSDKErrorCodes.h>
#  include <A3DSDKTypes.h>
#  include <A3DPDFPublishSDK.h>
#endif // A3DAPI_LOAD

/*!
\addtogroup a3d_publish_modelfile_loadexport Load / Export Module
\brief Methods and structures dedicated to 3D data reading and writing capabilities

This module describes the functions and structures that allow you to read and write 3D model files.
*/


/*!
\defgroup a3d_read Load Module
\ingroup a3d_publish_modelfile_loadexport
\brief Loads the model file with information from the input CAD file.

Please refer to \REF_SUPPORTED_FORMATS documentation for list of available formats.
*/




/*!
\defgroup a3d_write Export Module
\ingroup a3d_publish_modelfile_loadexport
\brief Structures to write the ModelFile defined through this API.

Please refer to \REF_SUPPORTED_FORMATS documentation for list of available formats.
*/




/*!
\defgroup a3d_publishhtml_scs_module Export SCS Module
\ingroup a3d_publishhtml_module
\brief <b>(HOOPS Publish Advanced) </b>Structure and function to write a ModelFile as a SCS stream cache file.

SCS is the Stream Cache Single file format used in HOOPS Communicator to hold the representation of a CAD model.
A SCS file is later expected to be referred by a HTML file.
This format stores the tessellated geometry, the assembly tree and all non-tessellation related data: PMI, views, user attributes, etc.
Also, exact measurement data can be optionally added to the SCS file to store geometrical information useful for measurement tool, such as cylinder and radius data on faces or edges.
This measurement information can take time to be computed and should only be added on purpose to handle measurement scenarios.
*/
/*!
\ingroup a3d_publishhtml_scs_module
\brief A structure that specifies parameters used to write a model file to SCS format.
\version 10.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bIncludeMeasurementInformation;	/*!< A value of \c TRUE includes the measurement information in the SCS representation.
												(see \ref a3d_publishhtml_scs_module). */
} A3DRWParamsExportScsData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_publishhtml_scs_module
\brief Writes a model file to a physical file in SCS format.

This function writes the \ref A3DAsmModelFile entity to a SCS stream cache file. (see \ref a3d_publishhtml_scs_module).

\param [in] pA3DAsmModelFile References The \ref A3DAsmModelFile to be exported
\param [in] pParamsExportData References the parameters for export.
\param [in] pcScsFileName References the path of the file into which the function stores the model file.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER if libprc2sc dll cannot be loaded \n
\return \ref A3D_WRITE_HTMLCONVERSION_FAILED if conversion execution failed \n
\return \ref A3D_NOT_AVAILABLE if called on unsupported platform \n
\return \ref A3D_SUCCESS \n

\version 10.1
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToSCSFile, (
	A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportScsData* pParamsExportData,
	const A3DUTF8Char* pcScsFileName));

#ifndef A3DAPI_LOAD
typedef void A3DPrcIdMap;
typedef A3DUTF8Char const* A3DPrcId;
#endif

/*!
\brief Function to create a map linking PrcIds and A3DEntities.

This function uses a \ref A3DAsmModelFile entity to create a map linking PrcIds and instances of \ref A3DEntity.
Use this function with no A3DAsmModelFile to delete a previously created \ref A3DPrcIdToEntityMap.

\param [in] pA3DAsmModelFile the modelFile used to create map.
\param [out] ppPrcIdToEntityMapOut the map of prc ids to A3DEntity pointer.
\return \ref A3D_SUCCESS \n
\version 13.0
*/
A3D_API(A3DStatus, A3DPrcIdMapCreate, (A3DAsmModelFile* pA3DAsmModelFile, A3DPrcIdMap** ppPrcIdToEntityMapOut));

/*!
\brief Function to get an \ref A3DEntity from its corresponding PrcId

\param [in] pEntityMap Pointer on the entity map generated by the function A3DPrcIdMapCreate
\param [in] pcSearchedId the Exchange Id of the entity to find
\param [out] ppSearchedEntityOut a pointer containing a pointer on the A3DEntity searched or null if not found
\param [out] ppSearchedEntityFatherOut a pointer containing a pointer on the Product Occurrence Father of the searched entity or null if not found
\return \ref A3D_SUCCESS \n
\version \version 13.0
*/
A3D_API(A3DStatus, A3DPrcIdMapFindEntity, (A3DPrcIdMap const* pEntityMap, A3DPrcId pcSearchedId, A3DEntity** ppSearchedEntityOut, A3DEntity** ppSearchedEntityFatherOut));

/*!
\brief Function to get the PrcId of a node from an A3DEntity and its father.

\param [in]	pEntityMap Pointer on the entity map generated by the function A3DPrcIdMapCreate
\param [in] searchedEntity Pointer to the A3DEntity.
\param [in] searchedEntityFather Pointer to the parent A3DEntity of searchedEntity.
\param [out] a_prcIdOut PrcId found for the A3DEntity given. This pointer is only available while the \ref A3DPrcIdMap is alive.
\return \ref A3D_SUCCESS \n
\version 13.0
*/
A3D_API(A3DStatus, A3DPrcIdMapFindId, (A3DPrcIdMap const* pEntityMap, A3DEntity const* searchedEntity, A3DEntity const* searchedEntityFather, A3DPrcId* a_prcIdOut));

/*!
\defgroup a3d_publishhtml_monolithic_module Export monolithic HTML Module
\ingroup a3d_publishhtml_module
\brief <b>(HOOPS Publish Advanced) </b>Structure and function to write a ModelFile as a monolithic HTML file.

This file embeds all javascript and css definitions useful to render the 3D.
The 3D definition (as a SCS cache file) is also embedded in the HTML file (see \ref a3d_publishhtml_scs_module).
*/
/*!
\ingroup a3d_publishhtml_monolithic_module
\brief A structure that specifies parameters used to write a model file to HTML format.
\version 10.1
*/
#ifndef A3DAPI_LOAD
typedef struct
{
	A3DUns16 m_usStructSize;			// Reserved: will be initialized by \ref A3D_INITIALIZE_DATA.
	A3DBool m_bIncludeMeasurementInformation;	/*!< A value of \c TRUE includes the measurement information in the SCS representation.
												(see \ref a3d_publishhtml_scs_module). */
	A3DUTF8Char* m_pcHtmlTemplateName;	/*!< File name for input HTML file to be used as template. */
	A3DUTF8Char* m_pcAdditionalFontDirectory; /*!< Path to a directory where additional fonts can be found. */
} A3DRWParamsExportHtmlData;
#endif // A3DAPI_LOAD

/*!
\ingroup a3d_publishhtml_monolithic_module
\brief Writes a model file to a physical file in HTML format.

This function writes the \ref A3DAsmModelFile entity to an HTML file.


\param [in] pA3DAsmModelFile References The \ref A3DAsmModelFile to be exported
\param [in] pParamsExportData References the parameters for export.
\param [in] pcHtmlFileName References the path of the file into which the function stores the model file.

\return \ref A3D_INITIALIZE_NOT_CALLED \n
\return \ref A3D_WRITE_CANNOT_LOAD_WRITER if libprc2sc dll cannot be loaded \n
\return \ref A3D_WRITE_HTMLCONVERSION_FAILED if conversion execution failed \n
\return \ref A3D_NOT_AVAILABLE if called on unsupported platform \n
\return \ref A3D_SUCCESS \n

\version 10.1
*/
A3D_API(A3DStatus, A3DAsmModelFileExportToHTMLFile, (
	A3DAsmModelFile* pA3DAsmModelFile,
	const A3DRWParamsExportHtmlData* pParamsExportData,
	const A3DUTF8Char* pcHtmlFileName));

/*!
\ingroup a3d_pdf_document_module
\brief Function to export the PDF document into a web format

\param [in] pDoc The Document object to export.
\param [in] pParamsExportData Export parameters used by each 3D annotation conversion.
\param [in] pcOutRootDirectory the root directory to save all created files
\param [in] pcOutXmlFileName the root xml file describing the document data
\param [in]	iOutFormats flag that describe in which format the export has to be done (see \ref a3d_publishhtml_module module)
\return \ref A3D_SUCCESS \n
\version 12.0
*/
A3D_API(A3DStatus, A3DPDFDocumentExportToWebFormat, (A3DPDFDocument* pDoc, const A3DRWParamsExportHtmlData* pParamsExportData, const A3DUTF8Char* pcOutRootDirectory, const A3DUTF8Char* pcOutXmlFileName, A3DUns32 iOutFormats));

/*!
\ingroup a3d_publishhtml_module
\brief	Function that export a pdf into a web format

\param [in] pcFileName The pdf path and name to export
\param [in] pParamsExportData Export parameters used by each 3D annotation conversion
\param [in] pcOutRootDirectory the root directory to save all created files
\param [in] pcOutXmlFileName the root xml file describing the document data
\param [in]	iOutFormats flag that describe in which format the export has to be done (see \ref a3d_publishhtml_module module)
\return \ref A3D_SUCCESS \n
\version 12.0
*/
A3D_API(A3DStatus, A3DConvertPDFToWebFormat, (const A3DUTF8Char* pcFileName, const A3DRWParamsExportHtmlData* pParamsExportData, const A3DUTF8Char* pcOutRootDirectory, const A3DUTF8Char* pcOutXmlFileName, A3DUns32 iOutFormats));


#endif	/*	__A3DPDFREADWRITE_H__ */
