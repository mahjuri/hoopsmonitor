/***********************************************************************************************************************
*
* Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc.
* The information contained herein is confidential and proprietary to Tech Soft 3D, Inc., and considered a trade secret
* as defined under civil and criminal statutes. Tech Soft 3D shall pursue its civil and criminal remedies in the event
* of unauthorized use or misappropriation of its trade secrets. Use of this information by anyone other than authorized
* employees of Tech Soft 3D, Inc. is granted only under a written non-disclosure agreement, expressly prescribing the
* scope and manner of such use.
*
***********************************************************************************************************************/

/*!
\file
\brief      Header file for type definitions
\author     Tech Soft 3D
\version    13.0
\date       October 2019
\par    	Copyright (c) 2010 - 2019 by Tech Soft 3D, Inc. All rights reserved.
*/

// Root include
#include <A3DSDK.h>

// Common code
#include <A3DSDKEnums.h>
#include <A3DSDKErrorCodes.h>
#ifndef A3D_EXPORTS
#  include <hoops_license.h>
#endif
#include <A3DSDKTypes.h>

// API code
#include <A3DCommonReadWrite.h>
#ifdef HOOPS_PRODUCT_PUBLISH_ADVANCED
#  include <A3DPDFAdvancedPublishSDK.h>
#endif
#ifdef HOOPS_PRODUCT_PUBLISH_STANDARD
#  include <A3DPDFPublishSDK.h>
#endif
#if defined HOOPS_PRODUCT_PUBLISH_ADVANCED || defined HOOPS_PRODUCT_PUBLISH_STANDARD
#  include <A3DPDFReadWrite.h>
#endif
#include <A3DSDKAdvancedTools.h>
#include <A3DSDKBase.h>
#include <A3DSDKDraw.h>
#include <A3DSDKDrawing.h>
#include <A3DSDKFeature.h>
#include <A3DSDKGeometry.h>
#include <A3DSDKGeometryCrv.h>
#include <A3DSDKGeometrySrf.h>
#include <A3DSDKGlobalData.h>
#include <A3DSDKGraphics.h>
#include <A3DSDKLicense.h>
#include <A3DSDKMarkup.h>
#include <A3DSDKMarkupDefinition.h>
#include <A3DSDKMarkupDimension.h>
#include <A3DSDKMarkupLeaderDefinition.h>
#include <A3DSDKMarkupSymbol.h>
#include <A3DSDKMarkupText.h>
#include <A3DSDKMarkupTolerance.h>
#include <A3DSDKMarkupWelding.h>
#include <A3DSDKMath.h>
#include <A3DSDKMisc.h>
#ifdef HOOPS_EXCHANGE
#  include <A3DSDKReadWrite.h>
#endif
#include <A3DSDKRepItems.h>
#include <A3DSDKRootEntities.h>
#include <A3DSDKStructure.h>
#include <A3DSDKTessellation.h>
#include <A3DSDKTexture.h>
#include <A3DSDKTools.h>
#include <A3DSDKTopology.h>

#ifdef A3DAPI_INTERNAL
#  include <A3DInternalAPI.h>
#endif

#ifdef A3DAPI_EXTENSIONS
#  include <A3DSDKExtensions.h>
#endif


// Loader included here
#ifdef INITIALIZE_A3D_API
# include <A3DSDKLoader.h>
#endif

#ifndef NO_CONVERTER
#	include <A3DSDKConvert.hxx>
#endif


