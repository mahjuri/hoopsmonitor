﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace FileMonitor {
    public partial class Form1 : Form {
        public ListBox listBox;
        public const String startMonitoring = "Start Minitoring…";
        public const String stopMonitoring = "Stop Minitoring…";
        metaData meta = new metaData();

        private FileSystemWatcher fsWatcher;
        private string InputFile = "";
        private string OutputFile = "";
        private string builderPath = "";
        private int WorkerCount = 2;
        private int WorkerAmount = 0; 
        List<string> buildables;

        public Form1() {
            InitializeComponent();

            listBox = new ListBox {
                FormattingEnabled = true,
                Location = new System.Drawing.Point(15, 122),
                Name = "lb_result",
                Size = new System.Drawing.Size(1265, 435),
                TabIndex = 2
            };
            this.Controls.Add(listBox);

            if (!Settings1.Default.ExChangerPath.Equals(String.Empty)) {
                tb_builderLocation.Text = Settings1.Default.ExChangerPath;
            }

            if (!tb_builderLocation.Text.Equals(String.Empty)) {
                builderPath = tb_builderLocation.Text;
            }



            if (!Settings1.Default.MonitorPath.Equals(String.Empty)) {
                tb_folderPath.Text = Settings1.Default.MonitorPath;
            }

            if (!Settings1.Default.WorkerAmount.Equals(String.Empty)) {
                tb_workerAmount.Text = Settings1.Default.WorkerAmount.ToString();
                WorkerCount = Settings1.Default.WorkerAmount;
            }

            Run();
        }

        private void btn_browse_Click(object sender, EventArgs e) {
            // Create FolderBrowserDialog object.
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            // Show a button to create a new folder.
            folderBrowserDialog.ShowNewFolderButton = true;
            DialogResult dialogResult = folderBrowserDialog.ShowDialog();
            // Get selected path from FolderBrowserDialog control.
            if (dialogResult == DialogResult.OK) {
                tb_folderPath.Text = folderBrowserDialog.SelectedPath;
                Settings1.Default.MonitorPath = folderBrowserDialog.SelectedPath;
                SaveSettings("Monitored", folderBrowserDialog.SelectedPath);
                Environment.SpecialFolder root = folderBrowserDialog.RootFolder;
                Stop();
                Run();
            }
        }

        private void Run() {
            // Create a new FileSystemWatcher object.
            fsWatcher = new FileSystemWatcher();

            if (!tb_folderPath.Text.Equals(String.Empty) && Directory.Exists(tb_folderPath.Text)) {
                listBox.Items.Add("Started FileSystemWatcher Service…");
                fsWatcher.Path = tb_folderPath.Text;
                // Set Filter.
                fsWatcher.Filter = (tb_filter.Text.Equals(String.Empty)) ? "*.*" : "*." + tb_filter.Text;
                // Monitor files and subdirectories.
                fsWatcher.IncludeSubdirectories = true;
                // Monitor all changes specified in the NotifyFilters.
                fsWatcher.NotifyFilter = NotifyFilters.Attributes |
                                         NotifyFilters.CreationTime |
                                         NotifyFilters.DirectoryName |
                                         NotifyFilters.FileName |
                                         NotifyFilters.LastAccess |
                                         NotifyFilters.LastWrite |
                                         NotifyFilters.Security |
                                         NotifyFilters.Size;
                fsWatcher.EnableRaisingEvents = true;
                // Raise Event handlers.
                //fsWatcher.Changed += new FileSystemEventHandler(OnChanged);
                fsWatcher.Created += new FileSystemEventHandler(OnCreated);
                //fsWatcher.Deleted += new FileSystemEventHandler(OnDeleted);
                //fsWatcher.Renamed += new RenamedEventHandler(OnRenamed);
                fsWatcher.Error += new ErrorEventHandler(OnError);
                //tb_folderPath.Enabled = false;
                tb_filter.Enabled = false;
            }
            else {
                listBox.Items.Add("Please select folder to monitor….");
            }
        }

        private void Stop() {
            fsWatcher.EnableRaisingEvents = false;
            fsWatcher = null;
            listBox.Items.Add("Stopped FileSystemWatcher Service…");
        }

        // FileSystemWatcher – OnCreated Event Handler
        public void OnCreated(object sender, FileSystemEventArgs e)
        {
            if (!builderPath.Equals(String.Empty)) {

                    // Add event details in listbox.
                    this.Invoke((MethodInvoker)delegate { listBox.Items.Add(String.Format("{2} Path : {0} || Action : {1}", e.FullPath, e.ChangeType, System.DateTime.Now)); });

                    if (buildables == null || buildables.Count == 0) {
                        buildables = new List<string>();
                        buildables.Add(e.FullPath);

                        Thread t = new Thread(ThreadedList);
                        t.IsBackground = true;
                        t.Start();
                    }
                    else {
                        buildables.Add(e.FullPath);
                    }
                this.Invoke((MethodInvoker)delegate { txt_queueList.DataSource = null; });
                this.Invoke((MethodInvoker)delegate { txt_queueList.DataSource = buildables; });
            }
            else {
                this.Invoke((MethodInvoker)delegate { listBox.Items.Add("Builderin ja/tai unityn polku puuttuu."); });
            }
        }
        //// FileSystemWatcher – OnChanged Event Handler
        //public void OnChanged(object sender, FileSystemEventArgs e)
        //{
        //    // Add event details in listbox.
        //    this.Invoke((MethodInvoker)delegate { listBox.Items.Add(String.Format("{2} Path : {0} || Action : {1}", e.FullPath, e.ChangeType, System.DateTime.UtcNow)); });
        //}
        //// FileSystemWatcher – OnRenamed Event Handler
        //public void OnRenamed(object sender, RenamedEventArgs e)
        //{
        //    // Add event details in listbox.
        //    this.Invoke((MethodInvoker)delegate { listBox.Items.Add(String.Format("{2} Path : {0} || Action : {1}", e.FullPath, e.ChangeType, System.DateTime.UtcNow)); });
        //}
        //// FileSystemWatcher – OnDeleted Event Handler
        //public void OnDeleted(object sender, FileSystemEventArgs e)
        //{
        //    // Add event details in listbox.
        //    this.Invoke((MethodInvoker)delegate { listBox.Items.Add(String.Format("{2} Path : {0} || Action : {1}", e.FullPath, e.ChangeType, System.DateTime.UtcNow)); });
        //}
        // FileSystemWatcher – OnError Event Handler
        public void OnError(object sender, ErrorEventArgs e)
        {
            // Add event details in listbox.
            this.Invoke((MethodInvoker)delegate { listBox.Items.Add(String.Format("Error : {0}", e.GetException().Message)); });
        }

        private void btn_builder_Click(object sender, EventArgs e) {
            // Create FolderBrowserDialog object.
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            // Show a button to create a new folder.
            folderBrowserDialog.ShowNewFolderButton = true;
            DialogResult dialogResult = folderBrowserDialog.ShowDialog();
            // Get selected path from FolderBrowserDialog control.
            if (dialogResult == DialogResult.OK) {
                tb_builderLocation.Text = folderBrowserDialog.SelectedPath;
                Environment.SpecialFolder root = folderBrowserDialog.RootFolder;
                this.Invoke((MethodInvoker)delegate { listBox.Items.Add(String.Format("Builder location updated.")); });
                Settings1.Default.ExChangerPath = folderBrowserDialog.SelectedPath;
                SaveSettings("Builder", tb_builderLocation.Text);
            }
        }

        private void tb_builderLocation_Leave(object sender, EventArgs e) {
            if (!tb_builderLocation.Text.Equals(String.Empty) && Directory.Exists(tb_builderLocation.Text) && tb_builderLocation.Text != builderPath) {
                builderPath = tb_builderLocation.Text;
                Settings1.Default.ExChangerPath = tb_builderLocation.Text;
                this.Invoke((MethodInvoker)delegate { listBox.Items.Add(String.Format("Builder location updated.")); });
                SaveSettings("Builder", tb_builderLocation.Text);
            }
        }

        private void tb_folderPath_Leave(object sender, EventArgs e) {
            if (!tb_folderPath.Text.Equals(String.Empty) && Directory.Exists(tb_folderPath.Text)) {
                Stop();
                Run();
                Settings1.Default.MonitorPath = tb_folderPath.Text;
                this.Invoke((MethodInvoker)delegate { listBox.Items.Add(String.Format("Monitored location updated.")); });
                SaveSettings("Monitored", tb_folderPath.Text);
            }
        }

        private void tb_workerAmount_Leave(object sender, EventArgs e) {
            if (!tb_workerAmount.Text.Equals(String.Empty)) {
                WorkerCount = int.Parse(tb_workerAmount.Text);
                Settings1.Default.WorkerAmount = int.Parse(tb_workerAmount.Text);
                this.Invoke((MethodInvoker)delegate { listBox.Items.Add(String.Format("Worker Amount updated.")); });
                SaveSettings("Monitored", tb_workerAmount.Text);
            }
        }

        private void SaveSettings(string varri, string path) {
            Settings1.Default.Save();
            this.Invoke((MethodInvoker)delegate { listBox.Items.Add(String.Format("{0} saved as {1} path", path, varri)); });
        }

        void ThreadedList() {
            List<string> tempList = buildables;
            int i = 0;

            do {
                if (WorkerAmount < WorkerCount) {
                    Thread t = new Thread(() => WorkerThread(tempList, i)) {
                        IsBackground = true
                    };
                    t.Start();

                    Thread.Sleep(100);
                    //RunBuilder(tempList[i]);

                    //i++;
                    WorkerAmount++;
                    tempList = buildables;
                    buildables.RemoveAt(0);

                    this.Invoke((MethodInvoker)delegate { txt_queueList.DataSource = null; });
                    this.Invoke((MethodInvoker)delegate { txt_queueList.DataSource = buildables; });
                }                
                Thread.Sleep(100);
            } while (tempList.Count > 0);

            buildables = null;
        }

        private void WorkerThread(List<string> tempList, int i) {
            RunBuilder(tempList[i]);
        }

        private void RunBuilder(string e) {
            // Run Exe with scene json path
            // "/C" + ctrl.getUnityLocation() + " -projectPath " + ctrl.getProjectLocation() + " -executeMethod ExeBuilder.BuildScene" + " -name " + name           

            FileInfo fileInfo = new FileInfo(e);
            while (IsFileLocked(fileInfo)) {
                System.Threading.Thread.Yield();
            }

            StreamReader sr = new StreamReader(e);
            string json = sr.ReadLine();
            sr.Close();

            this.Invoke((MethodInvoker)delegate { listBox.Items.Add("testi kohtana"); });

            meta = JsonConvert.DeserializeObject<metaData>(json);

            String[] tmp = meta.name.Split('.');

            ///jos useampia pisteiä niin käsittele tässä;

            meta.name = tmp[0] + ".fbx";           

            InputFile = Path.GetDirectoryName(Settings1.Default.MonitorPath) + "\\" + meta.input;
            OutputFile = Path.GetDirectoryName(Settings1.Default.MonitorPath) + "\\" + meta.output;

            this.Invoke((MethodInvoker)delegate { listBox.Items.Add(String.Format("{0} saved as {1}.fbx", InputFile, OutputFile)); });

            ///RUN EXPORT PROCESS HERE
            string command = String.Format("\"{0}\\hoopsConsoleTesti.exe\" \"{1}\" \"{2}.fbx\"", builderPath, InputFile, OutputFile);
            this.Invoke((MethodInvoker)delegate { listBox.Items.Add(command); });
            ProcessStartInfo procStartInfo = new ProcessStartInfo(command);

            procStartInfo.RedirectStandardOutput = true;
            procStartInfo.UseShellExecute = false;

            // wrap IDisposable into using (in order to release hProcess) 
            using (Process process = new Process()) {
                process.StartInfo = procStartInfo;
                process.Start();

                // Add this: wait until process does its work
                process.WaitForExit();
               
                while (IsFileLocked(fileInfo)) {
                    System.Threading.Thread.Yield();
                }

                StreamWriter sw = new StreamWriter(GetFullPathWithoutExtension(e) + ".json");
                
                sw.WriteLine(JsonConvert.SerializeObject(meta));
                sw.Close();

                this.Invoke((MethodInvoker)delegate { listBox.Items.Add(GetFullPathWithoutExtension(e) + ".json"); });

                //File.Delete(e);
                //File.Delete(InputFile);

                WorkerAmount--;
                // and only then read the result
                string result = process.StandardOutput.ReadToEnd();
                Console.WriteLine(result);
                this.Invoke((MethodInvoker)delegate { listBox.Items.Add(String.Format("{0} Path | {1} || Action : Ready", System.DateTime.Now, GetFullPathWithoutExtension(e))); });
            }
        }

        public String GetFullPathWithoutExtension(String path) {
            return Path.Combine(Path.GetDirectoryName(OutputFile), Path.GetFileNameWithoutExtension(path));
        }

        protected virtual bool IsFileLocked(FileInfo file) {
            FileStream stream = null;

            try {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            } catch (IOException) {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;

            } finally {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        
    }    
}
