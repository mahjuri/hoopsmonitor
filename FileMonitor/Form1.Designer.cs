﻿namespace FileMonitor {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.tb_folderPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_browse = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_filter = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_builderLocation = new System.Windows.Forms.TextBox();
            this.btn_builder = new System.Windows.Forms.Button();
            this.tb_workerAmount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_queueList = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // tb_folderPath
            // 
            this.tb_folderPath.Location = new System.Drawing.Point(13, 31);
            this.tb_folderPath.Name = "tb_folderPath";
            this.tb_folderPath.Size = new System.Drawing.Size(475, 20);
            this.tb_folderPath.TabIndex = 0;
            this.tb_folderPath.Text = "C:\\Users\\DS";
            this.tb_folderPath.Leave += new System.EventHandler(this.tb_folderPath_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Monitored Path";
            // 
            // btn_browse
            // 
            this.btn_browse.Location = new System.Drawing.Point(494, 29);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(128, 23);
            this.btn_browse.TabIndex = 2;
            this.btn_browse.Text = "Browse";
            this.btn_browse.UseVisualStyleBackColor = true;
            this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "File Filter";
            // 
            // tb_filter
            // 
            this.tb_filter.Location = new System.Drawing.Point(65, 96);
            this.tb_filter.Name = "tb_filter";
            this.tb_filter.Size = new System.Drawing.Size(176, 20);
            this.tb_filter.TabIndex = 4;
            this.tb_filter.Text = "cnvrt";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Builder path";
            // 
            // tb_builderLocation
            // 
            this.tb_builderLocation.Location = new System.Drawing.Point(13, 70);
            this.tb_builderLocation.Name = "tb_builderLocation";
            this.tb_builderLocation.Size = new System.Drawing.Size(475, 20);
            this.tb_builderLocation.TabIndex = 8;
            this.tb_builderLocation.Text = "C:\\Users\\Ville\\Desktop\\DSTOOLPROJ";
            this.tb_builderLocation.Leave += new System.EventHandler(this.tb_builderLocation_Leave);
            // 
            // btn_builder
            // 
            this.btn_builder.Location = new System.Drawing.Point(495, 68);
            this.btn_builder.Name = "btn_builder";
            this.btn_builder.Size = new System.Drawing.Size(128, 23);
            this.btn_builder.TabIndex = 10;
            this.btn_builder.Text = "Browse";
            this.btn_builder.UseVisualStyleBackColor = true;
            this.btn_builder.Click += new System.EventHandler(this.btn_builder_Click);
            // 
            // tb_workerAmount
            // 
            this.tb_workerAmount.Location = new System.Drawing.Point(468, 96);
            this.tb_workerAmount.Name = "tb_workerAmount";
            this.tb_workerAmount.Size = new System.Drawing.Size(20, 20);
            this.tb_workerAmount.TabIndex = 12;
            this.tb_workerAmount.Text = "2";
            this.tb_workerAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_workerAmount.WordWrap = false;
            this.tb_workerAmount.Leave += new System.EventHandler(this.tb_workerAmount_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(415, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Workers";
            // 
            // txt_queueList
            // 
            this.txt_queueList.FormattingEnabled = true;
            this.txt_queueList.Location = new System.Drawing.Point(629, 15);
            this.txt_queueList.Name = "txt_queueList";
            this.txt_queueList.Size = new System.Drawing.Size(651, 95);
            this.txt_queueList.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1292, 571);
            this.Controls.Add(this.txt_queueList);
            this.Controls.Add(this.tb_workerAmount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_builder);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_builderLocation);
            this.Controls.Add(this.tb_filter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_browse);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_folderPath);
            this.Name = "Form1";
            this.Text = "File Monitor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_folderPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_filter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_builderLocation;
        private System.Windows.Forms.Button btn_builder;
        private System.Windows.Forms.TextBox tb_workerAmount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox txt_queueList;
    }
}

